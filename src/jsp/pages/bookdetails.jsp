<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.data.books.Book"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.BooksBean"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Books" />
</jsp:include>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%-- Header --%>
<jsp:include flush="false"
	page="/WEB-INF/pages/includes/header_mainnav.jsp" />

<body ng-app="App">

	<%
	    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
		CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
        String id = StringUtility.trimAndEmptyIsNull(request.getParameter("id"));
        String isbn10 = StringUtility.trimAndEmptyIsNull(request.getParameter("isbn10"));
        String isbn13 = StringUtility.trimAndEmptyIsNull(request.getParameter("isbn13"));
	%>
	<!--content start here-->
	<div class="page-content" ng-controller="BookDisplayController">
			<div class="row" ng-if="book">
				<div class="container">
					{{book}}
				</div>
			</div>
			<div class="row" ng-if="!book">
				<div class="container">
					More details coming soon
				</div>
			</div>
	</div>
	<%--content end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>

	<%-- jQuery --%>
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/jquery/jquery-1.11.0.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<%-- Bootstrap --%>
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/bootstrap.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular-animate.min.js?ver=11112017")%>"></script>
		
	<script type="text/javascript">
		var baseURL = "<%=WebsiteActions.BOOKS.getActionURL(secureRequest, secureResponse)%>";
		var id = "<%=id%>";
		<%if(isbn10 != null) {%>
			var isbn10 = "<%=isbn10%>";
		<%} else { %>
			var isbn10 = null;
		<%}%>
		
		<%if(isbn13 != null) {%>
			var isbn13 = "<%=isbn13%>";
		<%} else { %>
			var isbn13 = null;
		<%}%>
	</script>
		
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/angular/bookdetails.js?ver="
                    + StaticFileVersions.JS_VERSION)%>"></script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />