<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Library Events" />
</jsp:include>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<body>

	<%-- Header --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/header_mainnav.jsp" />
	
	<!--content start here-->
	<div class="content">
		<div class="container">
			<div class="content-main">
				<div class="content-block1">
							We are updating the content here.
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<%--content end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />