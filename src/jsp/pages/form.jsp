<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.admin.entity.page.PageResource"%>
<%@page import="org.communitylibrary.ui.navigation.ContentActions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.beans.AudienceBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="User Form" />
</jsp:include>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
	String formResources = AudienceBean.getFormFromRequest(secureRequest);
%>
<body ng-app="App">

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />
		
	<!--content start here-->
	<div class="page-content" ng-controller="DispFormController">
		<div class="row">
			<div class="container" ng-if="isFormPristine">
				<div class="col-sm-12 col-md-12">
					<div class="content-box-header panel-heading">
						<div class="panel-title" style="text-align: center"><h2>{{userForm.title}}</h2></div>
					</div>
					<div class="content-box-large box-with-header">
						<div class="col-md-12 col-sm-12" ng-repeat = "field in userForm.fields">
							<div class="content-box-header">
		  						<div class="panel-title">{{field.question}}<span ng-if="field.isMandatory" title="Required Field">*</span></div>
			  				</div>
			  				<div class="content-box-large box-with-header">
			  					<div class="form-group" ng-if="field.type.code === 'C'">
									<div class="col-md-10">
										<div class="checkbox" ng-repeat="option in field.options">
											<label ng-if="option.isOtherField === false">
												<input type="checkbox" name="{{field.id}}" ng-model="field.answer[option.id]" />{{option.text}}
											</label>
											<label ng-if="option.isOtherField === true">
												<input type="checkbox" name="{{field.id}}" ng-model="field.answer[option.text]" />{{option.text}}
												<input ng-if="field.answer[option.text] === true" ng-model="field.otheranswer"  placeholder="Please write a few words about this" class="form-control"/>
											</label>
										</div>
									</div>
								</div>
			  					<div class="form-group" ng-if="field.type.code === 'R'">
									<div class="col-md-10">
										<div class="radio" ng-repeat="option in field.options">
											<label ng-if="option.isOtherField === false">
												<input type="radio" name="{{field.id}}" ng-model="field.answer" ng-value="option.text" />{{option.text}}
											</label>
											<label ng-if="option.isOtherField === true">
												<input type="radio" name="{{field.id}}" ng-model="field.answer" ng-value="option.text" />{{option.text}}
												<input ng-model="field.otheranswer"  ng-if="field.answer == option.text" placeholder="Please write a few words about this" class="form-control" />
											</label>
										</div>
									</div>
								</div>
			  					<div class="form-group" ng-if="field.type.code === 'T'">
									<input class="form-control" placeholder="Your Answer" type="text" ng-model="field.answer" />
								</div>
			  				</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12"  style="text-align: center;">
									<button class="btn btn-primary" type="button" ng-disabled="!isFormOkay()" ng-click="submitResponse()">
										<i class="fa fa-save"></i> Submit Response
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			<div class="container" ng-if="!isFormPristine">
				Thank you for your response.
			</div>
		</div>
	</div>
	<!-- End of page content -->

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	
	<%-- jQuery --%>
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/jquery/jquery-1.11.0.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	
	<%-- Bootstrap --%>
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/bootstrap.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/vendors/angular/angular.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<script type="text/javascript">
		var formURL;
		var formResources = <%=formResources%> ;
		$(document).ready(function() {
			formURL = "<%=WebsiteActions.FORM_RESPONSE.getActionURL(secureRequest, secureResponse)%>";
    	});
  	</script>

	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/angular/form.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />