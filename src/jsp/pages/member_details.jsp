<!Doctype html>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<html>
<%@page import="org.communitylibrary.data.members.LibraryGroups"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.data.common.Phone"%>
<%@page import="org.communitylibrary.ui.beans.MembersBean"%>
<%@page import="org.communitylibrary.data.members.LibraryMember"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%
	CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);

    LibraryMember member = MembersBean.getLibraryMemberFromReq(secureRequest);
	String lg = null;
	boolean isAdult = false;
	if (member.getLibraryGroup() != null) {
		lg = member.getLibraryGroup().name();
		isAdult = (member.getLibraryGroup() == LibraryGroups.ADULTS);
	}
%>
<head>
<!-- Icons & favicons -->
<link rel="icon" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/favicon_0.ico") %>">
<link rel="stylesheet" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/member_details.css?ver="+StaticFileVersions.CSS_VERSION) %>" media="all">
</head>

<body>

	<%--Manage user page content starts --%>
	<div class="member_det">
		<div class="section sectiontop">
			<div class="inner-container">
				<h1>
					Name:
					<%=StringUtility.trimWithNullAndEmptyAsUnset(member.getName())%></h1>
					<h1>
						<span>Library Id:<%=StringUtility.trimAndNullIsEmpty(member.getId())%></span>
						<span style="float: right; margin-right: 40px;">Honour Roll:</span>
					</h1>

					<%--Removing the image container --%>
					<%--
						<div class="img_con">
							<img
								src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/members/<%=member.getPhotoId() + ".JPG"%>"
								id="profilepicture" alt="profile_picture" id="profilepicture"
								alt="profile_picture" width="75px" height="113px" />
						</div>
					--%>
				<ul>
					<li><span>D.O.B.</span>
						<div><%=StringUtility.trimAndNullIsEmpty(member.getDob())%></div>
					</li>

					<li><span>Age</span> <%
 				    String age = null;
					
					if(member.getDob() != null) {
				         age = MembersBean.calculateAge(member.getDob());
					}
					
				     if (("".equals(age)) && member.getAge() != null) {
       					age = member.getAge();
     				}
 %>

						<div><%=StringUtility.trimAndNullIsEmpty(age)%></div></li>
					<li>
						<span>Phone</span>
					<%
					     String phone = null;
					     if (member.getPhone() != null && member.getPhone().getPhones() != null) {
					         phone = Phone.getPhoneNumberString(member.getPhone());
					     }
					%>
						<div><%=StringUtility.trimAndNullIsEmpty(phone)%></div>
					</li>
					<li><span>Address</span>
						<div><%=StringUtility.trimAndNullIsEmpty(member.getAddress())%></div>
					</li>

					<%
					    if (!isAdult || member.getMotherName() != null) {
					%>
					<li><span>Mothers Name</span>
						<div><%=StringUtility.trimAndNullIsEmpty(member.getMotherName())%></div>
					</li>
					<%
					    }
					%>

					<%
					    if (!isAdult || member.getFatherName() != null) {
					%>
					<li><span>Father Name</span>
						<div><%=StringUtility.trimAndNullIsEmpty(member.getFatherName())%></div>
					</li>
					<%
					    }
					%>

					<%
					    if (!isAdult) {
					%>
					<li><span>School</span>
						<div><%=StringUtility.trimAndNullIsEmpty(member.getSchool())%></div>
					</li>
					<li><span>Class</span>
						<div><%=StringUtility.trimAndNullIsEmpty(member.getStudyClass())%></div>
					</li>
					<%
					    }
					%>
					<li><span>Library Group</span> 
					<div><%=StringUtility.trimAndNullIsEmpty(lg)%></div></li>
				</ul>
			</div>
		</div>
	</div>
</body>

</html>