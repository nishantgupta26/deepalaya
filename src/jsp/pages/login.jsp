<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Login" />
</jsp:include>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@ page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />
	<jsp:include page="/WEB-INF/pages/includes/messages.jsp"></jsp:include>
	<!--Login start here-->
	<div class="login form">
		<ul class="tab-group">
			<li class="tab active"><a href="">Log In</a></li>
		</ul>
		<div class="tab-content">
			<div id="login">
				<form id="loginForm" method="post">
					<div class="field-wrap">
						<label> Email Address<span class="req">*</span></label> <input type="email" required autocomplete="off" placeholder="abc@xyz.com" name="email"/>
					</div>
					<div class="field-wrap">
						<label> Password<span class="req">*</span>
						</label> <input type="password" required autocomplete="off" name="password"/>
					</div>
					<button class="button button-block" />Log In</button>
				</form>
			</div>
		</div>
	</div>
	<%--contact end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />

	<%-- Page Specific Scripts --%>
	<script type="text/javascript">
	    $(document).ready(function(){
	    	var loginObj = new LoginJS($("#loginForm"), "<%=WebsiteActions.LOGIN.getActionURL(secureRequest,secureResponse, null, false)%>");
    });
  </script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />