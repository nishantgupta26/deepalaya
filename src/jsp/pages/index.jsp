<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%@page import="org.communitylibrary.app.resources.ResourceObject"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.data.entity.EventData"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%
	CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>

<body>

	<%-- Header --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/header_mainnav.jsp" />
	
	
	<%--Banners for advertisements etc --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/banners.jsp" />

	<%-- Deepalaya Community Library Strengths --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/library_strength_logos.jsp" />

	<%-- What we do: created an about page for the same --%>
	<%--<jsp:include page="/WEB-INF/pages/includes/whatwedo.jsp"></jsp:include> --%>

	<div id="mid-block">
		<div class="container">
		<jsp:include page="/WEB-INF/pages/includes/aboutusgist.jsp"></jsp:include>
		
		<div id="right-block">
		
			<%-- Latest Blog posts --%>
			<jsp:include page="/WEB-INF/pages/includes/latestblogposts.jsp"></jsp:include>
				
			<%-- Events in Library --%>
			<jsp:include page="/WEB-INF/pages/includes/libraryevents.jsp"></jsp:include>
		</div>
		</div>
	</div>
	
	<div style="clear: left;min-height: 1em;"></div>
	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	
	<% if(ConfigStore.getBooleanValue(CacheConfiguration.BOL_CAMPAIGN_POPUP, false)) {
	    	List<ResourceObject> campaignResources = WebsiteBean.getCampaignResourcesFromRequest(secureRequest);
			if(campaignResources != null && !campaignResources.isEmpty()) {
	%>
	
		<!-- Modal Popup for the campaigns code -->
	<div id="modalPopupContent" class="row" style="display:none;">
		<div class="panel-body flexslider">
			<ul class="slides" id="content-slider">
			<%for(ResourceObject resource : campaignResources) { %>
				<li>
					<div class="col-md-12" <%if(resource.getUrlLink() != null) { %> style="cursor:pointer;" onclick="window.open('<%=resource.getUrlLink()%>', '_blank')" <%} %>>
						<!-- Image content if available  -->
						<%if(resource.getImageUrl() != null) { %>
							<div class="row">
								<img src="<%=resource.getImageUrl() %>" style="width:100%; height: 100%"/>
							</div>
						<%} %>
						<!--  Text Content if Available -->
						<%if(resource.getTextContent() != null) { %>
							<div class="row">
								<%=resource.getTextContent() %>
		            		</div>
	            		<%} %>
					</div>
				</li>
			<%} %>
			</ul>
		</div>
	</div>	
		
	<% 
			}
 		} 
	%>
	
	<%-- end #site-content --%>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />
	
	<script>
		$(document).ready(function() {
		  new EventJS("<%=WebsiteActions.AJAX_GET_EVENTS.getActionURL(secureRequest, secureResponse, null, false)%>");
	  	<% if(StringUtility.trimAndEmptyIsNull(ConfigStore.getStringValue(CacheConfiguration.STR_BLOG_URL, null)) == null) {%>
		  	new FeedBlogsJS("<%=WebsiteActions.AJAX_GET_PUBLISHED_BLOGS.getActionURL(secureRequest, secureResponse, new String[] {"feed"}, false)%>");
		<%}%>
		});
	</script>
</body>

<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />
