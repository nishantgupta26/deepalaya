<%@page import="org.communitylibrary.data.books.BookAccount"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.BooksBean"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Books" />
</jsp:include>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%-- Header --%>
<jsp:include flush="false"
	page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<style type="text/css">
		.pagination>li>a {
    		color: #222;
		}
	</style>

<body ng-app="App">

	<%
	    CustomSecurityWrapperRequest secureRequest = SecurityBean
						.getRequest(request);
		CustomSecurityWrapperResponse secureResponse = SecurityBean
						.getResponse(response);
		String url = WebsiteActions.BOOKS.getActionURL(secureRequest,
						secureResponse);
		String totalBooks = BooksBean.getTotalBooks(secureRequest);
		String bookTypes = BookAccount.getBookAccountsJson();
	%>
	<!--content start here-->
	<div class="page-content" ng-controller="BooksListingController">
		<div class="row">
			<!-- For filters -->
			<div class="container">
				<div class="panel-group" id="filterpanel" style="margin-top: 3%;">
					<div class="panel panel-default" style="background: none;border: none;border-radius: 0;">
						<a data-toggle="collapse" data-parent="#filterpanel" href="#search" class="btn btn-default collapsed">
							<strong>Advance Search</strong>
						</a>
						<div id="search" class="panel-collapse collapse">
							<div class="panel-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="control-label col-sm-2" for="booktype">Book Type</label>
										<div class="col-sm-10">
											<select id="booktype" ng-model="filterObject.bookAccount">
												<option value="-">Select an option</option>
												<option ng-repeat="account in bookAccounts" value="{{account.code}}">{{account.name}}</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2" for="title">Title</label>
										<div class="col-sm-10"><input type="text" class="form-control"
											id="title" placeholder="Title" ng-model="filterObject.title"/></div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2" for="author">Author</label>
										<div class="col-sm-10"><input type="text" class="form-control"
											id="author" placeholder="Author" ng-model="filterObject.author" /></div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-2" for="isbn">ISBN</label>
										<div class="col-sm-10"><input type="text" class="form-control"
											id="isbn" placeholder="ISBN Number" ng-model="filterObject.isbn"/></div>
									</div>
									<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="button" class="btn btn-primary" ng-click="applyFilters()" ng-disabled="!filterObject">Search</button>
										<button type="button" class="btn btn-default" ng-click="clearFilter()" ng-disabled="!filterObject">Clear</button>
									</div>

									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="content-box-large box-with-header">
				 	<div class="content-box-large">
				 		<div class="panel-heading">
				 			<div class="panel-title">Page {{currentPage}}
					 			<div class="pull-right">Displaying {{dispbooks.length}}/{{totalBooks}} Books</div>
				 			</div>
			 			</div>
			 			<div class="panel-body" id="bookList">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
				            	      		<th>Account</th>
					                  		<th>Title</th>
					                  		<th>ISBN-10</th>
					                  		<th>ISBN-13</th>
					                  		<th>Authors</th>
					                  		<th>Copies</th>
											<!-- <th>Actions</th> -->
					                	</tr>
									</thead>
									<tbody>
				                		<tr ng-repeat="book in dispbooks">
											<td>{{book.type +  "-" + book.code}}</td>
											<td>{{book.title}}</td>
											<td>{{book.isbn10 == null ? "-" : book.isbn10}}</td>
											<td>{{book.isbn13 == null ? "-" : book.isbn13}}</td>
											<td>{{book.authors}}</td>
											<td>{{book.copies}}</td>
											<!-- <td><button class="btn btn-sm btn-md btn-lg btn-default" title="View This Book" ng-click="fetchBook(book)"><i class="glyphicon glyphicon-eye-open"></i></button></td> -->
										</tr>
									</tbody>
								</table>
							</div>
						</div>
		 			</div>
		 			<div style="text-align:center">
						<ul uib-pagination max-size="5" boundary-links="true" boundary-link-numbers="true" total-items="totalBooks" items-per-page="perPage" ng-model="currentPage" ng-change="pageChanged()"></ul>
		 			</div>

				</div>
			</div>
		</div>
	</div>
	<%--content end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>

	<%-- jQuery --%>
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/jquery/jquery-1.11.0.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<%-- Bootstrap --%>
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/bootstrap.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular-animate.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/ui-bootstrap-tpls-2.5.0.min.js?ver=11112017")%>"></script>

	<script type="text/javascript">
		var baseURL = "<%=url%>";    
		var totalBooks = <%=totalBooks%>;
		var bookAccounts = <%=bookTypes%>;
	</script>

	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/angular/bookslisting.js?ver="
                    + StaticFileVersions.JS_VERSION)%>"></script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />