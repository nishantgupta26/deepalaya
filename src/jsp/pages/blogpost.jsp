
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.utils.HtmlUtility"%>
<%@page import="org.communitylibrary.utils.DateUtility"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.beans.AudienceBean"%>
<%@page import="org.communitylibrary.admin.entity.blog.Blogpost"%>
<%
	CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
	Blogpost post = AudienceBean.getPostFromRequest(secureRequest);
	String url = WebsiteActions.BLOGPOST.getActionURL(secureRequest, secureResponse, new String[] {post.getShareableURL()});
	String postContent =post.getPostText();
	String shareableContent = StringUtility.trimToSizeAndAppendWithSpecialUnicodeCharacters(
	        StringUtility.removeHtmltags(StringUtility.removeImgtags(postContent)), 60,"...");
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="<%=post.getTitle()%>" />
	<jsp:param name="urlLink" value="<%= url %>" />
	<jsp:param name="imageLink" value="<%=HtmlUtility.extractFirstImageSrcURLFromContent(postContent)%>" />
	<jsp:param name="description" value="<%= shareableContent %>" />
</jsp:include>
<body>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<%--Main content starts --%>
	<div class="container">

		<div class="row">
			<!-- Blog Post Content Column -->
			<div class="col-lg-12">

				<!-- Blog Post -->

				<!-- Title -->
				<div class="panel-title text-center" id="blog-title">
					<h1><%=post.getTitle()%></h1>
				</div>

				<!-- Author -->
				<div class="lead col-md-12">
					by <span id="blog-author"> <%=post.getAuthor()%></span>
				</div>
				<div class="clearfix"></div>
				<hr>

				<!-- Date/Time -->
				<div class="col-md-8">
					<span class="glyphicon glyphicon-time"></span> Posted on
					<%=DateUtility.getDateInDDMMMYY(post.getPublishDate())%>
				</div>
				<div class="col-md-4">
					<div id="share-social-media" style="display:none;">
						<div class="fb-share-button"
							data-href="<%=WebsiteActions.BLOGPOST.getActionURL(secureRequest, secureResponse,
					new String[]{String.valueOf(post.getId())})%>"
							data-layout="button_count" data-size="small"
							data-mobile-iframe="true">
							<a class="fb-xfbml-parse-ignore" target="_blank"
								href="https://www.facebook.com/sharer/sharer.php?u="
								<%=HtmlUtility.encodeForURL(url)%>"&amp;src=sdkpreparse">Share</a>
						</div>
						<div id="twitter-share" style="margin: 1% 0;">
							<a href="https://twitter.com/share" class="twitter-share-button"
								data-text="New Article: '<%=post.getTitle()%>' by <%=post.getAuthor()%>"
								data-url="<%=url%>"
								data-via="communitylibpro" data-show-count="true">Tweet</a>
							<script async src="//platform.twitter.com/widgets.js"
								charset="utf-8"></script>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<hr>

				<!-- Post Content -->
				<div id="content">
					<%=post.getPostText()%>
				</div>

				<hr>
			</div>
		</div>
		<!-- /.row -->
		<hr>
		<!-- Footer -->
	</div>
	<%--Main content ends --%>
	<%-- Comment section begins here --%>
	<%-- Disqus comments --%>
	<div class="container">
		<div id="disqus_thread"></div>
		<script>
			/**
			 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
			 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
			 */
	
			var disqus_config = function() {
				this.page.url = '<%=url%>';
				this.page.identifier = '<%=post.getId()%>';
				//this.page.title = 'a unique title for each page where Disqus is present';
			};
	
			(function() { // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
				var d = document, s = d.createElement('script');
	
				s.src = '//commlibpro.disqus.com/embed.js'; // IMPORTANT: Replace EXAMPLE with your forum shortname!
	
				s.setAttribute('data-timestamp', +new Date());
				(d.head || d.body).appendChild(s);
			})();
		</script>
		<noscript>
			Please enable JavaScript to view the <a
				href="https://disqus.com/?ref_noscript" rel="nofollow">comments
				powered by Disqus.</a>
		</noscript>
	</div>
	<%--<jsp:include page="/WEB-INF/pages/includes/comments.jsp"></jsp:include> --%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />
	
	<script type="text/javascript">
		$(document).ready(function() {
			<%-- var title = $("#blog-title").text().trim();
			var content = $("#content").text();
			
			content =  content.length > 100 ? content.substr(0, content.lastIndexOf(' ', 97)) + '...' : content;
			content = content.trim().replace(/\n/g, " ");

			var author = $("#blog-author").text().trim();
			
			var imgSrc;
			if($("#content img").length > 0) {
				imgSrc = $("#content img")[0].src;
			} else {
				imgSrc = "<%=UIBean.getCompleteURL(secureRequest, secureResponse, "images/logo.jpg") %>";
			}

			
			$("head").append("<meta property=\"og:type\" content=\"article\"/>");
			$("head").append("<meta property=\"og:title\" content=\"" + title + "\"/>");
			$("head").append("<meta property=\"og:description\" content=\"" + content + "\"/>");
			$("head").append("<meta property=\"og:url\" content=\"<%=url%>\"/>");
			$("head").append("<meta property=\"og:site_name\" content=\"The Community Library Project\"/>");
			$("head").append("<meta property=\"og:image\" content=\"" + imgSrc + "\"/>");
			$("head").append("<meta property=\"fb:app_id\" content=\"644760552394824\"/>"); --%>
			$("#share-social-media").show();
		});
	
	</script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />