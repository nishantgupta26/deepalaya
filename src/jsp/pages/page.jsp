<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.admin.entity.page.PageResource"%>
<%@page import="org.communitylibrary.ui.navigation.ContentActions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.beans.AudienceBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
    PageResource customPage = AudienceBean.getPageFromRequest(secureRequest);
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="<%=customPage.getTitle() %>" />
</jsp:include>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<body>

	<%-- Header --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/header_mainnav.jsp" />
	
	<!--content start here-->
	<div class="content"  id="content">
		<div class="container">
			<div class="content-main">
				<div class="content-block1">
						<%=customPage.getPageText() %>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row" style="text-align: center;">
			<button class="btn btn-default btn-lg" id="download">Download/Print this Page</button>
		</div>
		<div style="display:none;" id="tooltipContent">
			<strong>Steps to download as pdf:</strong>
				<div>1. open the print dialog(Ctrl + P)</div>
				<div>2. Choose Save as PDF option</div>
				<div>3. Alternatively, you can print the page directly as well</div>
		</div>
	</div>
<!-- 	<div data-html2canvas-ignore="true">
    	<button id="pdf-new"><a id="download" class="button" style="color: black;">Generate PDF</a></button>
	</div> -->
	
	<%--content end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />
<%-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/html2canvas.js")%>"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		var doc = new jsPDF('p', 'pt', 'letter');
		$("#download").click(function() {
			doc.text($("#content").text(), 20, 20);
			doc.save("<%=customPage.getTitle()%>.pdf");
			// We'll make our own renderer to skip this editor
			var specialElementHandlers = {
				'#editor': function(element, renderer){
					return true;
				}
			};
			
		/* 	var margins = {
		            top: 80,
		            bottom: 60,
		            left: 40,
		            width: 522
		        };
			
			var options = {
		            background: '#fff' //background is transparent if you don't set it, which turns it black for some reason.
		        }; */

			// All units are in the set measurement for the document
			// This can be changed to "pt" (points), "mm" (Default), "cm", "in"
			doc.fromHTML($("#content").html(), margins.left, margins.top, {
				'width': margins.width,
				'elementHandlers': specialElementHandlers,
				'background': '#fff'
			}, function (dispose) {
	            // dispose: object with X, Y of the last line add to the PDF 
	            //          this allow the insertion of new lines after html
	            doc.save('<%=customPage.getTitle()%>.pdf');
	        }, margins);
	        
	        var options = {
	                background: '#fff' //background is transparent if you don't set it, which turns it black for some reason.
	            };
	            doc.addHTML($('#content'), options, function () {
	                    doc.save('Test.pdf');
	            });
			
		});
		
	});
	
	</script> --%>
	<script type="text/javascript">
	$(document).ready(function() {
		
		$("#download").tooltip({
			title: $("#tooltipContent").html(),
			html: true,
			placement: "top"
		});
		
		$("#download").click(function() {
			var toPrint = $("#content").html();
			params  = 'width='+screen.width;
			 params += ', height='+screen.height;
			 params += ', top=0, left=0'
			 params += ', fullscreen=no';
	        var popupWin = window.open('', '_blank', params);
	        popupWin.document.open();
	        var css1 = "<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/plugins/plugins-min.css?ver=" +StaticFileVersions.CSS_VERSION) %>";
	        var css2 = "<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/app-min.css?ver=" +StaticFileVersions.CSS_VERSION) %>";
	        popupWin.document.write('<html><title>::Preview::</title><link rel="stylesheet" type="text/css" href="' + css1 + '" media="screen" /><link rel="stylesheet" type="text/css" href="' + css2 + '" media="screen" /><style type="text/css">body{background-color: white;}</style></head><body onload="window.print()">');
	        popupWin.document.write(toPrint);
	        popupWin.document.write('</html>');
	        popupWin.focus();
	        popupWin.document.close();

		});
	});
	</script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />