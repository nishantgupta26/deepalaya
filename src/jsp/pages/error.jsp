<%@page import="org.communitylibrary.utils.HtmlUtility"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<jsp:include page="includes/page_top.jsp" />
<%@page import="org.communitylibrary.ui.beans.MessageBean"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.session.SessionStore"%>
<%
	CustomSecurityWrapperRequest zRequest = SecurityBean.getRequest(request);
	String trace = MessageBean.getMessage(zRequest, MessageBean.MessageType.EXCEPTION_TRACE);
%>
<HTML>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Error" />
</jsp:include>
<%-- <jsp:include page="includes/header.jsp" /> --%>
<body>
<div style="min-height: 40%; margin-bottom: 20%;">
<h1>Error</h1>
<jsp:include page="includes/messages.jsp" />

<br/>
<div>
<p>We have encountered an error while serving your request.<br/><br/>We regret the inconvenience caused to you. Kindly contact your system administrator for resolution.</p>
</div>

<% if (trace != null) { %>
<br/><br/>
<h2>Stack Trace</h2>
<div style="text-align: left">
<pre><%= HtmlUtility.convertToHTML(trace, false, true) %></pre>
</div>
<% } %>

</div>
<jsp:include page="/WEB-INF/pages/includes/footer.jsp" />
</body>
</html>
