<%@page import="java.util.Collections"%>
<%@page import="org.communitylibrary.utils.HtmlUtility"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%@page import="org.communitylibrary.data.resources.HomePageLink"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.data.payments.PledgeType"%>
<%@page import="org.communitylibrary.ui.beans.PaymentBean"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Quick Links" />
</jsp:include>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<style>
#linksContainer .panel-heading, #linksContainer .panel-body {
	padding:0;
}

#linksContainer ul {
	margin: auto;
	min-height: 100px; 
	list-style:none;
	padding:0;
}

#linksContainer li {
	/* border-bottom: 1px solid #016; */
	padding: 2%;
}
</style>
<body>

	<%-- Header --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<div class="container" id="linksContainer" style="margin-bottom: 1%">
		<% 
			List<HomePageLink> homePageLinks = WebsiteBean.getHomePageLinksFromRequest(secureRequest);
			if(homePageLinks != null) {
		%>
		
		<div class="col-sm-6 col-md-6 col-lg-6">
			<div class="content-box-large">
				<div class="panel-heading">
					<div class="panel-title" style="text-align:center"><strong >QUICK LINKS</strong></div>
				</div>
				<div class="panel-body">
					<ul>
					<% 
						int i = 1;
						WebsiteBean.sortLinks(homePageLinks, true);
					    for(HomePageLink link : homePageLinks) {
					%>
						<li>
							<div class="row">
								<span><%=i++ %></span>
								<span style="margin-left: 2%"><%= link.getName() %></span>
								<span style="margin-left: 2%">
									<a style="cursor:pointer;" href="javascript:void()" onclick="updateClick(<%=link.getId()%>, true, '<%= link.getUrl() %>')"><i class=" glyphicon glyphicon-link"></i>Click Here</a>
								</span>
							</div>
							<% if(link.getDesc() != null) { %>
							<div class="row" style="margin-left: 2%">
								<span><%= link.getDesc() %></span>
							</div>
							<% } %>
						</li>
				<%
						}
			    %>
					</ul>	        
				</div>
			</div>
		</div>
		<!-- Hindi panel links -->
		<div class="col-sm-6 col-md-6 col-lg-6">
			<div class="content-box-large">
				<div class="panel-heading">
					<div class="panel-title" style="text-align:center"><strong><%=HtmlUtility.getUTF8Text("शॉर्टकट") %></strong></div>
				</div>
				<div class="panel-body">
					<ul>
				<% 
					i = 1;
					WebsiteBean.sortLinks(homePageLinks, false);
				    for(HomePageLink link : homePageLinks) {
				    	if(link.getHindiName() == null) {
				    		continue;
				    	}
				%>
					<li>
						<div class="row">
							<span><%=i++ %></span>
							<span style="margin-left: 2%"><%= link.getHindiName() %></span>
							<span style="margin-left: 2%">
								<a style="cursor:pointer;" href="javascript:void();" onclick="updateClick(<%=link.getId()%>, false, '<%= link.getUrl() %>')"><i class=" glyphicon glyphicon-link"></i><%= HtmlUtility.getUTF8Text("यहां क्लिक करे") %></a>
							</span>
						</div>
					<% if(link.getHindiDesc() != null) { %>
						<div class="row" style="margin-left: 2%">
							<span><%= link.getHindiDesc() %></span>
						</div>
					<% } %>
					</li>
			<%
					}
		    %>
			</ul>	
			</div>
			</div>
		</div>
		<%
			} else {
		%>		
		<div>No Links Present</div>
		<% } %>
	</div>
	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />
	<script type="text/javascript">
		function updateClick(id, isEnglish, url) {
			var hitURL = '<%=WebsiteActions.HOMEPAGE_LINKS.getActionURL(secureRequest, secureResponse, new String[] {"updateCount"})%>';
			hitURL += "/" + id + "/" + isEnglish;
			window.open(url);
			WEB_UTILS.getJSONData(hitURL);
		}
	</script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />