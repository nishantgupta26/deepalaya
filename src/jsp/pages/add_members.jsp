<%@page import="org.communitylibrary.data.members.Gender"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.beans.MembersBean"%>
<%@page import="org.communitylibrary.data.members.LibraryMember"%>
<%@page import="org.communitylibrary.data.members.Relationship"%>
<jsp:include page="/WEB-INF/pages/includes/page_top.jsp" />
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.data.user.User"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%@page import="org.communitylibrary.data.members.LibraryGroups"%>
<%@page import="org.communitylibrary.utils.StringUtility" %>
<%@page import="org.communitylibrary.data.common.Phone" %>

<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean
					.getRequest(request);
			CustomSecurityWrapperResponse secureResponse = SecurityBean
					.getResponse(response);

/* 			LibraryMember member = MembersBean.getLibraryMemberFromReq(secureRequest); */
/*
Doing away from individual members. We will only add members through file only
*/
			boolean isEdit = false;
			String jsonString = "{";
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Add Members" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<%--Manage members page content starts --%>
	<div class="we-do">
		<div class="container">
			<jsp:include flush="false"
				page="/WEB-INF/pages/includes/messages.jsp" />
			<%--Add multiple members --%>
			<form id="membersForm" method="post">
				
				<% if(!isEdit) { %>
				<div class="we-do-main">
					<h2>Add Members from TXT/CSV file</h2>
					<div id="tableDiv">
						<table class="display"
							style="border-width: 2px; border-color: #1ab188; text-align: center; margin-left: 12%;"
							width="70%" cellspacing="0" border="1">
							<thead>
								<tr>
									<th>File</th>
									<th><input name="upload_file" accept=".csv,.txt"
										type="file"></th>
									<th><button type="button" name="download_format" onclick="memberObj.downloadFormat()">Download Format</button></th>
								</tr>
							</thead>
						</table>
						<input class="button" value="Upload Members File" type="button"
							onclick="memberObj.uploadFile()" />
					</div>
				</div>
				<%} %>
				
				<%--Add Single member--%>
				<%-- <div class="we-do-main">
					<h2><%if(!isEdit) { %>Add<%} else { %>Edit<%} %> Member(s)</h2>
					<%if(isEdit) { %>
						<input type="hidden" value="<%=member.getId() %>" name="editId" />
					<%} %>
					<h3 id="add_members"></h3>
					<div id="tableDiv">
						<table id="singleMemberTable" class="display"
							style="border-width: 2px; border-color: #1ab188; text-align: center; margin-left: 12%;"
							width="70%" cellspacing="0" border="1">
							<thead>
								<tr>
									<th>Column</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								
 								<tr>
									<td>Id</td>
									<td><input name="id" type="text" placeholder="Member id"
										required /></td>
								</tr> 
							
								<tr>
									<td>Name</td>
									<td><input name="name" type="text" placeholder="Name"
										required <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(member.getName())%>"<%}%>/></td>
								</tr>
								<tr>
									<td>D.O.B.(DD/MM/YYYY)</td>
									<td><input name="dob" type="text" <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(member.getDob())%>"<%}%>/></td>
								</tr>
								<tr>
									<td>Age(x years, y months)</td>
									<%
									
								    String age = null;
									if(isEdit) {
									    
										if(member.getDob() != null) {
									         age = MembersBean.calculateAge(member.getDob());
										}
										
								 	    if (("".equals(age)) && member.getAge() != null) {
					       					age = member.getAge();
					     				}
									}
									%>
									<td><input name="age" type="text"
										placeholder="Age in format: (x years, y months)" <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(age)%>"<%}%>/></td>
								</tr>
								<tr>
									<td>Gender</td>
									<!-- Library group -->
									<td><select name="gender">
											<%
											jsonString += "\"genders\" : [";
											    Gender[] genders = Gender.values();
														for (int genderCount = 0; genderCount < genders.length; genderCount++) {
															Gender group = genders[genderCount];
															jsonString += "{\"id\":\"" + group.serialize()
																	+ "\" , \"name\":\"" + group.name() + "\"}"
																	+ (genderCount < (genders.length - 1) ? "," : "");
											%>
											<option value="<%=group.serialize()%>" <%if(isEdit && (group == member.getGender())) {%>selected="selected"<%}%>><%=group.name()%></option>
											<%
											    }
														jsonString += "],";
											%>
									</select></td>
								</tr>
								<tr>
									<td title="Self if adult, otherwise parents">Phone Number</td>
									<%
				    					String phone = null;
				     					if (isEdit &&  member.getPhone() != null && member.getPhone().getPhones() != null) {
					         				phone = Phone.getPhoneNumberString(member.getPhone());
				     					}
									%>
									<td><input name="phone" type="text"
										placeholder="Phone number(s) separated by ," <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(phone)%>"<%}%>/></td>
								</tr>
								<tr>
									<td>Mother's name</td>
									<td><input name="mn" type="text"
										placeholder="Mother's name" <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(member.getMotherName())%>"<%}%>/></td>
								</tr>
								<tr>
									<td>Father's name</td>
									<td><input name="fn" type="text"
										placeholder="Father's name" <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(member.getFatherName() )%>"<%}%>/></td>
								</tr>
								<tr>
									<td>Address</td>
									<td><input name="address" type="text"
										placeholder="Home Address" <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(member.getAddress())%>"<%}%>/></td>
								</tr>
								<tr>
									<td>Library Group</td>
									<!-- Library group -->
									<td><select name="lg">
											<%
											jsonString += "\"lib_group\" : [";
											    LibraryGroups[] groups = LibraryGroups.values();
														for (int groupCount = 0; groupCount < groups.length; groupCount++) {
															LibraryGroups group = groups[groupCount];
															jsonString += "{\"id\":\"" + group.serialize()
																	+ "\" , \"name\":\"" + group.getDisplayName() + "\"}"
																	+ (groupCount < (groups.length - 1) ? "," : "");
											%>
											<option value="<%=group.serialize()%>" <%if(isEdit && (group == member.getLibraryGroup())) {%>selected="selected"<%}%>><%=group.getDisplayName()%></option>
											<%
											    }
												jsonString += "]";
											%>
									</select></td>
								</tr>
								<tr>
									<td>School</td>
									<td><input name="school" type="text" placeholder="School" <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(member.getSchool())%>"<%}%>/></td>
								</tr>
								<tr>
									<td>Class</td>
									<td><input name="class" type="text" placeholder="Class" <%if(isEdit) {%>value="<%=StringUtility.trimAndNullIsEmpty(member.getStudyClass())%>"<%}%>/></td>
								</tr>
																<tr>
									<td>Add a relation?</td>
									<td><a onclick="memberObj.addRelative()">Add a relative member</a></td>
								</tr>
							</tbody>
						</table>

					<%if(!isEdit){ %>
						<div id="anchor_container">
							<a id="rel_anchor" onclick="memberObj.addRelative(this);"
								href="javascript:void(0);">Add a relative</a>
						</div>
					<%} %>
						<input type="button" name="submit_button" value="<%if(isEdit){ %>Edit<%} else{ %>Add<%} %> Member(s)"
							class="button" />
					</div>
					<div class="clearfix"></div>
				</div> --%>
			</form>


			<!--Existing users end here-->
		</div>
	</div>

<%
	jsonString += "}";
%>
	<%-- Manage user page content ends --%>


	<%-- Footer --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/footer.jsp" />

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />

	<%-- Page Specific Scripts --%>
	<script type="text/javascript">
    var memberObj;
    $(document).ready(function() {
          memberObj = new MemberJS($("#membersForm"),  "<%=WebsiteActions.ADD_MEMBERS.getActionURL(secureRequest, secureResponse, new String[]{"add"}, false)%>", <%=jsonString%>, <%=Relationship.toJSON()%>);
        });
  </script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery-ui.min.js") %>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery-ui.min.css") %>" rel="stylesheet" />
</body>

<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />
