<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Contact us" />
</jsp:include>
<%@ page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<body>

	<%-- Header --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/header_mainnav.jsp" />
	<style type="text/css">
		#libaddress .tab-content,  #libmaps .tab-content {
			padding : 5px 15px;
		}
		
		#libaddress h3, #libmaps h3 {
  			background: cornsilk;
		}
		
		#libaddress a, #libmaps a {
			color: black;
			background:	cornsilk
		}
		
		#libadress nav-tabs li, #libmaps nav-tabs li {
			background:	cornsilk
		}
		
		#libadress nav-tabs li:active a, #libmaps nav-tabs li:active a {
			font-weight: bold;
		}
	</style>
	
	<!--contact start here-->
	<div class="contact">
		<div class="container">
			<jsp:include flush="false" page="/WEB-INF/pages/includes/messages.jsp" />
			<div class="contact-main">
				<div class="contact-top">
					<h2>Contact</h2>
					<p>You can reach out to us. We would like to know you better.</p>
				</div>
				<div class="contact-block1">
				<div class="row">
					<div class="col-md-6 contact-address">
						<div><h3>Our Libraries</h3></div>
						<div id="libaddress">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#add-sheikhsarai" data-toggle="tab">Sheikh Sarai - Head Office</a>
								</li>
								<li>
									<a href="#add-agrasargurugram" data-toggle="tab">Sikanderpur, Gurugram</a>
								</li>
							</ul>

							<div class="tab-content ">
								<div class="tab-pane active" id="add-sheikhsarai">
									<ul>
										<li>
											<p>
												The Community Library Project
												<br>
												B-65, Panchsheel Vihar,
												<br>
												Sheikh Sarai Phase I,
												<br>
												Delhi,
												<br>
												India
											</p>
										</li>
									</ul>
								</div>
								<div class="tab-pane" id="add-agrasargurugram">
									<ul>
										<li>
											<p>
												The Community Library Project
												<br>
												Agrasar Bachpan
												<br>
												House no 40, Sikanderpur,DlF phase I
												<br>
												Sector - 26
												<br>
												Gurugram
												<br>
												Haryana - 122002
												<br>
												India
											</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 contact-address">
						<h3>How to Contact Us</h3>
						<ol>
							<li>
								<p>For media queries, please contact <strong>Purnima Rao</strong> at <a href="mailto:clp.media2017@gmail.com">clp.media2017@gmail.com</a></p>
							</li>
							<li>
								<p>To support the work through donations, please write to <strong>Arti Mallik</strong> at <a href="mailto:booksforthepeopledelhi@gmail.com">booksforthepeopledelhi@gmail.com</a></p>
							</li>
							<li>
								<p>To contact us about our library in Sikandarpur, please write to <strong>Ritika Puri</strong> and <strong>Shubha Bahl</strong> at <a href="mailto:communitylib.agrasar@gmail.com">communitylib.agrasar@gmail.com</a></p>
							</li>
							<li>
								<p>For all other queries, please write to <strong>Mridula Koshy</strong> at <a href="mailto:mridulakoshy@gmail.com">mridulakoshy@gmail.com</a></p>
							</li>
						</ol>						
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="col-md-6 col-sm-6 col-lg-6">
		  				<div class="content-box-large">
			  				<div class="panel-heading">
								<div class="panel-title"><strong>You can also drop us a message here</strong></div>
							</div>
			  				<div class="panel-body">
			  					<div id="errorDiv"></div>
		  						<form id="contactUsForm" class="form-horizontal" method="post">
									<div class="form-group">
										<label for="name" class="col-sm-2 control-label">Name</label>
									    <div class="col-sm-10">
									      <input type="text" class="form-control" id="name" name="name" placeholder="Name" />
									    </div>
									</div>
									<div class="form-group">
										<label for="phone" class="col-sm-2 control-label">Phone Number</label>
									    <div class="col-sm-10">
									      <input type="text" class="form-control" id="phone" name="phone"  placeholder="Phone Number" />
									    </div>
									</div>
									<div class="form-group">
										<label for="email" class="col-sm-2 control-label">Email Id</label>
									    <div class="col-sm-10">
									      <input type="email" class="form-control" id="email" name="email"  placeholder="Email Id" />
									    </div>
									</div>
									<div class="form-group">
										<label for="subject" class="col-sm-2 control-label">Subject</label>
									    <div class="col-sm-10">
									      <input type="text" class="form-control" id="subject" name="subject"  placeholder="Subject" />
									    </div>
									</div>
									<div class="form-group">
									    <label class="col-sm-2 control-label">Message</label>
									    <div class="col-sm-10">
									      <textarea class="form-control" placeholder="Message" name="message" rows="3"></textarea>
									    </div>
								  	</div>
								  	<div class="form-group">
									    <div class="col-sm-offset-2 col-sm-10">
									      <button type="submit" class="btn btn-primary">Send</button>
									    </div>
								  	</div>
								</form>
			  				</div>
	  					</div>
		  			</div>
		  			<!-- Press Release scroller -->
		  			<div class="col-md-6 col-sm-6 col-lg-6">
			  			<jsp:include flush="false" page="/WEB-INF/pages/includes/press.jsp"></jsp:include>
		  			</div>
				</div>
			</div>
			
			<!-- Google maps and addresses -->
			<div><h3>Our Libraries</h3></div>
			<div id="libmaps">
				<ul class="nav nav-tabs">
					<li class="active">
        				<a  href="#map-sheikhsarai" data-toggle="tab">Sheikh Sarai</a>
					</li>
					<li>
						<a href="#map-agrasargurugram" data-toggle="tab">Sikanderpur</a>
					</li>
				</ul>
				<div class="tab-content ">
			  		<div class="tab-pane active" id="map-sheikhsarai">
						<div class="contact-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3505.1599889096847!2d77.22025491516187!3d28.534910482456375!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce220d873c2ff%3A0x4d299220cba652cb!2sRamditti+J+Narang+Deepalaya+School!5e0!3m2!1sen!2sin!4v1482663897356" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="tab-pane" id="map-agrasargurugram">
						<div class="contact-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d876.7379100475323!2d77.0960597!3d28.481003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d192ea08e8241%3A0x6367c24df89c5f30!2sAravali+Scholars!5e0!3m2!1sen!2sin!4v1523445677464" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<%--contact end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />
    <%-- Page Specific Scripts --%>
    <script type="text/javascript">
	    $(document).ready(function(){
	    	var contact = new contactJS($("#contactUsForm"), "<%=WebsiteActions.CONTACT.getActionURL(secureRequest,secureResponse, new String[] {"addContactRequest"}, false)%>");
	    });
    </script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />