<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.PaymentBean"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Payments" />
</jsp:include>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%-- Header --%>
<jsp:include flush="false"
	page="/WEB-INF/pages/includes/header_mainnav.jsp" />

<style type="text/css">
.mobile-social-share {
	background: none repeat scroll 0 0 antiquewhite;
	display: block !important;
	min-height: 70px !important;
	margin: 50px 0;
}

.mobile-social-share h3 {
	color: inherit;
	float: left;
	font-size: 15px;
	line-height: 20px;
	margin: 25px 25px 0 25px;
}

.share-group {
	float: right;
	margin: 18px 25px 0 0;
}

.btn-group {
	display: inline-block;
	font-size: 0;
	position: relative;
	vertical-align: middle;
	white-space: nowrap;
}

.mobile-social-share ul {
	float: right;
	list-style: none outside none;
	margin: 0;
	min-width: 61px;
	padding: 0;
}

.share {
	min-width: 17px;
}

.mobile-social-share li {
	display: block;
	font-size: 18px;
	list-style: none outside none;
	margin-bottom: 3px;
	margin-left: 4px;
	margin-top: 3px;
}

.btn-share {
	background-color: #BEBEBE;
	border-color: #CCCCCC;
	color: #333333;
}

.btn-twitter {
	background-color: #3399CC !important;
	width: 51px;
	color: #FFFFFF !important;
}

.btn-facebook {
	background-color: #3D5B96 !important;
	width: 51px;
	color: #FFFFFF !important;
}

.caret {
	border-left: 4px solid rgba(0, 0, 0, 0);
	border-right: 4px solid rgba(0, 0, 0, 0);
	border-top: 4px solid;
	display: inline-block;
	height: 0;
	margin-left: 2px;
	vertical-align: middle;
	width: 0;
}

#socialShare {
	max-width: 59px;
	margin-bottom: 18px;
}

.fa-inverse {
	color: #0011777d !important;
}

.dropdown-menu {
	background: cornsilk;
}

.btn-info {
	color: #0011777d;
	background-color: cornsilk;
	border-color: #0011777d;
}

.btn-info:hover, .btn-info:focus, .btn-info.focus, .btn-info:active,
	.btn-info.active, .open>.dropdown-toggle.btn-info {
	color: #0011777d;
	background-color: antiquewhite;
	border-color: black;
}

#socialShare>a {
	padding: 6px 10px 6px 10px;
}

@media ( max-width : 320px) {
	#socialHolder {
		padding-left: 5px;
		padding-right: 5px;
	}
	.mobile-social-share h3 {
		margin-left: 0;
		margin-right: 0;
	}
	#socialShare {
		margin-left: 5px;
		margin-right: 5px;
	}
	.mobile-social-share h3 {
		font-size: 15px;
	}
}

@media ( max-width : 238px) {
	.mobile-social-share h3 {
		font-size: 12px;
	}
}
</style>

<body ng-app="App">

	<%
	    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
		CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
		String responseJson = PaymentBean.getPaymentResponseFromRequest(secureRequest);
		String url = WebsiteActions.DONATE.getActionURL(secureRequest, secureResponse);
	%>
	<!--content start here-->
	<div class="page-content" ng-controller="PaymentStatusController">
		<div class="row">
			<div class="container">
				<div class="col-sm-12 col-md-12" ng-if="status.isSuccess == true">
					<div class="content-box-header panel-heading">
						<div class="panel-title" style="text-align: center">
							<h2>THANK YOU!!</h2>
						</div>
						<div class="content-box-large box-with-header">
							<div class="col-md-12" style="text-align: center;">
								<div>You are now part of the Library Movement</div>
								<div>Your contribution helps us create free libraries,
									readers & thinkers</div>
								<br /> <br /> <br />
								<div>Inspire others to do the same.</div>
								<div>
									Send them the link to donate to TCLP:
									<%=url%></div>
								<br />
							</div>

							<div class="col-md-12">
								<link
									href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
									rel="stylesheet" />
								<div class="row mobile-social-share">
									<div class="col-md-9">
										<h3>Tell your friends</h3>
									</div>
									<div id="socialHolder" class="col-md-3">
										<div id="socialShare" class="btn-group share-group">
											<a data-toggle="dropdown" class="btn btn-info"> <i
												class="fa fa-share-alt fa-inverse"></i>
											</a>
											<button href="#" data-toggle="dropdown"
												class="btn btn-info dropdown-toggle share">
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li>
													<a data-original-title="Twitter" rel="tooltip"
														data-text="I Donated to @communitylibpro to support the #librarymovement. To donate, click on "
														data-url="<%=url%>" href="https://twitter.com/share"
														class="btn btn-twitter twitter-share-button"
														data-placement="left"> <i class="fa fa-twitter"></i>
													</a> 
													<script async src="//platform.twitter.com/widgets.js"
														charset="utf-8"></script>
												</li>
												<%--<li class="fb-share-button"
													data-href="https://developers.facebook.com/docs/plugins/"
													data-layout="button" data-size="small"
													data-mobile-iframe="true"><a
													data-original-title="Facebook" class="btn btn-facebook"
													data-placement="left" rel="tooltip"
													class="fb-xfbml-parse-ignore" target="_blank"
													href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">
														<i class="fa fa-facebook"></i>
												</a></li> --%>
											</ul>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-12" ng-if="status.isSuccess == false">
					<div class="content-box-header panel-heading">
						<div class="panel-title" style="text-align: center">
							<h2>Uh Oh</h2>
						</div>
						<div class="content-box-large box-with-header">
							<div class="col-md-12" style="text-align: center;">
								<div>Your payment could not be processed.</div>
								<div>Please try again or hang tight! Someone from TCLP
									will be in touch with you soon.</div>
								<br /> <br /> <br />
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-12" ng-if="transactionDetails != null">
					<form class="form-horizontal" role="form">
						<fieldset>
							<legend>Transaction Details</legend>
							<div class="form-group"
								ng-if="transactionDetails.transactionId != null">
								<label for="fname" class="col-sm-3 control-label">Transaction
									Id</label>
								<div class="col-sm-5">
									<div class="form-control">{{transactionDetails.transactionId}}</div>
								</div>
							</div>
							<div class="form-group"
								ng-if="transactionDetails.gatewayId != null">
								<label for="fname" class="col-sm-3 control-label">Payment
									Gateway Id</label>
								<div class="col-sm-5">
									<div class="form-control">{{transactionDetails.gatewayId}}</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>

		</div>
	</div>
	<%--content end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>

	<%-- jQuery --%>
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse,
					"js/jquery/jquery-1.11.0.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<%-- Bootstrap --%>
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse,
					"js/admin/bootstrap.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse,
					"js/admin/vendors/angular/angular.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<script type="text/javascript">
		var responseJson =	<%=responseJson%>;
	</script>

	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse,
                    "js/angular/paymentstatus.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />