<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.data.payments.PledgeType"%>
<%@page import="org.communitylibrary.ui.beans.PaymentBean"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean
					.getRequest(request);
			CustomSecurityWrapperResponse secureResponse = SecurityBean
					.getResponse(response);
			String formResources = PaymentBean
					.getFormResourcesFromRequest(secureRequest);
			String donateBooks = ConfigStore.getStringValue(CacheConfiguration.STR_DONATE_BOOKS_TEXT, null);
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Donate to TCLP" />
</jsp:include>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<style type="text/css">
.panel-heading, .panel, .panel-default>.panel-heading {
	background-color: antiquewhite;
}
.panel-title {
	background: none;
}

.panel-body {
    background-color: white;
}

.panel-body .panel-heading {
    background: none;
}
</style>

	<jsp:include flush="false" page="/WEB-INF/pages/includes/messages.jsp" />

	<div class="container"
		style="text-align: left; margin-top: 5%; margin-bottom: 2%">
		<div>
			<center>
				<div class="panel-title">
					<h2>Donate to Us</h2>
				</div>
			</center>
		</div>
		<p dir="ltr">&nbsp;</p>
		<p dir="ltr">
			<span style="font-size: 16px"><span
				style="font-family: Georgia, serif">We need your support and
					contribution in fulfilling our mission of expanding access to books
					and promoting thinking through reading. We are working towards
					sustaining and building more libraries that are free and open to
					all, in Delhi and beyond. This not only requires commitment from
					our volunteers and collaborators, but also monetary contribution to
					run and maintain the libraries, by recruiting staff, buying books
					and running our programmes. </span></span>
		</p>
		<p>&nbsp;</p>
		<p dir="ltr">
			<strong> <em> <span style="font-size: 16px"> <span
						style="font-family: Georgia, serif"> We welcome donations
							in any amount but your pledge to become an annual donor will free
							us up from fundraising so that we can do what we do best -
							putting books in the hands of people! </span>
				</span>
			</em>
			</strong>
		</p>
		<div class="panel-group" id="donategroup" style="margin-top: 3%;">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#donategroup"
							href="#donateonline"><strong>DONATE ONLINE (Indian Account Holders Only)</strong></a>
					</h4>
				</div>
				<div id="donateonline" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="content-box-large">
							<div class="panel-heading">
								<div class="panel-title">Please fill in your details</div>
							</div>
							<div class="panel-body">
								<form class="form-horizontal" role="form" id="paymentForm"
									method="POST">
									<fieldset>
										<legend>Personal Information</legend>
										<div class="form-group">
											<label for="fname" class="col-sm-2 control-label">First
												Name*</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="fname"
													maxlength="50" name="fname"
													placeholder="eg John. Only English letters allowed. Do not use salutation or any special characters"
													required="required">
											</div>
										</div>
										<div class="form-group">
											<label for="lname" class="col-sm-2 control-label">Last
												Name*</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="lname"
													maxlength="50" name="lname"
													placeholder="eg Smith Only English letters allowed. Do not use any special characters"
													required="required">
											</div>
										</div>
										<div class="form-group">
											<label for="email" class="col-sm-2 control-label">Email
												Id*</label>
											<div class="col-sm-10">
												<input type="email" class="form-control" id="email"
													name="email" placeholder="eg name@gmail.com"
													required="required">
											</div>
										</div>
										<div class="form-group" title="We promise to not spam you">
											<label for="phone" class="col-sm-2 control-label">Mobile
												Number*</label>
											<div class="col-sm-10">
												<input type="tel" class="form-control" id="phone"
													name="phone" placeholder="eg 9810112233"
													required="required">
											</div>
										</div>
									</fieldset>
									<fieldset>
										<legend>Transaction Details</legend>
										<div class="form-group">
											<label class="control-label col-md-2" for="amount">Amount*</label>
											<div class="col-md-10">
												<div class="row">
													<div class="col-sm-3">
														<div class="input-group">
															<span class="input-group-addon">&#8377;</span> <input
																class="form-control" name="amount" id="amount"
																type="text" placeholder="" required="required">
															<span class="input-group-addon">.00</span>
														</div>
													</div>
													<div class="col-sm-9" style="display:none;">
														<div class="form-group col-sm-8">
															<div class="col-sm-10">
																<div class="checkbox">
																	<div class="col-sm-2 col-md-2">
																		<input id="receipt" name="receipt" type="checkbox">
																	</div>
																	<div class="col-md-10 col-sm-10">
																		<label for="receipt"> I need an 80G receipt
																			for this transaction </label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-2">I Pledge To Contribute</label>
											<div class="col-md-10">
												<% for(PledgeType pledge : PledgeType.values()) { %>
												<div class="radio">
													<label> 
														<input type="radio" name="pledgetype"
															value="<%=pledge.getCode() %>" /><%=pledge.getName() %>
													</label> 
												</div>
												<%} %>
											</div>
										</div>
									</fieldset>
									<fieldset id="receiptHolder">
										<legend>Mandatory Information for the 80G receipt</legend>
										<div class="form-group">
											<label class="control-label col-sm-2 col-sm-offset-1"
												for="pannum">Pan No.*</label>
											<div class="col-sm-9">
												<input type="text" class="form-control" id="pannum"
													name="pannum" placeholder="Your Pan Number">
											</div>
										</div>
										<fieldset class="col-sm-offset-1">
											<legend>Address Details</legend>
											<div class="form-group">
												<label class="control-label col-sm-2" for="addressline1">Address
													Line 1*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="addressline1"
														name="addressline1" placeholder="Address Line 1">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2" for="addressline2">Address
													Line 2*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="addressline2"
														name="addressline2" placeholder="Address Line 2">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2" for="city">City*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="city"
														name="city" placeholder="City">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2" for="state">State*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="state"
														name="state" placeholder="State">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2" for="country">Country*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="country"
														name="country" placeholder="Country">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2" for="zipcode">Zipcode*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="zipcode"
														name="zipcode" placeholder="Zipcode">
												</div>
											</div>
										</fieldset>
									</fieldset>
									<div class="form-group">
										<label for="extracomments" class="col-sm-2 control-label">Any
											Extra Comments?</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="extracomments"
												maxlength="100" name="extracomments"
												placeholder="Say Something nice in 100 characters">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" class="btn btn-primary">Donate</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#donategroup"
							href="#donateoffline"><strong>DONATE OFFLINE</strong></a>
					</h4>
				</div>
				<div id="donateoffline" class="panel-collapse collapse">
					<div class="panel-body">
						<p dir="ltr">
							<span style="font-size: 16px"><span
								style="font-family: Georgia, serif">You can make your
									cheque in favour of 'The Community Library Project' and mail it
									to us at:</span></span>
						</p>
						<p>
							<span style="font-size:16px">
								<span style="font-family:Georgia,serif">
									<strong>The Community Library Project</strong>
								</span>
							</span>
						</p>
						<p>
							<span style="font-size:16px">
								<span style="font-family:Georgia,serif">
									<strong>B 65 Panchsheel Vihar</strong>
								</span>
							</span>
						</p>

						<p>
							<span style="font-size:16px">
								<span style="font-family:Georgia,serif">
									<strong>(behind Triveni Commercial Complex)</strong>
								</span>
							</span>
						</p>

						<p>
							<span style="font-size:16px">
								<span style="font-family:Georgia,serif">
									<strong>Sheikh Sarai Phase 1</strong>
								</span>
							</span>
						</p>

						<p>
							<span style="font-size:16px">
								<span style="font-family:Georgia,serif">
									<strong>New Delhi 110017</strong>
								</span>
							</span>
						</p>

						<p>&nbsp;</p>

						<p dir="ltr">
							<span style="font-size: 16px"><span
								style="font-family: Georgia, serif">or, Transfer your
									contribution to </span></span>
						</p>
						<p dir="ltr">
							<span style="font-size: 16px"><span
								style="font-family: Georgia, serif"><strong>Account
										Name : THE COMMUNITY LIBRARY PROJECT </strong></span></span>
						</p>
						<p dir="ltr">
							<span style="font-size: 16px"><span
								style="font-family: Georgia, serif"><strong>Account
										Number : 50200026997804 </strong></span></span>
						</p>
						<p dir="ltr">
							<span style="font-size: 16px"><span
								style="font-family: Georgia, serif"><strong>Bank
										and Branch : HDFC, Sector -18, Noida </strong></span></span>
						</p>
						<p dir="ltr">
							<span style="font-size: 16px"><span
								style="font-family: Georgia, serif"><strong>IFSC
										Code: HDFC0000088</strong></span></span>
						</p>
						<p>&nbsp;</p>
					</div>
				</div>
			</div>
			<!-- Donate books section -->
			<% if(StringUtility.isNonEmpty(donateBooks)) {
			    %>
			    <div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#donategroup"
							href="#donatebooks"><strong>DONATE BOOKS</strong></a>
					</h4>
				</div>
				<div id="donatebooks" class="panel-collapse collapse">
					<div class="panel-body">
						<%= donateBooks %>
					</div>
				</div>
			</div>
			    
			<% }%>
		</div>

		<div>
			<p dir="ltr">
				<span style="font-size: 16px">
					<span style="font-family: Georgia, serif">
						<strong>Note: We are in the process of applying for 80G certificate and cannot confirm that we will be 
							able to provide a tax exempt 
							receipt this year. </strong>
					</span>
				</span>
			</p>
			<p>&nbsp;</p>
			<p dir="ltr">
				<span style="font-size: 16px"><span
					style="font-family: Georgia, serif">If you have any queries
						regarding donations, please do get in touch with Ritika Puri/Arti Malik, via
						email - <a href="mailto:booksforthepeopledelhi@gmail.com">booksforthepeopledelhi@gmail.com</a>,
						<br />or Phone - +91 9871193204/+91 9871451216.
				</span></span>
			</p>
		</div>
	</div>
	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />

	<script type="text/javascript">
		var paymentObj;
		
		$(document).ready(function() {
			var formResources = <%=formResources%> ;
			var formURL = "<%=WebsiteActions.DONATE.getActionURL(secureRequest, secureResponse, new String[]{"pay"})%>";
      		paymentObj = new PaymentJs(formResources, formURL, $("#paymentForm"));
   		});
  </script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />