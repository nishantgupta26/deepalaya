<%@page import="org.communitylibrary.utils.HtmlUtility"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.beans.AudienceBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.utils.ConfigStore" %>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
	String imageURL = UIBean.getCompleteURL(secureRequest, secureResponse, "/images/banner-1200.png");
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Blogs From TCLP" />
	<jsp:param name="urlLink" value="<%= WebsiteActions.BLOGS.getActionURL(secureRequest, secureResponse) %>" />
	<jsp:param name="imageLink" value="http://res.cloudinary.com/dlxjsuwkm/image/upload/v1507970357/IMG_2317_odeql3.jpg"/>
	<jsp:param name="description" value="Articles about the Library, by the people of the library. We will list down some really nice article, giving an insight about the updates about the library. Do connect with us." />
</jsp:include>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<style type="text/css">

a:hover { text-decoration:none; }
.btn
{
    transition: all .2s linear;
    -webkit-transition: all .2s linear;
    -moz-transition: all .2s linear;
    -o-transition: all .2s linear;
}
.btn-read-more
{
    padding: 5px;
    text-align: center;
    border-radius: 0px;
    display: inline-block;
    border: 2px solid #222;
    text-decoration: none;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 12px;
    width: 100%;
}

.btn-read-more:hover
{
    color: #FFF;
    background: #222;
}
.post {
	 border-bottom:1px solid #DDD;
	 margin-top : 1%; 
 }
 
.post-title-wrap {
	text-align: center;
	margin: 1% 0;
}
 
 .post  a {
	font-weight: 400;
	line-height: 1.25;
	color: #222;
	cursor: pointer;
 }

.blogs-listing {margin: 2% 0; margin-bottom: 5%;}
.post-title {  color:#662D91; }
.post .glyphicon { margin-right:5px; }
.post-header-line { border-top:1px solid #DDD;border-bottom:1px solid #DDD;padding:5px 0px 5px 15px;font-size: 12px; text-align: center}
.post-content { padding-bottom: 15px;padding-top: 15px;}
</style>
<body ng-app="BlogsAPP">

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<%--Main content starts --%>
	<div class="container" ng-controller="BlogsController">
		<div class="row" style="min-height: 200px; padding: 20% 0;" ng-show="dispPosts.length == 0">
			<div class="text-center">Hold Tight! Awesome Blog-posts coming your way.</div>
		</div>
		<div ng-show="dispPosts.length > 0">
			<div class="container blogs-listing">
				<div class="row">
					<div class="col-sm-2 col-md-2 col-lg-2"></div>
					<div class="col-md-8 col-sm-8 col-lg-8">
						<div class="row" ng-repeat="post in dispPosts">
							<div class="col-md-12 post">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-lg-12">
										<h2 class="post-title-wrap"><a
												ng-href="{{post.url}}"
												class="post-title" ng-bind-html="post.title | unsafe">Post title</a>
										</h2>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-lg-12 col-sm-12 post-header-line">
										<span class="glyphicon glyphicon-user"></span>by <a href="#">{{post.author}}</a>
										| <span class="glyphicon glyphicon-calendar"> </span>{{post.publishdate}} 
										| <span class="glyphicon glyphicon-comment"></span><a ng-href="{{post.url}}#disqus_thread" data-disqus-identifier="{{post.id}}"> 0 Comments</a>
									</div>
								</div>
								<div class="row" style="margin: 1% 0; text-align: center" ng-if="post.bannerImg">
									<img ng-src="{{post.bannerImg}}" max-width="100%" max-height="100%" width="100%">    
								</div>
								<div class="row post-content">
									<div class="col-md-12 col-sm-12 col-lg-12">
										<p ng-bind-html="post.content | unsafe"></p>
										<p style="margin:2% 0">
											<a class="btn btn-read-more"
												ng-href="{{post.url}}">Read
												more</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-2 col-md-2 col-lg-2"></div>
				</div>
				<div class="row" style="margin: 2% 0">
					<button class="btn btn-lg btn-primary btn-default" ng-click="getPreviousPage()" ng-disabled="dispPage==1">Newer Posts</button>
					<button class="btn btn-lg  pull-right" ng-click="getNextPage()" ng-disabled="haveMorePosts()">Older Posts</button>
				</div>
			</div>

		</div>
	</div>
	<%--Main content ends --%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>

	<%-- jQuery --%>
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/jquery/jquery-1.11.0.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	
	<%-- Bootstrap --%>
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/bootstrap.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/vendors/angular/angular.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

	<script type="text/javascript">
		var formURL = "<%=WebsiteActions.AJAX_GET_PUBLISHED_BLOGS.getActionURL(secureRequest, secureResponse, null, false)%>";
		var numPosts = <%=ConfigStore.getIntValue(CacheConfiguration.INT_NUM_POSTS, 1)%>;
  	</script>

	<script  src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/angular/angularblogs.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	
	<script id="dsq-count-scr" src="//commlibpro.disqus.com/count.js" async></script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />