<!--event start here-->
<div class="events">
		<div class="events-main">
			<div class="events-top">
				<h3>Events</h3>
			</div>
			<div id="no-events" style="display: none;">We do not have any
				upcoming events for now.</div>
			<div id="events-container"></div>
		</div>
</div>
<!--event end here-->