<%@page import="org.communitylibrary.ui.security.CustomSecurityRequestType"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><%=request.getParameter("title")%></title>
<meta name="viewport" content="width=device-width, initial-scale=0.85">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<%
CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
String title = secureRequest.getParameter("title");
String description = secureRequest.getParameter("description");
String urlLink = secureRequest.getParameter("urlLink");
String imageLink = secureRequest.getParameter("imageLink");
%>

<%
if(title != null) { %>
 <meta property="og:title" content="<%=title %>" />
 <meta property="fb:app_id" content="201150733774293"/>
 <meta property="og:site_name" content="The Community Library Project"/>
 <meta property="og:type" content="article"/>
<%} %> 
<% if(description != null) { %>
	<meta property="og:description" content="<%=description%>" /> 
<% } %>
<%
if(urlLink != null) { %>
  <meta property="og:url" content="<%=urlLink%>" />
<%} %>

<% 
if(imageLink != null) { %>
	<meta property="og:image" content="<%=imageLink%>" />
<%} %> 

<!-- Icons & favicons -->
<link rel="icon" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/favicon.ico") %>">

<!-- Stylesheet -->
<link rel="stylesheet" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/plugins/plugins-min.css?ver=" +StaticFileVersions.CSS_VERSION) %>" media="all">
<link rel="stylesheet" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/app-min.css?ver="+StaticFileVersions.CSS_VERSION) %>" media = "all">

<!--Google Fonts-->
<link href='/static/css/fonts/google-fonts-min.css' rel='stylesheet' type='text/css'>
<!--google fonts-->

<!-- Modernizr -->
<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/modernizr-min.js")%>"></script>

<%-- jQuery --%>
<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/jquery/jquery-1.11.0.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
</head>
