<%@page import="java.util.List"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%@page import="org.communitylibrary.app.resources.ResourceObject"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>

<div class="container banner-main">
	<%-- <div id="banner-img" class="col-sm-12">
		<img
			src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/banner-1200.png")%>"
			style="width: 100%;">
	</div>
	<div id="banner-txt" class="col-sm-12">
		<h3>The Community Library Project</h3>
	</div> --%>
	<% List<ResourceObject> bannerResources = WebsiteBean.getBannerResourcesFromRequest(secureRequest);%>
	<div id="bannerCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <% if(bannerResources != null && bannerResources.size() > 1) {  %>
	    <ol class="carousel-indicators">
	      <%
    		for(int i = 0; i < bannerResources.size(); i++) { %>
	      		<li data-target="#bannerCarousel" data-slide-to="<%=i %>" <% if(i==0){%>class="active"<% } %>></li>
    	  <%
    		  }
 			%>
    	</ol>
    <% } %>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    <% 
	   	if(bannerResources != null) {
			for(int i = 0; i < bannerResources.size(); i++) {
			    ResourceObject banner = bannerResources.get(i);
	%>
		      <div class="item <% if(i==0){%>active<%}%>">
		        <img src="<%=banner.getImageUrl()%>" style="width:100%;" alt="Library Banner">
		      </div>
    <% 	
	   		}
	   	} else {
	%>	   	    
      <div class="item active">
	       <img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/banner-1200.png")%>" alt="Library Banner" style="width:100%;">
      </div>	   	    
 	<%
	 	}
	%>  
    </div>

    <!-- Left and right controls -->
    <% if(bannerResources != null && bannerResources.size() > 1) {  %>
    <a class="left carousel-control" href="#bannerCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#bannerCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
    <%} %>
  </div>
	
</div>
