<!--educate logos start here-->
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.navigation.ContentActions"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
	CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>

<script type="text/javascript">

var isMobile = false;

var styleEl = document.createElement('style');

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
isMobile = true;
styleEl.innerHTML = '.ch-grid{margin:20px 0 0;padding:0;list-style:none;display:block;text-align:center;width:100%}.ch-grid li{width:100%!important;height:220px;display:inline-block}.ch-item{width:100%;height:100%;border-radius:50%;position:relative;cursor:default}.ch-item>.ch-info,.ch-item>.ch-info>div{position:absolute;width:100%;height:100%}.ch-info .ch-info-front{padding-top:4em}.ch-item>.ch-info>div{background-position:center center}.ch-item>.ch-info>div:nth-child(2){display:block}.ch-item>.ch-info>div:nth-child(1){display:none}.ch-img-1,.ch-info .ch-info-back{background:#3D3630}.ch-img-2{background:#b7295a}.ch-img-3{background:#a2b040}.ch-img-4{background:#f2af00}.ch-info h3{color:#fff;font-size:24px;height:90px}.ch-info p{color:#fff;font-size:13px;line-height:1.8em}.ch-info p a{display:block;color:rgba(255,255,255,.7);font-weight:700;text-transform:uppercase;font-size:14px;letter-spacing:1px;padding-top:4px;padding-bottom:4px}.ch-info p a:hover{color:#fff222;color:rgba(255,242,34,.8)}';
} else {
  var html = [];
  var i = 0;
  html[i++]=".ch-info-front h5 {";
  html[i++]="	font-size: 1.5em;";
  html[i++]="	color: #fff;";
  html[i++]="	width: 95%;";
  html[i++]="}";

  html[i++]=".ch-grid {";
  html[i++]="	margin: 20px 0 0 0;";
  html[i++]="	padding: 0;";
  html[i++]="	list-style: none;";
  html[i++]="	display: block;";
  html[i++]="	text-align: center;";
  html[i++]="	width: 100%;";
  html[i++]="}";

  html[i++]="ch-grid:after, .ch-item:before {";
  html[i++]="	content: '';";
  html[i++]="	display: table;";
  html[i++]="}";

  html[i++]=".ch-grid:after {";
  html[i++]="	clear: both;";
  html[i++]="}";

  html[i++]=".ch-grid li {";
  html[i++]="	width: 100% !important;";
  html[i++]="	height: 220px;";
  html[i++]="	display: inline-block;";
  html[i++]="}";

  html[i++]=".ch-item {";
  html[i++]="	width: 100%;";
  html[i++]="	height: 100%;";
  html[i++]="	border-radius: 50%;";
  html[i++]="	position: relative;";
  html[i++]="	cursor: default;";
  html[i++]="	-webkit-perspective: 900px;";
  html[i++]="	-moz-perspective: 900px;";
  html[i++]="	-o-perspective: 900px;";
  html[i++]="	-ms-perspective: 900px;";
  html[i++]="	perspective: 900px;";
  html[i++]="}";

  html[i++]=".ch-info {";
  html[i++]="	position: absolute;";
  html[i++]="	width: 100%;";
  html[i++]="	height: 100%;";
  html[i++]="	-webkit-transform-style: preserve-3d;";
  html[i++]="	-moz-transform-style: preserve-3d;";
  html[i++]="	-o-transform-style: preserve-3d;";
  html[i++]="	-ms-transform-style: preserve-3d;";
  html[i++]="	transform-style: preserve-3d;";
  html[i++]="}";

  html[i++]=".ch-info>div {";
  html[i++]="	display: block;";
  html[i++]="	position: absolute;";
  html[i++]="	width: 100%;";
  html[i++]="	height: 100%;";
  html[i++]="	background-position: center center;";
  html[i++]="	-webkit-transition: all 0.4s linear;";
  html[i++]="	-moz-transition: all 0.4s linear;";
  html[i++]="	-o-transition: all 0.4s linear;";
  html[i++]="	-ms-transition: all 0.4s linear;";
  html[i++]="	transition: all 0.4s linear;";
  html[i++]="	-webkit-transform-origin: 50% 0%;";
  html[i++]="	-moz-transform-origin: 50% 0%;";
  html[i++]="	-o-transform-origin: 50% 0%;";
  html[i++]="	-ms-transform-origin: 50% 0%;";
  html[i++]="	transform-origin: 50% 0%;";
  html[i++]="}";

  html[i++]=".ch-info .ch-info-front {";
  html[i++]="	padding-top: 4em;";
  html[i++]="}";

  html[i++]=".ch-info .ch-info-back {";
  html[i++]="	-webkit-transform: translate3d(0, 0, -220px) rotate3d(1, 0, 0, 90deg);";
  html[i++]="	-moz-transform: translate3d(0, 0, -220px) rotate3d(1, 0, 0, 90deg);";
  html[i++]="	-o-transform: translate3d(0, 0, -220px) rotate3d(1, 0, 0, 90deg);";
  html[i++]="	-ms-transform: translate3d(0, 0, -220px) rotate3d(1, 0, 0, 90deg);";
  html[i++]="	transform: translate3d(0, 0, -220px) rotate3d(1, 0, 0, 90deg);";
  html[i++]="	background: #3D3630;";
  html[i++]="	opacity: 0;";
  html[i++]="}";

  html[i++]=".ch-img-1 {";
  html[i++]="	background: #3D3630;";
  html[i++]="}";

  html[i++]=".ch-img-2 {";
  html[i++]="	background: #b7295a;";
  html[i++]="}";

  html[i++]=".ch-img-3 {";
  html[i++]="	background: #a2b040;";
  html[i++]="}";
  html[i++]=".ch-img-4 {";
  html[i++]="	background: #f2af00;";
  html[i++]="}";

  html[i++]=".ch-info h3 {";
  html[i++]="	color: #fff;";
  html[i++]="	font-size: 24px;";
  html[i++]="	margin: 0 15px;";
  html[i++]="	padding: 60px 0 0 0;";
  html[i++]="	height: 90px;";
  html[i++]="}";

  html[i++]=".ch-info p {";
  html[i++]="    color: #fff;";
  html[i++]="    padding: 7px 5px;";
  html[i++]="    margin: 4% 2%;";
  html[i++]="    font-size: 13px;";
  html[i++]="    line-height: 1.8em;";
  html[i++]="}";

  html[i++]=".ch-info p a {";
  html[i++]="	display: block;";
  html[i++]="	color: rgba(255, 255, 255, 0.7);";
  html[i++]="	font-weight: bold;";
  html[i++]="	text-transform: uppercase;";
  html[i++]="	font-size: 14px;";
  html[i++]="	letter-spacing: 1px;";
  html[i++]="	padding-top: 4px;";
  html[i++]="	padding-bottom: 4px;";
  html[i++]="}";

  html[i++]=".ch-info p a:hover {";
  html[i++]="	color: #fff222;";
  html[i++]="	color: rgba(255, 242, 34, 0.8);";
  html[i++]="}";

  html[i++]=".ch-item:hover .ch-info-front {";
  html[i++]="	-webkit-transform: translate3d(0, 280px, 0) rotate3d(1, 0, 0, -90deg);";
  html[i++]="	-moz-transform: translate3d(0, 280px, 0) rotate3d(1, 0, 0, -90deg);";
  html[i++]="	-o-transform: translate3d(0, 280px, 0) rotate3d(1, 0, 0, -90deg);";
  html[i++]="	-ms-transform: translate3d(0, 280px, 0) rotate3d(1, 0, 0, -90deg);";
  html[i++]="	transform: translate3d(0, 280px, 0) rotate3d(1, 0, 0, -90deg);";
  html[i++]="	opacity: 0;";
  html[i++]="}";

  html[i++]=".ch-item:hover .ch-info-back {";
  html[i++]="	-webkit-transform: rotate3d(1, 0, 0, 0deg);";
  html[i++]="	-moz-transform: rotate3d(1, 0, 0, 0deg);";
  html[i++]="	-o-transform: rotate3d(1, 0, 0, 0deg);";
  html[i++]="	-ms-transform: rotate3d(1, 0, 0, 0deg);";
  html[i++]="	transform: rotate3d(1, 0, 0, 0deg);";
  html[i++]="	opacity: 1;";
  html[i++]="}";

  styleEl.innerHTML = html.join("");
}

document.head.appendChild(styleEl);

$(document).ready(function() {
  
  $(".ch-info-back").each(function() {
    var height1= $(this).height();
    var elem2 = $(this).find("p");

    if(elem2.length > 0) {
	    var height2 = $(elem2).height();
    	$(elem2).css("margin-top", (height1 - height2)/2);
    }
  });


  
  if(isMobile === true) {
	  $(".ch-info-front").each(function() {
		    var mainElem = this;
		    $(mainElem).show();
		    $(mainElem).siblings().hide();
		  });
	  
	  $(".ch-info-front").click(function() {
		    var mainElem = this;
		    $(mainElem).hide();
		    $(mainElem).siblings().show();
		  });  
  }
});

</script>

<div class="educate">
	<div class="container">
		<div class="education-main">
			<ul class="ch-grid">
				 <div class="col-md-3 w3agile">
					<li>
						<div class="ch-item">				
							<div class="ch-info">
								<div class="ch-info-front ch-img-1">
									<span class="glyphicon glyphicon-grain" aria-hidden="true"> </span>
					                <h5>How We Work</h5>
								</div>
								<div class="ch-info-back">
									<p>
										<a href="<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.OMODEL, false)%>">Organising Model</a>
										<a href="<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.D2DOPERATIONS, false)%>">Day-to-day Operations</a>
										<a href="<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.ONOURSHELVES, false)%>">What's On Our Shelves</a>
										<a href="<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.POLICYDOCUMENTS, false)%>">TCLP Policies</a>
									</p>
								</div>	
							</div>
						</div>
					</li>
					</div> 
					 <div class="col-md-3 w3agile">
					<li>
						<div style="cursor:pointer;" class="ch-item" onclick="javascript:window.location.href='<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.CURRICULLUM, false)%>'">
							<div class="ch-info">
								<div class="ch-info-front ch-img-2">
									<span class="glyphicon glyphicon-education" aria-hidden="true"> </span>									
					                <h5>Curriculum</h5>
								</div>
								<div class="ch-info-back">
									<h3>Curriculum</h3>
									<%-- <p></p>--%>
								</div>
							</div>
						</div>
					</li>
					</div>
					 <div class="col-md-3 w3agile">
					<li>
						<div style="cursor:pointer;" class="ch-item" 
							onclick=<% boolean isOpenExternalSource = ConfigStore.getBooleanValue(CacheConfiguration.BOL_GALLERY_EXTERNAL, false); String galleryURL = ConfigStore.getStringValue(CacheConfiguration.STR_GALLERY_SOURCE_URL, null); if(isOpenExternalSource && galleryURL != null && !galleryURL.trim().isEmpty()) { %>"javascript:window.open('<%=galleryURL%>')"<%} else { %>"javascript:window.location.href='<%=WebsiteActions.GALLERY.getActionURL(secureRequest, secureResponse, null, false)%>'"<%} %>>
							<div class="ch-info">
								<div class="ch-info-front ch-img-3">
									<span class="glyphicon glyphicon-picture" aria-hidden="true"> </span>
					                <h5>Gallery</h5>
								</div>
								<div class="ch-info-back">
									<h3>Gallery</h3>
									<%-- <p></p>--%>
								</div>
							</div>
						</div>
					</li>
					</div>
					 <div class="col-md-3 w3agile">
					<li>
						<div style="cursor:pointer;" class="ch-item" onclick="javascript:window.location.href='<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.LIBRARY_MOVEMENT, false)%>'">
							<div class="ch-info">
								<div class="ch-info-front ch-img-4">
									<span class="glyphicon glyphicon-book" aria-hidden="true"> </span>
					                <h5>The Library Movement</h5>
								</div>
								<div class="ch-info-back">
									<h3>Elsewhere in the Library Movement</h3>
									<%-- <p></p>--%>
								</div>
							</div>
						</div>
					</li>
					</div>
					<div class="clearfix"> </div>
			 </ul>
		 </div>
	</div>
</div>
<!--educate logos end here-->