<!--Blogposts start here-->
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<div class="blogposts">
	<div class="blogposts-main">
		<%
			CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
			CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
		    String blogURL = StringUtility.trimAndEmptyIsNull(ConfigStore.getStringValue(CacheConfiguration.STR_BLOG_URL, null));
		    if (blogURL != null) {
		%>
		<script language="JavaScript" src="http://feed2js.org//feed2js.php?src=<%=blogURL%>%2Fatom.xml&chan=y&num=1&desc=200&au=n&targ=y&utf=y" charset="UTF-8" type="text/javascript"></script>
		<noscript>
			<a href="http://feed2js.org//feed2js.php?src=http%3A%2F%2Fthecommunitylibrary.blogspot.in%2Fatom.xml&chan=y&num=1&desc=200&au=n&targ=y&utf=y&html=y">View RSS feed</a>
		</noscript>
		<%
		    }
		%>

<!-- 		<div class="rss-box">
			<p class="rss-title">
				<a class="rss-title" href="http://thecommunitylibrary.blogspot.com/"
					target="_blank">Community Library Blog</a><br>
				<span class="rss-item"></span>
			</p>
			<ul class="rss-items">
				<li class="rss-item"><a class="rss-item"
					href="http://thecommunitylibrary.blogspot.com/2017/06/why-india-needs-library-movement-cross.html"
					target="_blank">Why India Needs A Library Movement
						(Cross-posted from 'Caravan' magazine)</a> <span class="rss-item-auth">(Community
						Library Blog)</span><br>Tushar pushes against weight.&nbsp;He might
					be eight or nine years old, wiry, muscled. He has a large head with
					lashes of disproportionate length, even for his large eyes. He
					insists he will read “this boo...</li>
			</ul>
		</div> -->
	</div>
	<div class="clearfix"></div>
</div>
<!--Blogposts end here-->