<%@page import="org.communitylibrary.ui.navigation.ContentActions"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%
CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
String content = ConfigStore.getStringValue(ContentActions.PRESS.getConfigType(), null);
%>


	<div class="content-box-large">
		<div class="panel-heading">
			<div class="panel-title"><strong>TCLP in PRESS</strong></div>
		</div>
		<div class="panel-body">
			<% if(content == null) { %>
				<div style="text-align:center; width:100%">
				    <div style="margin:auto; text-align:left;">
		       			We are bringing you the latest buzz about 'The Community Library Project' soon
		   			</div>
				</div>
			<%} else { %>
				<div id="press-container" data-offset="">
					<%= content %>
				</div>
			<%} %>
		</div>
	</div>
<style>
#press-container {
	height: 400px;
	overflow:hidden;
}
#press-container li {
	margin: 10px 0;
}

#press-container li:first-child {
	margin-top: 0;
}

</style>
<script type="text/javascript">
var timer;
$(document).ready(function() {
var elem = $("#press-container");
  if ($(elem).length > 0) {
   	var DELAY_READING = 4000;
      var links = $(elem).find('li');
      var olElem = $("<ul />").attr('id', 'li-container');
      $(olElem).html(links);
      
      $(elem).html(olElem);

    timer = setInterval(function() {
        delayLinks()
   	}, DELAY_READING);

      
    function delayLinks(i) {
        var liElem = $('#press-container li:nth-child(1)');
        $(liElem).remove();
        $('#press-container ul').append(liElem);          
    }
  }
});
</script>