<%@page import="org.communitylibrary.ui.beans.MessageBean"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    String success = MessageBean.getMessage(secureRequest, MessageBean.MessageType.MSG_SUCCESS);
    String error = MessageBean.getMessage(secureRequest, MessageBean.MessageType.MSG_ERROR);
%>
<%
    if (success != null || error != null) {
%>
<div class="row" id="serverMessageDiv">
	<div class="container">
		<div>
			<div class="panel-title">
				<strong>Server Message</strong>
				<span title="Hide this error" style="text-align:right;float: right;cursor: pointer;" onclick="javascript: $('#serverMessageDiv').hide();"><i class="glyphicon glyphicon-remove-circle"></i></span>
			</div>
		</div>
		<div>
			<%
			    if (success != null) {
			%>
			<div class="btn btn-success">
				<center>
					<strong><strong><%=success%></strong>
				</center>
			</div>
			<%
			    }
			%>

			<%
			    if (error != null) {
			%>
			<div class="btn btn-danger">
				<center>
					<strong><%=error%></strong>
				</center>
			</div>
			<%
			    }
			%>
		</div>
	</div>
</div>

<%
    }
%>