<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.ui.navigation.ContentActions"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
    WebsiteActions currentAction = UIBean.getCurrentUIAction(secureRequest);
    ContentActions currentContentAction = UIBean.getCurrentContentAction(secureRequest);
    boolean isFooter = "true".equals(secureRequest.getParameter("isFooter"));
    
    boolean isOnlineGateway = UIBean.isUseOnlineDonationPage();
    /* String donateToUsURL = UIBean.getDonateToUsURL(isOnlineGateway, secureRequest, secureResponse); */
    
    String blogURL = ConfigStore.getStringValue(CacheConfiguration.STR_BLOG_URL, null);
    boolean isNewWindow = ConfigStore.getBooleanValue(CacheConfiguration.BOL_BLOG_NEW_WINDOW, false);
%>
<li>
<%-- <a href="<%=WebsiteActions.HOME.getActionURL(secureRequest, secureResponse, null, false)%>" <%if (currentAction == WebsiteActions.HOME) {%> class="active" <%}%>>Home</a></li>

<%if(!isFooter) { %>
<li><a href="<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.ABOUT, false)%>"
	<%if (currentContentAction != null && currentContentAction == ContentActions.ABOUT) {%> class="active" <%}%>>About Us</a></li>

<li><a href="<%= WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.VOLUNTEER, false) %>">Volunteer With Us</a></li>

<li><a href="<%= donateToUsURL %>">Donate To Us</a></li>
	<% if(blogURL != null && !blogURL.isEmpty() && !"null".equalsIgnoreCase(blogURL)) { %>
	
		<li><a href="<%=blogURL%>" <% if(isNewWindow){ %>target="_blank"<%} %>>Blog</a></li>
	<%} else { %>
		<li><a <%if (currentAction == WebsiteActions.BLOGS) {%> class="active" <%}%> href="<%=WebsiteActions.BLOGS.getActionURL(secureRequest, secureResponse, null, false)%>">Blog</a></li>
	<%} %>
	
<li>
	<a href="<%=WebsiteActions.CONTACT.getActionURL(secureRequest, secureResponse, null, false)%>"
		<%if (currentAction == WebsiteActions.CONTACT) {%> class="active" <%}%>>Contact Us</a>
</li>
	
<%} %>
 --%>
<%
    if (WebsiteBean.isUserLoggedIn(secureRequest)) {
%>
<%
    if (isFooter) {
%>
<li class="dropdown"><a href="#">Members</a>
	<ul style="display: none;" class="dropdown-menu bold">
   		<%	if(WebsiteBean.isUIActionAllowed(secureRequest, WebsiteActions.ADD_MEMBERS)){ %>      
			<li><a href="<%= WebsiteActions.ADD_MEMBERS.getActionURL(secureRequest, secureResponse, null, false) %>">Add Members</a></li>
		<%} %>
    	<%-- <%	if(WebsiteBean.isUIActionAllowed(secureRequest, WebsiteActions.DISPLAY_MEMBER)){ %>      
				<li><a href="<%= WebsiteActions.DISPLAY_MEMBER.getActionURL(secureRequest, secureResponse, null, false) %>">Display Members</a></li>
		<%} %> --%>
	</ul>
</li>
<%
    }
%>
<%
    }
%>

<%-- <%if(isFooter) { %>
<li><a href="<%=WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null, ContentActions.PRESS, false)%>"
	<%if (currentContentAction != null && currentContentAction == ContentActions.PRESS) {%> class="active" <%}%>>Press</a></li>
<%} %> --%>
<%-- <%
    if (isFooter && WebsiteBean.isUIActionAllowed(secureRequest, WebsiteActions.LOGIN)) {
%>
<li><a
	href="<%=WebsiteActions.LOGIN.getActionURL(secureRequest, secureResponse, null, false)%>">Login</a></li>
<%
    }
%>

<%
    if (isFooter && WebsiteBean.isUIActionAllowed(secureRequest, WebsiteActions.LOGOUT)) {
%>
<li><a
	href="<%=WebsiteActions.LOGOUT.getActionURL(secureRequest, secureResponse, null, false)%>">Logout</a></li>
<%
    }
%> --%>

<%-- <%
    if (!isFooter && WebsiteBean.isUIActionAllowed(secureRequest, WebsiteActions.SWITCHTOADMIN)) {
%>
<li><a
	href="<%=AdminActions.SWITCHTOADMIN.getActionURL(secureRequest, secureResponse)%>">Admin Console</a></li>
<%
    }
%> --%>

<%-- <% if (isFooter) {%>
<li><a
	href="<%=WebsiteActions.BLOGS.getActionURL(secureRequest, secureResponse, null, false)%>"
	<%if (currentAction == WebsiteActions.BLOGS) {%> class="active" <%}%>>Local Blogs</a></li>
	<% } %> --%>