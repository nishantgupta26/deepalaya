<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean" %>
<%
	CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<%-- Bootstrap --%>
<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/bootstrap.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>

<script type="text/javascript" src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/jquery-ui.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
<link href="<%= UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery-ui.min.css?ver=" + StaticFileVersions.CSS_VERSION)%>" rel="stylesheet" />

<script type="text/javascript">
var ua = window.navigator.userAgent;    
var msie = ua.indexOf("MSIE ");
if (msie > 0) {
  	window.location.href = "<%=WebsiteActions.BROWSER_UNSUPPORTED.getActionURL(secureRequest, secureResponse, null, true)%>";
}

var WEB_UTILS;
var newsletter;
$(document).ready(function() {
  WEB_UTILS = new function() {
    
    this.trim = function(caption, length) {
      if(!caption) {
        return "";
      }
      
      caption =  caption.replace(/[^A-Za-z0-9#\"\-\_]/g, " ");
      
      caption =  caption.length > length ? caption.substr(0, caption.lastIndexOf(' ', length - 3)) + '...' : caption;
      caption = caption.trim().replace(/\n/g, " ");
      return caption;
    }
    
    this.SYS_VARS = new Object();
    this.initSysVars = function () {
       this.SYS_VARS.CUR_DATE = new Date();
    }
    
    this.initSysVars();
    
    this.getSysCurrentDate = function() {
      return this.copyDate(this.SYS_VARS.CUR_DATE);  
    }
    
    this.months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
   
    this.getMonthName = function(month) {
      return this.months[month];
    }
    
    this.copyDate = function(dt) {
      var dt2 = new Date();
      dt2.setTime(dt.getTime());
      return dt2;
    }
    
    this.initPreviousDate = function(inputField, dFormat) {
      var curDate = WEB_UTILS.getSysCurrentDate();
      $(inputField).datepicker({
        beforeShow : function(input) {
          $(input).css({
            "position" : "relative",
            "z-index" : 1000
          });
        },
        onSelect: function() {
          $(this).change(); 
        },
        onClose: function(input) {
          $(inputField).css({
            "z-index" : 1
          });
        },
        maxDate : curDate,
        numberOfMonths : 2,
        showOn : "focus",
        dateFormat: (dFormat != null ? dFormat : "mm/dd/yy"),
      });
      
    }
    
    
    this.isValidNonEmptyString = function(cStr) {
      return cStr && (cStr = cStr.trim()) != "";
    }
    
    this.isValidPanNumber = function(pannum) {
      
      if(!this.isValidNonEmptyString(pannum)) {
        return false;
      }
      
      var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
      var code = /([C,P,H,F,A,T,B,L,J,G])/;
      var code_chk = pannum.substring(3,4);
      if (pannum.search(panPat) == -1) {
          return false;
      }
      if (code.test(code_chk) == false) {
          return false;
      }
      
      return true;
    }
    
    this.isValidEmail = function(email) {
      return email.match(/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/);
     };
     
     this.isValidPassword = function(cStr) {
       return cStr.match(/^[0-9a-zA-Z\!\@\#\$\%\^\&\*\(\)\-\_\+\=\{\}\[\]\|\:\;\<\,\>\.\?\~]{8,}$/);
     };
     
     this.isValidPositiveInteger = function(cStr) {
       return cStr.match(/^[0-9]{1,}$/);
     }
     
     this.isValidNonZeroAmount = function(cStr) {
         return cStr.match(/^[0-9]{1,}$/);
       }
     
     this.isStrictValidPhone = function(cStr) {
       
       if(!this.isValidNonEmptyString(cStr)) {
         return false;
       }
       
       return cStr.match(/^[0-9]{10}((\,){0,1}(\s){0,}[0-9]{10}){0,}$/);
     }
     
     this.isLettersOnly = function(cStr) {
	    var regex = new RegExp("[a-zA-Z]+$");
	    return regex.test(cStr);
     }
     
     this.isValidPhone = function(cStr) {
       if(cStr == "") {
         return true;
       }
       
       return this.isStrictValidPhone(cStr);
     }
     
     
     this.addError = function(errorMsg) {
       alert(errorMsg);
     };
     
     this.setSelectBoxByValue = function(selectElem, value) {
       $(selectElem).val(value).prop('selected', true);
     }
     
     this.getJSONData = function(url , successHandler, errorHandler, dataFormat) {
       $.ajax({
         url: url,
         type: 'GET',
         dataType: 'json'
      }).done(function(data) {
          if(successHandler) {
            successHandler(data);
          }
         })
         .fail(function() {
           if(errorHandler) {
 				errorHandler();
           }
         });
     }
     
     this.makeAJAXHit = function(url, requestType, reqData, successHandler, errorHandler, dataFormat) {
       $.ajax({
         url: url,
         type: requestType,
         data: reqData ? jQuery.param(reqData) : null,
         contentType: reqData ? 'application/x-www-form-urlencoded; charset=UTF-8':"text/plain",
         dataType: 'json'
      }).done(function(data) {
          if(successHandler) {
            successHandler(data);
          }
         })
         .fail(function() {
           if(errorHandler) {
   			errorHandler();
           }
         });
     }
     
  }
  
  //using localstorage and session storage
  //var isDisplayedInSession = sessionStorage.isDisplayed;
  var isDoNotShow = sessionStorage.doNotShow;
  var modalWidth = screen.width <= 640 ?  screen.width : (screen.width/ 3);
  if($("#modalPopupContent").length > 0) {
	  if(((!isDoNotShow) || isDoNotShow === false)) {
			$("#modalPopupContent").dialog({
			  my: "center top", at: "center top+20", of: "body",
		        modal : true,
		        width : modalWidth,
		        closeOnEscape : true,
		        open : function(event, ui) {
					//add content for scroll
					$('#modalPopupContent .flexslider').flexslider({
						animation: "slide"
					});
		        },

		        buttons : [{
		        	text: "Close and do not show again",
		        	click: function() {
			            $(this).dialog("close");
			            sessionStorage.doNotShow = true;
		        	}
		        }, {
		          text: "Close",
		          click: function() {
		            $(this).dialog("close");
		          }
		        }]

		      });
	  }
  }
 

});

</script>

<!-- Plugins  -->
<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/all1-min.js?ver=" + StaticFileVersions.JS_VERSION) %>"></script>

<!-- script-for-menu -->
<script type="text/javascript">
$(document).ready(function() {
	
 $( "span.menu" ).click(function() {
	$( "ul.res" ).slideToggle( 300, function() {
	// Animation complete.
	 });
});
<%--Custom menu js code --%>
//Navi hover
$('ul li.dropdown').hover(function () {
	$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
}, function () {
	$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
});

});


<!-- /script-for-menu -->
</script>
<!-- FlexSlider -->

<%-- General --%>
<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "js/all2-min.js?ver=" + StaticFileVersions.JS_VERSION) %>"></script>

<%-- Google Map --%>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXXFTghlWuR_p2KohJtKJweWTM0e-NsL0"></script>


