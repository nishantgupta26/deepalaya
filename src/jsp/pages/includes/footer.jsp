<%@page import="org.communitylibrary.utils.HtmlUtility"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
    WebsiteActions currentAction = UIBean.getCurrentUIAction(secureRequest);
%>
<!--footer start here-->
<div class="footer w3ls">
	<div class="container">
		
		<div class="ftr-grid">
			<div class="ftr-banner">
				<h3>All are welcome</h3>
				<h3><%=HtmlUtility.getUTF8Text("सभी का स्वागत है") %></h3>
			</div>
			<div class="clearfix"></div>
		</div>
		
		<div class="footer-main">
			<div class="footer-top">
				<div class="col-md-4 ftr-grid">
					<h3>Our Address</h3>
					<div class="ftr-address">
						<div class="local">
							<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
						</div>
						<div class="ftr-text">
							<p>The Community Library Project<br>B-65, Panchsheel Vihar,<br>Delhi, <br>India</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-4 ftr-grid ftr-social">
<%--					<div style="display:hidden;" id = "ftr-msg"></div>
					<h3>Subscribe to Our Newsletter</h3>
					<form id="newsletter" method="post">
                    	<input type="hidden" name="cs" value="sub" />
						<input type="email" placeholder="Enter Email" name="email" required=""> <input type="button" value="" name="addSubscription">
					</form>--%>
					<%-- 
						<ul class="ftr-social-icons">
							<li><a class="fa" href="#"> </a></li>
							<li><a class="tw" href="#"> </a></li>
								<li><a class="dri" href="#"> </a></li>
								<li><a class="p" href="#"> </a></li>
						</ul>
					--%>
					
					<div style="margin-top: 2%;">
						<div style="color: white;font-weight: bold;">Socialize with us</div>
						<%
							String fbURL = ConfigStore.getStringValue(CacheConfiguration.STR_SOCIAL_FBURL, null);
							if(fbURL != null && !fbURL.trim().isEmpty()) {
					    %>
								<span style="cursor:pointer;"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/social/facebook.png")%>" title="Facebook" height="25" width="25" onclick="javascript: window.open('<%=fbURL%>')"> </span>					
						<% 
							}
						%>
						<%
							String twitterURL = ConfigStore.getStringValue(CacheConfiguration.STR_SOCIAL_TWITTERURL, null);
							if(twitterURL != null && !twitterURL.trim().isEmpty()) {
					    %>
							<span style="cursor:pointer;"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/social/twitter.png")%>" title="Twitter" height="25" width="25" onclick="javascript: window.open('<%=twitterURL%>')"> </span>
						<% 
							}
						%>
						<%
							String instagramURL = ConfigStore.getStringValue(CacheConfiguration.STR_SOCIAL_INSTAGRAMURL, null);
							if(instagramURL != null && !instagramURL.trim().isEmpty()) {
					    %>
							<span style="cursor:pointer;"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/social/instagram.png") %>" title="Instagram" height="25" width="25" onclick="javascript: window.open('<%=instagramURL%>')"> </span>
						<% 
							}
						%>	
						<%
							String youtubeURL = ConfigStore.getStringValue(CacheConfiguration.STR_SOCIAL_YOUTUBEURL, null);
							if(youtubeURL != null && !youtubeURL.trim().isEmpty()) {
					    %>
							<span style="cursor:pointer;"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/social/youtube.png")%>" title="Youtube" height="28" width="28" onclick="javascript: window.open('<%=youtubeURL%>')"> </span>
						<% 
							}
						%>						
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="footer-bottom">
				<div class="col-md-6 ftr-navg">
					<ul>
								<jsp:include page="/WEB-INF/pages/includes/menu_items.jsp"><jsp:param value="true" name="isFooter"/></jsp:include>						
					</ul>
				</div>
				<div class="col-md-4 copyrights">
					<p>
						&copy; 2018 Community Library Project
					</p>
				</div>
 				<div class="clearfix"></div>
				<div class="mid-credits">
					<p>
					 Logo by <a href="http://melangedesign.net" target="_blank">Melange Designs</a>| Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> <%--| Code by: <a href="https://in.linkedin.com/in/reachnishant" target="_blank">Nishant Gupta</a>--%> 
				 	</p>
				 </div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<!--footer end here-->
<%--Google Analytics --%>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99054238-1', 'auto');
  ga('send', 'pageview');

</script>