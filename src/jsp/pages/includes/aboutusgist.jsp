<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.AudienceBean"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
    String content = ConfigStore.getStringValue(CacheConfiguration.STR_ABOUT_GIST, null);
%>

<div id="left-block">
	<div class="content-main" style="height: 40em; overflow:auto">
		<div style="margin: 2% 5%;">
			<%
			    if (content == null) {
			%>
			We are updating the content here.
			<%
			    } else {
			%>
			<%=content%>
			<%
			    }
			%>
		</div>
	</div>
</div>