<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<jsp:include page="/WEB-INF/pages/includes/page_top.jsp" />
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Browser not supported - Zillious</title>
<meta name="description"
	content="Zillious Solution is Asias leading Travel Technology Provider. Empowering over USD 1 Billion worth travel transaction annually.">
<meta name="viewport" content="width=device-width, initial-scale=0.85">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<!-- Icons & favicons -->
<link rel="icon" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/favicon.png") %>">
<!--[if IE]>
        <link rel="shortcut icon" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/favicon.ico") %>">
    <![endif]-->
    <!-- Stylesheet -->
    <link rel="stylesheet" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/browser-min.css") %>">
</head>
<body>
<div id="wrapper">
	<div id="header"> 
				<!-- Logo -->
		<div class="logo" id="logo">
			<!-- image logo -->
			<a href="<%=WebsiteActions.HOME.getActionURL(secureRequest, secureResponse, null, true)%>" class="image-logo"> <img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/logo.png") %>" alt="Zillious Solutions" />
			</a>
		</div>
	</div>
	<div id="content">
		<!-- SITE CONTENT -->
		<center>
				<div id="site-content" class="browser">
			<div><z:message key="corp.browser.text1"><div>Sorry! But your Browser doesn't seem to be on the same page with us. But don't lose heart. You can check out these awesome browsers instead:</div></z:message></div>
			<br />
			<br />
			<div class="icon">
				<a href="http://www.opera.com/" target="_blank" title="<z:message key="corp.browser.operabrowser">Download Opera Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/opera.png") %>"/></a>
			</div>
			<div class="icon">
				<a href="https://www.google.com/chrome/"  target="_blank" title="<z:message key="corp.browser.chromebrowser">Download Chrome Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/chrome.png") %>"/></a>
			</div>
			<div class="icon">
				<a href="https://www.mozilla.org/firefox/new/" target="_blank" title="<z:message key="corp.browser.firefoxbrowser">Download Firefox Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/firefox.png") %>"/></a>
			</div class="icon">
			<div class="icon">
				<a href="https://support.apple.com/downloads/safari"  target="_blank" title="<z:message key="corp.browser.safaribrowser">Download Safari Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/safari.png") %>"/></a>
			</div>
		</div>
		</center>

	
	 </div>
	<div id="footer">
			<div class="bottom-bar">
				<div class="container">
					<div class="footer-wrapper">
						<!-- Copyright Section -->
						<div class="copyright" id="copyright">
							&copy; 2015,
							<z:message key="corp.common.zillious">Zillious Solutions Pvt Ltd (India)</z:message>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
<%--<div class="app-header" id="app-header">
		<div class="container">
			<div class="header-wrapper">
				<!-- Logo -->
				<div class="logo" id="logo">
					<!-- image logo -->
					<a
						href="<%=WebsiteActions.HOME.getActionURL(secureRequest, secureResponse, null, true)%>"
						class="image-logo"> <img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/logo.png"
						alt="Zillious Solutions" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- SITE CONTENT -->
	<div id="site-content" class="browser">
		<div><z:message key="corp.browser.text1">Sorry! But your Browser doesn't seem to be on the same page with us.</z:message></div>
		<br />
		<div><z:message key="corp.browser.text2">But don't lose heart. You can check out these awesome browsers instead:</z:message></div>
		<br />
		<br />
		<div>
			<a href="http://www.opera.com/" target="_blank" title="<z:message key="corp.browser.operabrowser">Download Opera Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/opera.png"/></a>
		</div>
		<div>
			<a href="https://www.google.com/chrome/"  target="_blank" title="<z:message key="corp.browser.chromebrowser">Download Chrome Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/chrome.png"/></a>
		</div>
		<div>
			<a href="https://www.mozilla.org/firefox/new/" target="_blank" title="<z:message key="corp.browser.firefoxbrowser">Download Firefox Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/firefox.png"/></a>
		</div>
		<div>
			<a href="https://support.apple.com/downloads/safari"  target="_blank" title="<z:message key="corp.browser.safaribrowser">Download Safari Browser</z:message>"><img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/icons/safari.png"/></a>
		</div>
	</div>
	<div class="app-footer">
	    <div class="bottom-bar">
	        <div class="container">
	            <div class="footer-wrapper">
	                <!-- Copyright Section -->
	                <div class="copyright" id="copyright">
	                    &copy; 2015, <z:message key="corp.common.zillious">Zillious Solutions Pvt Ltd (India)</z:message>
	                </div>
	            </div>
	        </div>
    </div>
	</div> --%>

	
</body>
</html>
