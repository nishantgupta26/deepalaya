<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Gallery" />
</jsp:include>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@ page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@ page import="org.communitylibrary.ui.beans.SecurityBean"%>
<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<!--content start here-->
	<%--  <div class="content">
		<div class="container">
			<div class="content-main">
				<div style="margin-top:10%">
					<div id="instafeed">We are updating the content here.</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>--%>
	
	<div class="container gallery-container">
			<h1>Gallery</h1>
			<%
				String instagramURL = ConfigStore.getStringValue(CacheConfiguration.STR_SOCIAL_INSTAGRAMURL, null);
				boolean linkInstagram = instagramURL != null && !instagramURL.trim().isEmpty();
			%>	
			<p class="page-description text-center">The pictures here are sourced from our <%if(linkInstagram) { %><a href="<%=instagramURL%>" target="_blank" style="text-decoration: underline;"><%} %>instagram<%if(linkInstagram) { %></a><%} %> feed</p>
			<div class="content-main" id="no-content">
				<p class="page-description text-center">We are updating the content here.</p>
			</div>
			<div class="gallery-main" id="gallery-main" style="display:none;">
				<div id="gallery-content" class="row">
					<%--
						<div class="col-md-4 gallery-grid">
							<div class="gallery-grid-effects">
			                    <div id="nivo-lightbox-demo">      
								<div class="ih-item square effect4 bottom_to_top"><a href="images/g1.jpg"data-lightbox-gallery="gallery1" id="nivo-lightbox-demo">
			                      <div class="img"><img src="images/g1.jpg" alt="img"></div>
				                      <!--<div class="mask1"></div>-->
	    	                 		 <div class="mask2"></div>
				                      <div class="info">
			    	                    <p>Divine Blessings\n. . . . . . . \n#travelgram #divine #meditation #isolation #calm #throwback #throwbackpic #lehdiaries #leh #pangong #morning</p>
				                      </div></a>
			                    </div>
	                   </div>
	                   </div>
					</div>
				 --%>
				</div>
				<div id="gallery-more" class="text-center" style="display:hidden">
					<form>
						<input type="hidden" name="next_max_id" id="next_max_id"/>
						 <input value="Load More..." type="button" class="button">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
	</div>
	<%--content end here--%>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/pages/includes/footer.jsp"></jsp:include>
	<%-- end #site-content --%>
	<%-- SCRIPTS --%>
	<%-- 	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/instafeed.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	<script type="text/javascript">
		var feed = new Instafeed({
    		get: 'user',
    		userId: '321207756',
    		sortBy: 'most-recent',
    		limit: 21,
    		resolution: 'low_resolution',
    		clientId: '25498243631e4c9494aa02df2ff9d9d9',
    		accessToken: '321207756.1677ed0.0ac256905ac4456c85213094ec359ec0'
		});
		feed.run();
	</script>--%>
	<link rel="stylesheet" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/baguetteBox.min.css?ver="+StaticFileVersions.CSS_VERSION) %>" media = "all">
	<link rel="stylesheet" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/thumbnail-gallery.css?ver="+StaticFileVersions.CSS_VERSION) %>" media = "all">
	
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />
	<script type="text/javascript" src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/baguetteBox.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	<script type="text/javascript">
		var galleryObj;
		$(document).ready(function() {
		  galleryObj = new GalleryJS("<%=WebsiteActions.AJAX_GET_GALLERYIMAGES.getActionURL(secureRequest, secureResponse)%>");
		});
	</script>
</body>
<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />