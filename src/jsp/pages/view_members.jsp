<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.data.members.LibraryGroups"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.data.common.Phone"%>
<%@page import="org.communitylibrary.ui.beans.MembersBean"%>
<%@page import="org.communitylibrary.data.members.LibraryMember"%>
<jsp:include page="/WEB-INF/pages/includes/page_top.jsp" />
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean
					.getRequest(request);
			CustomSecurityWrapperResponse secureResponse = SecurityBean
					.getResponse(response);

			List<LibraryMember> members = MembersBean.getLibraryMembersFromReq(secureRequest);
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="View Members" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/pages/includes/header_mainnav.jsp" />

	<%--Manage user page content starts --%>
	<div class="we-do">
		<div class="container">
			<jsp:include flush="false"
				page="/WEB-INF/pages/includes/messages.jsp" />
			<%--Existing users --%>
			<div class="we-do-main">
				<h2>All Members</h2>
				<div id="tableDiv">
				<form id="display_member" method="post"><input type="hidden" name="member_id" /></form>
					<table id="existing_members" style="text-align: center;"border="1">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Gender</th>
								<th>D.O.B.</th>
								<th>Phone Number(s)</th>
								<th>Mother's name</th>
								<th>Father's name</th>
								<th>Address</th>
								<th>Group</th>
								<th>School</th>
								<th>Class</th>
								<th>Photo Id</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<%
							if(members != null) {
							    int i = 0;
								for(LibraryMember member : members) {
							%>
							<tr>
								<td><%=member.getId()%></td>
								<td><%=member.getName()%></td>
								<td><%=member.getGender().name()%></td>
								<td><%=StringUtility.trimWithNullAndEmptyAsUnset(member.getDob()) %></td>
								<td><%=Phone.getPhoneNumberString(member.getPhone())%></td>
								<td><%=StringUtility.trimWithNullAndEmptyAsUnset(member.getMotherName())%></td>
								<td><%=StringUtility.trimWithNullAndEmptyAsUnset(member.getFatherName())%></td>
								<td><%=StringUtility.trimWithNullAndEmptyAsUnset(member.getAddress())%></td>
								<td><%=member.getLibraryGroup() != null ? member.getLibraryGroup().name() : "Unset"%></td>
								<td><%=StringUtility.trimWithNullAndEmptyAsUnset(member.getSchool())%></td>
								<td><%=StringUtility.trimWithNullAndEmptyAsUnset(member.getStudyClass())%></td>
								<td><%=StringUtility.trimAndNullIsEmpty(member.checkAndDisplayPhotoId(request.getSession().getServletContext()))%></td>
								<td>
								<% String action = "action_"+member.getId();  %>
									<form id=<%=action %>>
										<a onclick='view_members.getPrintablePage("<%=member.getId()%>")'>Print</a>
										|
										<a onclick='view_members.editMember("<%=member.getId()%>")'>Edit</a>
										|
										<a onclick='view_members.deleteMember("<%=member.getId()%>")'>Delete</a>
									</form>
								</td>
							</tr>
							<%
							    }
							}
							%>
						</tbody>
					</table>
				</div>
				<div class="clearfix"></div>
			</div>


			<!--Existing users end here-->
		</div>
	</div>


	<%-- Manage user page content ends --%>


	<%-- Footer --%>
	<jsp:include flush="false" page="/WEB-INF/pages/includes/footer.jsp" />

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />

	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/jquery.dataTables.min.js")%>"></script>
	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/dataTables.jqueryui.min.js")%>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery.dataTables.min.css")%>" rel="stylesheet" />

	<%-- Page Specific Scripts --%>
	<script type="text/javascript">
		var view_members;
	    $(document).ready(function(){
	      view_members = new ViewMemberJS($("#display_member"), "<%=WebsiteActions.DISPLAY_MEMBER.getActionURL(secureRequest, secureResponse, null, false)%>", "<%=WebsiteActions.AJAX_GET_MEMBER.getActionURL(secureRequest, secureResponse, null, false)%>");
        });
  </script>
</body>

<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />
