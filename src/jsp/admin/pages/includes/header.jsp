<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.data.user.User"%>
<%@page import="org.communitylibrary.ui.session.SessionStore"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
    User loggedInUser = SessionStore.getLoggedInUser(secureRequest);
%>
<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<!-- Logo -->
				<div class="logo">
					<h1>
						<a href="<%=AdminActions.HOME.getActionURL(secureRequest, secureResponse)%>">Admin Home</a>
					</h1>
				</div>
			</div>
			<%if(loggedInUser != null) { %>
			<div class="col-md-7">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=loggedInUser.getName() %><b class="caret"></b></a>
							<ul class="dropdown-menu animated fadeInUp">
								<li><a href="<%=WebsiteActions.ADD_MEMBERS.getActionURL(secureRequest, secureResponse)%>">Add Members</a></li>
								<li><a href="<%=AdminActions.LOGOUT.getActionURL(secureRequest, secureResponse)%>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	           <%} %>
		</div>
	</div>
</div>