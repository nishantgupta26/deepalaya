<%@page import="org.communitylibrary.ui.beans.MessageBean"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%
CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	String success = MessageBean.getMessage(secureRequest, MessageBean.MessageType.MSG_SUCCESS);
	String error = MessageBean.getMessage(secureRequest, MessageBean.MessageType.MSG_ERROR);
%>
<% if (success != null) { %><center><div class="msg"><%=success%></div></center><br /><% } %>
<% if (error != null) { %><center><div class="msg"><%=error%></div></center><br /><% } %>
