<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><%=request.getParameter("title")%></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<!-- Icons & favicons -->

<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
    CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
<link rel="icon" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/images/favicon.ico") %>">
<!-- Stylesheet -->
<link rel="stylesheet" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/admin/bootstrap.min.css?ver=" + StaticFileVersions.CSS_VERSION)%>" media="all">
<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/admin/styles.css?ver=" + StaticFileVersions.CSS_VERSION)%>" media="all" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
