<%@page import="org.communitylibrary.ui.session.SessionStore"%>
<%@page import="org.communitylibrary.data.user.User"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
	User loggedInUser = SessionStore.getLoggedInUser(secureRequest);
%>
<div class="col-md-2">
	<div class="sidebar content-box" style="display: block;">
		<ul class="nav">
			<!-- Main menu -->
<%-- 			<% if(AdminActions.SETTINGS.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.SETTINGS.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-wrench"></i> Manage Settings</a></li>
			<%} %> --%>
			<% if(AdminActions.MANAGE_USERS.isAllowedForUser(loggedInUser)) { %>
			<li><a href="<%=AdminActions.MANAGE_USERS.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-user"></i> Manage Users</a></li>
			<%} %>
	<%-- 		<% if(AdminActions.EVENTS.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.EVENTS.getActionURL(secureRequest, secureResponse)%>"><i
						class="glyphicon glyphicon-calendar"></i> Manage Events</a></li>
			<%} %>			 --%>
<%-- 			<% if(AdminActions.RESOURCES.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.RESOURCES.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-briefcase"></i> Manage Resources</a></li>
			<%} %> --%>
			<% if(AdminActions.MANAGE_USER_LOGINS.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.MANAGE_USER_LOGINS.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-calendar"></i> User Logins</a></li>
			<%} %>
 			<% if(AdminActions.DONATIONS.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.DONATIONS.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-credit-card"></i>Donations</a></li>
			<%} %>
			 
<%-- 			<% if(AdminActions.HOMEPAGELINKS.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.HOMEPAGELINKS.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-link"></i>Manage Links</a></li>
			<%} %> --%>
			<% if(AdminActions.DASLOTSETTINGS.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.DASLOTSETTINGS.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-link"></i>Manage Slots Settings</a></li>
			<%} %>
			<% if(AdminActions.DASLOTS.isAllowedForUser(loggedInUser)) { %>
				<li><a href="<%=AdminActions.DASLOTS.getActionURL(secureRequest, secureResponse)%>"><i
					class="glyphicon glyphicon-link"></i>Manage Slots</a></li>
			<%} %>
	
<%-- 			<% if(AdminActions.BLOGS.isAllowedForUser(loggedInUser)) { %>
			<li class="submenu">
				<a href="#"> <i
					class="glyphicon glyphicon-list"></i> Manage Blogs <span
					class="caret pull-right"></span>
				</a>
				<ul>
					<li><a href="<%=AdminActions.BLOGS.getActionURL(secureRequest, secureResponse, new String[] {"new"})%>">New Blogpost</a></li>
					<li><a href="<%=AdminActions.BLOGS.getActionURL(secureRequest, secureResponse)%>">All Blogposts</a></li>
				</ul>
			</li>
			<%} %> --%>
		
<%-- 			<% if(AdminActions.PAGES.isAllowedForUser(loggedInUser)) { %>
			<li class="submenu">
				<a href="#"> <i
					class="glyphicon glyphicon-list"></i> Manage Pages <span
					class="caret pull-right"></span>
				</a>
				<ul>
					<li><a href="<%=AdminActions.PAGES.getActionURL(secureRequest, secureResponse, new String[] {"new"})%>">New Custom Page</a></li>
					<li><a href="<%=AdminActions.PAGES.getActionURL(secureRequest, secureResponse)%>">All Pages</a></li>
				</ul>
			</li>
			<%} %> --%>
			
<%-- 			<% if(AdminActions.FORMS.isAllowedForUser(loggedInUser)) { %>
			<li class="submenu">
				<a href="#"> <i
					class="glyphicon glyphicon-list"></i> Manage Forms<span
					class="caret pull-right"></span>
				</a>
				<ul>
					<li><a href="<%=AdminActions.FORMS.getActionURL(secureRequest, secureResponse, new String[] {"new"})%>">New Form</a></li>
					<li><a href="<%=AdminActions.FORMS.getActionURL(secureRequest, secureResponse)%>">All Forms</a></li>
					<li><a href="<%=AdminActions.FORMS.getActionURL(secureRequest, secureResponse, new String[] {"responses"})%>">Form Responses</a></li>
				</ul>
			</li>
			<%} %> --%>
		</ul>
	</div>
</div>