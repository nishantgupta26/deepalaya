<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean" %>
<%
	CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>

<%-- jQuery --%>
<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/jquery/jquery-1.11.0.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/AppWrapper.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
<script type="text/javascript">
var ua = window.navigator.userAgent;    
var msie = ua.indexOf("MSIE ");
if (msie > 0) {
  	window.location.href = "<%=WebsiteActions.BROWSER_UNSUPPORTED.getActionURL(secureRequest, secureResponse, null, true)%>";
}

$(document).ready(function(){


  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });
  
});

</script>

<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/bootstrap.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/custom.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>