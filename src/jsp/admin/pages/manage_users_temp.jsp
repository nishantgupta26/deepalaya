<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.data.user.UserRoles"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.data.user.User"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
		<%-- Right side page content--%>
		<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				String jsonString = "{\"userroles\" : [";
				//{"id":"A", "name":"Admin"},{"id":"E", "name":"Employee"}]}
				
				List<User> users = WebsiteBean.getUsersFromReq(request);
		
		%>
			<%--Right panel content starts here --%>
			<div class="col-md-10">
				<%--right panel table content-box-large starts here --%>
				<div class="content-box-large">
		  				<div class="panel-heading">
							<div class="panel-title">Existing Users</div>
						</div>
						<div class="panel-body">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="existing_users">
								<thead>
									<tr>
										<th>ID</th>
										<th>Role</th>
										<th>Name</th>
										<th>Enabled</th>
										<th>Email</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<%
						    			int i = 0;
									    for(User user : users) {
									%>
											<tr id="tr_<%=++i%>">
												<td><%=user.getUserId()%></td>
												<td><%=user.getUserRole().getDisplayName()%></td>
												<td><%=user.getName()%></td>
												<td><%=user.isEnabled() ? "Enabled" : "Disabled"%></td>
												<td><%=user.getEmail()%></td>
												<td>
													<form>
														<a onclick='manage_users.editUser($("#tr_<%=i%>"))' >Edit User</a>
													</form>
												</td>
											</tr>
									<%
						    			}
									%>
								</tbody>
							</table>
						</div>
				</div>
				<%-- Finished right table block --%>
				<%--Add/Edit new user content form starts here --%>
				<div class="row form-div">
					<div class="col-md-6 content-box-large">
						<div class="content-box-header">
		  					<div class="panel-title">Add/Modify Users</div>
			  			</div>
			  			<div class="panel-body">
			  				<form class="form-horizontal" role="form" id="userForm" method="post">
			  					<div class="form-group">
								    <label for="role" class="col-sm-2 control-label">Role</label>
								    <div class="col-sm-4">
						     	 		<select name="role">
										<%
										    UserRoles[] roles = UserRoles.getLoggedInRoles();
											for (int roleCount = 0; roleCount < roles.length; roleCount++) {
												UserRoles role = roles[roleCount];						    			    
							    				jsonString += "{\"id\":\"" +role.serialize() + "\" , \"name\":\"" + role.getDisplayName() + "\"}" + (roleCount < (roles.length - 1) ? "," : "");
										%>
											<option value="<%=role.serialize()%>"><%=role.getDisplayName()%></option>
										<%
										    } 
							    			jsonString += "]}";
										%>
								  		</select>
								    </div>
								    <div class="col-sm-offset-2 col-sm-4" style="display:inline-block;">
								      <div class="checkbox">
								        <label>
								          <input type="checkbox" name="enabled" checked="checked">Enabled?
								        </label>
								      </div>
								    </div>
							  	</div>
							  	<div class="form-group">
								    <label for="user_id" class="col-sm-2 control-label">User ID</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="user_id" name="user_id" placeholder="" value="" size="30" maxlength="99">
								    </div>
							  	</div>
							  	<div class="form-group">
								    <label for="name" class="col-sm-2 control-label">Name</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="" size="50" maxlength="99">
								    </div>
							  	</div>
							  	<div class="form-group">
								    <label for="email" class="col-sm-2 control-label">E-mail Id</label>
								    <div class="col-sm-10">
								      <input type="email" class="form-control" id="email" name="email" placeholder="Enter E-mail Id" value="" size="50" maxlength="99">
								    </div>
							  	</div>
							  	<div class="form-group">
									<label for="password" class="col-sm-2 control-label">Password</label>
							    	<div class="col-sm-10">
					    				<div class="col-sm-offset-2 col-sm-6" id="password_div" style="display:none;">
								    		<div class="checkbox">
								        		<label>
								          			<input type="checkbox" name="update_password">Update Password?
								        		</label>
								      		</div>
								    	</div>
								    	<div>
								      		<input type="password" class="form-control" id="password" name="password" placeholder="Password">
								    	</div>
							    	</div>
						  		</div>
								<div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
										<button type="button" name="modButton" onclick="manage_users.modifyUser()" class="btn btn-primary">Add New User</button>
								    </div>
							  	</div>
			  				</form>
			  			</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />
	
	<%-- Page Specific Scripts --%>
	
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/jquery.dataTables.min.js") %>"></script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/dataTables.jqueryui.min.js") %>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery.dataTables.min.css") %>" rel="stylesheet" />
	
	<script type="text/javascript">
		var manage_users;
	    $(document).ready(function(){
	      manage_users = new ManageUsersJS($("#userForm"),"<%=AdminActions.MANAGE_USERS.getActionURL(secureRequest, secureResponse, new String[] {"modify"}, false)%>", <%=jsonString%>);
        });
  </script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />