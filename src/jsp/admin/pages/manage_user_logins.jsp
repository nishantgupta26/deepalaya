<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.utils.JSPUtility"%>
<%@page import="org.communitylibrary.data.user.dto.UserLoginDTO"%>
<%@page import="java.util.Map"%>
<%@page import="org.communitylibrary.data.user.UserRoles"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.data.user.User"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
		<%-- Right side page content--%>
		<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				
				Map<User, UserLoginDTO> userLogins = WebsiteBean.getUserLoginsFromReq(request);
		
		%>
			<%--Right panel content starts here --%>
			<div class="col-md-10">
				<%--right panel table content-box-large starts here --%>
				<div class="content-box-large">
		  				<div class="panel-heading">
							<div class="panel-title">Consolidated Info</div>
						</div>
						<div class="panel-body">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="existing_users">
								<thead>
									<tr>
										<th>ID</th>
										<th>Role</th>
										<th>Name</th>
										<th>Email</th>
										<th>Number of Logins</th>
										<th>Last Login</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<%
						    			int i = 0;
									    for(User user : userLogins.keySet()) {
									        UserLoginDTO loginDTO = userLogins.get(user);
									%>
											<tr id="tr_<%=++i%>">
												<td><%=user.getUserId()%></td>
												<td><%=user.getUserRole().getDisplayName()%></td>
												<td><%=user.getName()%></td>
												<td><%=JSPUtility.writeToJspDefaultEmpty(user.getEmail())%></td>
												<td><%=JSPUtility.writeToJspDefaultEmpty(loginDTO.getNumLogins()) %></td>
												<td><%=JSPUtility.writeToJspDefaultEmpty(loginDTO.getLastLogin()) %></td>
												<td>
													<form>
														<a onclick='user_login.showDetailsDiv(<%=user.getUserId()%>);' >Get Login Info</a>
													</form>
												</td>
											</tr>
									<%
						    			}
									%>
								</tbody>
							</table>
						</div>
				</div>
				<%-- Finished right table block --%>
				<%--Add/Edit new user content form starts here --%>
				<div class="row form-div" id="getLoginInfoDTO" style="display:none;">
					<div class="col-md-6 content-box-large">
						<div class="content-box-header">
		  					<div class="panel-title">Specific Login Information</div>
			  			</div>
			  			<div class="panel-body">
			  				<form class="form-horizontal" role="form" id="userLoginForm" method="post">
							  	<div class="form-group">
								    <label for="user_id_div" class="col-sm-2 control-label">User ID</label>
								    <div class="col-sm-10">
							    		<div id="user_id_div"></div>
							      		<input type="hidden" class="form-control" id="user_id" name="user_id" />
								    </div>
							  	</div>
							  	<div class="form-group">
								    <label for="date" class="col-sm-2 control-label">Pick a Date</label>
							        <div class='input-group date' id='datetimepicker1'>
          								<input type='text' class="form-control" name="date" id="date" />
       									<span class="input-group-addon">
       										<span class="glyphicon glyphicon-calendar"></span>
										</span>
        							</div>
      							</div>
								<div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
										<button type="button" name="getDetailsButton" class="btn btn-primary">Get Details</button>
										<button type="button" name="hideButton" class="btn btn-primary">Hide</button>
								    </div>
							  	</div>
							  	<div  class="form-group"  >
								    <label class="col-sm-2 control-label">Date</label>
								    <div class="col-sm-10" id="date_div"></div>
							  	</div>
							  	<div class="form-group"  >
   								    <label class="col-sm-2 control-label">Number of Logins</label>
								    <div class="col-sm-10" id="num_div"></div>
							  	</div>
			  				</form>
			  			</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />
	
	<%-- Page Specific Scripts --%>
	
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/jquery.dataTables.min.js") %>"></script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/dataTables.jqueryui.min.js") %>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery.dataTables.min.css") %>" rel="stylesheet" />
	
     <!-- bootstrap-datetimepicker -->
     <script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/moment-with-locales.min.js") %>"></script>
     <link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/admin/bootstrap-datetimepicker.min.css") %>" rel="stylesheet">
     <script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/bootstrap-datetimepicker.min.js") %>"></script>
	
	<script type="text/javascript">
		var user_login;
	    $(document).ready(function(){
	      user_login = new UserLoginJS($("#userLoginForm"), "<%= AdminActions.MANAGE_USER_LOGINS.getActionURL(secureRequest, secureResponse, new String[] {"getSpecificLogin"})%>");
        });
  </script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />