<%@page import="org.communitylibrary.app.resources.ResourceType"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.app.resources.ContentType"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
			%>
				<%--Main banner content row begins --%>
				<div class="col-md-10">
				<div class="row">
						<h4>Campaign Information</h4>
							<form id="banner-content-form">
							<table class="table table-bordered table-striped" style="clear: both">
								<tbody>
									<tr>
										<td width="25%">Type</td>
										<td width="75%">
											<select name="type" class="form-control">
												<% for(ResourceType type : ResourceType.values()) { %>
													<option value="<%=type.getCode()%>"><%=type.name() %></option>
												<%} %>
											</select>
										</td>
									</tr>
									<tr>
										<td width="25%">URL of the campaign</td>
										<td width="75%"><input type="text" name="urlLink" class="form-control"/></td>
									</tr>
									<tr>
										<td width="25%">Image URL(if Any)</td>
										<td width="75%"><input type="text" name="imageUrl" class="form-control"/></td>
									</tr>
									<tr>
										<td width="25%">Text Content(If Any)</td>
										<td width="75%"><textarea name="textContent"></textarea></td>
									</tr>
								</tbody>
							</table>
							<div class="col-sm-offset-2 col-sm-10">
								<button type="button" class="btn btn-primary">Add Info</button>
							</div>
							</form>
				</div>
				<div class="row">
				<div class="col-md-12">
					<h4>Banner Information added</h4>
					<table class="table table-striped" id="banner-content-disp">
						<thead>
							<tr>
								<th>#</th>
								<th>Type</th>
								<th>URL Link</th>
								<th>image url</th>
								<th>text content</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div>
				</div>
				<%--Main banner content row ends here --%>
			</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/ckeditor.js") %>"></script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/adapters/jquery.js") %>"></script>
	<script type="text/javascript">
		var manage_banner;
	    $(document).ready(function(){
	    	manage_banner = new ManageBannerJS($("#banner-content-form"), $("#banner-content-disp"), "<%=AdminActions.RESOURCES.getActionURL(secureRequest, secureResponse, null, false)%>", "<%=AdminActions.CDNUpload.getActionURL(secureRequest, secureResponse)%>");
        });
  </script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />