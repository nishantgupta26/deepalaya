<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
			%>
			<div class="col-md-10">
			<%--Shortcut row begins --%>
				<div class="row">
					<div class="col-md-6" id="shortcut_block">
						<h4>Shortcuts to Editors</h4>
						<p>
							<table class="table table-bordered table-striped" style="clear: both">
								<tbody>
								<%
									for(CacheConfiguration cacheConfig : CacheConfiguration.values()) {
								%>
									<tr>
										<td width="35%"><%=cacheConfig.name() %></td>
										<td width="65%"><a href="#<%=cacheConfig.name()%>" class="editable editable-click"><%=cacheConfig.getDesc() %></a></td>
									</tr>
								<%} %>
								</tbody>
							</table>
						</p>
					</div>
				</div>
				<%--Shortcut Row ends here --%>
				<%--Actual Content Row begins here --%>
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="content-box-header panel-heading">
	  						<div class="panel-title ">Content Panel</div>
			  			</div>
			  			<div class="content-box-large box-with-header">
				  			<div>
					  			<%
					  				for(CacheConfiguration cacheConfig : CacheConfiguration.values()) {
					  				    ConfigType configType = cacheConfig.getType();
					  				    ElementType elemType = cacheConfig.getElem();
					  			%>
				  				<form method="post" action="<%=AdminActions.SETTINGS.getActionURL(secureRequest, secureResponse, new String[] {"update"}, false)%>">
					  				<div class="row">
					  					<div class="content-box-large" id="<%=cacheConfig.name()%>">
					  						<div class="panel-heading">
					  							<div class="panel-title"><h2><%=cacheConfig.getDesc() %></h2><input type="hidden" name="cfg_id" value="<%=cacheConfig.name()%>"></input></div>
					  						</div>
										<div class="panel-body">
										<%
											switch(configType) {
											case BOOLEAN:
									   	%>
												<input type="checkbox" class="form-control" name="cfg_val" <% if(ConfigStore.getBooleanValue(cacheConfig, false)) { %>checked<%} %>/>
										<%
											break;
											case STRING:
											    switch(elemType) {
											    case TEXTAREA:
								        %>
								        			<textarea name="cfg_val" cols="80" rows="10" id="<%=cacheConfig.name()%>"><%=ConfigStore.getStringValue(cacheConfig, "") %></textarea>
										<%
												break;
										        default:
							            %>
													<input type="text" class="form-control" placeholder="<%=cacheConfig.getDesc()%>" name="cfg_val" value="<%=ConfigStore.getStringValue(cacheConfig, "")%>" />
										<%
												}
											    break;
											case INT:
										%>	    
													<input type="text" class="form-control" placeholder="<%=cacheConfig.getDesc()%>" name="cfg_val" value="<%= StringUtility.stringify(ConfigStore.getIntValue(cacheConfig, null))%>" />
										<%
												break;
											default:
										%>
													<input type="text" class="form-control" placeholder="<%=cacheConfig.getDesc()%>" name="cfg_val" value="<%=ConfigStore.getStringValue(cacheConfig, "")%>" />
										<%
											}
										%>
											<div class="form-group">
											    <div class="col-sm-offset-2 col-sm-10">
													<button type="submit" name="modButton" class="btn btn-primary">Update Settings</button>
													<a href="#shortcut_block" class="editable editable-click">Back To Top</a>
										    	</div>
							  				</div>
										</div>
									</div>
									</div>
									<hr>
					  			</form>
					  			<%
					  				}
					  			%>
				  			</div>
			  			</div>
					</div>
				</div>
				<%--Actual content row ends here --%>
			</div>
		
		</div>
		
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/ckeditor.js") %>"></script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/adapters/jquery.js") %>"></script>
	<script type="text/javascript">
		var manage_settings;
	    $(document).ready(function(){
	      manage_settings = new ManageSettingsJS("<%=AdminActions.CDNUpload.getActionURL(secureRequest, secureResponse)%>");
        });
  </script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />