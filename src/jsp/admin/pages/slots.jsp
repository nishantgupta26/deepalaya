<%@page import="org.communitylibrary.data.LibraryBranch"%>
<%@page import="org.communitylibrary.data.digitalaccess.SlotStatus"%>
<%@page import="org.communitylibrary.data.digitalaccess.Slot"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Manage slots" />
</jsp:include>

<body>
<style type="text/css">
.fc-agenda-allday {
    display: none;
}

/* highlight results */
.ui-autocomplete span.hl_results {
    background-color: #ffff66;
}
 
/* loading - the AJAX indicator */
.ui-autocomplete-loading {
    background: white url('../img/ui-anim_basic_16x16.gif') right center no-repeat;
}
 
/* scroll results */
.ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
    /* add padding for vertical scrollbar */
    padding-right: 5px;
}
 
.ui-autocomplete li {
    font-size: 16px;
}
 
/* IE 6 doesn't support max-height
* we use height instead, but this forces the menu to always be this tall
*/
* html .ui-autocomplete {
    height: 250px;
}
</style>

	<%-- Header --%>
	<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
<%
	CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
	CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
	List<Slot> slots = AdminBean.getSlotsFromRequest(secureRequest);
	String slotsAsJson = AdminBean.getSlotsJson(slots); 
	List<LibraryBranch> libraryBranches = AdminBean.getLibraryBranches(secureRequest);
	List<String> slotTypes = AdminBean.getSlotTypes(secureRequest);
%>
			<div class="col-md-10" ng-app="App">
				<div class="content-box-large box-with-header">
					<div class="panel-body">
						<div id="library-branch-container" class="row" style="margin-bottom: 1%">
							<select name="library-branch" id="library-branch" class="form-control">
								<% for(LibraryBranch branch : libraryBranches) { %>
									<option value="<%=branch.getCode()%>"><%= branch.getName() %></option>
								<%} %>
							</select>
						</div>
						<div class="rootwizard">
							<div class="navbar">
								<div class=" navbar-inner">
									<ul class="nav nav-tabs">
										<li class="">
											<a href="#slotlist" target="_self " data-toggle="tab">Current Slots</a>
										</li>
										<li class="active">
											<a href="#slotcalendar" target="_self " data-toggle="tab">Add Slots</a>
										</li>
										<!-- <li class="">
											<a href="#manageusers" target="_self " data-toggle="tab">Manage Users</a>
										</li> -->
									</ul>
								</div>
							</div>
							<div class="tab-content">
								<div class="tab-pane" id="slotlist" ng-controller="SlotsController">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-6">
												<div class="input-group">
													<input type="text" class="form-control" ng-click="open()" uib-datepicker-popup ng-model="dt" is-open="isopened" datepicker-options="options" close-text="Close" />
												 	<span class="input-group-btn">
											        	<button type="button" class="btn btn-default" ng-click="open()"><i class="glyphicon glyphicon-calendar"></i></button>
											       	</span>
												</div>
												<!-- <div uib-datepicker-popup is-open="isopened" ng-model="dt" class="well well-sm" datepicker-options="options"></div> -->
											</div>
											<div class="col-md-6"><button type="button" class="btn btn-sm btn-default" ng-click="getSlots()">Get Slots</button></div>
										</div>
										<div class="content-box-large">
											<div class="panel-heading">
												<div class="panel-title">Slots for {{selectedDate}}</div>
											</div>
											<div class="panel-body">
												<div class="table-responsive" ng-show = "slots.length > 0">
													<table class="table">
														<thead>
															<tr>
																<th>S.No</th>
																<th>Member</th>
																<th>Member Id</th>
																<th>Start Time</th>
																<th>End Time</th>
																<th>Slot Type</th>
																<th>Notes</th>
																<th>Action</th>
															</tr>
														</thead>
														<tbody>
															<tr ng-repeat="slot in slots" ng-style="set_color(slot)">
																<td>{{$index+1}}</td>
																<td>{{slot.title}}</td>
																<td>{{slot.memberid}}</td>
																<td>{{slot.starttime}}</td>
																<td>{{slot.endtime}}</td>
																<td>{{slot.slotType}}</td>
																<td>{{slot.notes}}</td>
																<td>
																	<div ng-if="slot.status=='A'">Attended</div>
																	<span ng-if="slot.status != 'A'">
																		<button class="btn btn-sm btn-primary" title="Mark slot as attended" ng-click="markAsAttended(slot)">Mark as atten</button>
																		<button class="btn btn-sm btn-danger" title="Delete Slot" ng-click="deleteSlot(slot)">Delete</button>
																	</span>
																	<!-- <button ng-disabled="slot.status === 'A'" class="btn btn-sm btn-danger" ng-click="deleteSlot(slot)">Delete</button> -->
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div ng-show="!slots || slots.length == 0" class="container" style="margin-top: 2%">No slots found for {{selectedDate}}</div>
											</div>
										</div>
										
										
									</div>
								</div>
								<div class="tab-pane active" id="slotcalendar">
									<!-- Space for actual week-view calendar  -->
									<div class="col-md-12">
										<div id="slottimingcalendar"></div>
									</div>
								</div>
							</div>
						</div>		
					</div>
				</div>
				<div id='slotContent' class="row" title="Add a slot"
					style="display:none;">

					<div class="col-md-12">
						<div class="content-box">
							<div class="panel-body">
								<form class="form-horizontal" role="form"
									id="add-slot-form">
									<div class="form-group">
										<label for="title" class="col-sm-2 control-label">Member</label>
										<div class="col-sm-10">
											<!-- <select id="member" class="form-control col-sm-12"></select> -->
											<input type="text" class="form-control" id="member" name="member"
												 placeholder="Member" />
											<input type="hidden" id="memberid"
												name="memberid" />
										</div>
									</div>
									<div class="form-group">
										<label for="date" class="col-sm-2 control-label">Date</label>
										<div class="col-sm-10">
											<div class="form-control" id="date"></div>
											<input type="hidden" name="date" />
										</div>
									</div>
									<div class="form-group">
										<label for="time" class="col-sm-2 control-label">Start Time</label>
										<div class="col-sm-10">
											<div class="form-control" id="starttime"></div>
											<input type="hidden" name="starttime" />
										</div>
									</div>
									<div class="form-group">
										<label for="time" class="col-sm-2 control-label">End Time</label>
										<div class="col-sm-5">
											<div class="form-control" id="endtime"></div>
											<input type="hidden" name="endtime" />
										</div>
										<div class="col-sm-4 input-group" title="Duration">
											<div class="form-control" id="duration"></div>
				    	    				<input type="hidden" class="form-control" name="duration" />
						    			</div>
									</div>
									<div class="form-group">
										<label for="slotType" class="col-sm-2 control-label">Slot Type</label>
										<div class="col-sm-10">
											<select class="form-control" id="slotType" name="slotType">
												<% for(String slotType : slotTypes) { %>
													<option value="<%=slotType%>"><%=slotType %></option>
												<%} %>
											</select>
										</div>
									</div>
									<div class="form-group">
									    <label for="notes" class="col-sm-2 control-label">Notes</label>
									    <div class="col-sm-10">
							    	    	<textarea class="form-control" placeholder="Notes" id="notes" name="notes" rows="3"></textarea>
									    </div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />
	
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular-animate.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/ui-bootstrap-tpls-2.5.0.min.js?ver=11112017")%>"></script>
	
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/vendors/moment/moment.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery-ui.min.js") %>"></script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/jquery/jquery.ui.autocomplete.html.js") %>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery-ui.min.css") %>" rel="stylesheet" />
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/fullcalendar.js") %>"></script>
    <link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/fullcalendar.css") %>" rel="stylesheet" media="screen">

	<script type="text/javascript">
		var slotURL = "<%=AdminActions.DASLOTS.getActionURL(secureRequest, secureResponse)%>";
		var ajaxSearchMemberURL = "<%=AdminActions.AJAX_GET_MEMBER.getActionURL(secureRequest, secureResponse)%>";
		var manageSlots;
	    $(document).ready(function(){
    	  <%-- manageSlots = new ManageSlotJS(slotURL, <%=slotsAsJson%>, ajaxSearchMemberURL); --%>
    	  manageSlots = new ManageSlotJS(slotURL, $("#library-branch"), <%=slotsAsJson%>,  ajaxSearchMemberURL);
      	});
  	</script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/slots.js?ver=20112018") %>"></script>
  	<%-- <script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/angular/slot.js?ver=15052018") %>"></script> --%>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />