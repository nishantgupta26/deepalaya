<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.admin.entity.page.PageResource"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false"
				page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				PageResource post = AdminBean.getPage(secureRequest);
			%>
			<div class="col-md-10">
				<!-- New blog post section begins -->
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="content-box-header panel-heading">
							<div class="panel-title ">Page Id: <% if(post == null)  {%>New Page<%} else { %><%=post.getId()%><%} %></div>
						</div>
						<div class="content-box-large box-with-header">
							<div>
								<form name="page-form" id="page-form" method = "post">
								<input type="hidden" name="id" <%if(post != null) { %>value="<%=post.getId()%>"<%} %>/>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control" placeholder="Title, should be added in page content as well." name="title" <%if(post != null) { %>value="<%=post.getTitle()%>"<%} %>/>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-6">
											<span class="col-sm-2">URL: </span>
											<div class="col-sm-8">
												<input type="text" class="form-control"
													placeholder="url of the page" name="url" <%if(post != null) { %>value="<%=StringUtility.trimAndNullIsEmpty(post.getUrl())%>"<%} %>/>
											</div>
											<div class="col-sm-2">
												<input type="checkbox" id="enter-manual" title="Enter url manually" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="col-sm-2">Generated URL: </div>
											<div class="col-sm-10" id="generated-url"><%if(post != null) { %><%=WebsiteActions.PAGE.getActionURL(secureRequest, secureResponse, new String[] {post.getUrl()}) %> <% } %>
											</div>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-6 col-md-6 col-lg-6">
											<button type="button"
												class="btn btn-md btn-block btn-default" name="preview" id="preview">Preview</button>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-12">
											<textarea class="form-control"
												style="width: 100%; height: 100%" name="textdata" id="textdata"><%if(post != null) { %><%=post.getPageText()%><%} %></textarea>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-12">
											<button type="button"
												class="btn btn-lg btn-block btn-primary" name="publish-now"><%if(post == null) { %>Add Now<%} else { %>Edit Now<%}%></button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- New page section ends -->

			</div>

		</div>

	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/ckeditor.js") %>"></script>
	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/adapters/jquery.js") %>"></script>

	<script type="text/javascript">
		var newPage;
		$(document).ready(function() {
			newPage = new AddPage($("#page-form"), "<%=AdminActions.PAGES.getActionURL(secureRequest, secureResponse, new String[] {"new"})%>", "<%=WebsiteActions.PAGE.getActionURL(secureRequest, secureResponse)%>", "<%=AdminActions.CDNUpload.getActionURL(secureRequest, secureResponse)%>");
			
			$("#preview").click(function() {
				var content = CKEDITOR.instances['textdata'].getData();
				params  = 'width='+screen.width;
				 params += ', height='+screen.height;
				 params += ', top=0, left=0'
				 params += ', fullscreen=no';
		        var popupWin = window.open('', '_blank', params);
		        popupWin.document.open();
		        var css1 = "<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/plugins/plugins-min.css?ver=" +StaticFileVersions.CSS_VERSION) %>";
		        var css2 = "<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/app-min.css?ver=" +StaticFileVersions.CSS_VERSION) %>";
		        popupWin.document.write('<html><head><title>Preview blogpost</title><link rel="stylesheet" type="text/css" href="' + css1 + '" media="screen" /><link rel="stylesheet" type="text/css" href="' + css2 + '" media="screen" /></head><body>');
		        var html = [];
		        var i = 0;
		        html[i++] = "<div class=\"content\"  id=\"content\"><div class=\"container\">";
		        html[i++] = "<div class=\"content-main\">";
		        html[i++] = "<div class=\"content-block1\">";
		        html[i++] = content
		        html[i++] = "</div>";
		        html[i++] = "</div>";
		        html[i++] = "</div>";
		        var toPrint = html.join("");
		        popupWin.document.write(toPrint);
		        popupWin.document.write('</body></html>');
		        popupWin.focus();
			});
		});
	</script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />