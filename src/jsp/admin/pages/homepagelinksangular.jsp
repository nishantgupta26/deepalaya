<%@page import="org.communitylibrary.utils.HtmlUtility"%>
<%@page import="org.communitylibrary.data.members.LibraryGroupForDigitalAccess"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.admin.entity.forms.Form"%>
<%@page import="org.communitylibrary.utils.DateUtility"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>
<body>
	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content" ng-app="App">
		<div class="row" ng-controller="HPLinksController">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
			    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				String availableLinks = AdminBean.getHomePageLinks(secureRequest);
			%>
			<div class="col-md-10">
				<!-- New blog post section begins -->
				<div class="row">
					<div class="col-md-12 panel-default">
						<div class="content-box-header panel-heading">
							<div class="panel-title">HomePage Links : {{availableLinks.length}}</div>
						</div>
						<div class="content-box-large box-with-header">
							<div class="table-responsive">
		  						<table class="table">
					              <thead>
					                <tr>
						                <th>S.No.</th>
										<th>Name</th>
										<th>Description</th>
										<th>Count</th>
										<th title="This will appear on the hindi panel">Name(Hindi)</th>
										<th title="This will appear on the hindi panel">Description(Hindi)</th>
										<th>Count</th>
										<th>Link</th>
										<th>Actions</th>
					                </tr>
					              </thead>
					              <tbody>
					                <tr ng-repeat="link in availableLinks">
										<td>{{$index + 1}}</td>
										<td>{{link.name}}</td>
										<td>{{link.desc}}</td>
										<td>{{link.englishclicks}}</td>
										<td>
											<span ng-if="link.hindiname" ng-bind-html="link.hindiname | unsafe"></span>
											<span ng-if="!link.hindiname"><input class="form-control" type="text" ng-model="link.newhindiname" placeholder="Name(Hindi)"/></span>
										</td>
										<td>
											<span ng-if="link.hindidesc" ng-bind-html="link.hindidesc | unsafe"></span>
											<span ng-if="!link.hindidesc"><textarea placeholder="Description(Hindi)" ng-model="link.newhindidesc" ></textarea></span>
										</td>
										<td>{{link.hindiclicks}}</td>
										<td>{{link.url}}</td>
										<td>{{link.group}}</td>
										<td>
											<span ng-if="!link.hindiname || !link.hindidesc" title="update hindi information">
												<button class="btn-sm btn-primary" ng-click="updateLink(link)"><i class="glyphicon glyphicon-pencil"></i>Edit</button>
											</span>
											<span>
												<button class="btn-sm btn-danger" ng-click="deleteLink(link)"><i class="glyphicon glyphicon-cross"></i>Delete</button>
											</span>
										</td>
									</tr>
									<tr>
										<td></td>
										<td><input class="form-control" type="text" ng-model="newlink.name" placeholder="name"/></td>
										<td><textarea placeholder="description" ng-model="newlink.desc" ></textarea></td>
										<td><input class="form-control" type="text" ng-model="newlink.hindiname" placeholder="Name(Hindi)"/></td>
										<td><textarea placeholder="Description(Hindi)" ng-model="newlink.hindidesc" ></textarea></td>
										<td><input class="form-control" type="text" ng-model="newlink.url" placeholder="URL"/></td>
										<td>
											<select class="form-control" ng-model="newlink.group">
												<% for(LibraryGroupForDigitalAccess group : LibraryGroupForDigitalAccess.values()) { %>
													<option value="<%=group.name()%>"><%=group.name() %></option>
												<%} %>
											</select>
										</td>
										<td>
											<button class="btn-sm btn-success" ng-click="addLink()"><i class="glyphicon glyphicon-plus"></i>Add</button>
										</td>
									</tr>
					              </tbody>
					            </table>
  							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular.min.js?ver=11112017")%>"></script>

	<script type="text/javascript">
		<%-- var availableLinks = <%=availableLinks%> ; --%>
		var formURL = "<%=AdminActions.HOMEPAGELINKS.getActionURL(secureRequest, secureResponse)%>";
  	</script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/angular/homepagelinks.js?ver=28122017")%>"></script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />