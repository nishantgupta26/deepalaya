<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.admin.entity.blog.Blogpost"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false"
				page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				Blogpost post = AdminBean.getBlogpost(secureRequest);
			%>
			<div class="col-md-10">
				<!-- New blog post section begins -->
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="content-box-header panel-heading">
							<div class="panel-title ">Post Id: <% if(post == null)  {%>New Post<%} else { %><%=post.getId()%><%} %></div>
						</div>
						<div class="content-box-large box-with-header">
							<div>
								<form name="blogpost-form" id="blogpost-form" method = "post">
								<input type="hidden" name="id" <%if(post != null) { %>value="<%=post.getId()%>"<%} %>/>
									<div class="row">
										<div class="col-sm-12">
											<input id="title" type="text" class="form-control" placeholder="Title" name="title" <%if(post != null) { %>value="<%=post.getTitle()%>"<%} %>/>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-6">
											<input type="text" class="form-control"
												placeholder="Author of the post" name="author" id="author" <%if(post != null) { %>value="<%=post.getAuthor()%>"<%} %> />
										</div>
										<div class="col-sm-6">
											<div class="col-sm-10">
												<input type="text" class="form-control"
													placeholder="url of the post" name="url" <%if(post != null) { %>value="<%=StringUtility.trimAndNullIsEmpty(post.getUrl())%>"<%} %>/>
											</div>
											<div class="col-sm-2">
												<input type="checkbox" id="enter-manual" title="Enter url manually" />
											</div>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-6 col-md-6 col-lg-6">
											<button type="button"
												class="btn btn-md btn-block btn-default" name="preview" id="preview">Preview</button>
										</div>
										<div class="col-sm-6 col-md-6 col-lg-6">
											<button type="button"
												class="btn btn-md btn-block btn-danger" name="discard" id="discard">Discard Changes</button>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-12">
											<textarea class="form-control"
												style="width: 100%; height: 100%" name="textdata" id="textdata"><%if(post != null) { %><%=post.getPostText()%><%} %></textarea>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-12">
											<button type="button"
												class="btn btn-lg btn-block btn-primary" name="publish-now"><%if(post == null) { %>Publish Now<%} else { %>Edit Now<%}%></button>
										</div>
									</div>
									<%if(post == null) {%>
									
									<hr>
									<div class="row">

										<div class="input-group date col-sm-4 pull-left"
											id="datetimepicker1" style="">
											<input type="text" class="form-control" name="date" id="date">
											<span class="input-group-addon"> <span
												class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
										<div class="col-sm-6">
											<button type="button"
												class="btn btn-lg btn-block btn-default"
												name="publish-later">Publish Later</button>
										</div>
									</div>
									<% } %>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- New blog post section ends -->

			</div>

		</div>

	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/ckeditor.js") %>"></script>
	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/adapters/jquery.js") %>"></script>

	<!-- bootstrap-datetimepicker -->
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/moment-with-locales.min.js") %>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/admin/bootstrap-datetimepicker.min.css") %>"
		rel="stylesheet" />
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/bootstrap-datetimepicker.min.js") %>"></script>
	
	<script type="text/javascript">
		var blog;
		$(document).ready(function() {
			blog = new AddBlog(
			    $("#blogpost-form"),
			    "<%=AdminActions.BLOGS.getActionURL(secureRequest, secureResponse, new String[] {"new"})%>",
			    "<%=AdminActions.CDNUpload.getActionURL(secureRequest, secureResponse)%>"
			    );
			
			$("#discard").click(function() {
			  window.location.href = "<%=AdminActions.BLOGS.getActionURL(secureRequest, secureResponse)%>";
			});
			
			$("#preview").click(function() {
				var content = CKEDITOR.instances['textdata'].getData();
				var title = $("#title").val();
				var author = $("#author").val();
				params  = 'width='+screen.width;
				 params += ', height='+screen.height;
				 params += ', top=0, left=0'
				 params += ', fullscreen=no';
		        var popupWin = window.open('', '_blank', params);
		        popupWin.document.open();
		        var css1 = "<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/plugins/plugins-min.css?ver=" +StaticFileVersions.CSS_VERSION) %>";
		        var css2 = "<%=UIBean.getStaticURL(secureRequest, secureResponse, "css/app-min.css?ver=" +StaticFileVersions.CSS_VERSION) %>";
		        popupWin.document.write('<html><head><title>Preview blogpost</title><link rel="stylesheet" type="text/css" href="' + css1 + '" media="screen" /><link rel="stylesheet" type="text/css" href="' + css2 + '" media="screen" /></head><body>');
		        var html = [];
		        var i = 0;
		        html[i++] = "<div class=\"container\">";
		        html[i++] = "<div class=\"row\">";
		        html[i++] = "<div class=\"col-lg-12\">";
		        html[i++] = "<div class=\"panel-title text-center\">";
		        html[i++] = "<h1>" + title + "</h1>";
		        html[i++] = "</div>";
		        html[i++] = "<div class=\"lead col-md-12\">";
		        html[i++] = "by <span id=\"blog-author\">" + author + "</span>"
		        html[i++] = "</div>";
		        html[i++] = "<div class=\"clearfix\"></div>";
		        html[i++] = "<hr>";
		        html[i++] = "<div class=\"col-md-8\">";
		        html[i++] = "<span class=\"glyphicon glyphicon-time\"></span> Posted on DDMMMYY";
		        html[i++] = "</div>";
		        html[i++] = "<div class=\"clearfix\"></div>";
		        html[i++] = "<hr>";
		        html[i++] = "<div id=\"content\">";
		        html[i++] = content
		        html[i++] = "</div>";
		        html[i++] = "<hr>";
		        html[i++] = "</div>";
		        html[i++] = "</div>";
		        html[i++] = "<hr>";
		        html[i++] = "</div>";
		        var toPrint = html.join("");
		        popupWin.document.write(toPrint);
		        popupWin.document.write('</body></html>');
		        popupWin.focus();
			});
		});
	</script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />