<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.admin.entity.forms.Form"%>
<%@page import="org.communitylibrary.utils.DateUtility"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body ng-app="App" ng-controller="DonationsController">
	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false"
				page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
			    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
			%>

			<div class="col-md-10">
				<!-- New blog post section begins -->
				<div class="row">
					<div id="rootwizard">
						<div class="navbar">
							<div class="navbar-inner">
								<div class="container">
									<ul class="nav nav-pills">
										<li class="active"><a href="#donations_received"
											data-toggle="tab">Donations Recieved</a></li>
										<li><a href="#upload-excel"
											data-toggle="tab">Upload Donations</a></li>
											
										<!-- <li><a href="#create_donations" data-toggle="tab">Add
												a manual donation Entry</a></li> -->
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="donations_received">
											<div class="col-md-10 panel-default">
									<div class="content-box-header panel-heading">
										<div class="panel-title">Donations :
											{{donations.length}}, Total Amount Collected: INR
											{{calculateAmount()}}</div>
											<div class="pull-right">
											    <button type="button" class="btn btn-sm btn-default" ng-click="getDonations(false)">More Donations</button>
    										</div>
									</div>
									<div class="content-box-large box-with-header">
										<div class="table-responsive">
											<table class="table">
												<thead>
													<tr>
														<th title="select all donations"><input type="checkbox" ng-click="selectAllDonations()" ng-model="selectedAll"></th>
														<th>
												            Transaction Id
														</th>
														<th>Donation Date</th>
														<th>
												            Name
														</th>
														<th>
												            Email Id
														</th>
														<th>
												            Amount
														</th>
														<th>
												            Mode
														</th>
														<th>Receipt number</th>
														<th>Receipt Date</th>
														<th><span ng-if="selectedDonations.length > 0"><button title="Export as CSV" class="btn btn-sm btn-primary" ng-click="downloadAsCSV()"><i class="glyphicon glyphicon-cloud-download"></i></button></span></th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="unit in donations"
														ng-style="{'background-color' : unit.color}"
														title="{{unit.pledge}}" ng-include="getTemplate(unit)">
													</tr>
												</tbody>
											</table>
											<script type="text/ng-template" id="edit">
												<td></td>
												<td>{{selectedDonation.txn_id}}</td>
												<td>{{selectedDonation.createdOn}}</td>
												<td>{{selectedDonation.fname + " " + selectedDonation.lname}}</td>
												<td>{{selectedDonation.email}}</td>
												<td>INR {{selectedDonation.amount}}</td>
												<td>{{selectedDonation.paymentmedium}}</td>
												<td><input type="text" ng-model="selectedDonation.receiptnumber" class="form-control"/></td>
												<td><input type="text" ng-model="selectedDonation.receiptdate" placeholder="DD-MMM-YY" class="form-control" /></td>
												<td>
													<button title="save changes" class="btn btn-sm btn-success" ng-click="saveEdits()"><i class="glyphicon glyphicon-floppy-disk"></i></button>
													<button class="btn btn-sm btn-default" ng-click = "cancelEdits()" title="Discard Changes"><i class="glyphicon glyphicon-remove"></i></button>
												</td>
											</script>
											<script type="text/ng-template" id="display">
												<td title="select this donation"><input type="checkbox" ng-click="selectDonation(unit)" ng-model="unit.selected"></td>
												<td>{{unit.txn_id}}</td>
												<td>{{unit.createdOn}}</td>
												<td>{{unit.fname + " " + unit.lname}}</td>
												<td>{{unit.email}}</td>
												<td>INR {{unit.amount}}</td>
												<td>{{unit.paymentmedium}}</td>
												<td>{{unit.receiptnumber}}</td>
												<td>{{unit.receiptdate}}</td>
												<td>

													<div class="btn-group" uib-dropdown>
      <a id="append-to-button{{unit.txn_id}}" type="button" class="btn btn-default" uib-dropdown-toggle ng-disabled="disabled">
        ...
      </a>
      <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="append-to-button{{unit.txn_id}}">
        <li role="menuitem"><a href="#" ng-click="deleteData(unit)">Delete</a></li>
		
	    <li role="menuitem"><a href="#" ng-click="openModal(unit.txn_id)">More</a></li>
		
        <li class="divider"></li>
		<li role="menuitem"><a href="#" ng-click="downloadAsCSV(unit)">Export as CSV</a></li>
        <!--<li role="menuitem"><a href="#" ng-click="sendDonationPDF(unit)" ng-show="unit.receiptnumber">Send E-mail</a></li>-->
        <li role="menuitem"><a href="#" ng-click="downloadAsPDF(unit)" ng-show="unit.receiptnumber" >Download As PDF</a></li>
      </ul>
    </div>
												</td>
											</script>
											<script type="text/ng-template" id="moreDetailsFailed.html">
								<div>Sorry! Could not load more details</div>
    						</script>
										</div>
									</div>
								</div>
								
							</div>
							<div class="tab-pane" id="upload-excel">
								<div class="col-md-10 content-box-large">
									<div class="panel-body">
										<form class="form-horizontal" role="form" id="donationsForm">
											<div class="form-group">
											    <label for="donationsFile">Please upload the donations file</label>
   												<input type="file" accept="*.csv" filemodel="donationsFile" class="form-control-file" id="donationsFile">
  											</div>
											
											<div class="modal-footer">
												<button class="btn btn-success" ng-click="upload()">Upload File</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="create_donations">
								<div class="col-md-10 content-box-large">
									<div class="panel-heading">
										<div class="panel-title">Please fill in your details</div>
									</div>
									<div class="panel-body">
										<form class="form-horizontal" role="form" id="paymentForm">
											<fieldset>
												<legend>Personal Information</legend>
												<div class="form-group">
													<label for="fname" class="col-sm-2 control-label">First
														Name*</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" ng-model="newEntry.fname" id="fname"
															maxlength="50" name="fname"
															placeholder="eg John. Only English letters allowed. Do not use salutation or any special characters"
															required="required">
													</div>
												</div>
												<div class="form-group">
													<label for="lname" class="col-sm-2 control-label">Last
														Name*</label>
													<div class="col-sm-10">
														<input type="text" ng-model="newEntry.lname" class="form-control" id="lname"
															maxlength="50" name="lname"
															placeholder="eg Smith Only English letters allowed. Do not use any special characters"
															required="required">
													</div>
												</div>
												<div class="form-group">
													<label for="email" class="col-sm-2 control-label">Email
														Id*</label>
													<div class="col-sm-10">
														<input type="email" ng-model="newEntry.email" class="form-control" id="email"
															name="email" placeholder="eg name@gmail.com"
															required="required">
													</div>
												</div>
												<div class="form-group" title="We promise to not spam you">
													<label for="phone" class="col-sm-2 control-label">Mobile
														Number*</label>
													<div class="col-sm-10">
														<input type="tel" class="form-control" ng-model="newEntry.phone" id="phone"
															name="phone" placeholder="eg 9810112233"
															required="required">
													</div>
												</div>
											</fieldset>
											<fieldset>
												<legend>Transaction Details</legend>
												<div class="form-group">
													<label class="control-label col-md-2">Received On</label>
													<div class="col-md-10">
														<div class="input-group">
															<input class="form-control" name="createdOn" id="createdOn"
																type="text" placeholder="DD-MMM-YY" required="required" ng-model="newEntry.createdOn" >
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2">Receipt Number</label>
													<div class="col-md-10">
														<div class="input-group">
															<input class="form-control" name="receiptnumber" id="receiptnumber"
																type="text" placeholder="" required="required" ng-model="newEntry.receiptnumber" >
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2">Receipt Date</label>
													<div class="col-md-10">
														<div class="input-group">
															<input class="form-control" name="receiptdate" id="receiptdate"
																type="text" placeholder="DD-MMM-YY" required="required" ng-model="newEntry.receiptdate" >
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2" for="amount">Amount*</label>
													<div class="col-md-10">
														<div class="row">
															<div class="col-sm-3">
																<div class="input-group">
																	<span class="input-group-addon">&#8377;</span> <input
																		class="form-control" name="amount" id="amount"
																		type="text" placeholder="" required="required" ng-model="newEntry.amount" >
																	<span class="input-group-addon">.00</span>
																</div>
															</div>
															<div class="col-sm-9">
																<div class="form-group col-sm-8">
																	<div class="col-sm-10">
																		<div class="checkbox">
																			<div class="col-sm-2 col-md-2">
																				<input id="receipt" name="receipt" type="checkbox" ng-model="newEntry.receipt">
																			</div>
																			<div class="col-md-10 col-sm-10">
																				<label for="receipt"> Needs an 80G receipt
																					for this transaction? </label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2">Pledge</label>
													<div class="col-md-10" id="pledge-container">
														<div class="radio" ng-repeat="type in pledgeTypes">
															<label> 
																<input type="radio" name="pledgetype" ng-model="newEntry.pledgetypecode"
																	ng-value="type.code" />{{type.name}}
															</label> 
														</div>
													</div>
												</div>
											</fieldset>
											<fieldset ng-show="newEntry.receipt || (newEntry.amount >= 100000)">
												<legend>Mandatory Information for the 80G receipt</legend>
												<div class="form-group">
													<label class="control-label col-sm-2 col-sm-offset-1"
														for="pannum">Pan No.*</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="pannum"
															name="pannum" placeholder="Your Pan Number" ng-model="newEntry.pannum">
													</div>
												</div>
												<fieldset class="col-sm-offset-1">
													<legend>Address Details</legend>
													<div class="form-group">
														<label class="control-label col-sm-2" for="addressline1">Address
															Line 1*</label>
														<div class="col-sm-10">
															<input type="text" class="form-control" ng-model="newEntry.addressline1" id="addressline1"
																name="addressline1" placeholder="Address Line 1">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-sm-2" for="addressline2">Address
															Line 2*</label>
														<div class="col-sm-10">
															<input type="text" class="form-control" ng-model="newEntry.addressline2" id="addressline2"
																name="addressline2" placeholder="Address Line 2">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-sm-2" for="city">City*</label>
														<div class="col-sm-10">
															<input type="text" class="form-control" ng-model="newEntry.city" id="city"
																name="city" placeholder="City">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-sm-2" for="state">State*</label>
														<div class="col-sm-10">
															<input type="text" class="form-control" ng-model="newEntry.state" id="state"
																name="state" placeholder="State">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-sm-2" for="country">Country*</label>
														<div class="col-sm-10">
															<input type="text" class="form-control" id="country"
																name="country" placeholder="Country" ng-model="newEntry.country">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-sm-2" for="zipcode">Zipcode*</label>
														<div class="col-sm-10">
															<input type="text" class="form-control" id="zipcode"
																name="zipcode" placeholder="Zipcode" ng-model="newEntry.zipcode">
														</div>
													</div>
												</fieldset>
											</fieldset>
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<button type="button" class="btn btn-primary" ng-click="saveEntry()">Add</button>
												</div>
											</div>
										</form>
									</div>
						</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse,
					"/js/admin/vendors/angular/angular.min.js?ver=13012018")%>"></script>
	
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular-animate.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/ui-bootstrap-tpls-2.5.0.min.js?ver=11112017")%>"></script>
	
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse,
					"/js/admin/vendors/angular/angular-ui-bootstrap-modal.js?ver=11112017")%>"></script>

	<script type="text/javascript">
		var baseURL = "<%=AdminActions.DONATIONS.getActionURL(secureRequest, secureResponse)%>";
	</script>
	
	<script type="text/javascript">
		var tclpAddress = "<%=ConfigStore.getStringValue(CacheConfiguration.STR_TCLPADDRESS,"The Community Library Project, B-65 Panchsheel Vihar, (behind Triveni Commercial Complex), Sheikh Sarai Phase 1, New Delhi 110017")%>";
		var tclpPhone = "<%=ConfigStore.getStringValue(CacheConfiguration.STR_TCLPPhone, "+91 9871193204/+91 9871451216")%>";
		var tclpURL = "<%= ConfigStore.getStringValue(CacheConfiguration.STR_SERVER_NAME, "www.thecommunitylibraryproject.org") %>";
	</script>
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/angular/donations.js?ver=33122019")%>"></script>


	<div class="container" modal="extraDetailsModal" close="cancel()"
		style="background: #eff0f3">
		<fieldset>
			<legend>Transaction Details</legend>
			<div class="form-group">
				<label class="col-sm-2 control-label">Transaction Id</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.txn_id}}</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Amount</label>
				<div class="col-sm-10">
					<div class="form-control">INR {{selectedUnit.amount}}</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Payment Successful?</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.isSuccess === true ?
						"Yes" : "No"}}</div>
				</div>
			</div>
			<div class="form-group" ng-show="selectedUnit.isSuccess === true">
				<label class="col-sm-2 control-label">Gateway Id</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.gatewayId}}</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Personal Details</legend>
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.fname + " " +
						selectedUnit.lname}}</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Email Id</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.email}}</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Phone number</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.phone}}</div>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Receipt Details</legend>
			<div class="form-group">
				<label class="col-sm-2 control-label">Need 80G Receipt</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.needReceipt ? "YES"
						: "NO"}}</div>
				</div>
			</div>

			<div class="form-group" ng-show="selectedUnit.pannum">
				<label class="col-sm-2 control-label">Pan Number</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.pannum}}</div>
				</div>
			</div>

			<div class="form-group" ng-show="selectedUnit.address">
				<label class="col-sm-2 control-label">Address</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.address}}</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Extra information</legend>

			<div class="form-group" ng-show="selectedUnit.pledge">
				<label class="col-sm-2 control-label">Pledge</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.pledge}}</div>
				</div>
			</div>

			<div class="form-group" ng-show="selectedUnit.notes">
				<label class="col-sm-2 control-label">Notes</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.notes}}</div>
				</div>
			</div>

			<div class="form-group" ng-show="selectedUnit.createdOn">
				<label class="col-sm-2 control-label">Created On</label>
				<div class="col-sm-10">
					<div class="form-control">{{selectedUnit.createdOn}}</div>
				</div>
			</div>
		</fieldset>
		<div class="modal-footer">
			<button class="btn btn-success" ng-click="cancel()">Close</button>
		</div>
	</div>

</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />