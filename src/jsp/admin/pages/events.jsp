<%@page import="org.communitylibrary.ui.resources.StaticFileVersions"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="org.communitylibrary.data.entity.EventData"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				
				List<EventData> events = SettingsBean.getEventsFromRequest(secureRequest);
				String eventsAsJSON = SettingsBean.convertRecordsToJSON(events);
			%>
			<div class="col-md-10">
			<%--Events Calendar Begins --%>
				<%--Actual Content Row begins here --%>
		  			<div class="content-box-large">
		  				<div class="panel-body">
		  					<div class="row">
		  						<div class="col-md-10">
		  							<div id='calendar'></div>
		  						</div>
		  					</div>
	  					</div>
  					</div>
				<%--Actual content row ends here --%>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />
	
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery-ui.min.js") %>"></script>
	
	<script src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "js/admin/vendors/moment/moment.min.js?ver=" + StaticFileVersions.JS_VERSION)%>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery-ui.min.css") %>" rel="stylesheet" />
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/fullcalendar.js") %>"></script>
    <link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/fullcalendar.css") %>" rel="stylesheet" media="screen">
    <script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/bootstrap-timepicker.min.js") %>"></script>
    <link type="text/css" href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/admin/bootstrap-timepicker.min.css") %>" />
	<script type="text/javascript">
		var manage_calendar;
	    $(document).ready(function(){
	      manage_calendar = new ManageCalendarJS("<%=AdminActions.EVENTS.getActionURL(secureRequest, secureResponse)%>", <%=eventsAsJSON%>);
        });
  </script>
  
	<div id="eventContent" class="row" style="display:none;">
		<div>
			<div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" id="event-details-form">
						<div class="form-group">
						    <label for="title" class="col-sm-2 control-label">Event Title</label>
						    <div class="col-sm-10">
				    	    	<input type="text" class="form-control" id="title" name="title" placeholder="Event Title">
						    </div>
						 </div>
						<div class="form-group">
						    <label for="start" class="col-sm-2 control-label">Start Date</label>
						    <div class="col-sm-5">
				    	    	<input type="text" class="form-control" id="start" name="start" placeholder="Start Date">
						    </div>
						    <div class="col-sm-4 bootstrap-timepicker timepicker input-group" title="Time">
				    	    	<input type="text" class="form-control input-small" id="time" name="time">
				    	    	<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
						    </div>
						 </div>
 						<div class="form-group">
						    <label for="end" class="col-sm-2 control-label">End Date</label>
						    <div class="col-sm-5">
				    	    	<input type="text" class="form-control" id="end" name="end" placeholder="End Date">
						    </div>
   						    <div class="col-sm-4 input-group" title="Duration">
				    	    	<input type="text" class="form-control" id="duration" name="duration" placeholder="Duration">
						    </div>
					 	</div>
  						<div class="form-group">
						    <label for="where" class="col-sm-2 control-label">Event Address</label>
						    <div class="col-sm-10">
				    	    	<textarea class="form-control" placeholder="Event Address" id="where" name="where" rows="3"></textarea>
						    </div>
						 </div>
  						<div class="form-group">
						    <label for="event-desc" class="col-sm-2 control-label">Event Description</label>
						    <div class="col-sm-10">
				    	    	<textarea class="form-control" id="event-desc" name="event-desc" placeholder="Event Description" rows="3"></textarea>
						    </div>
						 </div>
  						<div class="form-group">
						    <label for="event-link" class="col-sm-2 control-label">Event Link</label>
						    <div class="col-sm-10">
				    	    	<input type="text" class="form-control" id="event-link" name="event-link" placeholder="Event Link">
						    </div>
						 </div>
						 <%--<div class="form-group">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="button" class="btn btn-primary" onclick="javascript: $('#eventContent').dialog('close')">Add Event</button>
							</div>
						</div>--%>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />