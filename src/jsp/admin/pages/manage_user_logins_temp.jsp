<jsp:include page="includes/page_top.jsp" />
<%@page import="com.zillious.abacus.cache.ui.user.CacheUserRole"%>
<%@page import="com.zillious.abacus.cache.ui.navigation.UIActions"%>
<%@page import="com.zillious.abacus.cache.ui.user.CacheUser"%>
<%@page import="com.zillious.abacus.cache.ui.beans.UserBean"%>
<%@page import="com.zillious.abacus.cache.ui.beans.UIBean"%>
<%@page import="java.util.Map"%>
<%@page import="com.zillious.commons.utils.JSPUtility"%>
<%@page import="com.zillious.abacus.cache.ui.user.dto.UserLoginDTO"%>
<%@page import="com.zillious.abacus.cache.ui.resources.StaticFileVersions"%>
<%
	Map<CacheUser, UserLoginDTO> users = UserBean.getUserLoginsFromReq(request);
	int ucnt = 1;
%>
<HTML>
<head>
  <title>Manage User Logins - Abacus AWS</title>
  <jsp:include page="includes/headtags.jsp" />
</head>
<jsp:include page="includes/header.jsp" />
<script type="text/javascript" src="<%= UIBean.getStaticURL(request, response, "jquery/jquery-1.10.2.min.js?ver=" + StaticFileVersions.JS_VERSION) %>"></script>
<script type="text/javascript" src="<%= UIBean.getStaticURL(request, response, "jquery/ui/jquery-ui.min.js?ver=" + StaticFileVersions.JS_VERSION) %>"></script>
 <script type="text/javascript" src="<%= UIBean.getStaticURL(request, response, "jquery/ui/datepicker/datepicker-en-IN.js?ver=" + StaticFileVersions.JS_VERSION) %>"></script>
<h1>Manage Users</h1>
<jsp:include page="includes/messages.jsp" />

<h2>Existing Users</h2>
<table cellspacing="0" cellpadding="5" border="0" class="dataTable1" width="100%">
<tr valign="top">
  <th>ID</th>
  <th>Role</th>
  <th>Name</th>
  <th>Enabled</th>
  <th>Email</th>
  <th>Market</th>
  <th>Number of Logins</th>
  <th>Last Login</th>
  <th>Actions</th>
</tr>
<%	for (CacheUser user : users.keySet()) {
    UserLoginDTO loginDTO = users.get(user);
    %>
<tr class="<%= (ucnt++ % 2 > 0) ? "odd" : "even" %>" valign="top">
  <td><%= user.getUserId() %></td>
  <td><%= JSPUtility.writeToJspDefaultEmpty(user.getUserRole().getDisplayString()) %></td>
  <td><%= JSPUtility.writeToJspDefaultEmpty(user.getUserName()) %></td>
  <td><%= user.getIsEnabled() ? "Enabled" : "Disabled" %></td>
  <td><%= JSPUtility.writeToJspDefaultEmpty(user.getEmail()) %></td>
  <td><%= JSPUtility.writeToJspDefaultEmpty(user.getMarketName()) %></td>
  <td><%= JSPUtility.writeToJspDefaultEmpty(loginDTO.getNumLogins()) %></td>
  <td><%= JSPUtility.writeToJspDefaultEmpty(loginDTO.getLastLogin()) %></td>
  <td>
    <form>
      <input type="button" value="Get Login Info" onclick='user_login.showDetailsDiv(<%=user.getUserId()%>);' />
    </form>
  </td>
</tr>
<%	} %>
</table>

<br/>
<div id="getLoginInfoDTO" style="display:none;">
<h2>Specific Login Information</h2>
<form name="getLoginForm">
<table cellspacing="0" cellpadding="5" border="0" class="dataTable1">
<colgroup><col width="30%" /><col /></colgroup>
<tr valign="top">
  <th>User Id</th>
  <td>
		<div id="user_id_div" name="user_id_div"></div>
		<input type="hidden" name="user_id" />
  </td>
</tr>
<tr valign="top">
  <th>Date</th>
  <td>
      <label title="Date in dd/mm/yyyy format">
        <span class="cal_disp"><input type="text" name="date" size="10" id="dep" value="" maxlength="10" readonly/></span>	    
	  </label>
	  <div class="calendar_disp"></div>		
  </td>
</tr>
<tr valign="top">
  <td><input type="button"  name="getDetailsButton" value="Get Details" /></td>
  <td><input type="button"  name="hideButton" value="Hide" /></td>
</tr>
</table>
</form>
<span id="resultSpan"></span>
</div>



<jsp:include page="includes/footer.jsp" />
<script type="text/javascript">
function LoginJS(form) {
  this.form = form;
  
  this.init = function() {
    var self = this;
    CACHE_JS.initPreviousDate($(this.form).find("input[name='date']"), "<%= UIBean.getStaticURL(request, response, "img/calendar.gif") %>", 5);  
    $(this.form).find("input[name='hideButton']").click(function() {
      self.hideDetailsDiv();
    });
    
    $(this.form).find("input[name='getDetailsButton']").click(function(){
      self.getLoginInformation();
    });
  }
  
  this.showDetailsDiv = function(userId) {
    ($(this.form)[0]).reset();
    $(this.form).find("div[name='user_id_div']").html(userId);
    $(this.form).find("input[name='user_id']").val(userId);
    $("#getLoginInfoDTO").show();
  }
  
  this.hideDetailsDiv = function() {
    $("#getLoginInfoDTO").hide();
  }
  
  this.getLoginInformation = function() {
    var url = "<%= UIActions.MANAGE_USER_LOGINS.getActionURL(request, response, new String[] {"getSpecificLogin"})%>";
    $.post( url, $(this.form).serialize(), function( data ) {
      if(data.status == "MSG_SUCCESS") {
	      $("#resultSpan").html("Date: " + data.date+", Number of logins: " + data.numLogins);
      } else {
        $("#resultSpan").html("No login information obtained");
      }
    });
  }
  
  this.init();
}

var user_login = new UserLoginJS($("form[name='getLoginForm']"));

</script>
</html>
