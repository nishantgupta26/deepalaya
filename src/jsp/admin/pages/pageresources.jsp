<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.admin.entity.page.PageResource"%>
<%@page import="org.communitylibrary.utils.DateUtility"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="All Pages" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false"
				page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
				CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
			%>
			<div class="col-md-10">
				<!-- New blog post section begins -->
				<div class="row">
					<div class="content-box-large">
						<div class="panel-body">
							<div class="well" style="margin-top: 30px;">
								<button type="button" class="btn btn-lg btn-block btn-primary" onclick="window.location.href='<%=AdminActions.PAGES.getActionURL(secureRequest, secureResponse, new String[] {"new"})%>'">Add
									a new Document Page</button>
							</div>
						</div>
					</div>
				</div>
				<!-- New blog post section ends -->
				
				<%
				List<PageResource> pages = AdminBean.getPages(secureRequest);
				
				if(pages != null) { %>
				<div class="content-box-large">
					<div class="panel-heading">
						<div class="panel-title">Existing Pages</div>
					</div>
					<div class="panel-body">
					<form id="pageform" method="post"><input type="hidden" name="id" />
						<table cellpadding="0" cellspacing="0" border="0"
							class="table table-striped table-bordered"
							id="existing-pages">
							<thead>
								<tr>
									<th>#</th>
									<th>Title</th>
									<th>URI</th>
									<th>Creation Date</th>
									<th>Last Modified</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<% int row = 1; 
							for(PageResource post : pages) { %>
								<tr class=<% if(row == 1) { %>"odd" <%} else { %>"even"<%} row = 1-row; %>>
								<td><%=post.getId() %></td>
								<td><%=post.getTitle() %></td>
								<td><%=WebsiteActions.PAGE.getActionURL(secureRequest, secureResponse, new String[] {post.getUrl()}) %></td>
								<td><%=DateUtility.getDateInDDMMMYYHHMMDashSeparator(post.getCreationDate()) %></td>
								<td><%=DateUtility.getDateInDDMMMYYHHMMDashSeparator(post.getModificationDate())%></td>
								<td>
									<p>
										<button class="btn btn-primary" onclick="manage_pages.edit(<%=post.getId() %>)"><i class="glyphicon glyphicon-pencil"></i> Edit</button>
										<button class="btn btn-danger" onclick="manage_pages.delete(<%=post.getId() %>)"><i class="glyphicon glyphicon-remove"></i> Delete</button>
									</p>
	                			</td>
								</tr>
							<%} %>
							</tbody>
						</table>
					</form>
					</div>
				</div>
				<%} %>
			</div>

		</div>

	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/ckeditor.js") %>"></script>
	<script type="text/javascript"
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/editor/ckeditor/adapters/jquery.js") %>"></script>
	<script type="text/javascript">
		var manage_pages;
		$(document).ready(function() {
			manage_pages = new ManageBlogs($("#pageform"), "<%=AdminActions.PAGES.getActionURL(secureRequest, secureResponse)%>");
		});
	</script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />