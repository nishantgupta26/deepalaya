<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.admin.entity.forms.Form"%>
<%@page import="org.communitylibrary.admin.entity.blog.Blogpost"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>

	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content" ng-app="App">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false"
				page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
			    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				String formResources = AdminBean.getFormResourcesFromRequest(secureRequest);
			%>
			<div class="col-md-10" ng-controller="FormController">
				<!-- New Form section begins -->
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="content-box-header panel-heading">
							<div class="panel-title ">Form Details</div>
						</div>
						<div class="content-box-large box-with-header">
							<div>
								<input type="hidden" name="form_data" ng-model="formData" />
								<div class="row">
									<div class="form-group">
										<label class="col-sm-2 control-label">Title</label>
										<div class="col-sm-10">
											<p class="input-group">
												<input type="text" class="form-control" placeholder="Title"
													name="title" ng-model="formData.title"
													ng-change="generateURL()" /> <span class="input-group-btn">
													<button type="button" class="btn btn-default">
														<i ng-show="!status.isTitleValid"
															class="glyphicon glyphicon-exclamation-sign"
															title="Please enter only English title 5-50 characters"></i>
														<i ng-show="status.isTitleValid"
															class="glyphicon glyphicon-check"></i>
													</button>
												</span>
											</p>
										</div>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-2 control-label">Enter URL
											Manually?</label>
										<div class="col-sm-1">
											<input type="checkbox" title="Enter url manually"
												ng-model="formData.manualURL"
												>
										</div>

										<label class="col-sm-2 control-label">URL of the form</label>
										<div class="col-sm-7">
											<p class="input-group">
												<input type="text"
													class="form-control"
													placeholder="url of the post" name="url"
													ng-model="formData.url" ng-change="validateURL()">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default">
														<i ng-show="!status.isURLValid"
															class="glyphicon glyphicon-exclamation-sign"
															title="Please enter only English url without spaces"></i>
														<i ng-show="status.isURLValid"
															class="glyphicon glyphicon-check ng-hide"></i>
													</button>
												</span>
											</p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="emails">Email Id to send responses to: </label>
										<div class="col-sm-7">
											<p class="input-group">
												<input type="text"
													class="form-control"
													placeholder="Comma separated list of email-ids" name="emails" id="emails"
													ng-model="formData.emails" ng-change="validateEmailIds()">
												<span class="input-group-btn">
													<button type="button" class="btn btn-default">
														<i ng-show="!status.isEmailsValid"
															class="glyphicon glyphicon-exclamation-sign"
															title="This field should not be left blank"></i>
														<i ng-show="status.isEmailsValid"
															class="glyphicon glyphicon-check ng-hide"></i>
													</button>
												</span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row" ng-if="isFormDescValid()">
					<div class="col-md-12 panel-info">
						<div class="content-box-header panel-heading">
							<div class="panel-title ">Add a field</div>
						</div>
						<div class="content-box-large box-with-header">
							<div class="panel-body">
								<form class="form-horizontal" role="form" name="formFieldForm">
									<div class="form-group">
										<div class="col-sm-10">
											<p ng-class="{'input-group' :!isNew}">
												<input type="text" class="form-control"
													placeholder="Question?"
													ng-change="isValidQuestion()" ng-model="newField.question" />
												 <span class="input-group-btn" ng-show="!isNew"> 
												 	<span type="button" class="btn btn-default"> 
												 		<i ng-show="!newFieldStatus.isQuestionValid" class="glyphicon glyphicon-exclamation-sign"></i> 
												 		<i ng-show="newFieldStatus.isQuestionValid" class="glyphicon glyphicon-check"></i>
													</span>
												</span>
											</p>
										</div>
										<div class="col-sm-2">
												<select ng-model="newField.type" class="form-control"
													ng-options="type.name for type in getAllTypes() track by type.code">
													<option style="display: none" value="">Type of Field</option>
												</select> 
										</div>
									</div>
									
									<div class="form-group" ng-if="newField.type.code=='T'">
										<div class="col-sm-10">
											<input type="text" placeholder="Free Flow Text Type Answer" class="form-control" ng-disabled="true" />
										</div>
										<div class="col-sm-10">
											<input type="text" placeholder="Regex to match contents entered" class="form-control" ng-model="newField.regexString" />
										</div>
									</div>
									
									<div class="form-group" ng-if="newField.type.code=='C' || newField.type.code=='R'">
										<table class="table  table-responsive table-bordered">
											<thead>
												<tr>
													<th>Type</th>
													<th>Option Text</th>
													<th>Is Other Field?</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="option in newField.options">
													<td>
														<input type="checkbox" ng-if="newField.type.code=='C'"  ng-disabled="true" />
														<input type="radio" ng-if="newField.type.code=='R'"  ng-disabled="true" />
													</td>
													<td>
														<span class="form-control">{{option.text}}</span>
													</td>
													<td>
														<span class="form-control">{{option.isOtherField}}</span>
													</td>
													<td>
														<button title="Delete this option?" class="btn btn-sm btn-danger" ng-click="deleteOption($index)"><i class="glyphicon glyphicon-trash"></i></button>
													</td>
												</tr>
												<tr>
													<td>
														<input type="checkbox" ng-if="newField.type.code=='C'"  ng-disabled="true" />
														<input type="radio" ng-if="newField.type.code=='R'"  ng-disabled="true" />
													</td>
													<td>
														<p class="input-group">
															<input type="text" class="form-control" ng-model="newOption.text" placeholder="Text for the option" ng-change="isNewOptionTextValid()" ng-disabled="newOption.isOtherField" />
															<span class="input-group-btn">
																<button type="button" class="btn btn-default">
																	<i ng-show="!isValidNewOptionText" class="glyphicon glyphicon-exclamation-sign" title="Please enter only English title 5-50 characters">
																	</i>
																	<i ng-show="isValidNewOptionText" class="glyphicon glyphicon-check"></i>
																</button>
															</span>
														</p>
													</td>
													<td>
														<input type="checkbox" ng-model="newOption.isOtherField" ng-disabled="isOtherOptionPresent()" ng-change="addOtherOption()" />
													</td>
													<td>
														<button title="Add this option?" class="btn btn-sm btn-primary" ng-click="addOption()" ng-disabled="!isValidNewOptionText">
															<i class="glyphicon glyphicon-floppy-disk"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									
									<div class="form-group">
										<div class="col-sm-6">
											<label for="isMandatory" class="col-sm-2 control-label">Is Field Mandatory?</label>
											<div class="col-sm-10" title="click to toggle">
												<button type="button" class="btn btn-sm btn-warning" ng-if="!newField.isMandatory" ng-click="newField.isMandatory = true">No</button>
												<button type="button" class="btn btn-sm btn-primary" ng-if="newField.isMandatory" ng-click="newField.isMandatory = false">Yes</button>
											</div>
										</div>
										<div class="col-sm-6">
											<button type="button" class="btn btn-md  btn-primary"
												ng-disabled="!canAddField()" ng-click="addFieldToForm()">Add Field To Form</button>
										</div>
										
									</div>

									<div class="form-group">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="row" ng-if="formData.fields.length > 0">
					<div class="col-md-12 panel-info">
						<div class="content-box-header panel-heading">
							<div class="panel-title ">Form Fields</div>
						</div>
						<div class="content-box-large box-with-header">
							<div class="row">
								<div class="col-sm-12">
									<table class="table table-responsive table-bordered">
											<thead>
												<tr>
													<th>S.No</th>
													<th>Question Text</th>
													<th>Is Mandatory?</th>
													<th>Type</th>
													<th>Regex/Number Of Options</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="field in formData.fields">
													<td>
														{{$index+1}}
													</td>
													<td>
														<span class="form-control">{{field.question}}</span>
													</td>
													<td>
														<span class="form-control">{{field.isMandatory === true ? "YES" : "NO"}}</span>
													</td>
													<td>
														<span class="form-control">{{field.type.name}}</span>
													</td>
													<td>
														<span class="form-control">{{field.options ? "Options: " + field.options.length : field.regexString ? "Regex: " + field.regexString : "Not Required"}}</span>
													</td>
													<td>
														<button title="Edit this Field?" class="btn btn-sm btn-warning" ng-click="editField($index)"><i class="glyphicon glyphicon-pencil"></i></button>
														<button title="Delete this Field?" class="btn btn-sm btn-danger" ng-click="deleteField($index)" ng-disabled="editIndex > -1"><i class="glyphicon glyphicon-trash"></i></button>
													</td>
												</tr>
											</tbody>
										</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row" >
					<div class="col-md-12 panel-info">
						<div class="content-box-large">
							<div class="row">
								<div class="col-sm-12">
									<button type="button" class="btn btn-lg btn-block btn-primary" ng-disabled="!(isFormDescriptionValid && formData.fields.length > 0)"
										ng-click="submitForm()">Add Form</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- New Form section ends -->
			</div>

		</div>

	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />
	<script
		src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular.min.js?ver=11112017") %>"></script>

	<script type="text/javascript">
		var formURL;
		var formResources = <%=formResources%> ;
		$(document).ready(function() {
			formURL = "<%=AdminActions.FORMS.getActionURL(secureRequest, secureResponse)%>";
    	});
  	</script>

	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/angular/newform.js?ver=11112017") %>"></script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />