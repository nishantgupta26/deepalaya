<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.utils.NumberToWordsConverter"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.data.payments.PaymentMedium"%>
<%@page import="org.communitylibrary.utils.DateUtility"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.data.payments.PaymentUnit"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<html>
<head>
<style type="text/css">
td:first-child {
	font-weight: bold;
}

td {
	padding: 2%
}

table, tr, td {
    border: 2px solid black;
    border-collapse: collapse;
}
</style>
</head>
	<%
			    CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
				CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
				PaymentUnit unit = AdminBean.getPaymentUnitFromRequest(secureRequest);
	%>
	
	
<body style="height: 297mm;width: 210mm;padding: 10%;" onload="window.print();">
	<!-- Header -->
	<div style="
    margin-bottom: 2%;
">
		<!-- logo -->
		<img src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "images/donationlogo.jpeg") %>" width="140" height="120" style="float: left;">
		<div style="margin-bottom: 5%;">
			<center>
				<h2>The Community Library Project</h2>
				<br> <span>Regd office: B-16, Lower Ground Floor, Jungpura
					Extension</span> <br> <span>New Delhi -110014</span> <br> <span>Tel
					: +91 98100 55620 / +91 98184 36201 | <a style="text-decoration:none;" href="https://www.thecommunitylibraryproject.org" target="_blank">www.thecommunitylibraryproject.org</a>
				</span>
			</center>
		</div>
		<hr>
	</div>

	<!-- middle content -->
	<div>
		<div>Dated: <%= DateUtility.getDateInDDMMMYYDashSeparator(unit.getReceiptDate()) %></div>
		<div style="margin: 2% 0;">We confirm the receipt of donation from <b><%=unit.getName() %></b> <% if(unit.getPaymentType() == PaymentMedium.CHEQUE) { %>
			 vide Cheque Number <b><%=unit.getGatewayTransactionId() %></b> dated <b><%=DateUtility.getDateInDDMMMYYDashSeparator(unit.getReceiptDate()) %></b>
			<%} else if (unit.getPaymentType() != PaymentMedium.CASH) {%>
			vide Transaction Number <b><%=unit.getGatewayTransactionId() %></b>
			<%} %>
		</div>
		<div>
			Please find the details below: <br>
			<table style="margin: 3% 0;width: 100%;">
				<tbody><tr>
					<td>Receipt Number</td>
					<td><%=unit.getReceiptNumber() %></td>
				</tr>
				<tr>
					<td>Total Donation Received (INR)</td>
					<td><%=unit.getAmount() %></td>
				</tr>
				<tr>
					<td>Amount in Words (INR)</td>
					<td><%=NumberToWordsConverter.convert(unit.getAmount()) %> Only</td>
				</tr>
				<% if(unit.getDonor().getPanNumber() != null) { %>
				<tr>
					<td>Donor's PAN</td>
					<td><%=unit.getDonor().getPanNumber() %></td>
				</tr>
				<% } %>
			</tbody></table>
		</div>

		<div style="margin-bottom: 5%;">
			<div>For The Community Library Project</div>
			<div><img src="<%= UIBean.getStaticURL(secureRequest, secureResponse, "images/trustee_sign.png") %>" width="150" height="100"></div>
			<div style="font-weight: bold;">Trustee</div>
		</div>

		<hr>
	</div>

	<div style="font-weight: bold;
    font-style: italic;
    margin-top: 2%;">
		<center>
			<div>Donation tax-exempted under section 80G(5)(vi) of
			the Income Tax Act, 1961 vide order no.
			ITBA/EXM/S/80G/2019-20/1016745360(1) dated 11/07/2019 issued by
			Income Tax Department, Delhi.</div>
			<br>
			<div>TCLP PAN : AADTT0219K</div>
		</center>
	</div>

</body></html>