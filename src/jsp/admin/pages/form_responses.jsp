<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="org.communitylibrary.admin.entity.forms.Form"%>
<%@page import="org.communitylibrary.utils.DateUtility"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.admin.ui.beans.SettingsBean"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.utils.StringUtility"%>
<%@page import="org.communitylibrary.utils.ConfigStore"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.app.config.ElementType"%>
<%@page import="org.communitylibrary.app.config.CacheConfiguration"%>
<%@page import="org.communitylibrary.app.config.ConfigType"%>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false"
	page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>
	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content" ng-app="App">
		<div class="row" ng-controller="FormResponseController">
			<%--Side navigation panel --%>
			<jsp:include flush="false"
				page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
			<%
			    CustomSecurityWrapperRequest secureRequest = SecurityBean
								.getRequest(request);
						CustomSecurityWrapperResponse secureResponse = SecurityBean
								.getResponse(response);
						String availableForms = AdminBean.getAvailableFormsRequest(secureRequest);
			%>
			<div class="col-md-10">
				<!-- New blog post section begins -->
				<div class="row">
					<div class="col-md-12 panel-default">
						<div class="content-box-header panel-heading">
							<div class="panel-title">Forms and Responses : {{availableForms.length}}</div>
						</div>
						<div class="content-box-large box-with-header">
							<div class="table-responsive">
		  						<table class="table">
					              <thead>
					                <tr>
					                  <th>Form Id</th>
					                  <th>Title</th>
					                  <th>Number of Responses</th>
					                </tr>
					              </thead>
					              <tbody>
					                <tr ng-repeat="form in availableForms">
					                  <td>{{form.id}}</td>
					                  <td>{{form.title}}</td>
					                  <td>
										<button title="Click to get Responses" class="btn btn-sm btn-primary" ng-click="getResponses(form.id)" ng-disabled="form.count == 0">
											{{form.count}}
										</button>
					                </tr>
					              </tbody>
					            </table>
  							</div>
						</div>
					</div>
				</div>
				<div class="row" ng-show = "selectedForm">
					<div class="col-md-12 panel-default">
						<div class="content-box-header panel-heading">
							<div class="panel-title">{{selectedForm.title}} : {{selectedForm.totalResponses}} Responses</div>
						</div>
						<div class="content-box-large box-with-header">
							 <ul uib-pagination max-size="5" boundary-links="true" boundary-link-numbers="true" total-items="selectedForm.totalResponses" items-per-page="1" ng-model="currentPage" ng-change="pageChanged()"></ul>
							 	
							 	<div class="content-box-large">
							 		<div class="panel-heading">
							 			<div class="panel-title">Response ID: {{selectedForm.selectedResponse.id}}</div>
						 			</div>
					 			</div>
					 			<div class="panel-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
						                	  		<th>#</th>
						            	      		<th>Question</th>
							                  		<th>Answer</th>
							                	</tr>
											</thead>
											<tbody>
						                		<tr ng-repeat="field in selectedForm.selectedResponse.fields">
													<td>{{$index + 1}}</td>
													<td>{{field.question}}</td>
													<td>{{field.answer}}</td>
								                </tr>
						              		</tbody>
						            	</table>
									</div>
					 			</div>
							 <ul uib-pagination max-size="5" boundary-links="true" boundary-link-numbers="true" total-items="selectedForm.totalResponses" items-per-page="1" ng-model="currentPage" ng-change="pageChanged()"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />

	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular-animate.min.js?ver=11112017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/ui-bootstrap-tpls-2.5.0.min.js?ver=11112017")%>"></script>

	<script type="text/javascript">
		var availableForms = <%=availableForms%> ;
		var formURL;
		$(document).ready(function() {
			formURL = "<%=AdminActions.FORMS.getActionURL(secureRequest, secureResponse, new String[] {"responses"})%>";
    	});
  	</script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/angular/Angular.js?ver=28122017")%>"></script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/angular/form_responses.js?ver=28122017")%>"></script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />