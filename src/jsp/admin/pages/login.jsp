<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Welcome" />
</jsp:include>

<body>
<%
CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
%>
	<%-- Header --%>
	<jsp:include flush="false"
		page="/WEB-INF/admin/pages/includes/header.jsp" />
		
	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6>Sign In</h6>
			                <form method="post" action="<%=AdminActions.LOGIN.getActionURL(secureRequest, secureResponse)%>">
			                <input class="form-control" type="text" placeholder="E-mail address" name="email">
			                <input class="form-control" type="password" placeholder="Password" name="password">
			                <div class="action">
			                    <button class="btn btn-primary signup" type="submit">Login</button>
			                </div>
			                </form>                
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />