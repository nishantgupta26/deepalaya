<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<jsp:include page="/WEB-INF/pages/includes/page_top.jsp" />
<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page
	import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page
	import="org.communitylibrary.ui.navigation.WebsiteActions"%>
<%@page import="java.util.List"%>
<%@page import="org.communitylibrary.data.user.User"%>
<%@page import="org.communitylibrary.ui.beans.WebsiteBean"%>
<%@page import="org.communitylibrary.data.user.UserRoles"%>
<%
    CustomSecurityWrapperRequest secureRequest = SecurityBean
					.getRequest(request);
			CustomSecurityWrapperResponse secureResponse = SecurityBean
					.getResponse(response);

			String jsonString = "{\"userroles\" : [";
			//{"id":"A", "name":"Admin"},{"id":"E", "name":"Employee"}]}
			
			List<User> users = WebsiteBean.getUsersFromReq(request);
%>
<jsp:include flush="false" page="/WEB-INF/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Manage Users" />
</jsp:include>

<body>

		<%-- Header --%>
		<jsp:include flush="false" page="/WEB-INF/pages/includes/header_mainnav.jsp" />

		<%--Manage user page content starts --%>
			<div class="we-do">
				<div class="container">
							<jsp:include flush="false"
				page="/WEB-INF/pages/includes/messages.jsp" />
			<%--Existing users --%>
					<div class="we-do-main">
							<h2>Existing Users</h2>
								<div id="tableDiv">
									<table id="existing_users" class="display" style="border-width: 2px; border-color: #1ab188; text-align: center;" width="100%" cellspacing="0" border="1">
										<thead>
											<tr>
												<th>ID</th>
												<th>Role</th>
												<th>Name</th>
												<th>Enabled</th>
												<th>Email</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										<%
							    			int i = 0;
										    for(User user : users) {
										%>
											<tr id="tr_<%=++i%>">
												<td><%=user.getUserId()%></td>
												<td><%=user.getUserRole().getDisplayName()%></td>
												<td><%=user.getName()%></td>
												<td><%=user.isEnabled() ? "Enabled" : "Disabled"%></td>
												<td><%=user.getEmail()%></td>
												<td>
													<form>
														<a onclick='manage_users.editUser($("#tr_<%=i%>"))' >Edit User</a>
													</form>
												</td>
											</tr>
										<%
							    			}
										%>
										</tbody>
									</table>
								</div>
								<div class="clearfix"> </div>
							</div>
							
							
			<!--Existing users end here-->
			
			
						<%--Add/modify users --%>
					<div class="we-do-main">
							<h2>Add/Modify Users</h2>
								<div id="modifyUsersDiv">
								<form id="userForm" method="post">
							<div id="singleUploadDiv">
								<table class="display" style="border-width: 2px; border-color: #1ab188; text-align: center; margin-left: 12%;" width="70%" cellspacing="0" border="1">
									<thead>
										<tr>
											<th>Action</th>
											<th>
												<div id="actionDiv">Add New User</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Role</td>
											<td>
												<select name="role">
													<%
													    UserRoles[] roles = UserRoles.getLoggedInRoles();
														for (int roleCount = 0; roleCount < roles.length; roleCount++) {
															UserRoles role = roles[roleCount];						    			    
										    				jsonString += "{\"id\":\"" +role.serialize() + "\" , \"name\":\"" + role.getDisplayName() + "\"}" + (roleCount < (roles.length - 1) ? "," : "");
													%>
													<option value="<%=role.serialize()%>"><%=role.getDisplayName()%></option>
													<%
													    } 
										    			jsonString += "]}";
													%>
												</select>
											</td>
										</tr>
										<tr>
											<td>User ID</td>
											<td id="userIDTD"><input type="text" name="user_id"
												value="" size="30" maxlength="99" /></td>
										</tr>
										<tr>
											<td>Name</td>
											<td><input type="text" name="name" value="" size="50"
												maxlength="99" /></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><input type="text" name="email" value="" size="50"
												maxlength="99" /></td>
										</tr>
										<tr>
											<td>Enabled</td>
											<td><input type="checkbox" name="enabled"
												checked="checked" /></td>
										</tr>
										<tr>
											<td>Password</td>
											<td>
												<div id="passwordDiv" style="display: none">
													Update Password? <input name="update_password"
														type="checkbox">
												</div> <input type="password" name="password" value="" size="50"
												maxlength="20" />
											</td>
										</tr>
										<tr>
										<td></td>
										<td><button name="modButton" onclick="manage_users.modifyUser()" type="button" class="button" style="width:auto;">Add New User(s)</button></td>
										</tr>
									</tbody>
								</table>
							</div>
					</form>
								</div>
								<div class="clearfix"> </div>
							</div>
							
							
			<!--Add/modify users end here-->
			
				</div>
			</div>
		
		
		<%-- Manage user page content ends --%>


		<%-- Footer --%>
		<jsp:include flush="false" page="/WEB-INF/pages/includes/footer.jsp" />

	<%-- SCRIPTS --%>
	<jsp:include page="/WEB-INF/pages/includes/page_js.jsp" />

	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/jquery.dataTables.min.js") %>"></script>
	<script type="text/javascript" src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/jquery/dataTables.jqueryui.min.js") %>"></script>
	<link href="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/css/jquery.dataTables.min.css") %>" rel="stylesheet" />

	<%-- Page Specific Scripts --%>
	<script type="text/javascript">
		var manage_users;
	    $(document).ready(function(){
	      manage_users = new ManageUsersJS($("#userForm"),"<%=AdminActions.MANAGE_USERS.getActionURL(secureRequest, secureResponse, new String[] {"modify"}, false)%>", <%=jsonString%>);
        });
  </script>
</body>

<jsp:include page="/WEB-INF/pages/includes/page_bottom.jsp" />
