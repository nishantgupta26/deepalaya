<%@page import="org.communitylibrary.ui.beans.SecurityBean"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperResponse"%>
<%@page import="org.communitylibrary.ui.security.CustomSecurityWrapperRequest"%>
<%@page import="org.communitylibrary.admin.ui.navigation.AdminActions"%>
<%@page import="org.communitylibrary.admin.ui.beans.AdminBean"%>
<%@page import="org.communitylibrary.data.LibraryBranch"%>
<%@page import="org.communitylibrary.ui.beans.UIBean"%>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_top.jsp"></jsp:include>
<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/page_head.jsp">
	<jsp:param name="title" value="Manage slots Page" />
</jsp:include>
<body>
	<%-- Header --%>
	<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/header.jsp" />

	<div class="page-content" ng-app="App" ng-controller="SlotSettingsController">
		<div class="row">
			<%--Side navigation panel --%>
			<jsp:include flush="false" page="/WEB-INF/admin/pages/includes/sidebar.jsp" />
			<%--Right panel content starts here --%>
		<%
			CustomSecurityWrapperRequest secureRequest = SecurityBean.getRequest(request);
			CustomSecurityWrapperResponse secureResponse = SecurityBean.getResponse(response);
			String libraryBranchesAsJson = AdminBean.getLibraryBranchesJson(secureRequest);
			String slotTypesAsJson = AdminBean.getSlotTypesJson(secureRequest);
		%>
			<div class="col-md-5">
				<div class="content-box-large">
					<div class="panel-body">
						<div id="library-branch">
							<form class="form-horizontal" role="form" id="add-library-branch">
									<div class="form-group">
										<label for="libraryId" class="col-sm-2 control-label">Library ID</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="libraryId" name="libraryId" ng-model = "libraryBranch.code"
												 placeholder="TCLP-01" />
										</div>
									</div>
									<div class="form-group">
										<label for="libraryName" class="col-sm-2 control-label">Library Name</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="libraryName" name="libraryName" ng-model = "libraryBranch.name"
												 placeholder="TCLP Sikanderpur" />
										</div>
									</div>
									<div class="form-group">
										<label for="memberIDPrefix" class="col-sm-2 control-label">Member ID Prefix</label>
										<div class="col-sm-10" title="ID of members of this library starts with">
											<input type="text" class="form-control" id="memberIDPrefix" name="memberIDPrefix" ng-model = "libraryBranch.memberIDPrefix"
												 placeholder="MA" />
										</div>
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-lg btn-block btn-primary" ng-disabled="!isFormDescriptionValid()"
											ng-click="addLibraryBranch()">Add Library Branch</button>
									</div>
								</form>
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Library Code</th>
										<th>Library Name</th>
										<th>Member IDs prefix</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="branch in libraryBranches"
										title="{{branch.name}}">
										<td>{{branch.code}}</td>
										<td>{{branch.name}}</td>
										<td>{{branch.memberIDPrefix}}</td>
										<td>
											<button class="btn btn-sm btn-danger" title="Edit Branch" ng-click="editLibraryBranch(branch)">Edit</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- section for slot type begins -->
			<div class="col-md-5">
				<div class="content-box-large">
					<div class="panel-body">
						<div id="slot-type">
							<form class="form-horizontal" role="form" id="add-slot-type">
									<div class="form-group">
										<label for="slotType" class="col-sm-2 control-label">Slot Type</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="slotType" name="slotType" ng-model = "slotType"
												 placeholder="Laptop" />
										</div>
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-lg btn-block btn-primary" ng-disabled="!isSlotTypeValid()"
											ng-click="addSlotType()">Add A Slot Type</button>
									</div>
								</form>
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Slot Type</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="slotType in slotTypes">
										<td>{{slotType}}</td>
										<td>
											<button class="btn btn-sm btn-danger" title="delete slot type" ng-click="deleteSlotType(slotType)">Delete</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%-- Footer --%>
	<jsp:include page="/WEB-INF/admin/pages/includes/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/admin/pages/includes/page_js.jsp" />
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/vendors/angular/angular.min.js?ver=11112017")%>"></script>
	<script type="text/javascript">
		var slotSettingsURL; 
		var libraryBranchesJson;
		var slotTypesJson;
		slotSettingsURL =  "<%=AdminActions.DASLOTSETTINGS.getActionURL(secureRequest, secureResponse)%>";
		libraryBranchesJson = <%=libraryBranchesAsJson%>;
		slotTypesJson = <%=slotTypesAsJson%>;
  	</script>
	<script src="<%=UIBean.getStaticURL(secureRequest, secureResponse, "/js/admin/slotsettings.js") %>"></script>
</body>

<jsp:include page="/WEB-INF/admin/pages/includes/page_bottom.jsp" />