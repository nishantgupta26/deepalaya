package org.communitylibrary.utils;

/**
 * @author nishant.gupta
 *
 */
public class BookUtility {

    public static String convertToFlatISBN(String isbn) {
        if (isbn == null) {
            return isbn;
        }

        isbn = isbn.replaceAll("[^0-9X]", "");
        if (isbn.length() == 10 || isbn.length() == 13) {
            return isbn;
        }

        if (!(isbn.length() == 10 || isbn.length() == 13) && isbn.length() > 10) {
            if (isbn.length() > 13) {
                isbn = isbn.substring(isbn.length() - 13);
            } else {
                isbn = isbn.substring(isbn.length() - 10);
            }
            return isbn;
        }
        return null;
    }

    // public static void main(String[] args) {
    // System.out.println(convertToFlatISBN("0-9768573-0-8"));
    // System.out.println(convertToFlatISBN("0-7214-9546-X"));
    // System.out.println(convertToFlatISBN("978-81-89934-92-7"));
    // System.out.println(convertToFlatISBN("978-0-140-50296-1"));
    // System.out.println(convertToFlatISBN("10-81-7686-025-5"));
    // }

}
