package org.communitylibrary.utils;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author Nishant
 * 
 */
public class PaymentConfigStore {
    private static final String PROP_FILE = "payments.properties";
    private static Properties   s_prop    = null;
    private static Logger       s_logger  = Logger.getLogger(PaymentConfigStore.class);

    public static void initialise() {
        s_logger.info("Initialising properties file Payment Config Store");
        try {
            loadProperties();
        } catch (Exception e) {
            s_logger.fatal("Could not load Payment properties ", e);
        }
    }

    private static void loadProperties() throws IOException {
        s_prop = new Properties();
        s_prop.load(PaymentConfigStore.class.getClassLoader().getResourceAsStream(PROP_FILE));
        s_logger.info("payments properties file Config Store initialised");
    }

    public static String getStringValue(String key, String defaultVal) {
        try {
            String value = s_prop.getProperty(key);
            if (value != null) {
                return value;
            }
        } catch (Exception e) {
            s_logger.error("Error while getting property file value for the key: " + key);
        }
        return defaultVal;
    }
}
