package org.communitylibrary.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.communitylibrary.app.config.CacheConfiguration;
import org.communitylibrary.app.config.value.AbstractConfigValue;
import org.communitylibrary.app.config.value.BooleanValue;
import org.communitylibrary.app.config.value.IntValue;
import org.communitylibrary.app.config.value.StringValue;
import org.communitylibrary.db.WebsiteDBConnector;

/**
 * @author Nishant
 * 
 */
public class ConfigStore {
    private static final String                                 PROP_FILE  = "website_app.properties";
    private static Properties                                   s_prop     = null;
    private static Logger                                       s_logger   = Logger.getLogger(ConfigStore.class);
    private static Map<CacheConfiguration, AbstractConfigValue> s_configCache;
    private static final String                                 SQL_SELECT = "select * from config_store";
    private static final String                                 SQL_INSERT = "insert into config_store (config_id,config_value) values (?,?)";
    private static final String                                 SQL_UPDATE = "update config_store set config_value=? where config_id=?";

    public static void initialise() {
        s_logger.info("Initialising properties file Config Store");
        try {
            loadProperties();
            loadDBValues();
        } catch (Exception e) {
            s_logger.fatal("Could not load Website Application properties: ", e);
        }
    }

    private static void loadProperties() throws IOException {
        s_prop = new Properties();
        s_prop.load(ConfigStore.class.getClassLoader().getResourceAsStream(PROP_FILE));
        s_logger.info("properties file Config Store initialised");
    }

    /**
     * Returns the value associated with the FareSyncConfiguration from the
     * memory cache. In case the value type does not match the StringValue null
     * is returned without throwing any exception.
     * 
     * WARNING: In memory data could be out of sync with DB settings.
     * 
     * @param aCfg
     * @param defVal
     * @return
     */
    public static String getStringValue(CacheConfiguration aCfg, String defVal) {
        AbstractConfigValue cVal = getConfigValue(aCfg);
        if (cVal != null && cVal instanceof StringValue) {
            StringValue s = (StringValue) cVal;
            if (s.m_value == null) {
                return defVal;
            }
            return s.m_value;
        }
        return defVal;
    }

    /**
     * Returns the value associated with the FareSyncConfiguration from the
     * memory cache. In case the value type does not match the StringValue null
     * is returned without throwing any exception.
     * 
     * WARNING: In memory data could be out of sync with DB settings.
     * 
     * @param aCfg
     * @param defVal
     * @return
     */
    public static Integer getIntValue(CacheConfiguration aCfg, Integer defVal) {
        AbstractConfigValue cVal = getConfigValue(aCfg);
        if (cVal != null && cVal instanceof IntValue) {
            IntValue s = (IntValue) cVal;
            return s.m_value;
        }
        return defVal;
    }

    /**
     * Returns the value associated with the FareSyncConfiguration from the
     * memory cache. In case the value type does not match the StringValue null
     * is returned without throwing any exception.
     * 
     * WARNING: In memory data could be out of sync with DB settings.
     * 
     * @param aCfg
     * @param defVal
     * @return
     */
    public static Boolean getBooleanValue(CacheConfiguration aCfg, boolean defValue) {
        AbstractConfigValue cVal = getConfigValue(aCfg);
        if (cVal != null && cVal instanceof BooleanValue) {
            BooleanValue s = (BooleanValue) cVal;
            return s.m_value != null ? s.m_value : defValue;
        }
        return defValue;
    }

    /**
     * Returns the object with value for the given FareSyncConfiguration as per
     * the memory cache.
     * 
     * WARNING: In memory data could be out of sync with DB settings.
     * 
     * WARNING: The objects are returned as is and should not be edited by the
     * user. Any changes to the object will also updatethe memory cache
     * accordingly. Although these changes will not be reflected in the Database
     * and will go away on reloading the cache or restarting of the server.
     * 
     * @param aCfg
     * @return
     */
    public static AbstractConfigValue getConfigValue(CacheConfiguration aCfg) {
        try {
            return s_configCache.get(aCfg);
        } catch (Exception e) {
            s_logger.error("problem while getting value from the config store db", e);
        }
        return null;
    }

    public static String getStringValue(String key, String defaultVal) {
        try {
            String value = s_prop.getProperty(key);
            if (value != null) {
                return value;
            }
        } catch (Exception e) {
            s_logger.error("Error while getting property file value for the key: " + key);
        }
        return defaultVal;
    }

    /**
     * Reloads the server's memory cache with values from database.
     * 
     * @throws Exception
     */
    public static void loadDBValues() throws Exception {
        WebsiteDBConnector instace = WebsiteDBConnector.getStaticInstance();

        Map<CacheConfiguration, AbstractConfigValue> cMap = new HashMap<CacheConfiguration, AbstractConfigValue>();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = instace.getConnection(false);
            stmt = conn.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CacheConfiguration aCfg = CacheConfiguration.deserialize(rs.getString("CONFIG_ID"));
                if (aCfg == null) {
                    s_logger.error("Invalid ConfigID in DB: " + rs.getString("CONFIG_ID"));
                } else {
                    AbstractConfigValue cVal = aCfg.deserializeValue(rs.getString("CONFIG_VALUE"));
                    if (aCfg != null && cVal != null) {
                        cMap.put(aCfg, cVal);
                    }
                }
            }

        } finally {
            instace.closeAll(conn, stmt, rs, true);
        }

        s_configCache = new ConcurrentHashMap<CacheConfiguration, AbstractConfigValue>(cMap);
        s_logger.info("Reload ConfigStore successful");
    }

    /**
     * @return the s_configCache
     */
    public static Map<CacheConfiguration, AbstractConfigValue> getConfigCache() {
        return s_configCache;
    }

    /**
     * Updates the DB and executing server's memory cache values with the new
     * value. The function tries to update any existing value associated. In
     * case now matching rows exist in the DB an insert is made. cVal should not
     * be null. Currently there is no way to unset (delete) an existing
     * FareSyncConfiguration value set.
     * 
     * WARNING: Only current server's memory cache is updated. Later versions
     * might support use og JMX to reload all server's memory cache. For now
     * administrator has to login into each server and use the cache update
     * functionality provided.
     * 
     * @param aCfg
     * @param cVal
     * @throws Exception
     */
    public static void updateConfigValue(CacheConfiguration aCfg, AbstractConfigValue cVal) throws Exception {
        WebsiteDBConnector aCon = WebsiteDBConnector.getStaticInstance();
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = aCon.getConnection(true);

            stmt = conn.prepareStatement(SQL_UPDATE);
            aCon.setString(stmt, 1, cVal.serialize());
            aCon.setString(stmt, 2, aCfg.name());
            int rCnt = stmt.executeUpdate();

            if (rCnt != 1) {
                stmt.close();
                stmt = null;

                stmt = conn.prepareStatement(SQL_INSERT);
                aCon.setString(stmt, 1, aCfg.name());
                aCon.setString(stmt, 2, cVal.serialize());
                rCnt = stmt.executeUpdate();

                if (rCnt != 1) {
                    throw new RuntimeException("Invalid Row Update Count = " + rCnt);
                }
            }

            // Update value in memory
            s_configCache.put(aCfg, cVal);

        } finally {
            aCon.closeAll(conn, stmt, null, true);
        }
    }

}
