package org.communitylibrary.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * @author nishant.gupta
 */
public class JsonUtility {

    public static JsonElement getJsonDataFromRequest(CustomSecurityWrapperRequest request) {
        try {
            String jsonString = getJsonStringFromRequest(request);
            JsonParser parser = new JsonParser();
            return parser.parse(jsonString);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getJsonStringFromRequest(CustomSecurityWrapperRequest request) throws IOException {
        InputStream is = request.getInputStream();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buf = new byte[32];
        int r = 0;
        while (r >= 0) {
            r = is.read(buf);
            if (r >= 0)
                os.write(buf, 0, r);
        }

        return new String(os.toByteArray(), StandardCharsets.UTF_8);
    }

}
