package org.communitylibrary.admin.entity.page;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant
 *
 */
public class PageResource extends DBObject {
	
	private static final long serialVersionUID = -3731422806685644754L;

	private long              m_id;
    private String            m_title;
    private Date              m_creationDate;
    private Date              m_modificationDate;
    private String            m_pageText;
    private String            m_url;

    private static String     tableName        = "page_resources";

    public PageResource(ResultSet rs, boolean isGetPostText) throws Exception {
        WebsiteDBConnector s_staticInstance = WebsiteDBConnector.getStaticInstance();
        m_id = rs.getLong("id");
        m_title = rs.getString("title");
        m_creationDate = s_staticInstance.getTimeStampAsDate(rs, "creation_date");
        setModificationDate(s_staticInstance.getTimeStampAsDate(rs, "mod_date"));
        m_url = StringUtility.trimAndEmptyIsNull(rs.getString("url"));
        if (isGetPostText) {
            setPageText(StringUtility.trimAndEmptyIsNull(rs.getString("text_data")));
        }

    }

    public PageResource() {
    }

    public PageResource(long id) {
        m_id = id;
    }

    public static String getInsertQuery() {
        String query = "insert into " + tableName
                + " (title, creation_date, mod_date, text_data, url) values(?, ?, NOW(), ?, ?)";
        return query;
    }

    public boolean isValid() {
        if (m_url.length() > 100) {
            return false;
        }

        for (int i = 0; i < m_url.length(); i++) {
            char c = m_url.charAt(i);
            if (!((c >= 'a' && c <= 'z') || c == '-')) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected String getHistoryInsertQuery() {
        String historyTableName = tableName + "_hist";
        String query = "insert into " + historyTableName + " select * from " + tableName + " where id=?";
        return query;
    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        WebsiteDBConnector connector = WebsiteDBConnector.getStaticInstance();
        connector.setLongDefaultNull(stmt, 1, getId(), 0);
    }

    @Override
    protected void editTable(Connection conn) throws Exception {
        DBUtil.editPage(this, conn);
    }

    public String getEditQuery() {
        String query = "update " + tableName
                + " set title=?, mod_date=NOW(), text_data=?, url=? where id=?";
        return query;
    }

    public String getDeleteQuery() {
        return "delete from " + tableName + " where id=?";
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return m_url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        m_url = url;
    }

    public String getShareableURL() {
        if (m_url == null) {
            return String.valueOf(m_id);
        }

        return m_url;
    }

	public long getId() {
		return m_id;
	}

	public void setId(long id) {
		m_id = id;
	}

	public String getTitle() {
		return m_title;
	}

	public void setTitle(String title) {
		m_title = title;
	}

	public Date getCreationDate() {
		return m_creationDate;
	}

	public void setCreationDate(Date creationDate) {
		m_creationDate = creationDate;
	}

	public String getPageText() {
		return m_pageText;
	}

	public void setPageText(String pageText) {
		m_pageText = pageText;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return m_modificationDate;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		m_modificationDate = modificationDate;
	}
}
