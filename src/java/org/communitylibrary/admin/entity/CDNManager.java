package org.communitylibrary.admin.entity;

public enum CDNManager {
    Cloudinary("C"), ;

    private String m_code;

    private CDNManager(String code) {
        m_code = code;
    }

    public String getCode() {
        return m_code;
    }

    public static CDNManager getCDN(String cdnType) {
        if (cdnType == null || (cdnType = cdnType.trim()).isEmpty()) {
            return null;
        }

        for (CDNManager manager : CDNManager.values()) {
            if (manager.getCode().equals(cdnType)) {
                return manager;
            }
        }
        return null;
    }

}
