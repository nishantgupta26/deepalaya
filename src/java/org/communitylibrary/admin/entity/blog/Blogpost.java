package org.communitylibrary.admin.entity.blog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.data.user.User;
import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant
 *
 */
public class Blogpost extends DBObject {

    private static final long serialVersionUID = 4557253558418626939L;

    private long              m_id;
    private String            m_title;
    private String            m_author;
    private User              m_postedBy;
    private Date              m_postDate;
    private Date              m_publishDate;
    private String            m_postText;
    private String            m_url;

    private static String     tableName        = "blogposts";

    public Blogpost(ResultSet rs, boolean isGetPostText) throws Exception {
        WebsiteDBConnector s_staticInstance = WebsiteDBConnector.getStaticInstance();
        m_id = rs.getLong("id");
        m_title = rs.getString("title");
        m_author = rs.getString("author");
        m_postedBy = new User(rs.getInt("postedby_id"));
        m_postDate = s_staticInstance.getTimeStampAsDate(rs, "post_date");
        m_publishDate = s_staticInstance.getTimeStampAsDate(rs, "publish_date");
        m_url = StringUtility.trimAndEmptyIsNull(rs.getString("url"));
        if (isGetPostText) {
            setPostText(StringUtility.trimAndEmptyIsNull(rs.getString("text_data")));
        }

    }

    public Blogpost() {
    }

    public Blogpost(long id) {
        m_id = id;
    }

    public long getId() {
        return m_id;
    }

    public void setId(long id) {
        m_id = id;
    }

    public String getTitle() {
        return m_title;
    }

    public void setTitle(String title) {
        m_title = title;
    }

    public String getAuthor() {
        return m_author;
    }

    public void setAuthor(String author) {
        m_author = author;
    }

    public User getPostedBy() {
        return m_postedBy;
    }

    public void setPostedBy(User postedBy) {
        m_postedBy = postedBy;
    }

    public Date getPostDate() {
        return m_postDate;
    }

    public void setPostDate(Date post) {
        m_postDate = post;
    }

    public Date getPublishDate() {
        return m_publishDate;
    }

    public void setPublishDate(Date publishDate) {
        m_publishDate = publishDate;
    }

    /**
     * @return the postText
     */
    public String getPostText() {
        return m_postText;
    }

    /**
     * @param postText the postText to set
     */
    public void setPostText(String postText) {
        m_postText = postText;
    }

    public static String getInsertQuery() {
        String query = "insert into " + tableName
                + " (title, author, postedby_id, post_date, publish_date, text_data, url) values(?, ?, ?, ?, ?, ?, ?)";
        return query;
    }

    public boolean isValid() {
        if (m_url.length() > 100) {
            return false;
        }

        for (int i = 0; i < m_url.length(); i++) {
            char c = m_url.charAt(i);
            if (!((c >= 'a' && c <= 'z') || c == '-')) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected String getHistoryInsertQuery() {
        String historyTableName = tableName + "_hist";
        String query = "insert into " + historyTableName + " select * from " + tableName + " where id=?";
        return query;
    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        WebsiteDBConnector connector = WebsiteDBConnector.getStaticInstance();
        connector.setLongDefaultNull(stmt, 1, getId(), 0);
    }

    @Override
    protected void editTable(Connection conn) throws Exception {
        DBUtil.editBlogpost(this, conn);
    }

    public String getEditQuery() {
        String query = "update " + tableName
                + " set title=?, author=?, postedby_id=?, post_date=?, text_data=?, url=? where id=?";
        return query;
    }

    public String getDeleteQuery() {
        return "delete from " + tableName + " where id=?";
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return m_url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        m_url = url;
    }

    public String getShareableURL() {
        if (m_url == null) {
            return String.valueOf(m_id);
        }

        return m_url;
    }
}
