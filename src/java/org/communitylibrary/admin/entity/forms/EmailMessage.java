package org.communitylibrary.admin.entity.forms;

import java.util.HashSet;
import java.util.Set;

/**
 * @author nishant.gupta
 *
 */
public class EmailMessage {

    private static final String s_tableName = "msg_store";
    private Set<String>         m_to;
    private Set<String>         m_cc;
    private String              m_subject;
    private String              m_content;
    private String              m_from;

    /**
     * @return the content
     */
    public String getContent() {
        return m_content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        m_content = content;
    }

    public void addTo(String email) {
        if (email == null) {
            return;
        }

        if (m_to == null) {
            m_to = new HashSet<String>();
        }

        m_to.add(email);

    }

    public void addCC(String email) {
        if (email == null) {
            return;
        }

        if (m_cc == null) {
            m_cc = new HashSet<String>();
        }

        m_cc.add(email);
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return m_subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        m_subject = subject;
    }

    public void setFrom(String fromEmail) {
        m_from = fromEmail;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return m_from;
    }

    /**
     * @return the to
     */
    public Set<String> getTo() {
        return m_to;
    }

    /**
     * @return the cc
     */
    public Set<String> getCc() {
        return m_cc;
    }

    public static String getSaveEmailQuery() {
        String query = "insert into "
                + s_tableName
                + " (email_to, email_cc, email_from, subject, content, is_success, error_logs, gen_ts) values(?, ?, ?, ?, ?, ?, ?, NOW())";
        return query;
    }

}
