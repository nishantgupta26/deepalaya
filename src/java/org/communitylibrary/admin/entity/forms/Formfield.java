package org.communitylibrary.admin.entity.forms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.communitylibrary.app.config.ElementType;
import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.ui.navigation.WebsiteException;
import org.communitylibrary.ui.navigation.WebsiteExceptionType;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public class Formfield extends DBObject {

    private static final long serialVersionUID        = -8160893724156979272L;

    private static String     s_tableName             = "form_fields";
    private static String     s_formResponseTableName = "response_answers";

    private int               m_id;

    /**
     * foreign key with the form
     */
    private int               m_formId;
    private boolean           m_isMandatory;
    private ElementType       m_type;
    private String            m_questionText;
    private List<FormOption>  m_formOptions;

    /**
     * Regex to validate the value in the field
     */
    private String            m_validationRegex;

    private String            m_answer;

    public Formfield(JsonObject fieldObj, boolean isResponse) throws Exception {
        populateFields(fieldObj, isResponse);
    }

    public Formfield(ResultSet rs, int k) throws Exception {
        int i = 1;
        m_id = rs.getInt(k + i++);
        m_formId = rs.getInt(k + i++);
        m_questionText = rs.getString(k + i++);
        m_isMandatory = "Y".equalsIgnoreCase(rs.getString(k + i++));
        m_type = ElementType.getFromCode(rs.getString(k + i++));
        m_validationRegex = StringUtility.trimAndEmptyIsNull(rs.getString(k + i++));

    }

    public Formfield() {
    }

    private void populateFields(JsonObject fieldObj, boolean isResponse) throws Exception {
        JsonElement isMandatoryElement = fieldObj.get("isMandatory");
        setMandatory(isMandatoryElement == null ? false : isMandatoryElement.getAsBoolean());
        setQuestionText(fieldObj.get("question").getAsString());
        ElementType type = ElementType.parseFromJson(fieldObj.get("type").getAsJsonObject());
        if (type == null) {
            throw new WebsiteException(WebsiteExceptionType.INVALID_DATA);
        }

        setType(type);
        switch (type) {
        case TEXTBOX:
        case TEXTAREA:
            JsonElement regexElement = fieldObj.get("regexString");
            if (regexElement != null) {
                setValidationRegex(regexElement.getAsString());
            }
            break;
        case CHECKBOX:
        case RADIOBOX:
            JsonArray optionsArray = fieldObj.get("options").getAsJsonArray();
            for (int i = 0; i < optionsArray.size(); i++) {
                JsonObject optionObj = (JsonObject) optionsArray.get(i);
                FormOption option = new FormOption(optionObj);
                addFormOption(option);
            }

            break;
        }

        if (isResponse) {
            JsonElement answerElement = fieldObj.get("answer");
            JsonElement answerOtherElement = fieldObj.get("otheranswer");
            List<String> answerText = new ArrayList<String>();
            if (answerElement != null || answerOtherElement != null) {
                switch (type) {
                case TEXTBOX:
                case TEXTAREA:
                    answerText.add(answerElement.getAsString());
                    break;
                case CHECKBOX: {
                    JsonObject answerObject = null;
                    if (answerElement != null) {
                        answerObject = (JsonObject) answerElement;
                    }
                    for (FormOption option : getFormOptions()) {
                        if (option.isOtherOption()) {
                            if (answerOtherElement != null) {
                                answerText.add(answerOtherElement.getAsString());
                            }
                        } else {
                            if (answerObject != null) {
                                JsonElement elem = answerObject.get(option.getOptionId());
                                if (elem != null && elem.getAsBoolean()) {
                                    answerText.add(option.getOptionText());
                                }
                            }
                        }
                    }
                }
                    break;
                case RADIOBOX: {
                    if (answerOtherElement != null) {
                        answerText.add(answerOtherElement.getAsString());
                    } else if (answerElement != null) {
                        answerText.add(answerElement.getAsString());
                    } else if (!isMandatory()) {
                        throw new RuntimeException("Answer missing for mandatory field: form: " + m_formId + ", text: "
                                + m_questionText);
                    }
                }
                    break;
                }
            }

            if (answerText.isEmpty()) {
                if (isMandatory()) {
                    throw new RuntimeException("Answer missing for mandatory field: form: " + m_formId + ", text: "
                            + m_questionText);
                }
            } else {
                setAnswer(StringUtility.join(answerText, ","));
            }
        }

    }

    private void addFormOption(FormOption option) {
        if (m_formOptions == null) {
            m_formOptions = new ArrayList<FormOption>();
        }

        m_formOptions.add(option);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.communitylibrary.data.entity.DBObject#editTable(java.sql.Connection)
     */
    @Override
    protected void editTable(Connection conn) throws Exception {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.communitylibrary.data.entity.DBObject#fillHistoryTableStmt(java.sql
     * .PreparedStatement)
     */
    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.communitylibrary.data.entity.DBObject#getHistoryInsertQuery()
     */
    @Override
    protected String getHistoryInsertQuery() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return the id
     */
    public int getId() {
        return m_id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        m_id = id;
    }

    /**
     * @return the isMandatory
     */
    public boolean isMandatory() {
        return m_isMandatory;
    }

    /**
     * @param isMandatory the isMandatory to set
     */
    public void setMandatory(boolean isMandatory) {
        m_isMandatory = isMandatory;
    }

    /**
     * @return the questionText
     */
    public String getQuestionText() {
        return m_questionText;
    }

    /**
     * @param questionText the questionText to set
     */
    public void setQuestionText(String questionText) {
        m_questionText = questionText;
    }

    /**
     * @return the type
     */
    public ElementType getType() {
        return m_type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ElementType type) {
        m_type = type;
    }

    /**
     * @return the validationRegex
     */
    public String getValidationRegex() {
        return m_validationRegex;
    }

    /**
     * @param validationRegex the validationRegex to set
     */
    public void setValidationRegex(String validationRegex) {
        m_validationRegex = validationRegex;
    }

    /**
     * @return the formOptions
     */
    public List<FormOption> getFormOptions() {
        return m_formOptions;
    }

    /**
     * @param formOptions the formOptions to set
     */
    public void setFormOptions(List<FormOption> formOptions) {
        m_formOptions = formOptions;
    }

    public static String getInsertQuery() {
        String query = "insert into "
                + s_tableName
                + " (id, form_id, is_mandatory, field_type, question_text, validation_regex, gen_ts) values(?, ?, ?, ?, ?, ?, NOW())";
        return query;
    }

    /**
     * @return the formId
     */
    public int getFormId() {
        return m_formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(int formId) {
        m_formId = formId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Formfield)) {
            return false;
        }

        Formfield field = (Formfield) obj;

        return (getId() == field.getId()) && (getFormId() == field.getFormId());
    }

    @Override
    public int hashCode() {
        return 31 * getId();
    }

    public JsonObject convertToJson() {
        JsonObject field = new JsonObject();

        field.addProperty("id", getId());
        field.addProperty("isMandatory", isMandatory());
        field.add("type", m_type.convertToJsonObject());
        field.addProperty("question", getQuestionText());

        if (m_formOptions != null && !m_formOptions.isEmpty()) {
            JsonArray options = new JsonArray();
            for (FormOption option : m_formOptions) {
                JsonObject optionObj = option.convertToJson();
                if (optionObj != null) {
                    options.add(optionObj);
                }
            }

            field.add("options", options);
        }

        return field;
    }

    /**
     * @return the answer
     */
    public String getAnswer() {
        return m_answer;
    }

    /**
     * @param answer the answer to set
     */
    public void setAnswer(String answer) {
        m_answer = answer;
    }

    public static String getSaveResponseQuery() {
        String query = "insert into " + s_formResponseTableName + " (field_id, response_id, response) values(?, ?, ?)";
        return query;
    }

    public JsonObject convertToQuestionAnswerJson() {
        JsonObject object = new JsonObject();
        object.addProperty("question", getQuestionText());
        String answer = getAnswer();
        object.addProperty("answer", answer);
        return object;
    }

}
