package org.communitylibrary.admin.entity.forms;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public class FormResponse {

    private Form m_form;

    public int getResponseCount() {
        return m_responseCount;
    }

    public void setResponseCount(int responseCount) {
        m_responseCount = responseCount;
    }

    public Form getForm() {
        return m_form;
    }

    private int m_responseCount;

    /**
     * converts a list of forms into json string of id, title arrays
     * 
     * @param formResponses
     * @return
     */
    public static String convertToDisplayJson(List<FormResponse> formResponses) {
        JsonArray array = new JsonArray();

        for (FormResponse formResponse : formResponses) {
            Form form = formResponse.getForm();
            JsonObject object = form.convertFormToDisplayJson();
            if (object != null) {
                object.addProperty("count", formResponse.getResponseCount());
                array.add(object);
            }
        }

        return array.toString();
    }

    public void setForm(Form form) {
        m_form = form;
    }

    public static JsonObject convertToFormResponseJson(Map<Form, Map<String, List<Formfield>>> formResponses) {
        if (formResponses == null || formResponses.isEmpty()) {
            return null;
        }

        JsonObject formResponseJson = new JsonObject();
        Iterator<Form> iterator = formResponses.keySet().iterator();
        if (iterator.hasNext()) {
            Form form = iterator.next();
            JsonObject formJson = form.convertFormToDisplayJson();
            formResponseJson.add("form", formJson);

            Map<String, List<Formfield>> responseIdToFieldResponse = formResponses.get(form);
            JsonArray responses = new JsonArray();
            for (String responseId : responseIdToFieldResponse.keySet()) {
                List<Formfield> fields = responseIdToFieldResponse.get(responseId);
                JsonObject response = new JsonObject();
                response.addProperty("id", responseId);

                JsonArray fieldResponses = new JsonArray();
                for (Formfield field : fields) {
                    JsonObject fieldResponse = field.convertToQuestionAnswerJson();
                    fieldResponses.add(fieldResponse);
                }
                response.add("fields", fieldResponses);
                responses.add(response);
            }

            formResponseJson.add("responses", responses);
        }
        return formResponseJson;
    }

}
