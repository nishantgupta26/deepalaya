package org.communitylibrary.admin.entity.forms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.communitylibrary.data.entity.DBObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class FormOption extends DBObject {

    private static final long serialVersionUID = 3233419521397925467L;
    private static String     s_tableName      = "form_options";
    private String            m_optionId;

    /**
     * foreign key to the FormField to which this option is associated
     */
    private int               m_fieldId;

    private String            m_optionText;

    private boolean           m_isOtherOption;

    public FormOption(JsonObject optionObj) {
        populateFields(optionObj);
    }

    public FormOption(ResultSet rs, int k) throws Exception {
        int i = 1;
        m_optionId = rs.getString(k + i++);
        m_fieldId = rs.getInt(k + i++);
        m_optionText = rs.getString(k + i++);
        m_isOtherOption = "Y".equalsIgnoreCase(rs.getString(k + i++));
    }

    private void populateFields(JsonObject optionObj) {
        JsonElement jsonElement = optionObj.get("id");
        
        if (jsonElement != null) {
            setOptionId(jsonElement.getAsString());
        }
        
        setOptionText(optionObj.get("text").getAsString());
        JsonElement otherOptionElement = optionObj.get("isOtherField");
        setOtherOption(otherOptionElement == null ? false : otherOptionElement.getAsBoolean());
    }

    @Override
    protected void editTable(Connection conn) throws Exception {

    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected String getHistoryInsertQuery() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return the optionId
     */
    public String getOptionId() {
        return m_optionId;
    }

    /**
     * @param optionId the optionId to set
     */
    public void setOptionId(String optionId) {
        m_optionId = optionId;
    }

    /**
     * @return the fieldId
     */
    public int getFieldId() {
        return m_fieldId;
    }

    /**
     * @param fieldId the fieldId to set
     */
    public void setFieldId(int fieldId) {
        m_fieldId = fieldId;
    }

    /**
     * @return the optionText
     */
    public String getOptionText() {
        return m_optionText;
    }

    /**
     * @param optionText the optionText to set
     */
    public void setOptionText(String optionText) {
        m_optionText = optionText;
    }

    /**
     * @return the isOtherOption
     */
    public boolean isOtherOption() {
        return m_isOtherOption;
    }

    /**
     * @param isOtherOption the isOtherOption to set
     */
    public void setOtherOption(boolean isOtherOption) {
        m_isOtherOption = isOtherOption;
    }

    public static String getInsertQuery() {
        String query = "insert into " + s_tableName
                + " (option_id, field_id, option_text, is_other, gen_ts) values(?, ?, ?, ?, NOW())";
        return query;
    }

    public JsonObject convertToJson() {

        JsonObject optionObj = new JsonObject();
        optionObj.addProperty("id", getOptionId());
        optionObj.addProperty("text", getOptionText());
        optionObj.addProperty("isOtherField", isOtherOption());

        return optionObj;
    }

}
