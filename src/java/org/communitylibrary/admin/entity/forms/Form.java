package org.communitylibrary.admin.entity.forms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public class Form extends DBObject {

    private static final long serialVersionUID        = -6087643647741128556L;

    private int               m_id;

    private String            m_title;

    private String            m_url;

    private String            m_emails;

    private List<Formfield>   m_formFields;

    private static String     s_tableName             = "forms";
    private static String     s_formResponseTableName = "form_responses";

    public Form(JsonObject requestJson, boolean isResponse) throws Exception {
        populateFormFromRequestJson(requestJson, isResponse);
    }

    public Form() {

    }

    public Form(ResultSet rs, int k) throws Exception {
        int i = 1;
        m_id = rs.getInt(k + i++);
        m_title = rs.getString(k + i++);
        m_url = rs.getString(k + i++);
        m_emails = rs.getString(k + i++);

    }

    private void populateFormFromRequestJson(JsonObject requestJson, boolean isResponse) throws Exception {

        JsonElement idElement = requestJson.get("id");
        if (idElement != null) {
            setId(idElement.getAsInt());
        }
        setTitle(requestJson.get("title").getAsString());
        setUrl(requestJson.get("url").getAsString());
        setEmails(requestJson.get("emails").getAsString());

        JsonArray fieldsArray = requestJson.get("fields").getAsJsonArray();

        for (int i = 0; i < fieldsArray.size(); i++) {
            JsonObject fieldElement = (JsonObject) fieldsArray.get(i);
            Formfield field = new Formfield(fieldElement, isResponse);
            field.setId(i + 1);
            addFormField(field);
        }
    }

    private void addFormField(Formfield field) {
        if (m_formFields == null) {
            m_formFields = new ArrayList<Formfield>();
        }

        m_formFields.add(field);

    }

    @Override
    protected void editTable(Connection conn) throws Exception {

    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {

    }

    @Override
    protected String getHistoryInsertQuery() {
        return null;
    }

    /**
     * @return the id
     */
    public int getId() {
        return m_id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        m_id = id;
    }

    /**
     * @return the name
     */
    public String getTitle() {
        return m_title;
    }

    /**
     * @param name the name to set
     */
    public void setTitle(String name) {
        m_title = name;
    }

    /**
     * @return the formFields
     */
    public List<Formfield> getFormFields() {
        return m_formFields;
    }

    /**
     * @param formFields the formFields to set
     */
    public void setFormFields(List<Formfield> formFields) {
        m_formFields = formFields;
    }

    public void setUrl(String url) {
        m_url = url;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return m_url;
    }

    public boolean isValid() {
        if (m_url.length() > 100) {
            return false;
        }

        for (int i = 0; i < m_url.length(); i++) {
            char c = m_url.charAt(i);
            if (!((c >= 'a' && c <= 'z') || c == '-')) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return the emails
     */
    public String getEmails() {
        return m_emails;
    }

    /**
     * @param emails the emails to set
     */
    public void setEmails(String emails) {
        m_emails = emails;
    }

    public static String getInsertQuery() {
        String query = "insert into " + s_tableName + " (title, url, emails, gen_ts) values(?, ?, ?, NOW())";
        return query;
    }

    public static String getFormAndFieldsFetchSQL(boolean isID) {
        StringBuilder sql = new StringBuilder(
                "SELECT f.*, fi.*, fo.* FROM forms f LEFT JOIN form_fields fi ON f.id = fi.form_id LEFT JOIN form_options fo ON fi.id = fo.field_id where ");
        if (isID) {
            sql.append("f.id=?");
        } else {
            sql.append("f.url=?");
        }

        return sql.toString();
    }

    public JsonObject convertToJson() {
        JsonObject form = new JsonObject();

        form.addProperty("id", getId());
        form.addProperty("title", getTitle());
        form.addProperty("emails", getEmails());
        form.addProperty("url", getUrl());

        JsonArray fields = new JsonArray();
        for (Formfield field : getFormFields()) {
            JsonObject fieldObject = field.convertToJson();
            if (fieldObject != null) {
                fields.add(fieldObject);
            }
        }

        if (fields.size() > 0) {
            form.add("fields", fields);
        }
        return form;
    }

    public static String getDeleteQuery() {
        return "delete from " + s_tableName + " where id=?";
    }

    public static String getFormResponseSaveQuery() {
        String query = "insert into " + s_formResponseTableName + " (form_id, gen_ts) values(?, NOW())";
        return query;
    }

    public EmailMessage createResponseEmail() {
        EmailMessage message = new EmailMessage();
        List<String> recipients = StringUtility.splitToStringList(getEmails(), ",");
        for (String email : recipients) {
            message.addTo(email.trim());
        }

        message.setSubject("Response received for Form : " + getTitle());

        StringBuilder content = new StringBuilder("We have received the following answers for the form\n");
        content.append("\n\r");

        for (Formfield field : getFormFields()) {
            content.append("Question : ").append(field.getQuestionText()).append(",");
            content.append("\n\r");
            content.append("Answer : ").append(field.getAnswer() == null ? "Not Answered" : field.getAnswer());
            content.append("\n\r");
            content.append("\n\r");
        }

        content.append("\n\r");
        content.append("Regards,");
        content.append("\n\r");
        content.append("The Community Library Project");

        message.setContent(content.toString());
        return message;
    }

    public JsonObject convertFormToDisplayJson() {
        JsonObject formObj = new JsonObject();
        formObj.addProperty("id", getId());
        formObj.addProperty("title", getTitle());
        return formObj;
    }

    @Override
    public int hashCode() {
        return 31 * getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Form)) {
            return false;
        }

        Form form = (Form) obj;

        return form.getId() == getId();
    }
}
