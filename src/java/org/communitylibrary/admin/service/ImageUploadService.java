package org.communitylibrary.admin.service;

import java.io.File;

import org.communitylibrary.admin.entity.CDNManager;
import org.communitylibrary.admin.service.impl.CloudinaryService;
import org.communitylibrary.utils.ConfigStore;

public abstract class ImageUploadService {

    public static ImageUploadService getService() {
        String cdnType = ConfigStore.getStringValue("CDN_TYPE", String.valueOf(CDNManager.Cloudinary.getCode()));
        CDNManager manager = CDNManager.getCDN(cdnType);
        if (manager == null) {
            throw new RuntimeException("No CDN found");
        }

        switch (manager) {
        case Cloudinary:
            return new CloudinaryService();
        }

        throw new RuntimeException("No appropriate handler found");
    }

    public abstract String uploadFile(File file, String fileName);

}
