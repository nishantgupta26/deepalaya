package org.communitylibrary.admin.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.communitylibrary.admin.service.ImageUploadService;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

/**
 * @author nishant.gupta
 *
 */
public class CloudinaryService extends ImageUploadService {

    private static Logger s_logger = Logger.getLogger(CloudinaryService.class);

    @Override
    public String uploadFile(File file, String fileName) {

        Map<String, String> config = new HashMap<String, String>();

        config.put("cloud_name", "dlxjsuwkm");
        config.put("api_key", "157678661817657");
        config.put("api_secret", "PI3HWKOhURXK4mwnrRgsyHtdcnc");
        Cloudinary cloudinary = new Cloudinary(config);
        // Cloudinary cloudinary = new
        // Cloudinary("cloudinary://157678661817657:PI3HWKOhURXK4mwnrRgsyHtdcnc@dlxjsuwkm");
        try {
            String publicName = fileName != null ? fileName : file.getName();
            Map uploadResult = cloudinary.uploader().upload(file, ObjectUtils.asMap("public_id", publicName));
            // Iterator iterator = uploadResult.keySet().iterator();
            // s_logger.debug("upload result");
            // while (iterator.hasNext()) {
            // Object next = iterator.next();
            // s_logger.debug("key: " + next);
            // s_logger.debug("Value: " + uploadResult.get(next));
            // }
            String generatedURL = (String) uploadResult.get("url");
            s_logger.info("url generated for the uploaded image: " + generatedURL);

            return generatedURL;
        } catch (Exception e) {
            s_logger.error("Error while uploading the file to cloudinary", e);
            throw new RuntimeException("Could not upload file");
        }
    }

}
