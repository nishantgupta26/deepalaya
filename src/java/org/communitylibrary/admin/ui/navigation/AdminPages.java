package org.communitylibrary.admin.ui.navigation;

import java.net.URLEncoder;

import org.communitylibrary.ui.beans.UIBean;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;

public enum AdminPages {
    /*
     * Common pages
     */
    NO_PAGE("", false),
    // Redirect request
    REDIRECT_URL("", false),
    // Provide JSP page path from base
    JSP_PAGE_PATH("", false),

    // HOME_PAGE("admin.jsp", false),
    // HOME_PAGE("settings.jsp", false), SETTINGS_PAGE("settings.jsp", false),

    LOGIN_PAGE("login.jsp", false),

    ERROR_PAGE("error.jsp", false),

    MANAGE_USERS_PAGE("manage_users_temp.jsp", false),

    // EVENTS_PAGE("events.jsp", false),

    MANAGE_LOGINS_PAGE("manage_user_logins.jsp", false),

    // RESOURCES_PAGE("resources.jsp", false),

    // BLOG_PAGE("blogs.jsp", false),

    // NEW_BLOG("newblog.jsp", false),

    // PAGERESOURCES_PAGE("pageresources.jsp", false),

    // NEW_PAGE("newpage.jsp", false),

    // FORM_PAGE("forms.jsp", false),

    // NEW_FORM("newform.jsp", false),

    // FORM_RESPONSES("form_responses.jsp", false),

    DONATIONS_PAGE("donationslisting.jsp", false),

    // HOMEPAGELINKSPAGE("homepagelinksangular.jsp", false),

    DASLOTSPAGE("slots.jsp", false),
    DASLOTSETTINGPAGE("slotsettings.jsp", false),

    DONATION_RECEIPT_PAGE("donationreceipt.jsp", false),

    ;

    /*
     * Static Variables and Functions
     */
    public static final String BASE_PATH = "/WEB-INF/admin/pages/";
    private static final String REDIRECT_URL_ATTR = "REDIRECT_URL_ATTR";
    private static final String JSP_PAGE_PATH_ATTR = "VISIBLE_TEMP_PAGE_ATTR";

    /*
     * Object Variables and Functions
     */
    private String m_jspName = "";
    private boolean m_cachePage;

    AdminPages(String jspName, boolean cache) {
        m_jspName = jspName;
        m_cachePage = cache;
    }

    public boolean isCachePage() {
        return m_cachePage;
    }

    public String getJspFilePath() {
        return m_jspName;
    }

    public static AdminPages setRedirectUrlInRequest(CustomSecurityWrapperRequest request, String url) {
        request.setAttribute(REDIRECT_URL_ATTR, url);
        return AdminPages.REDIRECT_URL;
    }

    public static AdminPages setRedirectUrlInRequest(CustomSecurityWrapperRequest request, String url,
                                                     String fwdUrl) {
        try {
            fwdUrl = URLEncoder.encode(fwdUrl, "UTF-8");
        } catch (Exception e) {
        }
        if (!url.contains("?")) {
            request.setAttribute(REDIRECT_URL_ATTR, url + "?" + UIBean.FWD_URL_PARAM + "=" + fwdUrl);
        } else {
            request.setAttribute(REDIRECT_URL_ATTR, url + "&" + UIBean.FWD_URL_PARAM + "=" + fwdUrl);
        }
        return AdminPages.REDIRECT_URL;
    }

    public static String getRedirectUrlInRequest(CustomSecurityWrapperRequest request) {
        return (String) request.getAttribute(REDIRECT_URL_ATTR);
    }

    public String getActualJspFilePath(CustomSecurityWrapperRequest request) {
        return BASE_PATH + getJspFilePath();
    }

    public static AdminPages setJspPagePathInRequest(CustomSecurityWrapperRequest request, String jspPath) {
        request.setAttribute(JSP_PAGE_PATH_ATTR, jspPath);
        return AdminPages.JSP_PAGE_PATH;
    }

    public static String getJspPagePathInRequest(CustomSecurityWrapperRequest request) {
        return (String) request.getAttribute(JSP_PAGE_PATH_ATTR);
    }

    public static AdminPages setRedirectUrlInRequest(CustomSecurityWrapperRequest request,
                                                     CustomSecurityWrapperResponse response, AdminActions action) {
        return setRedirectUrlInRequest(request, action.getActionURL(request, response, null, false));
    }
}
