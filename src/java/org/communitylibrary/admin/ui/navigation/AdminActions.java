package org.communitylibrary.admin.ui.navigation;

import org.communitylibrary.admin.ui.beans.AdminBean;
import org.communitylibrary.admin.ui.beans.SettingsBean;
import org.communitylibrary.admin.ui.beans.UploadBean;
import org.communitylibrary.data.user.User;
import org.communitylibrary.data.user.UserRoles;
import org.communitylibrary.logger.Logger;
import org.communitylibrary.ui.beans.WebsiteBean;
import org.communitylibrary.ui.navigation.WebsiteActions;
import org.communitylibrary.ui.navigation.WebsiteNavigation;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;

/**
 * @author nishant.gupta
 */
public enum AdminActions {
    HOME("home", AdminPages.LOGIN_PAGE, UserRoles.getAdminPageLoggedInRoles()) {
        @Override
        protected AdminPages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                     String[] queryParams, boolean isPost) {
            return AdminBean.getHomePage(request, response, queryParams, this);
        }
    },

    // SWITCHTOADMIN("adminconsole", AdminPages.HOME_PAGE,
    // UserRoles.getAdminPageLoggedInRoles()) {
    // @Override
    // protected AdminPages execute(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return AdminBean.getHomePage(request, response, queryParams, this);
    // }
    // },

    // SETTINGS("settings", AdminPages.SETTINGS_PAGE) {
    // @Override
    // protected AdminPages execute(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return SettingsBean.manageSettings(request, response, queryParams);
    // }
    // },

    // CDNUpload("upload", AdminPages.NO_PAGE) {
    // @Override
    // protected AdminPages execute(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return UploadBean.upload(request, response, queryParams);
    // }
    // },

    // EVENTS("events", AdminPages.EVENTS_PAGE) {
    // @Override
    // protected AdminPages execute(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return SettingsBean.manageEvents(request, response, queryParams);
    // }
    // },
    //
    // RESOURCES("resources", AdminPages.RESOURCES_PAGE) {
    // @Override
    // protected AdminPages execute(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return SettingsBean.manageResources(request, response, queryParams);
    // }
    // },

    LOGIN("login", AdminPages.LOGIN_PAGE, UserRoles.getNoLoggedInRoles()) {
        @Override
        public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                        String[] queryParams, boolean isPost) throws Exception {
            return AdminBean.login(request, response, isPost);
        }
    },

    LOGOUT("logout", AdminPages.LOGIN_PAGE) {
        @Override
        public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                        String[] queryParams, boolean isPost) throws Exception {
            return AdminBean.logout(request, response);
        }
    },

    MANAGE_USERS("manage_users", AdminPages.MANAGE_USERS_PAGE) {
        @Override
        public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                        String[] queryParams, boolean isPost) throws Exception {
            return WebsiteBean.getManageUsersPage(request, response, queryParams);
        }
    },

    MANAGE_USER_LOGINS("manage_user_logins", AdminPages.MANAGE_LOGINS_PAGE) {
        @Override
        public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                        String[] queryParams, boolean isPost) throws Exception {
            return WebsiteBean.getManageUserLoginsPage(request, response, queryParams);
        }
    },

    // BLOGS("get_all_blogs", AdminPages.MANAGE_LOGINS_PAGE) {
    // @Override
    // public AdminPages executeAction(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return AdminBean.getBlogsPage(request, response, queryParams);
    // }
    // },
    //
    // PAGES("get_all_pages", AdminPages.NEW_PAGE) {
    // @Override
    // public AdminPages executeAction(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return AdminBean.getDocsPage(request, response, queryParams);
    // }
    // },
    //
    // FORMS("get_all_forms", AdminPages.FORM_PAGE) {
    // @Override
    // public AdminPages executeAction(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return AdminBean.getFormsPage(request, response, queryParams);
    // }
    // },
    //
    DONATIONS("get_all_donations", AdminPages.DONATIONS_PAGE, UserRoles.getAdminRoles()) {
        @Override
        public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                        String[] queryParams, boolean isPost) throws Exception {
            return AdminBean.getDonationsPage(request, response, queryParams);
        }
    },
    //
    // HOMEPAGELINKS("hplinks", AdminPages.HOMEPAGELINKSPAGE,
    // UserRoles.getSuprressedPrivilegeRoles()) {
    // @Override
    // public AdminPages executeAction(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response,
    // String[] queryParams, boolean isPost) throws Exception {
    // return AdminBean.getHomePageLinksPage(request, response, queryParams);
    // }
    // },

    /**
     * Allocating and managing slots for a day
     */
    DASLOTS("slots", AdminPages.DASLOTSPAGE, UserRoles.getSuprressedPrivilegeRoles()) {
        @Override
        public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                        String[] queryParams, boolean isPost) throws Exception {
            return AdminBean.getManageSlotsPage(request, response, queryParams);
        }
    },

    /**
     * Allocating and managing slots for a day
     *
     */
    DASLOTSETTINGS("slotsettings", AdminPages.DASLOTSETTINGPAGE, UserRoles.getSuprressedPrivilegeRoles()) {
        @Override
        public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                        String[] queryParams, boolean isPost) throws Exception {
            return AdminBean.getManageSlotSettingsPage(request, response, queryParams);
        }
    },

    AJAX_GET_MEMBER("get_members", AdminPages.NO_PAGE, UserRoles.getSuprressedPrivilegeRoles()) {
        @Override
        public AdminPages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                  String[] queryParams, boolean isPost) throws Exception {
            return AdminBean.getAjaxMembers(request, response, queryParams);
        }
    },

    ;

    /*
     * Object Variables and Functions
     */
    private String m_displayName;
    private UserRoles[] m_defaultAllowedRoles;
    private AdminPages m_defaultUIPage;

    AdminActions(String displayName, AdminPages page) {
        m_displayName = displayName;
        m_defaultUIPage = page;
        m_defaultAllowedRoles = UserRoles.getAdminRoles();
    }

    AdminActions(String displayName, AdminPages page, UserRoles[] allowedRoles) {
        this(displayName, page);
        m_defaultAllowedRoles = allowedRoles;
    }

    /*
     * Static Variables and Functions
     */
    private static final Logger s_logger = Logger.getInstance(WebsiteActions.class);

    public static AdminActions getUIAction(String action) {
        s_logger.info("WebsiteAction: " + action);

        if (action == null || action.length() < 1) {
            return HOME;
        }
        AdminActions[] actions = AdminActions.values();
        for (AdminActions adminAction : actions) {
            if (adminAction.getDisplayName().equalsIgnoreCase(action)) {
                return adminAction;
            }
        }
        throw new RuntimeException("Unknown action");
    }

    public static WebsiteActions deserializeUIAction(String action) {
        if (action == null || action.length() < 1) {
            return null;
        }

        WebsiteActions[] actions = WebsiteActions.values();
        for (WebsiteActions adminAction : actions) {
            if (adminAction.getDisplayName().equalsIgnoreCase(action)) {
                return adminAction;
            }
        }

        return null;
    }

    public String getActionURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response) {
        return getActionURL(request, response, null, false);
    }

    public String getActionURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                               String[] queryParams) {
        return getActionURL(request, response, queryParams, false);
    }

    public String getActionURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                               String[] queryParams, boolean forceSSL) {

        // Remove please if ssl not present.
        forceSSL = false;

        StringBuilder b = new StringBuilder(128);

        String requestURL = request.getRequestURL().toString();
        requestURL = WebsiteNavigation.extractInitialURL(requestURL, WebsiteAdminNavigation.getServletPath());

        if (forceSSL && !requestURL.startsWith("https")) {
            requestURL = "https" + requestURL.substring(4);
        }

        b.append(requestURL);
        b.append("/").append(getDisplayName());
        if (queryParams != null) {
            for (int i = 0; i < queryParams.length; i++) {
                b.append("/").append(queryParams[i]);
            }
        }

        return b.toString();

    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return m_displayName;
    }

    /**
     * @return the defaultAllowedRoles
     */
    public UserRoles[] getDefaultAllowedRoles() {
        return m_defaultAllowedRoles;
    }

    /**
     * @return the defaultUIPage
     */
    public AdminPages getDefaultUIPage() {
        return m_defaultUIPage;
    }

    public boolean isRoleDefaultAllowed(UserRoles role) {
        for (int i = 0; i < m_defaultAllowedRoles.length; i++) {
            if (m_defaultAllowedRoles[i] == role) {
                return true;
            }
        }
        return false;
    }

    public AdminPages executeAction(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                    String[] queryParams, boolean isPost) throws Exception {
        if (s_logger.isDebugEnabled()) {
            StringBuilder buf = new StringBuilder();
            buf.append("Executing action: ").append(m_displayName).append(", params{");
            for (int i = 0; (queryParams != null) && (i < queryParams.length); i++) {
                if (i != 0) {
                    buf.append(",");
                }
                buf.append(queryParams[i]);
            }
            buf.append("}");
            s_logger.debug(buf.toString());
        }
        return execute(request, response, queryParams, isPost);
    }

    protected AdminPages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                 String[] queryParams, boolean isPost) throws Exception {
        if (m_defaultUIPage != null) {
            return m_defaultUIPage;
        } else {
            throw new RuntimeException("Cannot rely on default execute when default page is not specified.");
        }
    }

    public boolean isAllowedForUser(User user) {
        UserRoles userRole = null;

        if (user == null) {
            userRole = UserRoles.NOT_LOGGED_IN;
        } else {
            userRole = user.getUserRole();
        }

        // Administrator gets maximum privileges
        if (userRole.isAdministrator()) {
            return true;
        }

        UserRoles[] allowedRoles = getDefaultAllowedRoles();
        for (UserRoles role : allowedRoles) {
            if (role.equals(userRole)) {
                return true;
            }
        }

        return false;
    }

}
