package org.communitylibrary.admin.ui.navigation;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.communitylibrary.admin.ui.beans.AdminBean;
import org.communitylibrary.app.WebsiteApplication;
import org.communitylibrary.data.user.User;
import org.communitylibrary.data.user.UserRoles;
import org.communitylibrary.logger.Logger;
import org.communitylibrary.ui.beans.MessageBean;
import org.communitylibrary.ui.beans.SecurityBean;
import org.communitylibrary.ui.beans.WebsiteBean;
import org.communitylibrary.ui.exception.AccessException;
import org.communitylibrary.ui.exception.AccessExceptionType;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.ui.session.SessionStore;
import org.communitylibrary.utils.StringUtility;
import org.omg.CORBA.UserException;

public class WebsiteAdminNavigation extends HttpServlet {
    private static final Logger s_logger         = Logger.getInstance(WebsiteAdminNavigation.class);
    private static final long   serialVersionUID = 1L;

    public static final String  CSRF_URL_PREPEND = "sec-";
    public static final String  CSRF_POST_PARAM  = "sec_token";

    private static String       s_contextURL     = "";
    private static String       s_servletURL     = "/admin";

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String ctxPath = config.getServletContext().getContextPath();
        if (ctxPath != null && !"/".equals(ctxPath)) {
            s_contextURL = ctxPath;
            // s_servletURL = s_contextURL + s_servletURL;
        }
        s_logger.info("Context Path: " + s_contextURL);
        s_logger.info("Servlet Path: " + s_servletURL);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.GenericServlet#destroy()
     */
    public void destroy() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
     * HttpServletRequest , javax.servlet.http.HttpServletResponse)
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPostGet(request, response, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
     * HttpServletRequest , javax.servlet.http.HttpServletResponse)
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPostGet(request, response, false);
    }

    private void doPostGet(HttpServletRequest origRequest, HttpServletResponse origResponse, boolean isPost)
            throws IOException, ServletException {
        CustomSecurityWrapperRequest request = SecurityBean.getRequest(origRequest);
        CustomSecurityWrapperResponse response = SecurityBean.getResponse(origResponse);
        String action = null;
        AdminActions actionObj = null;

        try {
            String csrfRecieved = null;
            String uri = removeBaseFromURI(request.getRequestURI(), getServletPath());

            // Remove CSRF Token from URL
            if (uri.startsWith(CSRF_URL_PREPEND)) {
                int idx = uri.indexOf("/");
                if (idx == -1) {
                    csrfRecieved = uri.substring(CSRF_URL_PREPEND.length());
                    uri = "";
                } else {
                    csrfRecieved = uri.substring(CSRF_URL_PREPEND.length(), idx);
                    uri = uri.substring(idx);
                }
            }
            while (uri.startsWith("/")) {
                uri = uri.substring(1);
            }

            // Query params
            String[] queryStringSplit = uri.split("/");
            String[] queryParams = null;
            if (queryStringSplit.length > 0) {
                action = StringUtility.trimAndEmptyIsNull(queryStringSplit[0]);
                if (queryStringSplit.length > 1) {
                    queryParams = new String[queryStringSplit.length - 1];
                    System.arraycopy(queryStringSplit, 1, queryParams, 0, queryParams.length);
                } else {
                    queryParams = new String[0];
                }
            }

            // Set SystemAction
            actionObj = AdminActions.getUIAction(action);

            // Action object
            User user = SessionStore.getLoggedInUser(request);
            boolean isActionAllowed = actionObj.isAllowedForUser(user);
            if (!isActionAllowed) {
                if (user != null) {
                    AdminBean.logout(request, response);
                } else {
                    actionObj = AdminActions.LOGIN;
                }
            } else if (action == null) {
                actionObj = AdminActions.HOME;
            }

            // HTTPS check
            if (!WebsiteApplication.isBypassHttpsCheck() && !request.isSecure()) {
                throw new AccessException(AccessExceptionType.HTTPS_REQUIRED);
            }

            // Execute action
            AdminPages page = actionObj.executeAction(request, response, queryParams, isPost);

            // Change CSRF token on each hit as per QSA
            if (!page.equals(AdminPages.NO_PAGE)) {
                SessionStore.setNewRandomCsrfTokens(request, response);
            }
            dispatchRequest(request, response, page);

        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                RuntimeException re = (RuntimeException) e;
                if (re.getCause() instanceof AccessException) {
                    e = (AccessException) re.getCause();
                }
            }

            if (action == null) {
                s_logger.error("Error in executing request.", e);
            } else {
                s_logger.error("Error in executing request with action: " + action, e);
            }
            if (e instanceof AccessException) {
                AccessException ae = (AccessException) e;
                try {
                    dispatchRequest(request, response, AdminPages.LOGIN_PAGE);
                } catch (Exception e2) {
                }

            } else if (e instanceof UserException) {
                MessageBean.setException(request, e);

            } else {
                MessageBean.setException(request, e);
            }

            if (!response.isCommitted()) {
                try {
                    dispatchRequest(request, response, AdminPages.ERROR_PAGE);
                } catch (Exception e2) {
                }
            } else {
                s_logger.error("Response committed could not do anything!");
            }
        }
    }

    public static String removeBaseFromURI(String uri, String base) {
        int idx = uri.indexOf(base);
        if (idx != -1) {
            uri = uri.substring(idx + base.length());
        }
        while (uri.startsWith("/")) {
            uri = uri.substring(1);
        }
        return uri;
    }

    public static String getContextPath() {
        return s_contextURL;
    }

    public static String getServletPath() {
        return s_servletURL;
    }

    protected void dispatchRequest(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            AdminPages page) throws Exception {
        if (page == AdminPages.NO_PAGE) {
            return;

        } else if (page == AdminPages.REDIRECT_URL) {
            String url = StringUtility.trimAndEmptyIsNull(AdminPages.getRedirectUrlInRequest(request));
            if (url == null) {
                throw new RuntimeException(
                        "Reditect URL UI Page returned without any redirect url being set using UIPages.setRedirectUrlInRequest");
            }
            response.sendRedirect(response.encodeRedirectURL(url));
            return;
        }

        String path = AdminPages.BASE_PATH + page.getJspFilePath();

        response.setHeader("Expires", "Sat, 1 Jan 2000 12:00:00 GMT");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");

        getServletContext().getRequestDispatcher(path).forward(request, response);
    }
}
