package org.communitylibrary.admin.ui.beans;

import java.io.File;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.communitylibrary.admin.service.ImageUploadService;
import org.communitylibrary.admin.ui.navigation.AdminPages;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;

import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public class UploadBean {
    private static Logger s_logger = Logger.getLogger(UploadBean.class);

    public static AdminPages upload(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams) {
        JsonObject responseObject = new JsonObject();
        boolean isSuccess = false;
        try {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> parsedRequest = upload.parseRequest(request);
            for (FileItem item : parsedRequest) {
                String itemName = item.getName();
                s_logger.debug(itemName);

                int lastIndexOfDot = itemName.lastIndexOf(".");

                String suffix = itemName.substring(lastIndexOfDot);
                String prefix = itemName.substring(0, lastIndexOfDot);

                File tempFile = File.createTempFile(prefix, suffix);
                item.write(tempFile);
                ImageUploadService uploadService = ImageUploadService.getService();
                String url = uploadService.uploadFile(tempFile, prefix);
                if (url != null) {
                    // success json response
                    responseObject.addProperty("fileName", itemName);
                    responseObject.addProperty("url", url);
                    isSuccess = true;
                }
            }
        } catch (Exception e) {
            s_logger.error("Error in getting and uploading the file", e);
            JsonObject errorObject = new JsonObject();
            errorObject.addProperty("message", e.getMessage());
            responseObject.add("error", errorObject);
        }

        try {
            responseObject.addProperty("uploaded", isSuccess ? 1 : 0);
            response.getWriter().write(responseObject.toString());
        } catch (Exception e) {
            s_logger.error("Error while writing response back to server", e);
        }

        return AdminPages.NO_PAGE;
    }
}
