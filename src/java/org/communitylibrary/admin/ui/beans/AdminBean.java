package org.communitylibrary.admin.ui.beans;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.communitylibrary.admin.ui.navigation.AdminActions;
import org.communitylibrary.admin.ui.navigation.AdminPages;
import org.communitylibrary.data.LibraryBranch;
import org.communitylibrary.data.SlotManager;
import org.communitylibrary.data.digitalaccess.Slot;
import org.communitylibrary.data.digitalaccess.SlotStatus;
import org.communitylibrary.data.payments.PaymentMedium;
import org.communitylibrary.data.payments.PaymentUnit;
import org.communitylibrary.data.payments.PledgeType;
import org.communitylibrary.data.payments.entity.Donor;
import org.communitylibrary.data.user.User;
import org.communitylibrary.ui.beans.MembersBean;
import org.communitylibrary.ui.beans.MessageBean;
import org.communitylibrary.ui.beans.MessageBean.MessageType;
import org.communitylibrary.ui.beans.PaymentBean;
import org.communitylibrary.ui.beans.WebsiteBean;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.ui.session.SessionStore;
import org.communitylibrary.ui.view.dto.ViewMemberDTO;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.JsonUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.StringUtility;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author nishant.gupta
 */
public class AdminBean {

    private static final String LIBRARY_BRANCH_JSON = "LIBRARY_BRANCH_JSON";
    private static final String SLOTS_TYPES_JSON = "SLOTS_TYPES_JSON";
    // private static final String ALL_BLOGPOSTS = "ALLBLOGPOSTS";
    // private static final String ALL_PAGES = "ALLPAGES";
    // private static final String FORM_RESOURCES = "FORMRESOURCES";
    // private static final String AVAILABLEFORMS = "AVAILABLEFORMS";
    private static Logger s_logger = Logger.getLogger(AdminBean.class);
    private static final String contentType = "application/json";
    // private static final String ALL_FORMS = "ALLFORMS";
    // private static final String ALL_DONATIONS = "ALLDONATIONS";
    // private static final String ALL_LINKS = "ALLLINKS";
    private static final String SLOTS_LIST = "SLOTSLIST";
    private static final String PAYMENT_UNIT = "PAYMENTUNIT";
    // private static final String BRANCHES = "BRANCHES";

    private static ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    public static AdminPages logout(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response) {
        SessionStore.setLoggedInUser(request, response, null);
        SessionStore.invalidateSession(request);
        MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "You have been logged out successfully!");
        return AdminPages.setRedirectUrlInRequest(request, response, AdminActions.LOGIN);
    }

    public static AdminPages login(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                   boolean isPost) {

        User loggedUser = SessionStore.getLoggedInUser(request);
        if (loggedUser != null) {
            return getHomePage(request, response, null, AdminActions.HOME);
        }

        String email = StringUtility.trimAndEmptyIsNull(request.getParameter("email"));
        int userId = NumberUtility.parsetIntWithDefaultOnErr(email, -1);
        String password = StringUtility.trimAndEmptyIsNull(request.getParameter("password"));
        if (email == null || password == null || !isPost) {
            return AdminPages.LOGIN_PAGE;
        }

        boolean isUserLoggedIn = WebsiteBean.loginUser(request, response, email, userId, password, true);
        if (isUserLoggedIn) {
            return getHomePage(request, response, null, AdminActions.HOME);
        }

        SessionStore.invalidateSession(request);
        return AdminPages.LOGIN_PAGE;
    }

    // public static List<Blogpost> getBlogposts(CustomSecurityWrapperRequest
    // request) {
    // Object list = request.getAttribute(ALL_BLOGPOSTS);
    // if (list != null && list instanceof List< ? >) {
    // return (List<Blogpost>) list;
    // }
    // return null;
    //
    // }
    //
    // public static AdminPages getBlogsPage(CustomSecurityWrapperRequest
    // request, CustomSecurityWrapperResponse response,
    // String[] queryParams) {
    //
    // if (queryParams.length > 0) {
    // if ("new".equalsIgnoreCase(queryParams[0])) {
    // if (queryParams.length > 1 && "addoredit".equals(queryParams[1])) {
    // User loggedInUser = SessionStore.getLoggedInUser(request);
    // String id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the blog to edit", "id",
    // CustomSecurityRequestType.INTEGER));
    // String title =
    // StringUtility.trimAndEmptyIsNull(request.getParameter("title of the
    // blog", "title",
    // CustomSecurityRequestType.DEFAULT_UNSAFE_HTML));
    //
    // String url = StringUtility.trimAndEmptyIsNull(request.getParameter("URL
    // of the blog", "url",
    // CustomSecurityRequestType.DEFAULT_SAFE_STRING));
    //
    // String author =
    // StringUtility.trimAndEmptyIsNull(request.getParameter("author of the
    // blog",
    // "author", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
    //
    // if (author == null) {
    // author = loggedInUser.getName();
    // }
    //
    // String text = StringUtility.trimAndEmptyIsNull(request.getParameter("text
    // of the post", "textdata",
    // CustomSecurityRequestType.DEFAULT_UNSAFE_HTML));
    // String publishDate = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("date to publish", "date",
    // CustomSecurityRequestType.DATE_TIME));
    //
    // Blogpost newPost = new Blogpost();
    // newPost.setAuthor(author);
    // newPost.setPostedBy(loggedInUser);
    // newPost.setTitle(title);
    // newPost.setPostText(text);
    // newPost.setUrl(url);
    // Date postDate = new Date();
    // newPost.setPostDate(postDate);
    //
    // try {
    // Date publishDateObj =
    // DateUtility.parseDateInDDMMYYYYNullIfUnset(publishDate);
    // newPost.setPublishDate(publishDateObj != null ? publishDateObj :
    // postDate);
    // } catch (Exception e) {
    // s_logger.error("Error in getting publish date");
    // }
    // try {
    // if (newPost.isValid()) {
    // if (id != null) {
    // newPost.setId(Long.parseLong(id));
    // DBUtil.editPost(newPost);
    // } else {
    // DBUtil.addBlogpost(newPost);
    // }
    // } else {
    // MessageBean.setMessage(request, MessageType.MSG_ERROR, "invalid data
    // entered");
    // }
    // } catch (Exception e) {
    // MessageBean.setMessage(request, MessageType.MSG_ERROR,
    // "Problem with the post. Please contact the administrator");
    // }
    //
    // } else {
    // return AdminPages.NEW_BLOG;
    // }
    // } else if ("edit".equalsIgnoreCase(queryParams[0])) {
    // String id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the blog to edit", "id",
    // CustomSecurityRequestType.INTEGER));
    // Blogpost post = DBUtil.getBlogpostById(id, false);
    // if (post != null) {
    // addPostsInRequest(request, post);
    // return AdminPages.NEW_BLOG;
    // }
    // } else if ("delete".equalsIgnoreCase(queryParams[0])) {
    // String id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the blog to delete", "id",
    // CustomSecurityRequestType.INTEGER));
    // if (id != null) {
    // DBUtil.deletePost(id);
    // }
    // }
    // }
    //
    // List<Blogpost> blogposts = DBUtil.getAllBlogposts();
    // if (blogposts != null) {
    // addPostsInRequest(request, blogposts);
    // }
    // return AdminPages.BLOG_PAGE;
    // }
    //
    // private static void addPostsInRequest(CustomSecurityWrapperRequest
    // request, List<Blogpost> blogposts) {
    // request.setAttribute(ALL_BLOGPOSTS, blogposts);
    // }
    //
    // private static void addPostsInRequest(CustomSecurityWrapperRequest
    // request, Blogpost blogpost) {
    // request.setAttribute(ALL_BLOGPOSTS, blogpost);
    // }
    //
    // public static Blogpost getBlogpost(CustomSecurityWrapperRequest request)
    // {
    // Object list = request.getAttribute(ALL_BLOGPOSTS);
    // if (list != null && list instanceof Blogpost) {
    // return (Blogpost) list;
    // }
    // return null;
    //
    // }
    //
    // public static AdminPages getDocsPage(CustomSecurityWrapperRequest
    // request, CustomSecurityWrapperResponse response,
    // String[] queryParams) {
    //
    // if (queryParams.length > 0) {
    // if ("new".equalsIgnoreCase(queryParams[0])) {
    // if (queryParams.length > 1 && "addoredit".equals(queryParams[1])) {
    // String id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the page to edit", "id",
    // CustomSecurityRequestType.INTEGER));
    // String title =
    // StringUtility.trimAndEmptyIsNull(request.getParameter("title of the
    // page", "title",
    // CustomSecurityRequestType.DEFAULT_UNSAFE_HTML));
    //
    // String url = StringUtility.trimAndEmptyIsNull(request.getParameter("URL
    // of the page", "url",
    // CustomSecurityRequestType.DEFAULT_SAFE_STRING));
    //
    // String text = StringUtility.trimAndEmptyIsNull(request.getParameter("text
    // of the post", "textdata",
    // CustomSecurityRequestType.DEFAULT_UNSAFE_HTML));
    //
    // PageResource newPage = new PageResource();
    // newPage.setTitle(title);
    // newPage.setPageText(text);
    // newPage.setUrl(url);
    // Date creationDate = new Date();
    // newPage.setCreationDate(creationDate);
    //
    // try {
    // if (newPage.isValid()) {
    // if (id != null) {
    // newPage.setId(Long.parseLong(id));
    // DBUtil.editPage(newPage);
    // } else {
    // DBUtil.addPage(newPage);
    // }
    // } else {
    // MessageBean.setMessage(request, MessageType.MSG_ERROR, "invalid data
    // entered");
    // }
    // } catch (Exception e) {
    // MessageBean.setMessage(request, MessageType.MSG_ERROR,
    // "Problem with the post. Please contact the administrator");
    // }
    //
    // } else {
    // return AdminPages.NEW_PAGE;
    // }
    // } else if ("edit".equalsIgnoreCase(queryParams[0])) {
    // String id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the page to edit", "id",
    // CustomSecurityRequestType.INTEGER));
    // PageResource page = DBUtil.getPageById(id);
    // if (page != null) {
    // addPagesInRequest(request, page);
    // return AdminPages.NEW_PAGE;
    // }
    // } else if ("delete".equalsIgnoreCase(queryParams[0])) {
    // String id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the page to delete", "id",
    // CustomSecurityRequestType.INTEGER));
    // if (id != null) {
    // DBUtil.deletePage(id);
    // }
    // }
    // }
    //
    // List<PageResource> pages = DBUtil.getAllPages();
    // if (pages != null) {
    // addPagesInRequest(request, pages);
    // }
    // return AdminPages.PAGERESOURCES_PAGE;
    // }
    //
    // private static void addPagesInRequest(CustomSecurityWrapperRequest
    // request, List<PageResource> pages) {
    // request.setAttribute(ALL_PAGES, pages);
    // }
    //
    // private static void addPagesInRequest(CustomSecurityWrapperRequest
    // request, PageResource pageResource) {
    // request.setAttribute(ALL_PAGES, pageResource);
    // }
    //
    // public static PageResource getPage(CustomSecurityWrapperRequest request)
    // {
    // Object list = request.getAttribute(ALL_PAGES);
    // if (list != null && list instanceof PageResource) {
    // return (PageResource) list;
    // }
    // return null;
    //
    // }
    //
    // public static List<PageResource> getPages(CustomSecurityWrapperRequest
    // request) {
    // Object list = request.getAttribute(ALL_PAGES);
    // if (list != null && list instanceof List< ? >) {
    // return (List<PageResource>) list;
    // }
    // return null;
    //
    // }

    // public static AdminPages getFormsPage(CustomSecurityWrapperRequest
    // request, CustomSecurityWrapperResponse response,
    // String[] queryParams) throws Exception {
    //
    // if (queryParams.length > 0) {
    //
    // if ("responses".equalsIgnoreCase(queryParams[0])) {
    // if (queryParams.length > 1 && "getResponses".equals(queryParams[1])) {
    // JsonElement requestJsonElement =
    // JsonUtility.getJsonDataFromRequest(request);
    //
    // JsonObject responseObject = new JsonObject();
    // if (requestJsonElement == null || !(requestJsonElement instanceof
    // JsonObject)) {
    // responseObject.addProperty("isSuccess", false);
    // } else {
    // try {
    // JsonObject requestJson = (JsonObject) requestJsonElement;
    //
    // JsonElement idElement = requestJson.get("id");
    //
    // if (idElement != null) {
    // // Form Edit flow
    // int formId = idElement.getAsInt();
    // JsonObject formObject = null;
    // Map<Form, Map<String, List<Formfield>>> formResponses = DBUtil
    // .getFormResponsesByFormId(formId);
    // if (formResponses != null) {
    // formObject = FormResponse.convertToFormResponseJson(formResponses);
    // if (formObject == null) {
    // responseObject.addProperty("isSuccess", false);
    // responseObject.addProperty("error-msg", "No Response recieved");
    // } else {
    // responseObject.addProperty("isSuccess", true);
    // responseObject.add("response", formObject);
    // }
    // } else {
    // responseObject.addProperty("isSuccess", false);
    // responseObject.addProperty("error-msg", "No Response recieved");
    // }
    // } else {
    // responseObject.addProperty("isSuccess", false);
    // responseObject.addProperty("error-msg", "Invalid Request.");
    // }
    //
    // } catch (Exception e) {
    // s_logger.error("Error while saving form", e);
    // responseObject.addProperty("isSuccess", false);
    // }
    //
    // }
    //
    // response.setContentType(contentType);
    // response.getWriter().write(responseObject.toString());
    // return AdminPages.NO_PAGE;
    // } else {
    // List<FormResponse> forms = DBUtil.getAllFormsWithResponseCount();
    // if (forms != null) {
    // String jsonConverted = FormResponse.convertToDisplayJson(forms);
    // addAvailableFormsInRequest(request, jsonConverted);
    // }
    // return AdminPages.FORM_RESPONSES;
    // }
    //
    // } else if ("new".equalsIgnoreCase(queryParams[0])) {
    // JsonObject formResources = new JsonObject();
    // formResources.add("fieldTypes",
    // ElementType.getFormFieldTypesAsJsonArray());
    // addFormResourcesToRequest(request, formResources);
    // return AdminPages.NEW_FORM;
    // } else if ("edit".equalsIgnoreCase(queryParams[0])) {
    // String id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the form to edit", "id",
    // CustomSecurityRequestType.INTEGER));
    //
    // Form form = DBUtil.getFormById(id, true);
    //
    // JsonObject formResources = new JsonObject();
    // formResources.add("fieldTypes",
    // ElementType.getFormFieldTypesAsJsonArray());
    // formResources.add("form", form.convertToJson());
    // addFormResourcesToRequest(request, formResources);
    // return AdminPages.NEW_FORM;
    // } else if ("delete".equalsIgnoreCase(queryParams[0])) {
    //
    // String id = null;
    // try {
    // id = StringUtility.trimAndEmptyIsNull(
    // request.getParameter("id of the form to delete", "id",
    // CustomSecurityRequestType.INTEGER));
    //
    // if (id != null) {
    // DBUtil.deleteForm(Integer.parseInt(id));
    // }
    // } catch (Exception e) {
    // s_logger.error("Error while deleting the form with id : " + id, e);
    // }
    // } else if ("saveForm".equals(queryParams[0])) {
    // JsonElement requestJsonElement =
    // JsonUtility.getJsonDataFromRequest(request);
    //
    // JsonObject responseObject = new JsonObject();
    //
    // if (requestJsonElement == null || !(requestJsonElement instanceof
    // JsonObject)) {
    // responseObject.addProperty("isSuccess", false);
    // response.setContentType(contentType);
    // response.getWriter().write(responseObject.toString());
    // } else {
    // try {
    // JsonObject requestJson = (JsonObject) requestJsonElement;
    //
    // JsonElement idElement = requestJson.get("id");
    //
    // if (idElement != null) {
    // // Form Edit flow
    // int formId = idElement.getAsInt();
    // DBUtil.deleteForm(formId);
    // }
    //
    // Form form = new Form(requestJson, false);
    // boolean isFormSaved = DBUtil.saveForm(form);
    // if (isFormSaved) {
    // responseObject.addProperty("isSuccess", true);
    // responseObject.addProperty("url",
    // AdminActions.FORMS.getActionURL(request, response));
    // } else {
    // responseObject.addProperty("isSuccess", false);
    // }
    // } catch (Exception e) {
    // s_logger.error("Error while saving form", e);
    // responseObject.addProperty("isSuccess", false);
    // }
    // }
    //
    // response.setContentType(contentType);
    // response.getWriter().write(responseObject.toString());
    // return AdminPages.NO_PAGE;
    // }
    // }
    //
    // List<Form> forms = DBUtil.getAllForms();
    // if (forms != null) {
    // addFormsInRequest(request, forms);
    // }
    // return AdminPages.FORM_PAGE;
    // }
    //
    // private static void addFormsInRequest(CustomSecurityWrapperRequest
    // request, List<Form> forms) {
    // request.setAttribute(ALL_FORMS, forms);
    // }
    //
    // private static void
    // addFormResourcesToRequest(CustomSecurityWrapperRequest request,
    // JsonObject formResources) {
    // if (formResources == null) {
    // return;
    // }
    //
    // request.setAttribute(FORM_RESOURCES, formResources.toString());
    // }
    //
    // private static void
    // addAvailableFormsInRequest(CustomSecurityWrapperRequest request, String
    // jsonConverted) {
    // request.setAttribute(AVAILABLEFORMS, jsonConverted);
    // }
    //
    // public static String
    // getFormResourcesFromRequest(CustomSecurityWrapperRequest request) {
    // Object attribute = request.getAttribute(FORM_RESOURCES);
    // if (attribute == null) {
    // return null;
    // }
    //
    // return (String) attribute;
    // }
    //
    // public static List<Form> getForms(CustomSecurityWrapperRequest request) {
    // Object list = request.getAttribute(ALL_FORMS);
    // if (list != null && list instanceof List< ? >) {
    // return (List<Form>) list;
    // }
    // return null;
    //
    // }
    //
    // public static String
    // getAvailableFormsRequest(CustomSecurityWrapperRequest request) {
    // Object attribute = request.getAttribute(AVAILABLEFORMS);
    // if (attribute == null) {
    // return null;
    // }
    //
    // return (String) attribute;
    // }

    public static AdminPages getDonationsPage(CustomSecurityWrapperRequest request,
                                              CustomSecurityWrapperResponse response, String[] queryParams) throws Exception {
        if (queryParams == null || queryParams.length == 0) {
            return AdminPages.DONATIONS_PAGE;
        }

        if (queryParams.length > 0) {
            if ("get_all_donations".equals(queryParams[0])) {
                JsonObject requestJsonElement = (JsonObject) JsonUtility.getJsonDataFromRequest(request);
                // boolean isFetchTotalAmount =
                // requestJsonElement.get("totalAmount").getAsBoolean();
                int page = requestJsonElement.get("page").getAsInt();
                int size = requestJsonElement.get("size").getAsInt();

                JsonObject responseObject = new JsonObject();
                List<PaymentUnit> donations = DBUtil.getAllDonations(page, size);
                JsonArray donationsArray = null;
                if (donations != null) {
                    donationsArray = PaymentUnit.convertPaymentListToJsonArray(donations);
                } else {
                    donationsArray = new JsonArray();
                }

                responseObject.add("donations", donationsArray);
                responseObject.add("pledgeTypes", PledgeType.getPledgeTypesAsJsonArray());
                // if (isFetchTotalAmount) {
                // responseObject.addProperty("totalAmount",
                // DBUtil.getTotalDonatedAmount());
                // }

                response.setContentType(contentType);
                response.getWriter().write(responseObject.toString());
                return AdminPages.NO_PAGE;
            } else if ("downloadCSV".equals(queryParams[0])) {

                JsonElement requestJsonElement = JsonUtility.getJsonDataFromRequest(request);

                if (requestJsonElement == null) {
                    JsonObject object = new JsonObject();
                    object.addProperty("error", true);
                    object.addProperty("errorMsg", "No Data Receieved from the user");

                    response.setContentType(contentType);
                    response.getWriter().write(object.toString());
                    return AdminPages.NO_PAGE;
                }

                List<PaymentUnit> units = null;
                JsonArray requestArray = (JsonArray) requestJsonElement;
                units = PaymentUnit.parsePaymentsFromJson(requestArray);
                if (units == null) {
                    JsonObject object = new JsonObject();
                    object.addProperty("error", true);
                    object.addProperty("errorMsg", "Please check the data entered");

                    response.setContentType(contentType);
                    response.getWriter().write(object.toString());
                    return AdminPages.NO_PAGE;
                }

                byte[] bytes = null;
                if ("downloadCSV".equals(queryParams[0])) {
                    String csvContent = PaymentBean.convertToCSV(units);
                    response.setContentType("text/csv");
                    bytes = csvContent.getBytes();
                }

                if (bytes == null) {
                    JsonObject object = new JsonObject();
                    object.addProperty("error", true);
                    object.addProperty("errorMsg", "Please check the data entered");

                    response.setContentType(contentType);
                    response.getWriter().write(object.toString());
                    return AdminPages.NO_PAGE;
                }
                response.getOutputStream().write(bytes);
                return AdminPages.NO_PAGE;
            } else if ("uploadDonations".equals(queryParams[0])) {
                JsonObject responseObject = null;
                try {

                    if (!ServletFileUpload.isMultipartContent(request)) {
                        throw new RuntimeException("Request is not MultipartContent");
                    }

                    DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
                    diskFileItemFactory.setSizeThreshold(51200);
                    ServletFileUpload upload = new ServletFileUpload(diskFileItemFactory);
                    upload.setFileSizeMax(MembersBean.MAX_SIZE_PER_FILE_IN_KB * 1024);
                    List<FileItem> items = (List<FileItem>) upload.parseRequest(request);

                    FileItem dataItem = null;
                    String extension = null;

                    for (FileItem itm : items) {
                        if (itm.isFormField()) {
                            // Nothing to doF

                        } else {
                            dataItem = itm;
                            if (dataItem.getSize() > (MembersBean.MAX_SIZE_PER_FILE_IN_KB * 1024)) {
                                throw new RuntimeException("File size too big!");
                            }

                            String name = StringUtility.trimAndEmptyIsNull(dataItem.getName());
                            if (name != null) {
                                extension = StringUtility
                                        .trimAndEmptyIsNull(name.substring(name.lastIndexOf(".") + 1).toLowerCase());
                            }
                        }
                    }
                    if (!"csv".equalsIgnoreCase(extension) && !"txt".equalsIgnoreCase(extension)) {
                        throw new RuntimeException("Only csv and txt extension files allowed!");
                    }

                    // Parse file
                    InputStream ins = null;
                    InputStreamReader isr = null;
                    CSVReader reader = null;

                    String[] keys = null;
                    String line = null;
                    try {
                        int lineNo = 0;

                        ins = dataItem.getInputStream();
                        isr = new InputStreamReader(ins);
                        reader = new CSVReaderBuilder(isr)
                                .withCSVParser(new CSVParserBuilder().withSeparator(',').build()).build();

                        List<PaymentUnit> paymentUnits = new ArrayList<PaymentUnit>();
                        while ((keys = reader.readNext()) != null) {

                            lineNo++;
                            // ignoring empty lines and first line
                            if (keys.length == 0 || "S.No.".equalsIgnoreCase(keys[0])) {
                                continue;
                            }

                            line = StringUtility.join(keys, " ");

                            if (keys.length < 18) {
                                s_logger.info("Ignoring line " + lineNo + " - invalid column count - " + line);
                                continue;
                            }

                            int i = 1;
                            PaymentUnit pu = new PaymentUnit();
                            Date donationDate = DateUtility
                                    .parseDateInDDMMMYYDashSeparator(StringUtility.trimAndEmptyIsNull(keys[i++]));
                            pu.setCreationDate(donationDate);
                            Donor donor = new Donor();

                            String name = StringUtility.trimAndEmptyIsNull(keys[i++]);

                            donor.setFirstName(name);

                            // skipping address
                            i++;

                            // skipping phone
                            i++;
                            donor.setEmail(StringUtility.trimAndNullIsEmpty(keys[i++]));
                            donor.setPhone("00000000");

                            pu.setDonor(donor);

                            // skipping collecting trustee
                            i++;

                            // skipping paid for project
                            i++;

                            PaymentMedium paymentType = PaymentMedium
                                    .getPaymentType(StringUtility.trimAndEmptyIsNull(keys[i++]));

                            if (paymentType == null) {
                                throw new RuntimeException("Payment type is null");
                            }
                            pu.setPaymentType(paymentType);

                            String gatewayTransactionID = StringUtility.trimAndEmptyIsNull(keys[i++]);

                            String transactionID = NumberUtility.generateIdWithTime("TCLP");
                            Thread.sleep(100);

                            pu.setTransactionId(transactionID);
                            pu.setGatewayTransactionId(gatewayTransactionID);

                            pu.setAmount(StringUtility.trimAndEmptyIsNull(keys[i++]));

                            // skipping annual donor
                            i++;

                            pu.setReceiptNumber(StringUtility.trimAndEmptyIsNull(keys[i++]));
                            pu.setReceiptDate(DateUtility
                                    .parseDateInDDMMMYYDashSeparator(StringUtility.trimAndEmptyIsNull(keys[i++])));

                            donor.setPanNumber(StringUtility.trimAndEmptyIsNull(keys[i++]));

                            pu.setExtraNotes(StringUtility.trimAndEmptyIsNull(keys[i++]));
                            pu.setIsPaymentSuccessful(true);
                            paymentUnits.add(pu);

                        }

                        s_logger.info("Report: Lines-" + lineNo + ";number of payment units-" + paymentUnits.size());

                        int numMembersAdded = DBUtil.addPaymentUnits(paymentUnits, request.getRemoteAddr());
                        if (numMembersAdded > 0) {
                            JsonArray donationsArray = PaymentUnit.convertPaymentListToJsonArray(paymentUnits);
                            responseObject = new JsonObject();
                            responseObject.addProperty("message", numMembersAdded + " donation entries recorded");
                        } else {
                            responseObject = new JsonObject();
                            responseObject.addProperty("error", true);
                            responseObject.addProperty("errorMsg", "no data to be saved in table");
                        }

                    } catch (Exception e) {
                        s_logger.error("Error in parsing of the file", e);
                        s_logger.info("line for Error: " + line, e);
                        throw e;
                    } finally {
                        if (isr != null) {
                            try {
                                isr.close();
                            } catch (Throwable t) {
                            }
                        }
                        if (reader != null) {
                            try {
                                reader.close();
                            } catch (Throwable t) {
                            }
                        }
                        if (ins != null) {
                            try {
                                ins.close();
                            } catch (Throwable t) {
                            }
                        }
                    }
                } catch (Exception e) {
                    responseObject = new JsonObject();
                    responseObject.addProperty("error", true);
                    responseObject.addProperty("errorMsg", e.getMessage());
                }

                response.setContentType(contentType);
                response.getWriter().write(responseObject.toString());
                return AdminPages.NO_PAGE;

            } else if ("downloadPDF".equals(queryParams[0])) {
                String transactionID = queryParams[1];
                PaymentUnit pu = DBUtil.getPaymentUnitByTransactionId(transactionID);

                if (pu != null) {
                    setPaymentUnitInRequest(request, pu);
                }

                return AdminPages.DONATION_RECEIPT_PAGE;

            } else {
                if ("deleteDonation".equals(queryParams[0])) {
                    JsonElement requestJsonElement = JsonUtility.getJsonDataFromRequest(request);
                    if (requestJsonElement == null) {
                        JsonObject object = new JsonObject();
                        object.addProperty("error", true);
                        object.addProperty("errorMsg", "No Data Receieved from the user");

                        response.setContentType(contentType);
                        response.getWriter().write(object.toString());
                        return AdminPages.NO_PAGE;
                    }

                    JsonObject requestObject = (JsonObject) requestJsonElement;
                    String transactionId = requestObject.get("txn_id").getAsString();

                    if (transactionId == null) {
                        JsonObject object = new JsonObject();
                        object.addProperty("error", true);
                        object.addProperty("errorMsg", "Please check the data entered");

                        response.setContentType(contentType);
                        response.getWriter().write(object.toString());
                        return AdminPages.NO_PAGE;
                    }

                    boolean isDeleted = false;
                    isDeleted = DBUtil.deletePaymentUnit(transactionId);

                    JsonObject responseObject = new JsonObject();
                    responseObject.addProperty("error", !isDeleted);
                    if (!isDeleted) {
                        responseObject.addProperty("errorMsg",
                                "Could not service your request. Please contact the Administrator");
                    }

                    response.setContentType(contentType);
                    response.getWriter().write(responseObject.toString());
                    return AdminPages.NO_PAGE;
                }
            }
        }

        return AdminPages.DONATIONS_PAGE;
    }
    //
    // private static void
    // addDonationsJsonToRequest(CustomSecurityWrapperRequest request, String
    // donationsJson) {
    // request.setAttribute(ALL_DONATIONS, donationsJson);
    // }
    //
    // public static String getDonations(CustomSecurityWrapperRequest request) {
    // Object list = request.getAttribute(ALL_DONATIONS);
    // if (list != null) {
    // return (String) list;
    // }
    // return "[]";
    //
    // }

    // public static AdminPages
    // getHomePageLinksPage(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response, String[] queryParams) throws
    // Exception {
    //
    // if (queryParams != null && queryParams.length > 0) {
    // JsonElement requestJsonElement =
    // JsonUtility.getJsonDataFromRequest(request);
    // JsonObject responseObject = new JsonObject();
    //
    // if (requestJsonElement == null || !(requestJsonElement instanceof
    // JsonObject)) {
    // JsonObject object = new JsonObject();
    // object.addProperty("error", true);
    // object.addProperty("errorMsg", "No Data Receieved from the user");
    //
    // response.setContentType(contentType);
    // response.getWriter().write(object.toString());
    // return AdminPages.NO_PAGE;
    // }
    //
    // try {
    // JsonObject requestJson = (JsonObject) requestJsonElement;
    //
    // if ("saveLink".equals(queryParams[0])) {
    // HomePageLink link = new HomePageLink(requestJson);
    // boolean isLinkSaved = DBUtil.saveHomePageLink(link);
    // if (isLinkSaved) {
    // responseObject.addProperty("isSuccess", true);
    // responseObject.addProperty("id", link.getId());
    // } else {
    // responseObject.addProperty("isSuccess", false);
    // responseObject.addProperty("error", true);
    // responseObject.addProperty("errorMsg", "Could not save the link");
    // }
    // } else if ("deleteLink".equals(queryParams[0])) {
    // JsonElement idElement = requestJson.get("id");
    //
    // if (idElement != null) {
    // int linkId = idElement.getAsInt();
    // boolean isDeleted = DBUtil.deleteLink(linkId);
    // responseObject.addProperty("isSuccess", isDeleted);
    // }
    // } else if ("editLink".equals(queryParams[0])) {
    // JsonElement idElement = requestJson.get("id");
    //
    // if (idElement != null) {
    //
    // HomePageLink link = new HomePageLink();
    // int linkId = idElement.getAsInt();
    // link.setId(linkId);
    //
    // JsonElement element = requestJson.get("hindiname");
    // if (element != null) {
    // link.setHindiName(element.getAsString());
    // }
    //
    // element = requestJson.get("hindidesc");
    // if (element != null) {
    // link.setHindiDesc(element.getAsString());
    // }
    //
    // boolean isUpdated = DBUtil.updateLinkInfo(link);
    // responseObject.addProperty("isSuccess", isUpdated);
    // }
    // } else if ("getAll".equals(queryParams[0])) {
    // List<HomePageLink> homePageLinks = DBUtil.getAllHomePageLinks(null);
    // if (homePageLinks != null) {
    // Collections.sort(homePageLinks, new Comparator<HomePageLink>() {
    //
    // @Override
    // public int compare(HomePageLink o1, HomePageLink o2) {
    // if (o1 == null && o2 == null) {
    // return 0;
    // }
    //
    // if (o1 != null && o2 != null) {
    // if (o1.getGroup() == o2.getGroup()) {
    // return 0;
    // }
    //
    // if (o1.getGroup() == LibraryGroupForDigitalAccess.UPPER) {
    // return -5;
    // }
    //
    // return 5;
    // }
    //
    // if (o1 != null) {
    // return -5;
    // }
    //
    // return 5;
    // }
    //
    // });
    // JsonArray homePageLinksJson =
    // HomePageLink.convertToJsonArray(homePageLinks);
    // responseObject.addProperty("isSuccess", true);
    // responseObject.add("availableLinks", homePageLinksJson);
    // } else {
    // responseObject.addProperty("isSuccess", false);
    // }
    //
    // }
    //
    // } catch (Exception e) {
    // s_logger.error("Error while saving form", e);
    // JsonObject object = new JsonObject();
    // object.addProperty("error", true);
    // object.addProperty("errorMsg", "No Data Receieved from the user");
    //
    // response.setContentType(contentType);
    // response.getWriter().write(object.toString());
    // return AdminPages.NO_PAGE;
    // }
    //
    // response.setCharacterEncoding("utf-8");
    // response.setContentType(contentType);
    // response.getWriter().write(responseObject.toString());
    // return AdminPages.NO_PAGE;
    // }
    //
    // // addHomePageLinksToRequest(request, homePageLinksJson.toString());
    // return AdminPages.HOMEPAGELINKSPAGE;
    //
    // }

    // private static void
    // addHomePageLinksToRequest(CustomSecurityWrapperRequest request, String
    // homePageLinksJson) {
    // request.setAttribute(ALL_LINKS, homePageLinksJson);
    // }
    //
    // public static String getHomePageLinks(CustomSecurityWrapperRequest
    // request) {
    // Object list = request.getAttribute(ALL_LINKS);
    // if (list != null) {
    // return (String) list;
    // }
    // return "[]";
    // }

    private static void setPaymentUnitInRequest(CustomSecurityWrapperRequest request, PaymentUnit pu) {
        request.setAttribute(PAYMENT_UNIT, pu);
    }

    public static PaymentUnit getPaymentUnitFromRequest(CustomSecurityWrapperRequest request) {
        Object object = request.getAttribute(PAYMENT_UNIT);
        if (object != null && object instanceof PaymentUnit) {
            return (PaymentUnit) object;
        }

        return null;
    }

    public static AdminPages getHomePage(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                                         String[] queryParams, AdminActions adminAction) {

        // User loggedInUser = SessionStore.getLoggedInUser(request);

        // if (AdminActions.SETTINGS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.SETTINGS.getDefaultUIPage();
        // }
        //
        // if (AdminActions.MANAGE_USERS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.MANAGE_USERS.getDefaultUIPage();
        // }
        //
        // if (AdminActions.EVENTS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.EVENTS.getDefaultUIPage();
        // }
        //
        // if (AdminActions.RESOURCES.isAllowedForUser(loggedInUser)) {
        // return AdminActions.RESOURCES.getDefaultUIPage();
        // }
        // if (AdminActions.MANAGE_USER_LOGINS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.MANAGE_USER_LOGINS.getDefaultUIPage();
        // }
        // if (AdminActions.DONATIONS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.DONATIONS.getDefaultUIPage();
        // }
        //
        // if (AdminActions.HOMEPAGELINKS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.HOMEPAGELINKS.getDefaultUIPage();
        // }
        //
        // if (AdminActions.BLOGS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.BLOGS.getDefaultUIPage();
        // }
        //
        // if (AdminActions.PAGES.isAllowedForUser(loggedInUser)) {
        // return AdminActions.PAGES.getDefaultUIPage();
        // }
        //
        // if (AdminActions.FORMS.isAllowedForUser(loggedInUser)) {
        // return AdminActions.FORMS.getDefaultUIPage();
        // }

        return AdminPages.setRedirectUrlInRequest(request, response, AdminActions.DASLOTS);

        // return adminAction.getDefaultUIPage();

    }

    public static void setSlotsInRequest(CustomSecurityWrapperRequest request, List<Slot> slots) {
        request.setAttribute(SLOTS_LIST, slots);
    }

    public static List<Slot> getSlotsFromRequest(CustomSecurityWrapperRequest request) {
        Object object = request.getAttribute(SLOTS_LIST);
        if (object != null && object instanceof List) {
            return (List<Slot>) object;
        }

        return null;
    }

    public static String getSlotsJson(List<Slot> slots) {
        if (slots == null) {
            return null;
        }

        JsonArray slotArray = SlotManager.convertToSlotEventJson(slots);
        return slotArray.toString();

    }

    public static AdminPages getManageSlotsPage(CustomSecurityWrapperRequest request,
                                                CustomSecurityWrapperResponse response, String[] queryParams) throws Exception {

        String action = (queryParams == null || queryParams.length < 1) ? null : queryParams[0];

        boolean isDML = false;
        // StringBuilder jsonString = new StringBuilder("{");
        JsonObject jsonString = new JsonObject();
        if ("add".equals(action)) {
            isDML = true;
            Slot slot = Slot.getSlotObject(request);
            StringBuilder reason = new StringBuilder();
            if (slot.validate(reason)) {
                try {
                    DBUtil.addSlotInDB(slot);
                    jsonString.addProperty("status", true);
                } catch (Exception e) {
                    s_logger.error("Error in saving event info: ", e);
                    jsonString.addProperty("status", false);
                    jsonString.addProperty("reason", e.getMessage());
                }
            } else {
                jsonString.addProperty("status", false);
                jsonString.addProperty("reason", reason.toString());
            }
        } else if ("deleteSlot".equals(action)) {
            JsonElement requestJsonElement = JsonUtility.getJsonDataFromRequest(request);

            JsonObject responseObject = new JsonObject();
            if (requestJsonElement == null || !(requestJsonElement instanceof JsonObject)) {
                responseObject.addProperty("isSuccess", false);
            } else {
                try {
                    JsonObject requestJson = (JsonObject) requestJsonElement;

                    JsonElement idElement = requestJson.get("id");

                    if (idElement != null) {
                        // Form Edit flow
                        int id = idElement.getAsInt();
                        Slot slot = new Slot();
                        slot.setId(String.valueOf(id));
                        boolean isDeleted = DBUtil.deleteSlotFromDB(slot);
                        responseObject.addProperty("isSuccess", isDeleted);
                    }
                } catch (Exception e) {
                    s_logger.error("Error while getting slots for request: " + requestJsonElement.toString(), e);
                    responseObject.addProperty("isSuccess", false);
                }
            }

            response.setContentType(contentType);
            response.getWriter().write(responseObject.toString());
            return AdminPages.NO_PAGE;

        } else if ("getEvents".equals(action)) {
            String startDate = StringUtility.trimAndEmptyIsNull(request.getParameter("startdate"));
            String endDate = StringUtility.trimAndEmptyIsNull(request.getParameter("enddate"));
            String branchCode = StringUtility.trimAndEmptyIsNull(request.getParameter("branch"));
            LibraryBranch libraryBranch = LibraryBranch.deserialize(branchCode);

            JsonObject responseObject = new JsonObject();
            try {
                LocalDate firstDate = null;
                LocalDate lastDate = null;
                if (startDate != null && endDate != null) {
                    firstDate = LocalDate.parse(startDate);
                    lastDate = LocalDate.parse(endDate);
                } else {
                    DayOfWeek firstDayOfWeek = DayOfWeek.SUNDAY;
                    DayOfWeek lastDayOfWeek = DayOfWeek.SATURDAY;
                    firstDate = LocalDate.now().with(TemporalAdjusters.previousOrSame(firstDayOfWeek));
                    lastDate = LocalDate.now().with(TemporalAdjusters.nextOrSame(lastDayOfWeek));
                }

                List<Slot> slots = DBUtil.getSlots(firstDate, lastDate, libraryBranch);
                responseObject.addProperty("status", true);
                responseObject.add("slots", SlotManager.convertToSlotEventJson(slots));
            } catch (Exception e) {
                s_logger.error("Error in getting slots for dates: " + startDate + " to " + endDate);
                responseObject.addProperty("status", false);
                responseObject.addProperty("reason", e.getMessage());
            }

            response.setContentType(contentType);
            response.getWriter().write(responseObject.toString());
            return AdminPages.NO_PAGE;

        } else if ("get".equals(action)) {
            JsonElement requestJsonElement = JsonUtility.getJsonDataFromRequest(request);

            JsonObject responseObject = new JsonObject();
            if (requestJsonElement == null || !(requestJsonElement instanceof JsonObject)) {
                responseObject.addProperty("isSuccess", false);
            } else {
                try {
                    JsonObject requestJson = (JsonObject) requestJsonElement;

                    JsonElement dateElement = requestJson.get("date");

                    if (dateElement != null) {
                        // Form Edit flow
                        String date = dateElement.getAsString();
                        JsonElement branchElement = requestJson.get("branch");
                        String branchCode = null;
                        if (branchElement != null) {
                            branchCode = branchElement.getAsString();
                        }

                        LibraryBranch branch = LibraryBranch.deserialize(branchCode);

                        List<Slot> slots = DBUtil.getSlotsForDate(date, branch);
                        if (slots != null) {
                            JsonArray slotsData = SlotManager.convertToSlotEventJson(slots);
                            if (slotsData == null) {
                                responseObject.addProperty("isSuccess", false);
                                responseObject.addProperty("error-msg", "No Response received");
                            } else {
                                responseObject.addProperty("isSuccess", true);
                                responseObject.add("slots", slotsData);
                            }
                        } else {
                            responseObject.addProperty("isSuccess", false);
                            responseObject.addProperty("error-msg", "No Response recieved");
                        }
                    } else {
                        responseObject.addProperty("isSuccess", false);
                        responseObject.addProperty("error-msg", "Invalid Request.");
                    }

                } catch (Exception e) {
                    s_logger.error("Error while getting slots for request: " + requestJsonElement.toString(), e);
                    responseObject.addProperty("isSuccess", false);
                }

            }

            response.setContentType(contentType);
            response.getWriter().write(responseObject.toString());
            return AdminPages.NO_PAGE;
        } else if ("changeStatus".equals(action)) {
            JsonElement requestJsonElement = JsonUtility.getJsonDataFromRequest(request);

            JsonObject responseObject = new JsonObject();
            if (requestJsonElement == null || !(requestJsonElement instanceof JsonObject)) {
                responseObject.addProperty("isSuccess", false);
            } else {
                try {
                    JsonObject requestJson = (JsonObject) requestJsonElement;

                    JsonElement idElement = requestJson.get("id");

                    if (idElement != null) {
                        // Form Edit flow
                        int id = idElement.getAsInt();
                        boolean isStatusChanged = DBUtil.updateSlotStatus(id, SlotStatus.Attended);
                        responseObject.addProperty("isSuccess", isStatusChanged);
                        if (isStatusChanged) {
                            responseObject.addProperty("error-msg", "Could not update status");
                        }
                    } else {
                        responseObject.addProperty("isSuccess", false);
                        responseObject.addProperty("error-msg", "Invalid Request.");
                    }

                } catch (Exception e) {
                    s_logger.error("Error while getting slots for request: " + requestJsonElement.toString(), e);
                    responseObject.addProperty("isSuccess", false);
                }

            }

            response.setContentType(contentType);
            response.getWriter().write(responseObject.toString());
            return AdminPages.NO_PAGE;
        }

        if (isDML) {
            response.setContentType("application/json");
            response.getWriter().write(jsonString.toString());
            return AdminPages.NO_PAGE;
        }

        DayOfWeek firstDayOfWeek = DayOfWeek.SUNDAY;
        DayOfWeek lastDayOfWeek = DayOfWeek.SATURDAY;
        LocalDate firstDate = LocalDate.now().with(TemporalAdjusters.previousOrSame(firstDayOfWeek));
        LocalDate lastDate = LocalDate.now().with(TemporalAdjusters.nextOrSame(lastDayOfWeek));

        List<LibraryBranch> libraryBranches = DBUtil.getAllLibraryBranches();
        LibraryBranch libraryBranch = LibraryBranch.deserialize(libraryBranches != null ? libraryBranches.get(0).getCode() : null);

        List<Slot> slots = DBUtil.getSlots(firstDate, lastDate, libraryBranch);
        setSlotsInRequest(request, slots);


        List<String> slotTypes = DBUtil.getSlotTypes();

        setLibraryBranchesInRequest(request, libraryBranches);
        setSlotTypesInRequest(request, slotTypes);

        return AdminPages.DASLOTSPAGE;
    }

    public static AdminPages getAjaxMembers(CustomSecurityWrapperRequest request,
                                            CustomSecurityWrapperResponse response, String[] queryParams) {

        String searchQuery = StringUtility.trimAndEmptyIsNull(request.getParameter("search"));
        String branchCode = StringUtility.trimAndEmptyIsNull(request.getParameter("branch"));
        LibraryBranch libraryBranch = LibraryBranch.deserialize(branchCode);

        ViewMemberDTO dto = new ViewMemberDTO(null, null, null, searchQuery);
        dto.setLibraryBranch(libraryBranch);

        StringBuilder returnJSON = null;
        try {
            DBUtil.populateViewMembersDTO(dto);
            returnJSON = new StringBuilder(dto.getMembersJSON());
        } catch (Exception e) {
            returnJSON = new StringBuilder("{").append("\"error\":\"There was an error in processing\"}");
        }
        s_logger.debug("json:" + returnJSON.toString());
        try {
            response.setContentType("application/json");
            response.getWriter().write(returnJSON.toString());
        } catch (Exception e) {
            s_logger.error("Error while writing to output stream", e);
        }

        return AdminPages.NO_PAGE;

    }

    public static AdminPages getManageSlotSettingsPage(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response, String[] queryParams) throws IOException {
        String action = queryParams.length < 1 ? null : queryParams[0];

        if (action != null) {
            JsonObject responseObject = new JsonObject();
            boolean isSuccess = false;
            switch (action) {
                case "library":
                    String subAction = queryParams.length < 2 ? null : queryParams[1];
                    switch (subAction) {
                        case "branches":
                            String requestJsonElement = JsonUtility.getJsonStringFromRequest(request);
                            LibraryBranch newBranch = objectMapper.readValue(requestJsonElement, LibraryBranch.class);
                            try {
                                DBUtil.addLibraryBranch(newBranch);
                                isSuccess = true;
                            } catch (Exception e) {
                                s_logger.error("Error in saving library branch info: ", e);
                            }
                            break;
                    }
                    break;
                case "slotType":
                    String requestMethod = request.getMethod().toUpperCase();
                    switch (requestMethod) {
                        case "POST": {
                            JsonObject jsonDataFromRequest = (JsonObject) JsonUtility.getJsonDataFromRequest(request);
                            String slotType = jsonDataFromRequest.get("slotType").getAsString();
                            try {
                                DBUtil.addSlotTypeInDB(slotType);
                                isSuccess = true;
                            } catch (Exception e) {
                                s_logger.error("Error in saving new slot type: ", e);
                            }
                        }
                        break;
                        case "GET": {
                            String slotType = queryParams.length == 2 ? queryParams[1] : null;
                            if (slotType != null) {
                                try {
                                    DBUtil.deleteSlotType(slotType);
                                    isSuccess = true;
                                } catch (Exception e) {
                                    s_logger.error("Error in deleting slot type from db: ", e);
                                }
                            }
                        }
                        break;
                    }
                    break;
            }

            response.setContentType(contentType);
            responseObject.addProperty("isSuccess", isSuccess);
            response.getWriter().write(responseObject.toString());
            return AdminPages.NO_PAGE;
        }

        List<LibraryBranch> libraryBranches = DBUtil.getAllLibraryBranches();

        List<String> slotTypes = DBUtil.getSlotTypes();

        setLibraryBranchesJsonInRequest(request, libraryBranches);
        setSlotTypesJsonInRequest(request, slotTypes);

        return AdminPages.DASLOTSETTINGPAGE;
    }

    private static void setSlotTypesJsonInRequest(CustomSecurityWrapperRequest request, List<String> slotTypes) throws JsonProcessingException {
        String jsonArr;
        if (slotTypes != null) {
            jsonArr = objectMapper.writeValueAsString(slotTypes);
        } else {
            jsonArr = "[]";
        }

        request.setAttribute(SLOTS_TYPES_JSON, jsonArr);
    }

    private static void setLibraryBranchesJsonInRequest(CustomSecurityWrapperRequest request, List<LibraryBranch> libraryBranches) throws JsonProcessingException {
        String jsonArr;
        if (libraryBranches != null) {
            jsonArr = objectMapper.writeValueAsString(libraryBranches);
        } else {
            jsonArr = "[]";
        }

        request.setAttribute(LIBRARY_BRANCH_JSON, jsonArr);

    }

    public static String getLibraryBranchesJson(CustomSecurityWrapperRequest request) {
        Object object = request.getAttribute(LIBRARY_BRANCH_JSON);
        if (object != null && object instanceof String) {
            return (String) object;
        }

        return null;
    }

    public static String getSlotTypesJson(CustomSecurityWrapperRequest request) {
        Object object = request.getAttribute(SLOTS_TYPES_JSON);
        if (object != null && object instanceof String) {
            return (String) object;
        }

        return null;
    }

    private static void setSlotTypesInRequest(CustomSecurityWrapperRequest request, List<String> slotTypes) throws JsonProcessingException {
        if (slotTypes == null) {
            slotTypes = new ArrayList<>();
        }

        request.setAttribute(SLOTS_TYPES_JSON, slotTypes);
    }

    private static void setLibraryBranchesInRequest(CustomSecurityWrapperRequest request, List<LibraryBranch> libraryBranches) throws JsonProcessingException {
        if (libraryBranches == null) {
            libraryBranches = new ArrayList<>();
        }

        request.setAttribute(LIBRARY_BRANCH_JSON, libraryBranches);

    }

    public static List<LibraryBranch> getLibraryBranches(CustomSecurityWrapperRequest request) {
        Object object = request.getAttribute(LIBRARY_BRANCH_JSON);
        if (object != null && object instanceof List) {
            return (List<LibraryBranch>) object;
        }

        return null;
    }

    public static List<String> getSlotTypes(CustomSecurityWrapperRequest request) {
        Object object = request.getAttribute(SLOTS_TYPES_JSON);
        if (object != null && object instanceof List) {
            return (List<String>) object;
        }

        return null;
    }
}
