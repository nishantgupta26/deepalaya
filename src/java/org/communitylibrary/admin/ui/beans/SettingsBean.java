package org.communitylibrary.admin.ui.beans;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.communitylibrary.admin.ui.navigation.AdminPages;
import org.communitylibrary.app.config.CacheConfiguration;
import org.communitylibrary.app.config.value.AbstractConfigValue;
import org.communitylibrary.app.resources.ResourceObject;
import org.communitylibrary.app.resources.ResourceType;
import org.communitylibrary.data.entity.EventData;
import org.communitylibrary.data.user.User;
import org.communitylibrary.ui.beans.MessageBean;
import org.communitylibrary.ui.beans.MessageBean.MessageType;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.security.CustomSecurityRequestType;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.ui.session.SessionStore;
import org.communitylibrary.utils.ConfigStore;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class SettingsBean {
    private static final Logger s_logger       = Logger.getLogger(SettingsBean.class);
    private static final String EVENTS_RECORDS = "EVENTS_RECORDS";

    /**
     * Displays Settings page after executing the provided action (if any).
     * 
     * @param request
     * @param response
     * @param queryParams
     * @return
     */
    // public static AdminPages manageSettings(CustomSecurityWrapperRequest
    // request,
    // CustomSecurityWrapperResponse response, String[] queryParams) {
    // String action = (queryParams == null || queryParams.length < 1) ? null :
    // queryParams[0];
    //
    // if ("update".equals(action)) {
    // try {
    // CacheConfiguration aCfg =
    // CacheConfiguration.deserialize(request.getParameter("cfg_id", "cfg_id",
    // CustomSecurityRequestType.DEFAULT_SAFE_STRING));
    //
    // if (aCfg == null) {
    // throw new RuntimeException("Config id cannot be null");
    // }
    //
    // CustomSecurityRequestType type = null;
    // switch (aCfg.getType()) {
    // case STRING:
    // switch (aCfg.getElem()) {
    // case TEXTAREA:
    // type = CustomSecurityRequestType.DEFAULT_UNSAFE_HTML;
    // break;
    // default:
    // type = CustomSecurityRequestType.DEFAULT_SAFE_STRING;
    // }
    // break;
    // default:
    // type = CustomSecurityRequestType.DEFAULT_SAFE_STRING;
    // }
    // String val = request.getParameter("Value", "cfg_val", type);
    // AbstractConfigValue cVal = aCfg.convertFromHtmlFormValue(val);
    //
    // ConfigStore.updateConfigValue(aCfg, cVal);
    // MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "Updated user
    // settings successfully!");
    //
    // } catch (Exception e) {
    // s_logger.error("Error in updating settings", e);
    // MessageBean.setMessage(request, MessageType.MSG_ERROR, "Error: Could not
    // update user settings!");
    // }
    //
    // } else if ("reload".equals(action)) {
    // try {
    // ConfigStore.loadDBValues();
    // MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "Reloaded user
    // settings successfully!");
    //
    // } catch (Exception e) {
    // s_logger.error("Error in reloading settings", e);
    // MessageBean.setMessage(request, MessageType.MSG_ERROR, "Error: Could not
    // reload user settings!");
    // }
    // }
    //
    // return AdminPages.SETTINGS_PAGE;
    // }

    private SettingsBean() {
    }

    /**
     * This API helps to manage the events. Accessible only to the Admin
     * 
     * @param request
     * @param response
     * @param queryParams
     * @return
     * @throws IOException
     */
    // public static AdminPages manageEvents(CustomSecurityWrapperRequest
    // request, CustomSecurityWrapperResponse response,
    // String[] queryParams) throws IOException {
    //
    // String action = (queryParams == null || queryParams.length < 1) ? null :
    // queryParams[0];
    //
    // boolean isDML = false;
    // StringBuilder jsonString = new StringBuilder("{");
    // if ("add".equals(action)) {
    // isDML = true;
    // EventData event = getEventObject(request);
    // StringBuilder reason = new StringBuilder();
    // if (event.validate(reason)) {
    // try {
    // DBUtil.addEventInDB(event);
    // jsonString.append("\"status\" : \"true\"");
    // } catch (Exception e) {
    // s_logger.error("Error in saving event info: ", e);
    // jsonString.append("\"status\" : \"false\",
    // \"reason\":\"").append(e.getMessage()).append("\"");
    // }
    // } else {
    // jsonString.append("\"status\" : \"false\",
    // \"reason\":\"").append(reason.toString()).append("\"");
    // }
    // } else if ("update".equals(action)) {
    // isDML = true;
    // EventData event = getEventObject(request);
    // StringBuilder reason = new StringBuilder();
    // if (event.validate(reason)) {
    // try {
    // DBUtil.editEventInDB(event);
    // jsonString.append("\"status\" : \"true\"");
    // } catch (Exception e) {
    // s_logger.error("Error in saving event info: ", e);
    // jsonString.append("\"status\" : \"false\",
    // \"reason\":\"").append(e.getMessage()).append("\"");
    // }
    // } else {
    // jsonString.append("\"status\" : \"false\",
    // \"reason\":\"").append(reason.toString()).append("\"");
    // }
    // } else if ("delete".equals(action)) {
    // isDML = true;
    // EventData event = new EventData();
    // String id = request.getParameter("id");
    // event.setId(id);
    //
    // try {
    // DBUtil.deleteEventInDB(event);
    // jsonString.append("\"status\" : \"true\"");
    // } catch (Exception e) {
    // s_logger.error("Error in saving event info: ", e);
    // jsonString.append("\"status\" : \"false\",
    // \"reason\":\"").append(e.getMessage()).append("\"");
    // }
    // }
    //
    // jsonString.append("}");
    //
    // if (isDML) {
    // response.setContentType("application/json");
    // response.getWriter().write(jsonString.toString());
    // return AdminPages.NO_PAGE;
    // }
    //
    // Integer pastMonths =
    // ConfigStore.getIntValue(CacheConfiguration.INT_PREV_EVENTS, 2);
    // Date currentDate = DateUtility.getCurrentDate();
    // Calendar cal = Calendar.getInstance();
    // cal.setTime(currentDate);
    // cal.add(Calendar.MONTH, pastMonths * -1);
    // cal.set(Calendar.DATE, 1);
    // List<EventData> events = DBUtil.getEvents(cal);
    // setEventsInRequest(request, events);
    // return AdminPages.EVENTS_PAGE;
    // }

    private static void setEventsInRequest(CustomSecurityWrapperRequest request, List<EventData> events) {
        request.setAttribute(EVENTS_RECORDS, events);
    }

    public static List<EventData> getEventsFromRequest(CustomSecurityWrapperRequest request) {
        Object object = request.getAttribute(EVENTS_RECORDS);
        if (object != null && (object instanceof List< ? >)) {
            return (List<EventData>) object;
        }

        return null;
    }

    private static EventData getEventObject(CustomSecurityWrapperRequest request) {
        EventData event = new EventData();
        String id = request.getParameter("id");
        String title = request.getParameter("title");
        Date start = DateUtility.parseDateInYYYYMMDDDashNullForException(request.getParameter("start"));
        Date end = DateUtility.parseDateInYYYYMMDDDashNullForException(request.getParameter("end"));
        String where = request.getParameter("where");
        String desc = request.getParameter("event-desc");
        String link = request.getParameter("event-link");
        String time = StringUtility.trimAndEmptyIsNull(request.getParameter("time"));
        String duration = StringUtility.trimAndEmptyIsNull(request.getParameter("duration"));
        event.setId(id);
        event.setTitle(title);
        event.setStart(start);
        event.setEnd(end);
        event.setAddress(where);
        event.setDescription(desc);
        event.setEventLink(link);
        event.setTime(time);
        event.setDuration(duration);
        return event;
    }

    public static String convertRecordsToJSON(List<EventData> events) {
        StringBuilder json = new StringBuilder("[");
        if (events != null && !events.isEmpty()) {
            for (EventData event : events) {
                json.append("{")

                        .append("\"id\":\"").append(event.getId()).append("\"")

                        .append(", \"title\": \"").append(StringUtility.getEscapedDoubleQuoteString(event.getTitle()))
                        .append("\"")

                        .append(", \"start\":\"").append(DateUtility.getDateInYYYYMMDDDash(event.getStart()))
                        .append("\"")

                        .append(", \"end\":\"").append(DateUtility.getDateInYYYYMMDDDash(event.getEnd())).append("\"")

                        .append(", \"location\":\"")
                        .append(StringUtility.escapeNewLineAndQuotes(event.getAddress(), ", ")).append("\"")

                        .append(", \"url\":\"").append(StringUtility.getEscapedDoubleQuoteString(event.getEventLink()))
                        .append("\"")

                        .append(", \"description\":\"")
                        .append(StringUtility.escapeNewLineAndQuotes(event.getDescription(), null))

                        .append("\", \"time\":\"").append(StringUtility.escapeNewLineAndQuotes(event.getTime(), null))

                        .append("\", \"duration\":\"")
                        .append(StringUtility.escapeNewLineAndQuotes(event.getDuration(), null))

                        .append("\"},");
            }
            json.deleteCharAt(json.length() - 1);// deleting the last
            // comma
            // character
        }
        json.append("]");
        s_logger.debug("json: \n" + json);
        return json.toString();
    }

    // public static AdminPages manageResources(CustomSecurityWrapperRequest
    // request,
    // CustomSecurityWrapperResponse response, String[] queryParams) throws
    // Exception {
    // User loggedInUser = SessionStore.getLoggedInUser(request);
    // if (queryParams != null && queryParams.length > 0) {
    // if ("add".equalsIgnoreCase(queryParams[0])) {
    // JsonObject jsonObj = new JsonObject();
    // String urlLink = StringUtility.trimAndEmptyIsNull(request.getParameter(
    // "url of the page to redirect user", "urlLink",
    // CustomSecurityRequestType.URL));
    // String imageURL =
    // StringUtility.trimAndEmptyIsNull(request.getParameter("the resource
    // value",
    // "imageUrl", CustomSecurityRequestType.URL));
    // s_logger.debug("urlLink : " + urlLink + ", imageURL: " + imageURL);
    //
    // String textContent =
    // StringUtility.trimAndEmptyIsNull(request.getParameter("Textual Campaign",
    // "textContent", CustomSecurityRequestType.DEFAULT_UNSAFE_HTML));
    //
    // ResourceType type =
    // ResourceType.getResourceType(StringUtility.trimAndEmptyIsNull(request.getParameter(
    // "resource type", "type",
    // CustomSecurityRequestType.DEFAULT_SAFE_STRING)));
    // s_logger.debug("textContent: " + textContent);
    //
    // ResourceObject bannerObj = new ResourceObject(urlLink, imageURL,
    // textContent, type);
    // int result = DBUtil.addBannerObject(bannerObj, loggedInUser);
    // if (result == -1) {
    // jsonObj.addProperty("status", false);
    // } else {
    // jsonObj.addProperty("status", true);
    // JsonArray jsonArr = new JsonArray();
    // jsonObj.add("rows", jsonArr);
    // JsonObject rowObj = new JsonObject();
    // jsonArr.add(rowObj);
    // rowObj.addProperty("id", result);
    // rowObj.addProperty("urlLink",
    // StringUtility.trimAndNullIsEmpty(bannerObj.getUrlLink()));
    // rowObj.addProperty("imageUrl",
    // StringUtility.trimAndNullIsEmpty(bannerObj.getImageUrl()));
    // rowObj.addProperty("textContent",
    // StringUtility.trimAndNullIsEmpty(bannerObj.getTextContent()));
    // rowObj.addProperty("type", type.name());
    // }
    //
    // response.setContentType("application/json");
    // response.getWriter().write(jsonObj.toString());
    // return AdminPages.NO_PAGE;
    // } else if ("getAll".equalsIgnoreCase(queryParams[0])) {
    // Map<ResourceType, List<ResourceObject>> resourceObjects =
    // DBUtil.getAllBannerObjects();
    // JsonObject jsonObj = new JsonObject();
    // if (resourceObjects != null) {
    // jsonObj.addProperty("status", true);
    // JsonArray jsonArr = new JsonArray();
    // jsonObj.add("rows", jsonArr);
    // for (ResourceType type : resourceObjects.keySet()) {
    // List<ResourceObject> bannerObjects = resourceObjects.get(type);
    // for (ResourceObject bannerObj : bannerObjects) {
    // JsonObject rowObj = new JsonObject();
    // jsonArr.add(rowObj);
    // rowObj.addProperty("id", bannerObj.getId());
    // rowObj.addProperty("urlLink",
    // StringUtility.trimAndNullIsEmpty(bannerObj.getUrlLink()));
    // rowObj.addProperty("imageUrl",
    // StringUtility.trimAndNullIsEmpty(bannerObj.getImageUrl()));
    // rowObj.addProperty("textContent",
    // StringUtility.trimAndNullIsEmpty(bannerObj.getTextContent()));
    // rowObj.addProperty("type", type.name());
    // }
    // }
    // } else {
    // jsonObj.addProperty("status", false);
    // }
    //
    // response.setContentType("application/json");
    // response.getWriter().write(jsonObj.toString());
    // return AdminPages.NO_PAGE;
    //
    // } else if ("delete".equalsIgnoreCase(queryParams[0])) {
    // String id = request.getParameter("Banner content id", "id",
    // CustomSecurityRequestType.DEFAULT_SAFE_STRING);
    //
    // boolean isDeleted = DBUtil.deleteBanner(id);
    // JsonObject jsonObj = new JsonObject();
    // if (isDeleted) {
    // jsonObj.addProperty("status", true);
    // jsonObj.addProperty("id", id);
    // } else {
    // jsonObj.addProperty("status", false);
    // }
    //
    // response.setContentType("application/json");
    // response.getWriter().write(jsonObj.toString());
    // return AdminPages.NO_PAGE;
    // }
    // }
    //
    // return AdminPages.RESOURCES_PAGE;
    // }

}
