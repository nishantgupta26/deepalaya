package org.communitylibrary.data.common;

import java.util.List;

import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class Phone {
    List<String> m_phones = null;

    public Phone(String phone) {
        m_phones = StringUtility.splitToStringList(phone, ";");
    }

    public static String serialize(Phone phone) {
        if (phone == null || phone.m_phones == null || phone.m_phones.isEmpty()) {
            return null;
        }

        return StringUtility.join(phone.m_phones, ";");
    }

    public static Phone deserialize(String str) {
        Phone phone = new Phone(str);
        return phone;
    }

    public static String getPhoneNumberString(Phone phone) {
        if (phone == null) {
            return StringUtility.EMPTY_STRING;
        }

        if (phone.m_phones == null || phone.m_phones.isEmpty()) {
            return StringUtility.EMPTY_STRING;
        }

        return StringUtility.join(phone.m_phones, ",");
    }

    /**
     * @return the phones
     */
    public List<String> getPhones() {
        return m_phones;
    }

}
