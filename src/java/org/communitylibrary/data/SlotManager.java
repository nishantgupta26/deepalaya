package org.communitylibrary.data;

import java.util.List;

import org.communitylibrary.data.digitalaccess.Slot;

import com.google.gson.JsonArray;

/**
 * @author nishant.gupta
 *
 */
public class SlotManager {

    public static JsonArray convertToSlotEventJson(List<Slot> slots) {
        JsonArray array = new JsonArray();

        if (slots != null) {
            for (Slot slot : slots) {
                array.add(slot.convertToJson());
            }
        }
        return array;
    }

}
