package org.communitylibrary.data.user;

import org.communitylibrary.admin.ui.navigation.AdminActions;

public enum UserRoles {
    USER('U', "User"),

    ADMINISTRATOR('A', "Administrator"),

    NOT_LOGGED_IN('N', "Not Logged In"),

    ADMINNORMAL('L', "NormalAdmin"),

    ;

    private String m_serial;
    private String m_displayName;

    UserRoles(char serial, String displayName) {
        m_serial = "" + serial;
        m_displayName = displayName;
    }

    public String serialize() {
        return m_serial;
    }

    public String getDisplayName() {
        return m_displayName;
    }

    public static UserRoles[] getAllRoles() {
        return new UserRoles[] { NOT_LOGGED_IN, USER, ADMINISTRATOR };
    }

    public static UserRoles deserialize(String serial) {
        if (serial == null || serial.length() != 1) {
            return null;
        }

        for (UserRoles r : UserRoles.values()) {
            if (r.serialize().equals(serial)) {
                return r;
            }
        }

        return null;
    }

    public static UserRoles[] getLoggedInRoles() {
        return new UserRoles[] { USER, ADMINISTRATOR, ADMINNORMAL };
    }

    public static UserRoles[] getAdminPageLoggedInRoles() {
        return new UserRoles[] { ADMINISTRATOR, ADMINNORMAL };
    }

    public boolean isLoggedInRole() {
        return !isNotLoggedInRole();
    }

    public boolean isNotLoggedInRole() {
        return this == NOT_LOGGED_IN;
    }

    public boolean isAdministrator() {
        return this == ADMINISTRATOR;
    }

    public static UserRoles[] getNoLoggedInRoles() {
        return new UserRoles[] { NOT_LOGGED_IN };
    }

    public static UserRoles[] getAdminRoles() {
        return new UserRoles[] { ADMINISTRATOR };
    }

    public static UserRoles[] getSuprressedPrivilegeRoles() {
        return new UserRoles[] { ADMINNORMAL };
    }

    public boolean isAdminPageRole() {
        return (this == ADMINNORMAL || this == ADMINISTRATOR);
    }
}
