package org.communitylibrary.data.user.dto;

import java.sql.ResultSet;
import java.util.Date;

import org.communitylibrary.db.WebsiteDBConnector;

/**
 * @author nishant.gupta
 *
 */
public class UserLoginDTO {

    private int  numLogins = -1;
    private Date lastLogin = null;

    public UserLoginDTO(WebsiteDBConnector dbCon, ResultSet result) throws Exception {
        numLogins = result.getInt("num_logins");
        lastLogin = dbCon.getTimeStampAsDate(result, "last_login");
    }

    /**
     * @return the numLogins
     */
    public int getNumLogins() {
        return numLogins;
    }

    /**
     * @return the lastLogin
     */
    public Date getLastLogin() {
        return lastLogin;
    }

}
