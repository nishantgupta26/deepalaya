package org.communitylibrary.data.members;

public enum LibraryGroups {
    LOWERGROUP("L", "Lower Group"),

    UPPERGROUP("U", "Upper Group"),

    HEADSTART("H", "Headstart"),

    ADULTS("A", "Adults"),;

    private String m_serial;
    private String m_displayName;

    LibraryGroups(String serial, String displayName) {
        m_serial = serial;
        m_displayName = displayName;
    }

    public String serialize() {
        return m_serial;
    }

    public static String serialize(LibraryGroups group) {
        if (group == null) {
            return null;
        }
        return group.m_serial;
    }

    public String getDisplayName() {
        return m_displayName;
    }

    public static LibraryGroups deserialize(String serial) {
        if (serial == null || serial.length() != 1) {
            return null;
        }

        for (LibraryGroups r : LibraryGroups.values()) {
            if (r.serialize().equals(serial)) {
                return r;
            }
        }

        return null;
    }

    public static LibraryGroups getLibraryGroupFromName(String name) {
        if (name == null || name.trim().isEmpty()) {
            return null;
        }

        name = name.replaceAll("[^A-Za-z]", "").toLowerCase();

        for (LibraryGroups r : LibraryGroups.values()) {
            if (r.m_displayName.replaceAll("[^A-Za-z]", "").toLowerCase().startsWith(name)) {
                return r;
            }
        }

        return null;
    }
}
