package org.communitylibrary.data.members;

import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public enum LibraryGroupForDigitalAccess {
    UPPER('U', "Unrestricted access on the internet"),

    LOWER('L', "Restricted access on the internet"), ;

    private char   m_code;
    private String m_desc;

    private LibraryGroupForDigitalAccess(char code, String desc) {
        m_code = code;
        m_desc = desc;
    }

    public char getCode() {
        return m_code;
    }

    public void setCode(char code) {
        m_code = code;
    }

    public String getDesc() {
        return m_desc;
    }

    public void setDesc(String desc) {
        m_desc = desc;
    }

    public static LibraryGroupForDigitalAccess deserialize(String code) {
        if (StringUtility.isEmpty(code)) {
            return null;
        }

        char repCharacter = code.toUpperCase().charAt(0);

        for (LibraryGroupForDigitalAccess group : LibraryGroupForDigitalAccess.values()) {
            if (group.getCode() == repCharacter) {
                return group;
            }
        }

        return null;
    }

}
