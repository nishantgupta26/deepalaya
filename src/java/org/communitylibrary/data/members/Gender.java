package org.communitylibrary.data.members;

/**
 * @author nishant.gupta
 *
 */
public enum Gender {
    Male("M"),

    Female("F"),

    Transgender("T"),

    Others("O"),

    ;

    private String m_code;

    private Gender(String code) {
        m_code = code;
    }

    public String getCode() {
        return m_code;
    }

    public static Gender deserialize(String genderCode) {
        if (genderCode == null || (genderCode = genderCode.trim()).isEmpty()) {
            return Male;
        }

        for (Gender g : Gender.values()) {
            if (g.getCode().equals(genderCode)) {
                return g;
            }
        }

        return Male;
    }

    public String serialize() {
        return getCode();
    }

}
