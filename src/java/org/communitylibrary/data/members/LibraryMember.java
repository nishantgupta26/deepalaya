package org.communitylibrary.data.members;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletContext;

import org.communitylibrary.data.LibraryBranch;
import org.communitylibrary.data.common.Phone;
import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class LibraryMember extends DBObject {

    private static final long serialVersionUID = 4459838080373075768L;
    private String            m_studyClass;
    private String            m_id;
    private String            m_name;
    private String            m_dob;
    private String            m_age;
    private Phone             m_phone;
    private String            m_motherName;
    private String            m_fatherName;
    private String            m_address;
    private LibraryGroups     m_libraryGroup;
    private String            m_school;
    private String            m_photoId;
    private Gender            m_gender;
    private LibraryBranch     m_branch;
    private String            m_joinDate;

    public LibraryMember(String id, String name, String dob, String age, String phone, String motherName,
            String fatherName, String address, LibraryGroups lGroup, String school, String studyClass, Gender gender,
            LibraryBranch branch, String joinDate) {
        m_id = id;
        m_name = name;
        m_dob = dob;
        m_age = age;
        if (phone != null && !(phone = phone.trim()).isEmpty()) {
            m_phone = new Phone(phone);
        }
        m_motherName = motherName;
        m_fatherName = fatherName;
        m_address = address;
        m_libraryGroup = lGroup;
        m_school = school;
        m_studyClass = studyClass;
        m_gender = gender;
        m_branch = branch;
        m_joinDate = joinDate;
    }

    public LibraryMember(String id) {
        m_id = id;
    }

    public LibraryMember(ResultSet result, int a) throws Exception {
        m_id = result.getString("lm.id");
        m_name = result.getString("lm.name");
    }

    public LibraryMember(ResultSet result) throws Exception {
        m_id = result.getString("id");
        m_name = result.getString("name");
        m_dob = StringUtility.trimAndEmptyIsNull(result.getString("dob"));
        m_age = StringUtility.trimAndEmptyIsNull(result.getString("age"));
        m_phone = Phone.deserialize(StringUtility.trimAndEmptyIsNull(result.getString("phone")));
        m_motherName = StringUtility.trimAndEmptyIsNull(result.getString("mother_name"));
        m_fatherName = StringUtility.trimAndEmptyIsNull(result.getString("father_name"));
        m_address = StringUtility.trimAndEmptyIsNull(result.getString("address"));
        m_libraryGroup = LibraryGroups.deserialize(StringUtility.trimAndEmptyIsNull(result.getString("library_group")));
        m_school = StringUtility.trimAndEmptyIsNull(result.getString("school"));
        m_studyClass = StringUtility.trimAndEmptyIsNull(result.getString("class"));
        m_photoId = StringUtility.trimAndEmptyIsNull(result.getString("photo_id"));
        m_gender = Gender.deserialize(StringUtility.trimAndEmptyIsNull(result.getString("gender")));
    }

    // public LibraryMember(String id, String name, String dob, String age,
    // String phone, String motherName,
    // String fatherName, String address, LibraryGroups lGroup, String school,
    // String studyClass) {
    // this(id, name, dob, age, phone, motherName, fatherName, address, lGroup,
    // school, studyClass, null, null);
    // if (id != null) {
    // generatePhotoId();
    // }
    // }

    /**
     * @return the class
     */
    public String getStudyClass() {
        return m_studyClass;
    }

    /**
     * @return the id
     */
    public String getId() {
        return m_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return m_name;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return m_dob;
    }

    /**
     * @return the phone
     */
    public Phone getPhone() {
        return m_phone;
    }

    /**
     * @return the motherName
     */
    public String getMotherName() {
        return m_motherName;
    }

    /**
     * @return the fatherName
     */
    public String getFatherName() {
        return m_fatherName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return m_address;
    }

    /**
     * @return the libraryGroup
     */
    public LibraryGroups getLibraryGroup() {
        return m_libraryGroup;
    }

    /**
     * @return the school
     */
    public String getSchool() {
        return m_school;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return m_age;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the photoId
     */
    public String getPhotoId() {
        return m_photoId;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        m_id = id;
    }

    public void generatePhotoId() {
        if (m_id == null) {
            return;
        }

        m_photoId = m_id + "_" + (m_name.replaceAll("[^A-Za-z]", "").toLowerCase());

    }

    public String checkAndDisplayPhotoId(ServletContext sc) {
        if (m_photoId == null) {
            generatePhotoId();
        }

        String memberPhotoURL = sc.getRealPath("/static/images/members/" + m_photoId + ".JPG");
        File file = new File(memberPhotoURL);
        // s_logger.debug("member photo url: " + file.getAbsolutePath());
        if (file.exists()) {
            return "Exists";
        }

        return m_photoId + ".JPG";
    }

    public String generatePhotoIdIfNull() {
        if (m_photoId != null) {
            return m_photoId;
        }

        generatePhotoId();
        return m_photoId;
    }

    @Override
    protected void editTable(Connection conn) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected String getHistoryInsertQuery() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return m_gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        m_gender = gender;
    }

    /**
     * @return the branch
     */
    public LibraryBranch getBranch() {
        return m_branch;
    }

    /**
     * @param branch the branch to set
     */
    public void setBranch(LibraryBranch branch) {
        m_branch = branch;
    }

    /**
     * @return the joinDate
     */
    public String getJoinDate() {
        return m_joinDate;
    }

    /**
     * @param joinDate the joinDate to set
     */
    public void setJoinDate(String joinDate) {
        m_joinDate = joinDate;
    }

}
