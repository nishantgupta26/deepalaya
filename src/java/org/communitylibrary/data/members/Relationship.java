package org.communitylibrary.data.members;

import org.communitylibrary.logger.Logger;

public enum Relationship {
    Father("F"), Mother("M"), Sister("S"), Brother("B"), Guardian("G"), Son("O"), Daughter("D"), Husband("H"), Wife("W");

    private String        m_relInd;

    private static Logger s_logger = Logger.getInstance(Relationship.class);

    private Relationship(String ind) {
        m_relInd = ind;
    }

    public String serialize() {
        return m_relInd;
    }

    public static Relationship deserialize(String ind) {
        if (ind == null || (ind = ind.trim()).isEmpty()) {
            return null;
        }

        for (Relationship relation : Relationship.values()) {
            if (relation.m_relInd.equals(ind)) {
                return relation;
            }
        }

        return null;

    }

    public static String toJSON() {
        StringBuilder json = new StringBuilder("[");
        for (Relationship relation : Relationship.values()) {
            json.append("{\"id\":\"").append(relation.serialize()).append("\", \"name\":\"").append(relation.name())
                    .append("\"},");
        }
        json.deleteCharAt(json.length() - 1);
        json.append("]");
        String toString = json.toString();
        s_logger.debug("Relationship json: " + toString);
        return toString;
    }

}
