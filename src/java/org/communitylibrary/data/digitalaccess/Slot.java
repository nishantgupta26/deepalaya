package org.communitylibrary.data.digitalaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.apache.log4j.Logger;
import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.data.members.LibraryMember;
import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 */
public class Slot extends DBObject {
    private static final long serialVersionUID = 1L;
    private static Logger s_logger = Logger.getLogger(Slot.class);

    private String m_id;
    private Date m_start;
    private Date m_end;
    private String m_notes;
    private String m_date;
    private String m_startTime;
    private String m_endTime;
    private String m_duration;
    private LibraryMember m_member;
    private SlotStatus m_status;
    private String m_slotType;

    public Slot(ResultSet result) throws Exception {
        WebsiteDBConnector s_staticInstance = WebsiteDBConnector.getStaticInstance();

        m_id = result.getString("id");
        m_start = s_staticInstance.getTimeStampAsDate(result, "s.start_date");
        m_end = s_staticInstance.getTimeStampAsDate(result, "s.end_date");
        m_duration = StringUtility.trimAndEmptyIsNull(result.getString("s.duration"));
        LibraryMember member = new LibraryMember(result, 1);
        setMember(member);
        m_notes = StringUtility.trimAndEmptyIsNull(result.getString("s.notes"));
        m_date = StringUtility.trimAndEmptyIsNull(result.getString("s.slot_date"));
        m_startTime = StringUtility.trimAndEmptyIsNull(result.getString("s.start_time"));
        m_endTime = StringUtility.trimAndEmptyIsNull(result.getString("s.end_time"));
        m_status = SlotStatus.getStatusFromCode(result.getString("s.status"));
        m_slotType = StringUtility.trimAndEmptyIsNull(result.getString("s.slot_type"));
    }

    public Slot() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void editTable(Connection conn) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected String getHistoryInsertQuery() {
        // TODO Auto-generated method stub
        return null;
    }

    public JsonElement convertToJson() {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("id", getId());
        jsonObject.addProperty("title", getMember().getName());
        jsonObject.addProperty("start", DateUtility.getDateInGivenFormat(getStart(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.addProperty("end", DateUtility.getDateInGivenFormat(getEnd(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.addProperty("notes", StringUtility.trimAndNullIsEmpty(getNotes()));
        jsonObject.addProperty("slotdate", StringUtility.trimAndNullIsEmpty(getDate()));
        jsonObject.addProperty("starttime", getStartTime());
        jsonObject.addProperty("endtime", getEndTime());
        jsonObject.addProperty("duration", getDuration());
        jsonObject.addProperty("memberid", getMember().getId());
        jsonObject.addProperty("status", getStatus().getCode());
        jsonObject.addProperty("slotType", getSlotType());
        jsonObject.addProperty("color", getEventColorForSlotType());
        jsonObject.addProperty("allDay", false);

        return jsonObject;
    }

    private String getEventColorForSlotType() {
        if(!"laptop".equalsIgnoreCase(m_slotType)) {
            return "#a6ad3a";
        }

        return "#3a87ad";
    }

    public String getSlotType() {
        return m_slotType;
    }

    public String getId() {
        return m_id;
    }

    public void setId(String id) {
        m_id = id;
    }

    public Date getStart() {
        return m_start;
    }

    public void setStart(Date start) {
        m_start = start;
    }

    public Date getEnd() {
        return m_end;
    }

    public void setEnd(Date end) {
        m_end = end;
    }

    public String getNotes() {
        return m_notes;
    }

    public void setNotes(String notes) {
        m_notes = notes;
    }

    public String getStartTime() {
        return m_startTime;
    }

    public void setStartTime(String startTime) {
        m_startTime = startTime;
    }

    public String getEndTime() {
        return m_endTime;
    }

    public void setEndTime(String endTime) {
        m_endTime = endTime;
    }

    public String getDuration() {
        return m_duration;
    }

    public void setDuration(String duration) {
        m_duration = duration;
    }

    public LibraryMember getMember() {
        return m_member;
    }

    public void setMember(LibraryMember member) {
        m_member = member;
    }

    public SlotStatus getStatus() {
        return m_status;
    }

    public void setStatus(SlotStatus status) {
        m_status = status;
    }

    public String getDate() {
        return m_date;
    }

    public void setDate(String date) {
        m_date = date;
    }

    public static Slot getSlotObject(CustomSecurityWrapperRequest request) throws Exception {
        Slot slot = new Slot();
        String id = request.getParameter("id");
        String memberId = request.getParameter("memberid");
        Date start = DateUtility.parseDateInMysqlTSFormat(request.getParameter("start"));
        Date end = DateUtility.parseDateInMysqlTSFormat(request.getParameter("end"));
        String duration = StringUtility.trimAndEmptyIsNull(request.getParameter("duration"));
        String notes = StringUtility.trimToSize(StringUtility.trimAndEmptyIsNull(request.getParameter("notes")), 500);
        String date = StringUtility.trimAndEmptyIsNull(request.getParameter("date"));
        String startTime = StringUtility.trimAndEmptyIsNull(request.getParameter("starttime"));
        String endTime = StringUtility.trimAndEmptyIsNull(request.getParameter("endtime"));
        String slotType = StringUtility.trimAndEmptyIsNull(request.getParameter("slotType"));
        slot.setId(id);
        LibraryMember member = new LibraryMember(memberId);
        slot.setMember(member);
        slot.setStart(start);
        slot.setEnd(end);
        slot.setNotes(notes);
        slot.setDuration(duration);
        slot.setDate(date);
        slot.setStartTime(startTime);
        slot.setEndTime(endTime);
        slot.setStatus(SlotStatus.New);
        slot.setSlotType(slotType);
        return slot;
    }

    public boolean validate(StringBuilder reason) {

        if (!StringUtility.isNonEmpty(getId())) {
            reason.append("Slot ID cannot be empty");
            s_logger.info("Slot ID cannot be empty");
            return false;
        }

        if (!StringUtility.isNonEmpty(getMember().getId())) {
            reason.append("Library Member cannot be empty");
            s_logger.info("Library Member cannot be empty");
            return false;
        }

        if (getStart() == null || getStartTime() == null) {
            reason.append("Slot Start time cannot be empty");
            s_logger.info("Slot Start time cannot be empty");
            return false;
        }

        if (getEnd() == null || getEndTime() == null) {
            reason.append("Slot End time cannot be empty");
            s_logger.info("Slot End time cannot be empty");
            return false;
        }

        if (!StringUtility.isNonEmpty(getDate())) {
            reason.append("Slot date cannot be empty");
            s_logger.info("Slot date cannot be empty");
            return false;
        }

        if (!StringUtility.isNonEmpty(getDuration())) {
            reason.append("Slot Duration cannot be empty");
            s_logger.info("Slot Duration cannot be empty");
            return false;
        }

        return true;

    }

    public void setSlotType(String slotType) {
        m_slotType = slotType;
    }

}
