package org.communitylibrary.data.digitalaccess;

public enum SlotStatus {
    Attended('A'),

    NoShow('O'),

    New('N'),

    ;

    private char m_code;

    private SlotStatus(char code) {
        m_code = code;
    }

    public char getCode() {
        return m_code;
    }

    public static SlotStatus getStatusFromCode(String code) {
        if (code == null || (code = code.trim()).isEmpty()) {
            return SlotStatus.New;
        }

        char c = code.charAt(0);

        for (SlotStatus status : SlotStatus.values()) {
            if (c == status.getCode()) {
                return status;
            }
        }

        return SlotStatus.New;
    }

}
