package org.communitylibrary.data.books;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public enum BookAccount {
    BFE("Bengali - Fiction Easy", "BFE", "B/FE"),

    BFG("Bengali - Fiction General", "BFG", "B/FG"),

    BFPB("Bengali - Fiction Picture Book", "BFPB", "B/FPB"),

    BGR("Bengali - Graphics", "BGR", "B/Gr"),

    BNF("Bengali - NonFiction", "BNF", "B/NF"),

    EFE("English - Fiction Easy", "EFE", "E/FE"),

    EFGEN("English - Fiction General", "EFG", "E/FG"),

    EFPB("English - Fiction Picture Book", "EFPB", "E/F-PB"),

    EGR("English - Graphics", "EGR", "E/Gr"),

    ENF("English - NonFiction", "ENF", "E/NF"),

    HFE("Hindi - Fiction Easy", "HFE", "H/FE"),

    HFGEN("Hindi - Fiction General", "HFG", "H/FG"),

    HFPB("Hindi - Fiction Picture Book", "HFPB", "H/F-PB"),

    HGR("Hindi - Graphics", "HGR", "H/Gr"),

    HNF("Hindi - NonFiction", "HNF", "H/NF"),

    UFE("Urdu - Fiction Easy", "UFE", "U/FE"),

    UFG("Urdu - Fiction General", "UFG", "U/FG"),

    UPBB("Urdu - Picture Book", "UPB", "U/F-PB"),

    UNF("Urdu - NonFiction", "UNF", "U/NF"),

    POEM("Poem", "P", "P"),

    REF("Reference", "REF", "REF"),

    ;

    private String m_desc;
    private String m_code;
    private String m_disp;

    private BookAccount(String desc, String code, String disp) {
        m_desc = desc;
        m_code = code;
        m_disp = disp;
    }

    public String getDesc() {
        return m_desc;
    }

    public String getCode() {
        return m_code;
    }

    public static BookAccount getBookAccount(String accountNumber) {
        if (accountNumber == null || (accountNumber = accountNumber.trim()).isEmpty()) {
            return null;
        }

        accountNumber = accountNumber.replaceAll("[0-9]+.*", "");
        accountNumber = accountNumber.replaceAll("[^A-Za-z]", "");
        for (BookAccount account : BookAccount.values()) {
            if (account.getCode().equalsIgnoreCase(accountNumber)) {
                return account;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(getBookAccount("U/PB-39 (B)"));
    }

    public static BookAccount getBookAccountByCode(String code) {
        if (code == null || (code = code.trim()).isEmpty()) {
            return null;
        }

        for (BookAccount account : BookAccount.values()) {
            if (code.equals(account.getCode())) {
                return account;
            }
        }
        return null;
    }

    public static String getBookAccountsJson() {
        JsonArray array = new JsonArray();
        for (BookAccount account : BookAccount.values()) {
            JsonObject object = new JsonObject();
            object.addProperty("name", account.getDesc().toUpperCase());
            object.addProperty("code", account.getCode());
            object.addProperty("disp", account.getDisp());
            array.add(object);
        }
        return array.toString();
    }

    public String getDisp() {
        return m_disp;
    }
}
