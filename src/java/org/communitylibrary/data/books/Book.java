package org.communitylibrary.data.books;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.communitylibrary.data.books.util.BookLoaderAgent;
import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.util.CollectionsUtil;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * @author nishant.gupta
 *
 */
public class Book extends DBObject {

    private static final long   serialVersionUID = 3998016328200540770L;
    private static final String s_tableName      = "books";

    private int                 m_id;
    private BookAccount         m_bookType;
    private int                 m_bookCode;
    private String              m_isbn10;
    private String              m_isbn13;
    private String              m_title;
    private String              m_description;
    private List<String>        m_authors;
    private String              m_publisher;
    private boolean             m_isLoadedThroughAPI;
    private BookLoaderAgent     m_loader;
    private Date                m_genTS;
    private Date                m_loaderTS;
    private List<String>        m_imageLinks;
    private String              m_thumbnailImageLink;
    private int                 m_numCopies;

    public Book(ResultSet rs, boolean loadMetadata) throws Exception {
        m_id = rs.getInt("id");
        m_title = rs.getString("title");
        m_bookType = BookAccount.getBookAccountByCode(rs.getString("book_type"));
        m_bookCode = rs.getInt("book_code");
        m_isbn10 = rs.getString("isbn_10");
        m_isbn13 = rs.getString("isbn_13");
        m_numCopies = rs.getInt("copies");
        m_authors = StringUtility.splitToStringList(rs.getString("author"), ";");
        if (loadMetadata) {
            m_description = StringUtility.trimAndEmptyIsNull(rs.getString("description"));
            m_publisher = StringUtility.trimAndEmptyIsNull(rs.getString("publisher"));
            m_isLoadedThroughAPI = "Y".equals(StringUtility.trimAndEmptyIsNull(rs.getString("is_loaded")));
            m_loader = BookLoaderAgent.getLoaderAgent(StringUtility.trimAndEmptyIsNull(rs.getString("loader")));
            List<String> imageLinks = StringUtility.splitToStringList(
                    StringUtility.trimAndEmptyIsNull(rs.getString("images")), ";");
            if (imageLinks.isEmpty()) {
                m_imageLinks = imageLinks;
            }
        }
    }

    public Book() {
    }

    @Override
    protected void editTable(Connection conn) throws Exception {

    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected String getHistoryInsertQuery() {
        return null;
    }

    public int getId() {
        return m_id;
    }

    public void setId(int id) {
        m_id = id;
    }

    public BookAccount getBookType() {
        return m_bookType;
    }

    public void setBookType(BookAccount bookType) {
        m_bookType = bookType;
    }

    public int getBookCode() {
        return m_bookCode;
    }

    public void setBookCode(int bookCode) {
        m_bookCode = bookCode;
    }

    public String getTitle() {
        return m_title;
    }

    public void setTitle(String title) {
        m_title = title;
    }

    public String getDescription() {
        return m_description;
    }

    public void setDescription(String description) {
        m_description = description;
    }

    public List<String> getAuthors() {
        return m_authors;
    }

    public void setAuthors(List<String> authors) {
        m_authors = authors;
    }

    public boolean isLoadedThroughAPI() {
        return m_isLoadedThroughAPI;
    }

    public void setLoadedThroughAPI(boolean isLoadedThroughAPI) {
        m_isLoadedThroughAPI = isLoadedThroughAPI;
    }

    public BookLoaderAgent getLoaderAgent() {
        return m_loader;
    }

    public void setLoaderAgent(BookLoaderAgent agent) {
        m_loader = agent;
    }

    public Date getGenTS() {
        return m_genTS;
    }

    public void setGenTS(Date genTS) {
        m_genTS = genTS;
    }

    public Date getLoaderTS() {
        return m_loaderTS;
    }

    public void setLoaderTS(Date loaderTS) {
        m_loaderTS = loaderTS;
    }

    public String getIsbn10() {
        return m_isbn10;
    }

    public void setIsbn10(String isbn10) {
        m_isbn10 = isbn10;
    }

    public String getIsbn13() {
        return m_isbn13;
    }

    public void setIsbn13(String isbn13) {
        m_isbn13 = isbn13;
    }

    public List<String> getImageLinks() {
        return m_imageLinks;
    }

    public void setImageLinks(List<String> imageLinks) {
        m_imageLinks = imageLinks;
    }

    public String getThumbnailImageLink() {
        return m_thumbnailImageLink;
    }

    public void setThumbnailImageLink(String thumbnailImageLink) {
        m_thumbnailImageLink = thumbnailImageLink;
    }

    public boolean setIsbn(String isbn) {
        if (isbn == null) {
            return false;
        }

        if (isbn.length() == 10 && getIsbn10() == null) {
            setIsbn10(isbn);
            return true;
        }

        if (isbn.length() == 13 && getIsbn13() == null) {
            setIsbn13(isbn);
            return true;
        }

        return false;

    }

    public String getPublisher() {
        return m_publisher;
    }

    public void setPublisher(String publisher) {
        m_publisher = publisher;
    }

    public void addImage(String link) {
        if (link == null) {
            return;
        }
        if (m_imageLinks == null) {
            m_imageLinks = new ArrayList<String>();
        }

        m_imageLinks.add(link);

    }

    public static String getInsertQuery() {
        String query = "insert into "
                + s_tableName
                + " (book_type, book_code, isbn_10, isbn_13, title, author, copies, gen_ts) values(?, ?,?,?,?,?,?, NOW())";
        return query;
    }

    public boolean isAdd() {
        return m_title != null && m_bookType != null
                && (!CollectionsUtil.isEmpty(m_authors) || m_isbn10 != null || m_isbn13 != null);
    }

    public static JsonArray convertToListJson(List<Book> books) {
        if (books == null || books.isEmpty()) {
            return null;
        }

        JsonArray array = new JsonArray();
        for (Book book : books) {
            JsonObject object = book.convertToJsonObject(false);
            array.add(object);
        }
        return array;
    }

    public JsonObject convertToJsonObject(boolean addMoreDetails) {
        JsonObject object = new JsonObject();
        object.addProperty("id", getId());
        object.addProperty("type", getBookType().getDisp());
        object.addProperty("code", getBookCode());
        object.addProperty("isbn10", getIsbn10());
        object.addProperty("isbn13", getIsbn13());
        object.addProperty("authors", StringUtility.join(getAuthors(), ";"));
        object.addProperty("title", getTitle());
        object.addProperty("copies", getNumCopies());
        if (addMoreDetails) {
            if (getDescription() != null) {
                object.addProperty("desc", getDescription());
            }
            if (getPublisher() != null) {
                object.addProperty("publisher", getPublisher());
            }

            if (getImageLinks() != null) {
                JsonArray images = new JsonArray();
                for (String image : m_imageLinks) {
                    JsonPrimitive primitive = new JsonPrimitive(image);
                    images.add(primitive);
                }
                object.add("images", images);
            }
        }
        return object;
    }

    public static String getSearchBookQuery(Book book) {
        String query = "select * from " + s_tableName + " where id=?";
        if (book.getIsbn10() != null) {
            query += " and isbn_10=?";
        }

        if (book.getIsbn13() != null) {
            query += " and isbn_13=?";
        }

        return query;
    }

    public void addAuthor(String author) {
        if (author == null) {
            return;
        }

        if (m_authors == null) {
            m_authors = new ArrayList<String>();
        }

        m_authors.add(author);
    }

    public static String getBookListQuery(Book filterObject) {
        String query = "select * from " + s_tableName;

        query = addFilters(filterObject, query);

        return query;
    }

    private static String addFilters(Book filterObject, String query) {
        if (filterObject != null) {
            query += " where ";
            boolean isAdded = false;
            if (filterObject.getTitle() != null) {
                query += "LOWER(title) like ?";
                isAdded = true;
            }

            if (filterObject.getIsbn10() != null || filterObject.getIsbn13() != null) {
                if (isAdded) {
                    query += " and ";
                }

                // String isbn = filterObject.getIsbn10() != null ?
                // filterObject.getIsbn10() : filterObject.getIsbn13();
                query += "(isbn_10 = ? or isbn_13=?)";
                isAdded = true;
            }

            if (filterObject.getBookType() != null) {
                if (isAdded) {
                    query += " and ";
                }

                query += "book_type=?";
                isAdded = true;
            }

            if (filterObject.getAuthors() != null) {
                if (isAdded) {
                    query += " and ";
                }

                query += "LOWER(author) like ?";
                isAdded = true;
            }
        }
        return query;
    }

    public static String getBookCountQuery(Book filterObject) {
        String query = "select count(*) from " + s_tableName;

        query = addFilters(filterObject, query);

        return query;
    }

    public int getNumCopies() {
        return m_numCopies;
    }

    public void setNumCopies(int numCopies) {
        m_numCopies = numCopies;
    }

    public String getEditQuery() {
        return "update "
                + s_tableName
                + " set isbn_10=?, isbn_13=?, title=?, author=?, publisher=?, description=?, is_loaded=?, loader=?, images=?, gen_ts=NOW() where id=?";
    }
}
