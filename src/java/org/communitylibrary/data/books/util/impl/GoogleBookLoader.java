package org.communitylibrary.data.books.util.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.communitylibrary.app.engine.WebserviceAPI;
import org.communitylibrary.data.books.Book;
import org.communitylibrary.data.books.util.BookLoader;
import org.communitylibrary.data.books.util.BookLoaderAgent;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author nishant.gupta
 *
 */
public class GoogleBookLoader implements BookLoader {

    private Book                m_book;
    private static Logger       s_logger  = Logger.getLogger(GoogleBookLoader.class);
    private final static String m_baseURL = "https://www.googleapis.com/books/v1/volumes?q=isbn:";

    public GoogleBookLoader(Book book) {
        setBook(book);
    }

    public Book getBook() {
        return m_book;
    }

    public void setBook(Book book) {
        m_book = book;
    }

    @Override
    public boolean populateBookDetailsFromAPI() {
        String firstIsbn = m_book.getIsbn10();
        String secondIsbn = m_book.getIsbn13();

        String responseJSONString = null;
        if (firstIsbn != null) {
            String url = m_baseURL + firstIsbn;
            responseJSONString = WebserviceAPI.getResponse(url, true);
        }

        if (responseJSONString == null && secondIsbn != null) {
            String url = m_baseURL + secondIsbn;
            responseJSONString = WebserviceAPI.getResponse(url, true);
        }

        if (responseJSONString == null) {
            return false;
        }

        s_logger.debug("rsponse: " + responseJSONString);
        try {
            JsonParser parser = new JsonParser();
            JsonElement parsedResponse = parser.parse(responseJSONString);
            if (parsedResponse == null) {
                throw new RuntimeException("invalid json response");
            }

            JsonObject responseObject = (JsonObject) parsedResponse;
            JsonElement itemElement = responseObject.get("items");
            if (itemElement == null) {
                throw new RuntimeException("no item array");
            }
            JsonArray items = itemElement.getAsJsonArray();
            JsonObject item = (JsonObject) items.get(0);

            JsonElement volumeInfoElement = item.get("volumeInfo");
            if (volumeInfoElement != null) {
                JsonObject volumeInfo = volumeInfoElement.getAsJsonObject();
                JsonElement titleElement = volumeInfo.get("title");
                if (titleElement != null) {
                    String title = titleElement.getAsString();
                    title = StringUtility.removeNonAsciiCharactersAndTrim(title);
                    if (title != null) {
                        String prevTitle = m_book.getTitle();
                        if (prevTitle != null && !StringUtility.isContains(title, prevTitle)) {
                            title = prevTitle + " ; " + title;
                        }
                        m_book.setTitle(title);
                    }
                }

                // authors
                JsonElement authorsElement = volumeInfo.get("authors");
                if (authorsElement != null) {
                    JsonArray authors = authorsElement.getAsJsonArray();
                    List<String> authorNames = new ArrayList<String>();
                    for (int i = 0; i < authors.size(); i++) {
                        String author = authors.get(i).getAsString();
                        author = StringUtility.removeNonAsciiCharactersAndTrim(author);
                        if (author != null) {
                            authorNames.add(author);
                        }
                    }
                    m_book.setAuthors(authorNames);
                }

                JsonElement publisherElement = volumeInfo.get("publisher");
                if (publisherElement != null) {
                    String publisher = publisherElement.getAsString();
                    publisher = StringUtility.removeNonAsciiCharactersAndTrim(publisher);
                    if (publisher != null) {
                        m_book.setPublisher(publisher);
                    }
                }

                JsonElement descriptionElement = volumeInfo.get("description");
                if (descriptionElement != null) {
                    String desc = descriptionElement.getAsString();
                    desc = StringUtility.removeNonAsciiCharactersAndTrim(desc);
                    if (desc != null) {
                        m_book.setDescription(desc);
                    }
                }

                JsonElement industryIdentifierElement = volumeInfo.get("industryIdentifiers");
                if (industryIdentifierElement != null) {
                    JsonArray industryIdentifier = industryIdentifierElement.getAsJsonArray();
                    for (int i = 0; i < industryIdentifier.size(); i++) {
                        JsonObject element = industryIdentifier.get(i).getAsJsonObject();
                        String isbn = element.get("identifier").getAsString();
                        m_book.setIsbn(isbn);
                    }
                }

                JsonElement imageElement = volumeInfo.get("imageLinks");
                if (imageElement != null) {
                    JsonObject imageObject = imageElement.getAsJsonObject();
                    if (imageObject.get("smallThumbnail") != null) {
                        m_book.addImage(imageObject.get("smallThumbnail").getAsString());
                    }

                    JsonElement thumbnailElement = imageObject.get("thumbnail");
                    if (thumbnailElement != null) {
                        String thumbnailImage = thumbnailElement.getAsString();
                        m_book.addImage(thumbnailImage);
                        m_book.setThumbnailImageLink(thumbnailImage);
                    }
                }
            }

            m_book.setLoadedThroughAPI(true);
            m_book.setLoaderAgent(BookLoaderAgent.Google);
            return true;

        } catch (Exception e) {
            s_logger.error("Error while populating fields from the google webservice", e);
        }
        return false;
    }
}
