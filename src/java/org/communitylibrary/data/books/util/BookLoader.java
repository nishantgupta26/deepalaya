package org.communitylibrary.data.books.util;

/**
 * @author nishant.gupta
 *
 */
public interface BookLoader {

    boolean populateBookDetailsFromAPI();

}
