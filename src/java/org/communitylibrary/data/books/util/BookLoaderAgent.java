package org.communitylibrary.data.books.util;

import java.lang.reflect.Constructor;

import org.apache.log4j.Logger;
import org.communitylibrary.data.books.Book;
import org.communitylibrary.data.books.util.impl.GoogleBookLoader;

public enum BookLoaderAgent {
    Google(GoogleBookLoader.class, "G"), ;

    private Class< ? extends BookLoader> m_class;
    private String                       m_code;
    private static Logger                s_logger = Logger.getLogger(BookLoaderAgent.class);

    private BookLoaderAgent(Class< ? extends BookLoader> className, String code) {
        m_class = className;
        m_code = code;
    }

    public BookLoader loaderInstance(Book book) {
        try {
            Constructor< ? extends BookLoader> constr = m_class.getConstructor(Book.class);
            if (constr != null) {
                return (BookLoader) constr.newInstance(book);
            }
        } catch (Exception e) {
            s_logger.error("Error while loading constructor instance for book loader", e);
        }

        return null;
    }

    public static BookLoaderAgent getLoaderAgent(String code) {
        if (code == null || (code = code.trim()).isEmpty()) {
            return null;
        }

        for (BookLoaderAgent agent : BookLoaderAgent.values()) {
            if (agent.getCode().equals(code)) {
                return agent;
            }
        }
        return null;
    }

    public String getCode() {
        return m_code;
    }

    public void setCode(String code) {
        m_code = code;
    }

}
