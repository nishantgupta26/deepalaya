package org.communitylibrary.data.payments;

/**
 * @author nishant
 *
 */
public enum PaymentMedium {

    ONLINE('N'),

    OFFLINE('F'),

    CASH('C'),

    BANKTRANSFER('B'),

    CHEQUE('H'),

    NEFT('E'),

    TPT('T'),

    WEBSITE('W'),
    
    IMPS('I'),

    ;

    private char m_code;

    private PaymentMedium(char code) {
        m_code = code;
    }

    public static PaymentMedium getMedium(String paymentType) {
        if (paymentType == null || (paymentType = paymentType.trim()).isEmpty()) {
            return PaymentMedium.ONLINE;
        }

        char code = paymentType.charAt(0);
        for (PaymentMedium medium : PaymentMedium.values()) {
            if (medium.getCode() == code) {
                return medium;
            }
        }
        return null;
    }

    public char getCode() {
        return m_code;
    }

    public static PaymentMedium getPaymentType(String med) {
        if (med == null || (med = med.trim()).isEmpty()) {
            return null;
        }

        for (PaymentMedium medium : PaymentMedium.values()) {
            String replaceAll = med.toUpperCase().replaceAll("[^A-Za-z]", "");
            if (replaceAll.contains(medium.name())) {
                return medium;
            }
        }
        return null;
    }
    
    public static void main(String[] args) {
        System.out.println(getPaymentType("Website (Instamojo)"));
    }

}
