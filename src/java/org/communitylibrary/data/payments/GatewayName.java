package org.communitylibrary.data.payments;

public enum GatewayName {

    PayUMoney('P'),

    Instamojo('I'),

    ;

    private char m_code;

    private GatewayName(char code) {
        m_code = code;
    }

    public char getCode() {
        return m_code;
    }

    public static GatewayName getGateway(String pgType) {
        if (pgType == null || (pgType = pgType.trim()).isEmpty()) {
            return GatewayName.PayUMoney;
        }

        char code = pgType.charAt(0);
        for (GatewayName name : GatewayName.values()) {
            if (name.getCode() == code) {
                return name;
            }
        }
        return null;
    }

}
