package org.communitylibrary.data.payments.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.communitylibrary.data.entity.Address;
import org.communitylibrary.data.payments.PledgeType;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Donor {
	private String m_firstName;
	private String m_lastName;
	private String m_email;
	private String m_phone;

	/**
	 * Non Mandatory
	 */
	private PledgeType m_subscriptionType;
	private String m_panNumber;

	/**
	 * For 80G receipt purpose
	 */
	private Address m_address;

	public Donor(ResultSet rs) throws SQLException {
		m_firstName = StringUtility.trimAndEmptyIsNull(rs.getString("first_name"));
		m_lastName = StringUtility.trimAndNullIsEmpty(rs.getString("last_name"));
		m_phone = StringUtility.trimAndEmptyIsNull(rs.getString("phone"));
		m_email = StringUtility.trimAndEmptyIsNull(rs.getString("email"));
		String subscriptionTypeCode = StringUtility.trimAndEmptyIsNull(rs.getString("subscription_type"));
		PledgeType type = PledgeType.getPledgeTypeFromCode(subscriptionTypeCode);
		m_subscriptionType = type;
		m_address = Address.deserialize(StringUtility.trimAndEmptyIsNull(rs.getString("address")));
		m_panNumber = StringUtility.trimAndEmptyIsNull(rs.getString("pan"));
	}

	public Donor() {
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return m_firstName;
	}

	public void setFirstName(String firstName) {
		m_firstName = firstName;
	}

	public String getLastName() {
		return m_lastName;
	}

	public void setLastName(String lastName) {
		m_lastName = lastName;
	}

	public String getEmail() {
		return m_email;
	}

	public void setEmail(String email) {
		m_email = email;
	}

	public String getPhone() {
		return m_phone;
	}

	public void setPhone(String phone) {
		m_phone = phone;
	}

	public PledgeType getSubscriptionType() {
		return m_subscriptionType;
	}

	public void setSubscriptionType(PledgeType subscriptionType) {
		m_subscriptionType = subscriptionType;
	}

	public String getPanNumber() {
		return m_panNumber;
	}

	public void setPanNumber(String panNumber) {
		m_panNumber = panNumber;
	}

	public Address getAddress() {
		return m_address;
	}

	public void setAddress(Address address) {
		m_address = address;
	}

	public boolean isValid() {

		if (!StringUtility.isValidName(getFirstName())) {
			return false;
		}

		if (!StringUtility.isValidName(getLastName())) {
			return false;
		}

		if (!StringUtility.isValidEmail(getEmail())) {
			return false;
		}

		if (!StringUtility.isValidMobileNo(getPhone())) {
			return false;
		}

		if (m_address != null) {
			if (!m_address.isValid()) {
				return false;
			}
		}

		return true;
	}

	public void convertToJson(JsonObject obj) {
		obj.addProperty("fname", m_firstName);
		obj.addProperty("lname", m_lastName);
		obj.addProperty("phone", m_phone);
		obj.addProperty("email", m_email);

		if (m_subscriptionType != null) {
			obj.addProperty("pledge", m_subscriptionType.getName());
			obj.addProperty("color", m_subscriptionType.getColorCode());
		}

		if (m_address != null) {
			obj.addProperty("address", m_address.serialize());
		}

		if (m_panNumber != null) {
			obj.addProperty("pannum", m_panNumber);
		}
	}

	public static Donor parse(JsonObject requestObject) {
		Donor paymentUnit = new Donor();
		JsonElement pledgeElement = requestObject.get("pledge");
		String pledgeTypeCode = pledgeElement == null ? null : pledgeElement.getAsString();
		PledgeType pledgeType = PledgeType.getPledgeTypeFromCode(pledgeTypeCode);
		paymentUnit.setSubscriptionType(pledgeType);

		String firstName = requestObject.get("fname").getAsString();
		paymentUnit.setFirstName(firstName);

		String lastName = requestObject.get("lname").getAsString();
		paymentUnit.setLastName(lastName);

		String email = requestObject.get("email").getAsString();
		paymentUnit.setEmail(email);

		String phoneNumber = requestObject.get("phone").getAsString();
		paymentUnit.setPhone(phoneNumber);

		JsonElement panElement = requestObject.get("pannum");
		if (panElement != null) {
			paymentUnit.setPanNumber(panElement.getAsString());
		}

		JsonElement addressObject = requestObject.get("address");
		if (addressObject != null) {
			Address address = Address.deserialize(addressObject.getAsString());
			paymentUnit.setAddress(address);
		}

		return paymentUnit;
	}

	public static Donor parseAgain(JsonObject requestObject) {
		JsonElement pledgeElement = requestObject.get("pledgetypecode");
		String pledgeTypeCode = pledgeElement == null ? null : pledgeElement.getAsString();

		PledgeType pledgeType = PledgeType.getPledgeTypeFromCode(pledgeTypeCode);
		Donor paymentUnit = new Donor();
		paymentUnit.setSubscriptionType(pledgeType);
		String firstName = requestObject.get("fname").getAsString();
		paymentUnit.setFirstName(firstName);

		String lastName = requestObject.get("lname").getAsString();
		paymentUnit.setLastName(lastName);

		String email = requestObject.get("email").getAsString();
		paymentUnit.setEmail(email);

		String phoneNumber = requestObject.get("phone").getAsString();
		paymentUnit.setPhone(phoneNumber);
		JsonElement panElement = requestObject.get("pannum");
		if (panElement != null) {
			paymentUnit.setPanNumber(panElement.getAsString());
		}

		JsonElement addressElement = requestObject.get("addressline1");
		String addressLine1 = addressElement == null ? null : addressElement.getAsString();

		addressElement = requestObject.get("addressline2");
		String addressLine2 = addressElement == null ? null : addressElement.getAsString();

		addressElement = requestObject.get("city");
		String city = addressElement == null ? null : addressElement.getAsString();

		addressElement = requestObject.get("state");
		String state = addressElement == null ? null : addressElement.getAsString();

		addressElement = requestObject.get("country");
		String country = addressElement == null ? null : addressElement.getAsString();

		addressElement = requestObject.get("zipcode");
		String zipCode = addressElement == null ? null : addressElement.getAsString();

		if (addressLine1 != null || addressLine2 != null || city != null || state != null || country != null
				|| zipCode != null) {
			Address address = new Address();
			address.setAddressLine1(addressLine1);
			address.setAddressLine2(addressLine2);
			address.setCity(city);
			address.setState(state);
			address.setCountry(country);
			address.setZipCode(zipCode);
			paymentUnit.setAddress(address);
		}

		return paymentUnit;
	}

}
