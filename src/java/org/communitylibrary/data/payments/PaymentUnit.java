package org.communitylibrary.data.payments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.data.payments.entity.Donor;
import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author nishant
 *
 */
public class PaymentUnit extends DBObject {
    private static final long   serialVersionUID      = -6027845981712529689L;
    private static Logger       s_logger              = Logger.getLogger(PaymentUnit.class);

    private static final String s_tableName           = "payment_unit";

    private String              m_transactionId;

    private String              m_receiptNumber;
    private Date                m_receiptDate;

    private String              m_amount;

    private Donor               m_donor;

    /**
     * Some additional comments
     */
    private String              m_extraNotes;

    private boolean             m_isRequire80GReceipt;

    private GatewayName         m_pgName;

    private PaymentMedium       m_paymentType;

    /**
     * Fields returned from the payment gateway. Currently PayuMoney
     */
    private Boolean             m_isPaymentSuccessful = null;

    private String              m_gatewayTransactionId;

    private Date                m_creationDate;

    public PaymentUnit() {
        this(PaymentMedium.ONLINE);
    }

    public PaymentUnit(PaymentMedium medium) {
        m_paymentType = medium;
    }

    public PaymentUnit(ResultSet rs) throws Exception {
        WebsiteDBConnector s_staticInstance = WebsiteDBConnector.getStaticInstance();
        m_transactionId = rs.getString("txn_id");

        m_receiptNumber = StringUtility.trimAndEmptyIsNull(rs.getString("receipt_number"));

        m_amount = rs.getString("amount");
        m_donor = new Donor(rs);

        m_extraNotes = StringUtility.trimAndEmptyIsNull(rs.getString("extra_notes"));
        m_isRequire80GReceipt = "Y".equals(StringUtility.trimAndEmptyIsNull(rs.getString("require_receipt")));

        String successMessage = StringUtility.trimAndEmptyIsNull(rs.getString("payment_status"));
        m_isPaymentSuccessful = successMessage == null ? null : "Y".equals(successMessage);
        m_gatewayTransactionId = StringUtility.trimAndEmptyIsNull(rs.getString("gateway_transaction_id"));

        String pgType = rs.getString("gateway_name");
        m_pgName = GatewayName.getGateway(pgType);

        String paymentType = rs.getString("payment_type");
        m_paymentType = PaymentMedium.getMedium(paymentType);

        m_creationDate = s_staticInstance.getTimeStampAsDate(rs, "gen_ts");
        m_receiptDate = s_staticInstance.getTimeStampAsDate(rs, "receiptdate");
    }

    public String getTransactionId() {
        return m_transactionId;
    }

    public void setTransactionId(String transactionId) {
        m_transactionId = transactionId;
    }

    public String getAmount() {
        return m_amount;
    }

    public void setAmount(String amount) {
        m_amount = amount;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.communitylibrary.data.entity.DBObject#editTable(java.sql.Connection)
     */
    @Override
    protected void editTable(Connection conn) throws Exception {

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.communitylibrary.data.entity.DBObject#fillHistoryTableStmt(java.sql.
     * PreparedStatement)
     */
    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.communitylibrary.data.entity.DBObject#getHistoryInsertQuery()
     */
    @Override
    protected String getHistoryInsertQuery() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return
     */
    public String getExtraNotes() {
        return m_extraNotes;
    }

    public void setExtraNotes(String extraNotes) {
        m_extraNotes = extraNotes;
    }

    /**
     * validate if all the information received through the ui is as expected
     * and adheres to the terms and conditions
     * 
     * @return
     */
    public boolean validate() {
        if (getTransactionId() == null) {
            return false;
        }

        if (getAmount() == null) {
            return false;
        }

        try {
            NumberUtility.parsePositiveDoubleWithExceptionOnErr(getAmount());
        } catch (Exception e) {
            s_logger.error("invalid amount");
            return false;
        }

        if (!m_donor.isValid()) {
            return false;
        }

        return true;
    }

    public Boolean getIsPaymentSuccessful() {
        return m_isPaymentSuccessful;
    }

    public void setIsPaymentSuccessful(Boolean isPaymentSuccessful) {
        m_isPaymentSuccessful = isPaymentSuccessful;
    }

    public boolean isRequire80GReceipt() {
        return m_isRequire80GReceipt;
    }

    public void setRequire80GReceipt(boolean isRequire80GReceipt) {
        m_isRequire80GReceipt = isRequire80GReceipt;
    }

    public String getGatewayTransactionId() {
        return m_gatewayTransactionId;
    }

    public void setGatewayTransactionId(String gatewayTransactionId) {
        m_gatewayTransactionId = gatewayTransactionId;
    }

    /**
     * TODO: fix the insert into payment unit code
     * 
     * @return
     */
    public static String getCreatePaymentUnitQuery() {
        String query = "insert ignore into " + s_tableName
                + " (txn_id, first_name, last_name, phone, email, amount, subscription_type, extra_notes, require_receipt, address,pan, gateway_name, payment_type, payment_status, gateway_transaction_id, receipt_number, receiptdate, gen_ts, ipaddress) values(?, ?, ?, ?,?,?,?,?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?)";
        return query;
    }

    public static String getSelectPaymentUnitQuery() {
        return "select * from " + s_tableName + " where txn_id = ?";
    }

    public String getEditQuery() {
        return "update " + s_tableName + " set payment_status=?, gateway_transaction_id=? where txn_id=?";
    }

    public static JsonArray convertPaymentListToJsonArray(List<PaymentUnit> donations) {
        JsonArray array = new JsonArray();

        for (PaymentUnit unit : donations) {
            array.add(unit.convertToJsonObject());
        }
        return array;
    }

    public JsonObject convertToJsonObject() {
        JsonObject obj = new JsonObject();

        obj.addProperty("txn_id", m_transactionId);

        if (m_receiptNumber != null) {
            obj.addProperty("receiptnumber", m_receiptNumber);
        }

        if (m_receiptDate != null) {
            obj.addProperty("receiptdate", DateUtility.getDateInMMMDDYYYY(m_receiptDate));
        }

        obj.addProperty("amount", m_amount);
        m_donor.convertToJson(obj);

        if (m_extraNotes != null) {
            obj.addProperty("notes", m_extraNotes);
        }

        obj.addProperty("needReceipt", m_isRequire80GReceipt);

        if (m_isPaymentSuccessful != null) {
            obj.addProperty("isSuccess", m_isPaymentSuccessful);
        }

        if (m_gatewayTransactionId != null) {
            obj.addProperty("gatewayId", m_gatewayTransactionId);
        }

        obj.addProperty("paymentmedium", m_paymentType.name());
        if (m_pgName != null && m_paymentType != PaymentMedium.OFFLINE) {
            obj.addProperty("pgtype", m_pgName.name());
        }

        obj.addProperty("createdOn", DateUtility.getDateInMMMDDYYYY(m_creationDate));

        return obj;

    }

    public Date getCreationDate() {
        return m_creationDate;
    }

    public void setCreationDate(Date creationDate) {
        m_creationDate = creationDate;
    }

    public GatewayName getPgType() {
        return m_pgName;
    }

    public void setPgType(GatewayName pgType) {
        m_pgName = pgType;
    }

    public PaymentMedium getPaymentType() {
        return m_paymentType;
    }

    public void setPaymentType(PaymentMedium paymentType) {
        m_paymentType = paymentType;
    }

    public static PaymentUnit parseJsonToGetPaymentUnit(JsonObject requestObject, boolean isManual) {
        PaymentMedium medium = isManual ? PaymentMedium.OFFLINE : PaymentMedium.ONLINE;
        try {
            PaymentUnit paymentUnit = new PaymentUnit(medium);

            String receiptNumber = null;
            try {
                receiptNumber = requestObject.get("receiptnumber").getAsString();
                JsonElement dateElement = requestObject.get("receiptdate");
                paymentUnit.setReceiptDate(dateElement == null ? DateUtility.getCurrentDate()
                        : DateUtility.parseDateInDDMMMYYDashSeparator(dateElement.getAsString()));
            } catch (Exception e) {
                s_logger.info("Error while getting receipt number for manual entry");
            }

            paymentUnit.setReceiptNumber(receiptNumber);

            String amount = requestObject.get("amount").getAsString();
            paymentUnit.setAmount(amount);

            paymentUnit.setTransactionId(NumberUtility.generateIdWithTime("TCLP"));

            Donor donor = Donor.parseAgain(requestObject);
            paymentUnit.setDonor(donor);

            boolean isRequire80GReceipt = requestObject.get("receipt").getAsBoolean();
            paymentUnit.setRequire80GReceipt(isRequire80GReceipt);

            paymentUnit.setIsPaymentSuccessful(true);
            paymentUnit.setGatewayTransactionId("manualentry");
            JsonElement dateElement = requestObject.get("createdOn");
            paymentUnit.setCreationDate(dateElement == null ? DateUtility.getCurrentDate()
                    : DateUtility.parseDateInDDMMMYYDashSeparator(dateElement.getAsString()));

            if (!paymentUnit.validate()) {
                throw new RuntimeException("Validation Problem with the payment unit obtained");
            }

            return paymentUnit;

        } catch (Exception e) {
            s_logger.error("Error in parsing json object into payment unit", e);
            s_logger.info("Json obtained: " + requestObject.toString());

        }
        return null;
    }

    public void setReceiptNumber(String receiptNumber) {
        m_receiptNumber = receiptNumber;
    }

    public String getReceiptNumber() {
        return m_receiptNumber;
    }

    public String getUpdatePaymentUnitQuery() {
        return "update " + s_tableName + " set receipt_number=?, receiptdate=? where txn_id=?";
    }

    public static PaymentUnit parseJsonForEdit(JsonObject requestObject) {
        PaymentMedium medium = PaymentMedium.OFFLINE;
        try {
            PaymentUnit paymentUnit = new PaymentUnit(medium);

            String receiptNumber = null;
            try {
                receiptNumber = StringUtility.trimAndEmptyIsNull(requestObject.get("receiptnumber").getAsString());
                JsonElement dateElement = requestObject.get("receiptdate");
                paymentUnit.setReceiptDate(dateElement == null ? DateUtility.getCurrentDate()
                        : DateUtility.parseDateInDDMMMYYDashSeparator(dateElement.getAsString()));
            } catch (Exception e) {
                s_logger.info("Error while getting receipt number for manual entry");
            }

            paymentUnit.setReceiptNumber(receiptNumber);

            String transactionId = requestObject.get("txn_id").getAsString();
            paymentUnit.setTransactionId(transactionId);

            return paymentUnit;

        } catch (Exception e) {
            s_logger.error("Error in parsing json object into payment unit for edit", e);
            s_logger.info("Json obtained: " + requestObject.toString());

        }
        return null;
    }

    public static List<PaymentUnit> parsePaymentsFromJson(JsonArray requestArray) {
        if (requestArray == null) {
            return null;
        }

        List<PaymentUnit> units = null;
        try {
            units = new ArrayList<PaymentUnit>();
            for (int i = 0; i < requestArray.size(); i++) {
                JsonObject nextUnit = (JsonObject) requestArray.get(i);
                PaymentUnit unit = parseJsonToGetPaymentUnit(nextUnit);
                if (unit == null) {
                    throw new RuntimeException("unit parsed to be null");
                }

                units.add(unit);
            }

            return units;

        } catch (Exception e) {
            s_logger.error("Error while converting requestArray to payment unit list", e);
            s_logger.info("requestArray: " + requestArray);
        }
        return null;
    }

    private static PaymentUnit parseJsonToGetPaymentUnit(JsonObject requestObject) {
        try {
            PaymentMedium medium = PaymentMedium.valueOf(requestObject.get("paymentmedium").getAsString());
            PaymentUnit paymentUnit = new PaymentUnit(medium);

            paymentUnit.setTransactionId(requestObject.get("txn_id").getAsString());

            String receiptNumber = null;
            try {
                receiptNumber = requestObject.get("receiptnumber").getAsString();
                JsonElement dateElement = requestObject.get("receiptdate");
                paymentUnit.setReceiptDate(dateElement == null ? DateUtility.getCurrentDate()
                        : DateUtility.parseDateInDDMMMYYDashSeparator(dateElement.getAsString()));
            } catch (Exception e) {
            }

            paymentUnit.setReceiptNumber(receiptNumber);

            String amount = requestObject.get("amount").getAsString();
            paymentUnit.setAmount(amount);

            // paymentUnit.setTransactionId(NumberUtility.generateIdWithTime("TCLP"));
            Donor donor = Donor.parse(requestObject);
            paymentUnit.setDonor(donor);

            JsonElement notes = requestObject.get("notes");
            if (notes != null) {
                paymentUnit.setExtraNotes(StringUtility.trimAndEmptyIsNull(notes.getAsString()));
            }

            boolean isRequire80GReceipt = requestObject.get("needReceipt").getAsBoolean();
            paymentUnit.setRequire80GReceipt(isRequire80GReceipt);

            JsonElement jsonElement = requestObject.get("isSuccess");
            if (jsonElement != null) {
                paymentUnit.setIsPaymentSuccessful(jsonElement.getAsBoolean());
            }

            JsonElement gatewayElement = requestObject.get("gatewayId");
            if (gatewayElement != null) {
                paymentUnit.setGatewayTransactionId(gatewayElement.getAsString());
            }

            JsonElement pgElement = requestObject.get("pgtype");
            if (pgElement != null) {
                GatewayName gatewayName = GatewayName.valueOf(pgElement.getAsString());
                paymentUnit.setPgType(gatewayName);
            }

            JsonElement createdOnElement = requestObject.get("createdOn");
            if (createdOnElement != null) {
                paymentUnit.setCreationDate(DateUtility.parseDateInMMMDDYYYY(createdOnElement.getAsString()));
            }

            if (!paymentUnit.validate()) {
                throw new RuntimeException("Validation Problem with the payment unit obtained");
            }

            return paymentUnit;

        } catch (Exception e) {
            s_logger.error("Error in parsing json object into payment unit", e);
            s_logger.info("Json obtained: " + requestObject.toString());

        }
        return null;
    }

    public String getName() {
        return m_donor.getFirstName() + " " + m_donor.getLastName();
    }

    public Date getReceiptDate() {
        return m_receiptDate;
    }

    public void setReceiptDate(Date receiptDate) {
        m_receiptDate = receiptDate;
    }

    public Donor getDonor() {
        return m_donor;
    }

    public void setDonor(Donor donor) {
        m_donor = donor;
    }

    public String getFirstName() {
        // TODO Auto-generated method stub
        return m_donor.getFirstName();
    }

}
