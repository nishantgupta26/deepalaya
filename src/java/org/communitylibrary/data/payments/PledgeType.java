package org.communitylibrary.data.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Pledge for donations to the Library
 * 
 * @author nishant.gupta
 *
 */
public enum PledgeType {

    Monthly("M", "MONTHLY", "#E6E6FA"),

    Quarterly("Q", "QUARTERLY", "#D8BFD8"),

    SemiAnnually("S", "SEMI-ANNUALLY", "#B0E0E6"),

    Annually("A", "ANNUALLY", "#4682B4"),

    OneTime("O", "ONE TIME PAYMENT", "#fff"),

    ;
    private String m_code;
    private String m_name;
    private String m_colorCode;

    PledgeType(String code, String name, String color) {
        m_code = code;
        m_name = name;
        m_colorCode = color;
    }

    public String getCode() {
        return m_code;
    }

    public String getName() {
        return m_name;
    }

    public static JsonElement getPledgeTypesAsJsonArray() {
        JsonArray arr = new JsonArray();
        for (PledgeType type : PledgeType.values()) {
            arr.add(type.convertToJsonObject());
        }

        return arr;
    }

    private JsonElement convertToJsonObject() {
        JsonObject obj = new JsonObject();
        obj.addProperty("name", m_name);
        obj.addProperty("code", m_code);
        return obj;

    }

    public static PledgeType parseFromJson(JsonObject pledgeTypeObject) {
        if (pledgeTypeObject == null) {
            return null;
        }

        try {
            String code = pledgeTypeObject.get("code").getAsString();
            for (PledgeType type : PledgeType.values()) {
                if (type.getCode().equals(code)) {
                    return type;
                }
            }

        } catch (Exception e) {

        }
        return null;
    }

    public static PledgeType getPledgeTypeFromCode(String pledgeTypeCode) {
        if (pledgeTypeCode == null) {
            return null;
        }

        try {
            for (PledgeType type : PledgeType.values()) {
                if (type.getCode().equals(pledgeTypeCode)) {
                    return type;
                }
            }

        } catch (Exception e) {

        }
        return null;
    }

    public String getColorCode() {
        return m_colorCode;
    }

}
