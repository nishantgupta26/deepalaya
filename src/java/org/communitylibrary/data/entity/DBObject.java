package org.communitylibrary.data.entity;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;
import org.communitylibrary.db.WebsiteDBConnector;

/**
 * @author nishant.gupta
 *
 */
public abstract class DBObject implements Serializable {

	private static final long serialVersionUID = 9091912935495445490L;

	private static Logger s_logger = Logger.getLogger(DBObject.class);

	public void addInHistory(Connection conn) {
		PreparedStatement stmt = null;
		boolean isCloseConnection = false;
		WebsiteDBConnector staticInstance = WebsiteDBConnector.getStaticInstance();
		try {
			if (conn == null) {
				isCloseConnection = true;
				conn = staticInstance.getConnection(false);
			}
			stmt = conn.prepareStatement(getHistoryInsertQuery());
			fillHistoryTableStmt(stmt);
			stmt.execute();
			conn.commit();
		} catch (Exception e) {
			s_logger.error("Error while adding in history", e);
		} finally {
			staticInstance.closeAll(conn, stmt, null, isCloseConnection);
		}

	}

	public void updateTable() throws Exception {
		WebsiteDBConnector staticInstance = WebsiteDBConnector.getStaticInstance();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = staticInstance.getConnection(false);
			addInHistory(conn);
			editTable(conn);
		} catch (Exception e) {
			s_logger.error("error in saving history", e);
			throw e;
		} finally {
			staticInstance.closeAll(conn, stmt, null, true);
		}

	}

	protected abstract void editTable(Connection conn) throws Exception;

	protected abstract void fillHistoryTableStmt(PreparedStatement stmt) throws Exception;

	protected abstract String getHistoryInsertQuery();
}
