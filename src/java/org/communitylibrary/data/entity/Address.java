package org.communitylibrary.data.entity;

import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant
 *
 */
public class Address {

    private String m_addressLine1;
    private String m_addressLine2;
    private String m_city;
    private String m_state;
    private String m_country;
    private String m_zipCode;

    public String getAddressLine1() {
        return m_addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        m_addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return m_addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        m_addressLine2 = addressLine2;
    }

    public String getCity() {
        return m_city;
    }

    public void setCity(String city) {
        m_city = city;
    }

    public String getState() {
        return m_state;
    }

    public void setState(String state) {
        m_state = state;
    }

    public String getCountry() {
        return m_country;
    }

    public void setCountry(String country) {
        m_country = country;
    }

    public String getZipCode() {
        return m_zipCode;
    }

    public void setZipCode(String zipCode) {
        m_zipCode = zipCode;
    }

    public boolean isValid() {
        if (m_addressLine1 == null) {
            return false;
        }

        if (m_addressLine2 == null) {
            return false;
        }

        if (m_city == null) {
            return false;
        }

        if (m_state == null) {
            return false;
        }

        if (m_country == null) {
            return false;
        }

        if (m_zipCode == null) {
            return false;
        }

        return true;
    }

    public String serialize() {
        StringBuilder serial = new StringBuilder();
        serial.append(m_addressLine1).append("|").append(m_addressLine2).append("|").append(m_city).append("|")
                .append(m_state).append("|").append(m_country).append("|").append("|").append(m_zipCode);

        return serial.toString().replaceAll("[,]", ";");
    }

    public static Address deserialize(String serial) {
        if (serial == null || "null".equals(serial)) {
            return null;
        }

        String[] tokens = StringUtility.splitNullForEmpty(serial, "|");
        if (tokens == null) {
            return null;
        }
        Address address = new Address();
        address.setAddressLine1(tokens[0]);
        address.setAddressLine2(tokens[1]);
        address.setCity(tokens[2]);
        address.setState(tokens[3]);
        address.setCountry(tokens[4]);
        address.setZipCode(tokens[5]);
        return address;
    }
}
