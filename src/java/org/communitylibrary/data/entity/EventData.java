package org.communitylibrary.data.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.apache.log4j.Logger;
import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class EventData extends DBObject {

    private static final long serialVersionUID = -2022335797291730801L;
    private static Logger     s_logger         = Logger.getLogger(EventData.class);
    private String            m_id;
    private String            m_title;
    private Date              m_start;
    private String            m_time;
    private String            m_duration;
    private Date              m_end;
    private String            m_address;
    private String            m_description;
    private String            m_eventLink;

    public EventData() {
    }

    public EventData(ResultSet result, WebsiteDBConnector s_staticInstance) throws Exception {
        m_id = result.getString("id");
        m_title = result.getString("title");
        m_start = s_staticInstance.getTimeStampAsDate(result, "start_date");
        m_end = s_staticInstance.getTimeStampAsDate(result, "end_date");
        m_address = result.getString("address");
        m_description = StringUtility.trimAndEmptyIsNull(result.getString("description"));
        m_eventLink = StringUtility.trimAndEmptyIsNull(result.getString("link"));
        m_time = StringUtility.trimAndEmptyIsNull(result.getString("time"));
        m_duration = StringUtility.trimAndEmptyIsNull(result.getString("duration"));
    }

    /**
     * @return the id
     */
    public String getId() {
        return m_id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        m_id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return m_title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        m_title = title;
    }

    /**
     * @return the start
     */
    public Date getStart() {
        return m_start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(Date start) {
        m_start = start;
    }

    /**
     * @return the end
     */
    public Date getEnd() {
        return m_end;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(Date end) {
        m_end = end;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return m_address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        m_address = address;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return m_description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        m_description = description;
    }

    /**
     * @return the eventLink
     */
    public String getEventLink() {
        return m_eventLink;
    }

    /**
     * @param eventLink the eventLink to set
     */
    public void setEventLink(String eventLink) {
        m_eventLink = eventLink;
    }

    /**
     * @return
     */
    public boolean validate(StringBuilder reason) {
        if (!StringUtility.isNonEmpty(getTitle())) {
            reason.append("Event title cannot be empty");
            s_logger.info("Event title cannot be empty");
            return false;
        }

        if (getStart() == null) {
            reason.append("Event Start date cannot be empty");
            s_logger.info("Event Start date cannot be empty");
            return false;
        }

        if (getEnd() == null) {
            reason.append("Event End date cannot be empty");
            s_logger.info("Event End date cannot be empty");
            return false;
        }

        if (!StringUtility.isNonEmpty(getAddress())) {
            reason.append("Event Venue cannot be empty");
            s_logger.info("Event Venue cannot be empty");
            return false;
        }

        if (!StringUtility.isNonEmpty(getEventLink())) {
            return true;
        } else if (!(getEventLink().startsWith("http") || getEventLink().startsWith("www"))) {
            s_logger.info("Valid protocol for link are http, https and www only");
            reason.append("Valid protocol for link are http, https and www only");
            return false;
        }

        return true;
    }

    @Override
    protected void editTable(Connection conn) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    protected String getHistoryInsertQuery() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return m_time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        m_time = time;
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return m_duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        m_duration = duration;
    }

}
