package org.communitylibrary.data.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.communitylibrary.data.entity.DBObject;
import org.communitylibrary.data.members.LibraryGroupForDigitalAccess;
import org.communitylibrary.util.CollectionsUtil;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public class HomePageLink extends DBObject {

	private static final long serialVersionUID = 3147371560978896020L;

	private static Logger s_logger = Logger.getLogger(HomePageLink.class);

	private static String s_tablename = "homepagelinks";

	private int id;
	private String name;
	private String desc;

	private String hindiName;
	private String hindiDesc;

	private String url;
	private LibraryGroupForDigitalAccess group;

	/**
	 * These will keep track of number of links clicked
	 */
	private int hindiLinkClicks;
	private int englishLinkClicks;

	public HomePageLink(ResultSet rs) throws Exception {
		id = rs.getInt("id");
		name = rs.getString("name");
		desc = StringUtility.trimAndEmptyIsNull(rs.getString("description"));
		hindiName = rs.getString("hindiname");
		hindiDesc = StringUtility.trimAndEmptyIsNull(rs.getString("hindidescription"));
		url = rs.getString("url");
		setGroup(LibraryGroupForDigitalAccess.deserialize(rs.getString("library_group")));
		englishLinkClicks = rs.getInt("numEnglish");
		hindiLinkClicks = rs.getInt("numHindi");
	}

	public HomePageLink(JsonObject requestJson) throws Exception {
		name = requestJson.get("name").getAsString();
		try {
			desc = StringUtility.trimAndEmptyIsNull(requestJson.get("desc").getAsString());
			hindiName = requestJson.get("hindiname").getAsString();
			hindiDesc = StringUtility.trimAndEmptyIsNull(requestJson.get("hindidesc").getAsString());
			id = requestJson.get("id").getAsInt();
		} catch (Exception e) {
			s_logger.error("invalid description/id of the link");
		}
		url = requestJson.get("url").getAsString();

		try {
			setGroup(LibraryGroupForDigitalAccess
					.valueOf(StringUtility.trimAndEmptyIsNull(requestJson.get("group").getAsString())));
		} catch (Exception e) {
			setGroup(LibraryGroupForDigitalAccess.UPPER);
		}
	}

	public HomePageLink() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void editTable(Connection conn) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	protected void fillHistoryTableStmt(PreparedStatement stmt) throws Exception {

	}

	@Override
	protected String getHistoryInsertQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static JsonArray convertToJsonArray(List<HomePageLink> homePageLinks) {
		if (CollectionsUtil.isEmpty(homePageLinks)) {
			return new JsonArray();
		}

		JsonArray array = new JsonArray();
		for (HomePageLink link : homePageLinks) {
			array.add(link.convertToJson());
		}
		return array;
	}

	private JsonObject convertToJson() {
		JsonObject object = new JsonObject();
		object.addProperty("id", getId());
		object.addProperty("name", getName());
		object.addProperty("desc", getDesc());
		object.addProperty("englishclicks", getEnglishLinkClicks());
		object.addProperty("hindiname", getHindiName());
		object.addProperty("hindidesc", getHindiDesc());
		object.addProperty("hindiclicks", getHindiLinkClicks());
		object.addProperty("url", getUrl());
		object.addProperty("group", getGroup().name());
		return object;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInsertQuery() {
		return "insert into " + s_tablename
				+ "(name, description, hindiname, hindidescription, url, library_group) values (?,?,?,?,?,?)";
	}

	public static String getDeleteQuery() {
		return "delete from homepagelinks where id = ?";
	}

	/**
	 * @return the group
	 */
	public LibraryGroupForDigitalAccess getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(LibraryGroupForDigitalAccess group) {
		this.group = group;
	}

	public String getHindiName() {
		return hindiName;
	}

	public void setHindiName(String hindiName) {
		this.hindiName = hindiName;
	}

	public String getHindiDesc() {
		return hindiDesc;
	}

	public void setHindiDesc(String hindiDesc) {
		this.hindiDesc = hindiDesc;
	}

	public String getUpdateQuery() {
		String query = "update " + s_tablename + " set " + (hindiName != null ? "hindiname=?" : "")
				+ ((hindiName != null && hindiDesc != null) ? "," : "")
				+ ((hindiDesc != null) ? "hindidescription=?" : "") + " where id=?";
		return query;
	}

	/**
	 * @return the hindiLinkClicks
	 */
	public int getHindiLinkClicks() {
		return hindiLinkClicks;
	}

	/**
	 * @param hindiLinkClicks the hindiLinkClicks to set
	 */
	public void setHindiLinkClicks(int hindiLinkClicks) {
		this.hindiLinkClicks = hindiLinkClicks;
	}

	/**
	 * @return the englishLinkClicks
	 */
	public int getEnglishLinkClicks() {
		return englishLinkClicks;
	}

	/**
	 * @param englishLinkClicks the englishLinkClicks to set
	 */
	public void setEnglishLinkClicks(int englishLinkClicks) {
		this.englishLinkClicks = englishLinkClicks;
	}

	public static String getLinkCountUpdateQuery(boolean isEnglish) {
		String query = "update " + s_tablename + " set "
				+ (isEnglish ? "numEnglish=numEnglish+1" : "numHindi=numHindi+1") + " where id=?";
		return query;
	}

}
