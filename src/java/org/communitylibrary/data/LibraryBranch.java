package org.communitylibrary.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author nishant.gupta
 */
public class LibraryBranch {
    private String code;
    private String name;
    @JsonProperty(value = "memberIDPrefix")
    private String idPrefix;

    @JsonIgnore
    private static final String s_tableName = "librarybranch";

    public LibraryBranch() {}
    public LibraryBranch(ResultSet result) throws SQLException {
        code = result.getString("code");
        name = result.getString("name");
        idPrefix = result.getString("member_id_prefix");
    }

    public static LibraryBranch deserialize(String branchCode) {
        LibraryBranch branch = new LibraryBranch();
        branch.setCode(branchCode);
        return branch;
    }

    public static String getInsertQuery() {
        return "insert into " + s_tableName + " (code, name, member_id_prefix, gen_ts) values(?, ?, ?, NOW())";
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public String serialize() {
        return getCode();
    }

    /**
     * @return the idPrefix
     */
    public String getIdPrefix() {
        return idPrefix;
    }

    /**
     * @param idPrefix the idPrefix to set
     */
    public void setIdPrefix(String idPrefix) {
        this.idPrefix = idPrefix;
    }
}
