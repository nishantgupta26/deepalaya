package org.communitylibrary.app;

import javax.servlet.ServletConfig;

import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.logger.Logger;
import org.communitylibrary.timer.TimerUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.StringUtility;

public class WebsiteApplication {
    private static final Logger  s_logger             = Logger.getInstance(WebsiteApplication.class);

    // 0=undone,1=processing,2=success,3=failed
    private static Object        s_initLock           = new Object();
    private static int           s_initStatus         = 0;

    private static ServletConfig s_servletConfig      = null;

    private static int           s_serverId           = -1;
    private static boolean       s_isBypassHttpsCheck = true;
    private static String        s_serverProxyHost    = null;
    private static int           s_serverProxyPort    = -1;

    private WebsiteApplication() {
    }

    public static void initialize(ServletConfig config) {
        synchronized (s_initLock) {
            if (s_initStatus == 0) {
                s_logger.info("Intializing WebsiteApp...");
                s_initStatus = 1;
            } else {
                s_logger.info("CWebsiteApp Already Initialized.");
                return;
            }
        }
        try {
            if (config != null && config.getServletContext() != null) {
                s_servletConfig = config;

                s_serverId = NumberUtility.parsetIntWithDefaultOnErr(
                        config.getServletContext().getInitParameter("AppServer.Id"), -1);

                s_serverProxyHost = StringUtility.trimAndEmptyIsNull(config.getServletContext().getInitParameter(
                        "AppServer.ProxyHost"));
                s_serverProxyPort = NumberUtility.parsetIntWithDefaultOnErr(config.getServletContext()
                        .getInitParameter("AppServer.ProxyPort"), -1);

                s_isBypassHttpsCheck = s_isBypassHttpsCheck
                        || "true".equalsIgnoreCase(config.getServletContext().getInitParameter(
                                "AppServer.BypassHttpsCheck"));
            }

            // Database
            WebsiteDBConnector.initialize();

            // Reload everything with timer
            TimerUtility.initialize();

            synchronized (s_initLock) {
                s_initStatus = 2;
            }

            s_logger.info("Intialized WebsiteApp.");

        } catch (Exception e) {
            s_logger.error("Error in initializing WebsiteApp.", e);
            synchronized (s_initLock) {
                s_initStatus = 3;
            }
        }
    }

    public static void shutdown() {
        s_logger.info("Shutdown WebsiteApp.");
        TimerUtility.shutdown();
    }

    public static int getServerId() {
        return s_serverId;
    }

    public static ServletConfig getServletConfig() {
        return s_servletConfig;
    }

    public static boolean hasProxy() {
        return s_serverProxyPort > 0 && s_serverProxyHost != null;
    }

    public static String getProxyHost() {
        return s_serverProxyHost;
    }

    public static int getProxyPort() {
        return s_serverProxyPort;
    }

    public static boolean isBypassHttpsCheck() {
        return s_isBypassHttpsCheck;
    }
}
