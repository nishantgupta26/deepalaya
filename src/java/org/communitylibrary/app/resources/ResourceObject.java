package org.communitylibrary.app.resources;

import java.sql.ResultSet;

import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant
 *
 */
public class ResourceObject {
    private int          m_id;
    private String       m_urlLink;
    private String       m_imageUrl;
    private String       m_textContent;
    private ResourceType m_resourceType;

    public ResourceObject(String m_url) {
        setImageUrl(m_url);
    }

    public ResourceObject(String url, String imageURL, String textContent, ResourceType type) {
        setUrlLink(url);
        setImageUrl(imageURL);
        setTextContent(textContent);
        setResourceType(type);
    }

    public ResourceObject(ResultSet rs) throws Exception {
        m_imageUrl = StringUtility.trimAndEmptyIsNull(rs.getString("imageURL"));
        m_id = rs.getInt("id");
        m_urlLink = StringUtility.trimAndEmptyIsNull(rs.getString("urlLink"));
        m_textContent = StringUtility.trimAndEmptyIsNull(rs.getString("textContent"));
        m_resourceType = ResourceType.getResourceType(StringUtility.trimAndEmptyIsNull(rs.getString("type")));
    }

    public int getId() {
        return m_id;
    }

    public String getImageUrl() {
        return m_imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        m_imageUrl = imageUrl;
    }

    /**
     * @return the textContent
     */
    public String getTextContent() {
        return m_textContent;
    }

    /**
     * @param textContent the textContent to set
     */
    public void setTextContent(String textContent) {
        m_textContent = textContent;
    }

    /**
     * @return the urlLink
     */
    public String getUrlLink() {
        return m_urlLink;
    }

    /**
     * @param urlLink the urlLink to set
     */
    public void setUrlLink(String urlLink) {
        m_urlLink = urlLink;
    }

    public ResourceType getResourceType() {
        return m_resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        m_resourceType = resourceType;
    }

}
