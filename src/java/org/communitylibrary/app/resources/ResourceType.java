package org.communitylibrary.app.resources;

/**
 * @author nishant
 *
 */
public enum ResourceType {
    Banner('B'),

    Campaign('C'),

    ;

    private char m_code;

    private ResourceType(char code) {
        m_code = code;
    }

    public char getCode() {
        return m_code;
    }

    public static ResourceType getResourceType(String type) {
        if (type == null || (type = type.trim()).isEmpty()) {
            return Campaign;
        }

        char code = type.charAt(0);
        for (ResourceType typeR : ResourceType.values()) {
            if (typeR.getCode() == code) {
                return typeR;
            }
        }
        return Campaign;
    }

}
