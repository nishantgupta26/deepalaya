package org.communitylibrary.app.resources;

/**
 * @author nishant
 *
 */
public enum ContentType {

	TEXT("t", "Text"),

	IMAGE("i", "Image"),

	;

	private String m_code;
	private String m_string;

	private ContentType(String code, String string) {
		m_code = code;
		m_string = string;
	}

	public String getCode() {
		return m_code;
	}

	public String getString() {
		return m_string;
	}

	public static ContentType deserialize(String contentType) {
		if (contentType == null || (contentType = contentType.trim()).isEmpty()) {
			return null;
		}

		for (ContentType type : ContentType.values()) {
			if (type.getCode().equalsIgnoreCase(contentType)) {
				return type;
			}
		}
		return null;
	}
}
