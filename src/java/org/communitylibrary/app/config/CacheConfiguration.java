package org.communitylibrary.app.config;

import org.communitylibrary.app.config.value.AbstractConfigValue;
import org.communitylibrary.logger.Logger;

/**
 * @author nishant.gupta
 *
 */
public enum CacheConfiguration {
    /* Config */
    INT_PREV_EVENTS(ConfigType.INT, ElementType.TEXTBOX, "Number of months in past to get events"),

    INT_NUM_EVENTS(ConfigType.INT, ElementType.TEXTBOX, "Number of events to be displayed in homepage"),

    INT_NUM_POSTS(ConfigType.INT, ElementType.TEXTBOX, "Number of blogposts to fetch on the blogs dashboard"),

    /* Settings */
    BOL_CAMPAIGN_POPUP(ConfigType.BOOLEAN, ElementType.CHECKBOX, "Show Campaign popups?"),

    BOL_HIDE_LOGO(ConfigType.BOOLEAN, ElementType.CHECKBOX, "Hide logo from the main website?"),

    BOL_BLOG_NEW_WINDOW(ConfigType.BOOLEAN, ElementType.CHECKBOX, "Open blog in new window?"),

    STR_BLOG_URL(ConfigType.STRING, ElementType.TEXTBOX, "The url of the blog"),

    BOL_GALLERY_EXTERNAL(ConfigType.BOOLEAN, ElementType.CHECKBOX, "If gallery tab points to an external link"),

    BOL_USE_ONLINE_DONATIONS(ConfigType.BOOLEAN, ElementType.CHECKBOX, "Use Online Payment Gateway donations"),

    STR_GALLERY_SOURCE_URL(ConfigType.STRING, ElementType.TEXTBOX, "The url for the instagram feed"),

    /* Social urls */
    STR_SOCIAL_FBURL(ConfigType.STRING, ElementType.TEXTBOX, "URL for the Facebook page"),

    STR_SOCIAL_TWITTERURL(ConfigType.STRING, ElementType.TEXTBOX, "URL for the Twitter page"),

    STR_SOCIAL_INSTAGRAMURL(ConfigType.STRING, ElementType.TEXTBOX, "URL for the Instagram page"),

    STR_SOCIAL_YOUTUBEURL(ConfigType.STRING, ElementType.TEXTBOX, "URL for the Youtube page"),

    /* Content */

    STR_ABOUT_GIST(ConfigType.STRING, ElementType.TEXTAREA, "Content to appear on the home-page"),

    STR_ABOUT_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Content to appear in About tab"),

    STR_PRESS_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Content to appear in Press tab"),

    STR_VOLUNTEER_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Content to appear in Volunteer With us tab"),

    STR_DONATE_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Content to appear in Donate To Us tab"),

    STR_DONATE_BOOKS_TEXT(ConfigType.STRING, ElementType.TEXTAREA, "Content to appear in Donate Books tab"),

    STR_OMODEL_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Organising Model Content"),

    STR_D2DOPERATIONS_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Day-2-Day Operations Content"),

    STR_ONOURSHELVES_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "What's on our shelves content"),

    STR_POLICYDOCUMENT_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Policy Documents"),

    STR_CURRICULLUM_CONTENT(ConfigType.STRING, ElementType.TEXTAREA, "Content Regarding Curriculum"),

    STR_LIBRARY_MOVEMENT(ConfigType.STRING, ElementType.TEXTAREA, "Elsewhere in the Library Movement"),

    STR_SOCIAL_MEDIA_GUIDELINES(ConfigType.STRING, ElementType.TEXTAREA, "Social Media Guidelines content"),

    STR_VOLUNTEER_OPERATIONS(ConfigType.STRING, ElementType.TEXTAREA, "Volunteer Operations Content"),

    STR_SERVER_NAME(ConfigType.STRING, ElementType.TEXTBOX, "The server url"),

    STR_TCLPADDRESS(ConfigType.STRING, ElementType.TEXTBOX, "TCLP Address for official communications"),

    STR_TCLPPhone(ConfigType.STRING, ElementType.TEXTBOX, "TCLP Phone for official communications"),

    ;

    private ConfigType  m_type;
    private String      m_desc;
    Logger              s_logger = Logger.getInstance(CacheConfiguration.class);
    private ElementType m_elem;

    CacheConfiguration(ConfigType type, ElementType elem, String description) {
        m_type = type;
        m_elem = elem;
        m_desc = description;
    }

    /**
     * Deserialize and get value object for FareSyncConfiguration
     * 
     * @param serial
     * @return
     */
    public AbstractConfigValue deserializeValue(String serial) {
        try {
            return ConfigType.deserializeValue(m_type, serial);
        } catch (Exception e) {
            s_logger.error("Error in deserializeValue", e);
        }
        return null;
    }

    /**
     * Convert html form value based string value to value object
     * 
     * @param value
     * @return
     */
    public AbstractConfigValue convertFromHtmlFormValue(String value) {
        try {
            return ConfigType.convertFromHtmlFormValue(m_type, value);
        } catch (Exception e) {
            s_logger.error("Error in convertFromHtmlFormValue", e);
        }
        return null;
    }

    /**
     * Similar to valueOf function on enums, exception null is returned if no
     * match is found instead of NPE.
     * 
     * @param name
     * @return
     */
    public static CacheConfiguration deserialize(String name) {
        for (CacheConfiguration cfg : values()) {
            if (cfg.name().equals(name)) {
                return cfg;
            }
        }
        return null;
    }

    /**
     * @return the type
     */
    public ConfigType getType() {
        return m_type;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return m_desc;
    }

    /**
     * @return the elem
     */
    public ElementType getElem() {
        return m_elem;
    }

}
