package org.communitylibrary.app.config.value;

/**
 * BooleanValue extension for storing boolean values :D
 */
public class BooleanValue extends AbstractConfigValue {
    private static final long serialVersionUID = 1L;

    public Boolean            m_value;

    public BooleanValue(boolean isHttpConstructor, String value) {
        super(isHttpConstructor, value);
        if (isHttpConstructor) {
            m_value = "on".equalsIgnoreCase(value);
        } else {
            m_value = Boolean.parseBoolean(value);
        }
    }

    public BooleanValue(boolean bol) {
        super(false, null);
        m_value = Boolean.valueOf(bol);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.zillious.abacus.faresync.config.value.AbstractConfigValue#serialize()
     */
    @Override
    public String serialize() {
        return m_value.toString();
    }
}
