package org.communitylibrary.app.config.value;

import org.communitylibrary.utils.StringUtility;

/**
 * StringValue extension for storing String values :D
 */
public class StringValue extends AbstractConfigValue {

    private static final long serialVersionUID = -7756238705455711344L;
    public String             m_value;

    public StringValue(boolean isHttpConstructor, String value) {
        super(isHttpConstructor, value);
        m_value = StringUtility.emptyIsNull(value);
    }

    @Override
    public String serialize() {
        return m_value;
    }
}
