package org.communitylibrary.app.config.value;

/**
 * IntValue extension for storing integer values :D
 */
public class IntValue extends AbstractConfigValue {

    /**
     * 
     */
    private static final long serialVersionUID = -2338680496152622311L;
    public int                m_value;

    public IntValue(boolean isHttpConstructor, String value) {
        super(isHttpConstructor, value);
        m_value = Integer.parseInt(value);
    }

    @Override
    public String serialize() {
        return Integer.toString(m_value);
    }
}
