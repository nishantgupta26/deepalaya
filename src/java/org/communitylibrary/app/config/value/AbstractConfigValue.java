package org.communitylibrary.app.config.value;

import java.io.Serializable;

/**
 * AbstractConfigValue is the base class for Value Classes for each of the
 * ConfigTypes used by ConfigStore
 */
public abstract class AbstractConfigValue implements Serializable {

    private static final long serialVersionUID = 2426099257844217388L;

    /**
     * isHttpConstructor is true for http based constructor and false for
     * serialized values based constructor.
     * 
     * @param isHttpConstructor
     * @param httpValue
     */
    public AbstractConfigValue(boolean isHttpConstructor, String httpValue) {
    }

    /**
     * Serialize the Value object into string. Example, used for DB storage.
     * 
     * WARNING: Serialized value should not be null as that might break the code
     * in few places.
     * 
     * @return
     */
    public abstract String serialize();

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return serialize();
    }
}
