package org.communitylibrary.app.config;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.communitylibrary.app.config.value.AbstractConfigValue;
import org.communitylibrary.app.config.value.BooleanValue;
import org.communitylibrary.app.config.value.IntValue;
import org.communitylibrary.app.config.value.StringValue;

/**
 * Enum for various types of values that can be associated with each of the
 * FareSyncConfiguration.
 */
public enum ConfigType {
    // String type configuration - Textbox used in UI for update
    STRING(StringValue.class),
    // Boolean type configuration - Checkbox used in UI for update
    BOOLEAN(BooleanValue.class),
    // Integer type configuration - Textbox used in UI for update
    INT(IntValue.class),

    // This type of Type Configuration will present the input field as password
    // field instead of text field
    PASSWORDSTRING(StringValue.class),

    ;

    private Class< ? extends AbstractConfigValue> m_implementorClass;

    /**
     * Takes the implementing class (which should be extending
     * AbstractConfigValue). This class should have a constructor only taking
     * one String parameter (for construction from serialized value) and a
     * constructor taking a boolean and String parameter (for construction from
     * Html returned value).
     * 
     * @param klass
     */
    ConfigType(Class< ? extends AbstractConfigValue> klass) {
        m_implementorClass = klass;
    }

    /**
     * Function to deserialize the serialized string value to
     * AbstractConfigValue.
     * 
     * @param cTyp
     * @param serial
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IllegalArgumentException
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    public static AbstractConfigValue deserializeValue(ConfigType cTyp, String serial) throws IllegalArgumentException,
            InstantiationException, IllegalAccessException, InvocationTargetException, SecurityException,
            NoSuchMethodException {
        Constructor< ? extends AbstractConfigValue> cons = cTyp.m_implementorClass.getConstructor(boolean.class,
                String.class);
        return cons.newInstance(false, serial);
    }

    /**
     * Converts the value
     * 
     * @param cTyp
     * @param value
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IllegalArgumentException
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    public static AbstractConfigValue convertFromHtmlFormValue(ConfigType cTyp, String value)
            throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException,
            SecurityException, NoSuchMethodException {
        Constructor< ? extends AbstractConfigValue> cons = cTyp.m_implementorClass.getConstructor(boolean.class,
                String.class);
        return cons.newInstance(true, value);
    }

    /**
     * Serialize the AbstractConfigValue to String. Example, seriallized value
     * is used for storing it in DB.
     * 
     * @param cTyp
     * @param val
     * @return
     */
    public static String serializeValue(ConfigType cTyp, AbstractConfigValue val) {
        return val.serialize();
    }
}
