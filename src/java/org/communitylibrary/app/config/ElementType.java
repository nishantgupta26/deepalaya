package org.communitylibrary.app.config;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Enum for various types of values that can be associated with each of the
 * FareSyncConfiguration.
 */
public enum ElementType {
    TEXTAREA('A'), TEXTBOX('T', true), CHECKBOX('C', true), RADIOBOX('R', true), ;

    private char    m_code;
    private boolean m_isForForm;

    private ElementType(char code) {
        m_code = code;
    }

    private ElementType(char code, boolean isForForm) {
        this(code);
        m_isForForm = isForForm;
    }

    /**
     * @return the code
     */
    public char getCode() {
        return m_code;
    }

    /**
     * @return the isForForm
     */
    public boolean isForForm() {
        return m_isForForm;
    }

    /**
     * @param isForForm the isForForm to set
     */
    public void setForForm(boolean isForForm) {
        m_isForForm = isForForm;
    }

    public static JsonArray getFormFieldTypesAsJsonArray() {
        JsonArray arr = new JsonArray();
        for (ElementType type : ElementType.values()) {
            if (type.isForForm()) {
                arr.add(type.convertToJsonObject());
            }
        }

        return arr;
    }

    public JsonObject convertToJsonObject() {
        JsonObject obj = new JsonObject();
        obj.addProperty("name", name());
        obj.addProperty("code", String.valueOf(getCode()));
        return obj;
    }

    public static void main(String[] args) {
        System.out.println(getFormFieldTypesAsJsonArray().toString());
    }

    public static ElementType parseFromJson(JsonObject obj) {
        if (obj == null) {
            return null;
        }

        return ElementType.valueOf(obj.get("name").getAsString());
    }

    public static ElementType getFromCode(String code) {
        if (code == null || (code = code.trim()).isEmpty()) {
            return null;
        }

        for (ElementType type : ElementType.values()) {
            if (type.getCode() == code.charAt(0)) {
                return type;
            }
        }

        return null;
    }
}
