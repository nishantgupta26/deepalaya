package org.communitylibrary.app.engine;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

/**
 * @author nishant.gupta
 *
 */
public class WebserviceAPI {
    private static Logger s_logger = Logger.getLogger(WebserviceAPI.class);

    public static String getResponse(String reqUrl, boolean isHTTPS) {
        String response = null;
        InputStream inpStrm = null;
        InputStream inp = null;
        URLConnection urlCon = null;
        try {
            URL url;

            url = new URL(reqUrl);

            if (isHTTPS) {
                urlCon = (HttpsURLConnection) url.openConnection();
            } else {
                urlCon = (HttpURLConnection) url.openConnection();
            }
            urlCon.setConnectTimeout(3 * 60 * 1000);
            urlCon.setReadTimeout(3 * 60 * 1000);
            urlCon.setDoInput(true);
            urlCon.setDoOutput(true);
            urlCon.setUseCaches(false);
            urlCon.setRequestProperty("Content-type", "text/plain; charset=utf-8");
            urlCon.setRequestProperty("Connection", "close");
            urlCon.setRequestProperty("Accept-Encoding", "gzip");

            inp = urlCon.getInputStream();
            String enc = urlCon.getContentEncoding();
            if (enc != null && enc.equalsIgnoreCase("gzip")) {
                inpStrm = new GZIPInputStream(new BufferedInputStream(inp));
            } else {
                inpStrm = new BufferedInputStream(inp);
            }
            StringBuilder buf = new StringBuilder();
            byte buffer[] = new byte[1000];

            int len = 0;
            while ((len = inpStrm.read(buffer)) != -1) {
                buf.append(new String(buffer, 0, len));
            }
            response = buf.toString();
            s_logger.debug("Response: " + response);
            return response;
        } catch (Exception e) {
            s_logger.error("Error in getting instagram feed: " + reqUrl, e);
            return null;
        } finally {
            try {
                if (inpStrm != null) {
                    inpStrm.close();
                }
                if (inp != null) {
                    inp.close();
                }
            } catch (Exception e) {
            }
        }

    }

}
