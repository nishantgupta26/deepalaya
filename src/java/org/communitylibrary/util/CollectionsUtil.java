package org.communitylibrary.util;

import java.util.Collection;
import java.util.Map;

/**
 * @author nishant.gupta
 *
 */
public class CollectionsUtil {

    /**
     * This api checks whether the given collection is empty with null check.
     * 
     * @param col
     * @return
     */
    public static boolean isEmpty(Collection< ? > col) {
        return col == null || col.isEmpty();
    }

    public static boolean isEmpty(Map< ? , ? > map) {
        return map == null || map.isEmpty();
    }

}
