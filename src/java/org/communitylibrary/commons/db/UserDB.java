package org.communitylibrary.commons.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.communitylibrary.data.user.User;
import org.communitylibrary.data.user.dto.UserLoginDTO;
import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.ui.navigation.WebsiteException;
import org.communitylibrary.ui.navigation.WebsiteExceptionType;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.NumberUtility;

/**
 * @author nishant.gupta
 *
 */
public class UserDB {
    private static WebsiteDBConnector s_connector = WebsiteDBConnector.getStaticInstance();
    private static Logger             s_logger    = Logger.getLogger(UserDB.class);

    public static User authenticateUser(Connection conn, int userId, String password) throws WebsiteException,
            Exception {
        User user = UserDB.getUserByID(conn, userId);
        if (!User.authenthicateUser(user.getUserId(), user.getAuthToken(), password)) {
            throw new WebsiteException(WebsiteExceptionType.USER_AUTH_FAILED);
        }
        if (!user.isEnabled()) {
            throw new WebsiteException(WebsiteExceptionType.USER_DISABLED);
        }
        return user;
    }

    private static final String SQL_SELECT_BY_ID = "select * from library_users where user_id=?";

    public static User getUserByID(Connection conn, int userId) throws Exception {
        WebsiteDBConnector aCon = WebsiteDBConnector.getStaticInstance();
        boolean isCloseConnection = false;
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            if (conn == null) {
                isCloseConnection = true;
                conn = aCon.getReadOnlyConnection();
            }
            stmt = conn.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setInt(1, userId);
            result = stmt.executeQuery();
            if (result.next()) {
                User user = new User(result);
                return user;

            } else {
                return null;
            }

        } finally {
            aCon.closeAll(conn, stmt, result, isCloseConnection);
        }
    }

    public static User authenticateUser(Connection conn, String email, String password) throws Exception {
        User user = UserDB.getUserByEmail(conn, email);
        if (user == null || !User.authenthicateUser(user.getUserId(), user.getAuthToken(), password)) {
            throw new WebsiteException(WebsiteExceptionType.USER_AUTH_FAILED);
        }
        if (!user.isEnabled()) {
            throw new WebsiteException(WebsiteExceptionType.USER_DISABLED);
        }
        return user;
    }

    private static final String SQL_SELECT_BY_EMAIL = "select * from library_users where LOWER(email)=LOWER(?) limit 1";

    public static User getUserByEmail(Connection conn, String email) throws Exception {
        WebsiteDBConnector aCon = WebsiteDBConnector.getStaticInstance();
        boolean isCloseConnection = false;
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            if (conn == null) {
                isCloseConnection = true;
                conn = aCon.getReadOnlyConnection();
            }
            stmt = conn.prepareStatement(SQL_SELECT_BY_EMAIL);
            aCon.setString(stmt, 1, email);
            result = stmt.executeQuery();
            if (result.next()) {
                User user = new User(result);
                return user;
            } else {
                return null;
            }

        } finally {
            aCon.closeAll(conn, stmt, result, isCloseConnection);
        }
    }

    private static final String SQL_SELECT_ALL = "select * from library_users order by user_id asc";

    public static List<User> getAllUsers(Connection conn) throws Exception {
        List<User> users = new ArrayList<User>();
        WebsiteDBConnector aCon = WebsiteDBConnector.getStaticInstance();
        boolean isCloseConnection = false;
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            if (conn == null) {
                isCloseConnection = true;
                conn = aCon.getReadOnlyConnection();
            }
            stmt = conn.prepareStatement(SQL_SELECT_ALL);
            result = stmt.executeQuery();
            while (result.next()) {
                users.add(new User(result));
            }

            return users;

        } finally {
            aCon.closeAll(conn, stmt, result, isCloseConnection);
        }
    }

    private static final String SQL_ADD = "insert ignore into library_users (user_id, auth_token, is_enabled,role,name,email,gen_ts) values (?,?,?,?,?,?,NOW())";

    public static void addUser(Connection conn, User user) throws SQLException {
        boolean isCloseConnection = false;
        PreparedStatement stmt = null;
        try {
            if (conn == null) {
                isCloseConnection = true;
                conn = s_connector.getConnection(false);
            }
            stmt = conn.prepareStatement(SQL_ADD);
            addValuesToStatement(user, stmt);
            s_logger.debug("query: " + stmt);
            int rCnt = stmt.executeUpdate();
            if (rCnt != 1) {
                throw new RuntimeException("Record update count=" + rCnt);
            }
            stmt.close();

            s_connector.commit(conn, isCloseConnection);

        } finally {
            s_connector.closeAll(conn, stmt, null, isCloseConnection);
        }
    }

    private static void addValuesToStatement(User user, PreparedStatement stmt) throws SQLException {
        int i = 1;
        stmt.setInt(i++, extractUserId(user.getUserId()));
        stmt.setString(i++, user.getAuthToken());
        s_connector.setString(stmt, i++, user.isEnabled() ? "Y" : "N", 1);
        s_connector.setString(stmt, i++, user.getUserRole().serialize(), 1);
        s_connector.setString(stmt, i++, user.getName(), 99);
        s_connector.setString(stmt, i++, user.getEmail(), 99);
    }

    public static int extractUserId(String userID) {
        if (userID == null) {
            return -1;
        }
        if (userID.startsWith("E_")) {
            return NumberUtility.parsetIntWithDefaultOnErr(userID.substring(2), -1);
        }
        return NumberUtility.parsetIntWithDefaultOnErr(userID, -1);
    }

    private static final String SQL_EDIT = "update library_users set is_enabled=?,role=?,name=?,email=? where user_id=?";

    public static void editUser(Connection conn, User user, boolean updatePassword) throws SQLException {
        WebsiteDBConnector aCon = WebsiteDBConnector.getStaticInstance();
        boolean isCloseConnection = false;
        PreparedStatement stmt = null;
        try {
            if (conn == null) {
                isCloseConnection = true;
                conn = aCon.getConnection(false);
            }
            stmt = conn.prepareStatement(SQL_EDIT);
            int i = 1;
            aCon.setString(stmt, i++, user.isEnabled() ? "Y" : "N", 1);
            aCon.setString(stmt, i++, user.getUserRole().serialize(), 1);
            aCon.setString(stmt, i++, user.getName(), 99);
            aCon.setString(stmt, i++, user.getEmail(), 99);
            stmt.setInt(i++, extractUserId(user.getUserId()));
            s_logger.debug("query: " + stmt);
            int rCnt = stmt.executeUpdate();
            if (rCnt != 1) {
                throw new RuntimeException("Record update count=" + rCnt);
            }
            stmt.close();

            // Update password if required
            if (updatePassword) {
                user.hashPassword();
                updatePassword(conn, user);
            }

            aCon.commit(conn, isCloseConnection);

        } finally {
            aCon.closeAll(conn, stmt, null, isCloseConnection);
        }
    }

    private static final String SQL_UPDATE_PASSWORD = "update library_users set auth_token=? where user_id=?";

    private static void updatePassword(Connection conn, User user) throws SQLException {
        WebsiteDBConnector aCon = WebsiteDBConnector.getStaticInstance();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            user.hashPassword();
            stmt = conn.prepareStatement(SQL_UPDATE_PASSWORD);
            stmt.setString(1, user.getAuthToken());
            stmt.setInt(2, extractUserId(user.getUserId()));
            int rCnt = stmt.executeUpdate();
            if (rCnt != 1) {
                throw new RuntimeException("Record update count=" + result);
            }

        } finally {
            aCon.closeAll(conn, stmt, result, false);
        }
    }

    public static int addUsers(List<User> users) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int totalCount = 0;
        int batchCount = 0;
        try {
            conn = s_connector.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(SQL_ADD);
            for (User user : users) {
                addValuesToStatement(user, stmt);
                stmt.addBatch();
                batchCount++;

                if (batchCount == 100) {
                    stmt.executeBatch();
                    totalCount += batchCount;
                    batchCount = 0;
                }
            }

            if (batchCount > 0) {
                stmt.executeBatch();
                totalCount += batchCount;
                batchCount = 0;
            }
            s_connector.commit(conn, true);
        } catch (Exception e) {
            s_logger.error("Error while saving user info into the database, lineNumber:" + (totalCount + batchCount), e);
            return 0;
        } finally {
            s_connector.closeAll(conn, stmt, null, true);
        }

        return totalCount;

    }

    public static void addUserLoginInfo(User user) throws Exception {
        Connection conn = null;
        PreparedStatement pStmt = null;
        try {
            conn = s_connector.getConnection(false);
            pStmt = conn.prepareStatement("insert into user_login_info(user_id, login_ts) values (?, NOW())");
            s_connector.setIntDefaultNull(pStmt, 1, NumberUtility.parsetIntWithDefaultOnErr(user.getUserId(), -1), -1);
            s_logger.debug("query: " + pStmt);
            pStmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in adding user login information", e);
            throw new WebsiteException(WebsiteExceptionType.USER_LOGIN_INFO);
        } finally {
            s_connector.closeAll(conn, pStmt, null, true);
        }

    }

    /**
     * return number of login attempt on specific date
     * 
     * @param userId
     * @param date
     * @return
     */
    public static int getnumberOfLogins(int userId, Date date) {
        if (date == null) {
            return -1;
        }

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            conn = s_connector.getReadOnlyConnection();
            stmt = conn
                    .prepareStatement("select count(user_id) as num_logins from user_login_info where user_id = ? and date(login_ts) = ?");
            s_connector.setIntDefaultNull(stmt, 1, userId, -1);
            s_connector.setString(stmt, 2, DateUtility.getDateInMysqlDateFormat(date));
            result = stmt.executeQuery();
            s_logger.debug("statement: " + stmt);
            while (result.next()) {
                String number = result.getString("num_logins");
                return NumberUtility.parsetIntWithDefaultOnErr(number, -1);
            }
        } catch (Exception e) {
            s_logger.error("Error while fetching number of logins for user: " + userId + ", for date: " + date, e);
        } finally {
            s_connector.closeAll(conn, stmt, result, true);
        }

        return -1;
    }

    private static final String SQL_SELECT_ALL_LOGIN = "select users.*, count(login.user_id) as num_logins, max(login.login_ts) as last_login from library_users as users left join user_login_info as login on users.user_id = login.user_id group by users.user_id";

    public static Map<User, UserLoginDTO> getAllUserLogins() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        Map<User, UserLoginDTO> userMap = null;
        try {
            conn = s_connector.getReadOnlyConnection();
            stmt = conn.prepareStatement(SQL_SELECT_ALL_LOGIN);
            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                userMap = new LinkedHashMap<User, UserLoginDTO>();
            }
            while (result.next()) {
                userMap.put(new User(result), new UserLoginDTO(s_connector, result));
            }

            return userMap;
        } catch (Exception e) {
            s_logger.error("Error while fetching the user login information", e);
        } finally {
            s_connector.closeAll(conn, stmt, result, true);
        }

        return null;
    }
}
