package org.communitylibrary.ui.payment.util;

import org.apache.log4j.Logger;
import org.communitylibrary.admin.entity.forms.EmailMessage;
import org.communitylibrary.data.entity.Address;
import org.communitylibrary.data.payments.PaymentUnit;
import org.communitylibrary.ui.beans.MessageBean;
import org.communitylibrary.ui.beans.MessageBean.MessageType;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.navigation.WebsiteActions;
import org.communitylibrary.ui.security.CustomSecurityRequestType;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.ConfigStore;
import org.communitylibrary.utils.EmailSender;
import org.communitylibrary.utils.PaymentConfigStore;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public class PayuMoneyHandler extends PaymentHelper {
//
//    private static Logger s_logger = Logger.getLogger(PayuMoneyHandler.class);
//
//    @Override
//    public ResponseObject generatePaymentHandlingObject(PaymentUnit unit, CustomSecurityWrapperRequest request,
//            CustomSecurityWrapperResponse response) {
//
//        String txnid = unit.getTransactionId();
//        String amount = StringUtility.formatAmount(unit.getAmount());
//        String firstName = unit.getFirstName();
//        String lastName = unit.getDonor().getLastName();
//        String email = unit.getDonor().getEmail();
//        String phone = unit.getDonor().getPhone();
//        String productInfo = "Donation for the library";
//
//        String additionalComments = StringUtility.trimAndEmptyIsNull(unit.getExtraNotes());
//
//        // success url
//        String surl = WebsiteActions.PAYMENT.getActionURL(request, response, new String[] { "status" });
//        // failure url
//        String furl = surl;
//
//        String subscriptionTypeCode = unit.getDonor().getSubscriptionType() != null ? unit.getDonor().getSubscriptionType().getCode() : null;
//        String pan = unit.getDonor().getPanNumber();
//
//        String key = PaymentConfigStore.getStringValue("PG_PU_KEY", null);
//        String salt = PaymentConfigStore.getStringValue("PG_PU_SALT", null);
//        String action = PaymentConfigStore.getStringValue("PG_PU_URL", null);
//
//        if (salt == null) {
//            throw new RuntimeException("Need salt");
//        }
//
//        // String hashSequence =
//        // "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10|salt";
//
//        StringBuilder hashString = new StringBuilder();
//        hashString.append(key).append("|").append(txnid).append("|").append(amount).append("|").append(productInfo)
//                .append("|").append(firstName).append("|").append(email).append("|").append(pan != null ? pan : "")
//                .append("|").append(subscriptionTypeCode != null ? subscriptionTypeCode : "").append("|")
//                .append(additionalComments != null ? additionalComments : "").append("||||||||").append(salt);
//
//        String hash = hashCal("SHA-512", hashString.toString());
//        // build HTML code
//        StringBuilder htmlResponse = new StringBuilder("<!DOCTYPE html><html><body>");
//
//        htmlResponse.append("<form id=\"payuform\" action=\"").append(action)
//                .append("\"  name=\"payuform\" method=\"POST\" >")
//
//                .append("<input type=\"hidden\" name=\"key\" value=\"").append(key).append("\" />")
//                .append("<input type=\"hidden\" name=\"hash_string\" value=\"").append(hashString.toString())
//                .append("\" />")
//
//                .append("<input type=\"hidden\" name=\"hash\" value=\"").append(hash).append("\" />")
//
//                .append("<input type=\"hidden\" name=\"txnid\" value=\"").append(txnid).append("\" />")
//
//                .append("<input type=\"hidden\" name=\"amount\" value=\"").append(amount).append("\" />")
//
//                .append("<input type=\"hidden\" name=\"firstname\" value=\"").append(firstName).append("\" />")
//                .append("<input type=\"hidden\" name=\"email\" value=\"").append(email).append("\" />")
//                .append("<input type=\"hidden\" name=\"phone\" value=\"").append(phone).append("\" />")
//                .append("<input type=\"hidden\" name=\"productinfo\" value=\"").append(productInfo).append("\" />")
//                .append("<input type=\"hidden\" name=\"surl\" value=\"").append(surl).append("\" />")
//                .append("<input type=\"hidden\" name=\"furl\" value=\"").append(furl).append("\" />")
//                .append("<input type=\"hidden\" name=\"service_provider\" value=\"payu_paisa\" />")
//                .append("<input type=\"hidden\" name=\"lastname\" value=\"").append(lastName).append("\" />");
//
//        if (unit.getDonor().getAddress() != null) {
//            Address address = unit.getDonor().getAddress();
//
//            htmlResponse
//                    .append("<input type=\"hidden\" name=\"address1\" value=\"" + address.getAddressLine1() + "\"/>");
//            htmlResponse
//                    .append("<input type=\"hidden\" name=\"address2\" value=\"" + address.getAddressLine2() + "\"/>");
//            htmlResponse.append("<input type=\"hidden\" name=\"city\" value=\"" + address.getCity() + "\"/>");
//            htmlResponse.append("<input type=\"hidden\" name=\"state\" value=\"" + address.getState() + "\"/>");
//            htmlResponse.append("<input type=\"hidden\" name=\"country\" value=\"" + address.getCountry() + "\"/>");
//            htmlResponse.append("<input type=\"hidden\" name=\"zipcode\" value=\"" + address.getZipCode() + "\"/>");
//
//        }
//
//        if (pan != null) {
//            htmlResponse.append("<input type=\"hidden\" name=\"udf1\" value=\"").append(pan).append("\" />");
//        }
//
//        if (subscriptionTypeCode != null) {
//            htmlResponse.append("<input type=\"hidden\" name=\"udf2\" value=\"").append(subscriptionTypeCode)
//                    .append("\" />");
//        }
//
//        if (additionalComments != null) {
//            htmlResponse.append("<input type=\"hidden\" name=\"udf3\" value=\"").append(additionalComments)
//                    .append("\" />");
//        }
//
//        htmlResponse.append("</form>").append("<center><div><strong>Please Wait...</strong></div></center>")
//                .append("<script>document.getElementById(\"payuform\").submit();</script>").append("</body></html>");
//
//        ResponseObject object = new ResponseObject();
//        object.setResponseContent(htmlResponse.toString());
//        return object;
//    }
//
//    @Override
//    public JsonObject handleGatewayResponse(CustomSecurityWrapperRequest request, String[] queryParams)
//            throws Exception {
//        String transactionId = StringUtility.trimAndEmptyIsNull(request.getParameter("txnid"));
//        String payumoneyId = StringUtility.trimAndEmptyIsNull(request.getParameter("payuMoneyId"));
//        String statusFromResponse = request.getParameter("status");
//        boolean isSuccess = "success".equals(StringUtility.trimAndEmptyIsNull(statusFromResponse));
//        String errorMessage = null;
//        String errorCode = null;
//
//        PaymentUnit unit = DBUtil.getPaymentUnitByTransactionId(transactionId);
//
//        String requestParameters = convertRequestParametersToString(request);
//
//        String fromEmail = ConfigStore.getStringValue(EMAIL_ALL_RECIPIENT, "booksforthepeopledelhi@gmail.com");
//
//        JsonObject response = new JsonObject();
//        if (unit == null) {
//
//            EmailMessage email = new EmailMessage();
//            String emailTo = ConfigStore.getStringValue(EMAIL_WEBSITE_DEV, "reachnishant26@gmail.com");
//            email.addTo(emailTo);
//            email.setFrom(fromEmail);
//            email.setSubject("Payment Failure notification");
//            email.setContent(getPaymentFailedDevEmailContent(requestParameters));
//            EmailSender.sendEmail(email);
//
//            response.addProperty("isSuccess", false);
//            response.addProperty("error-msg", "Payment Gateway error");
//            return response;
//        }
//
//        JsonObject details = new JsonObject();
//        details.addProperty("transactionId", unit.getTransactionId());
//
//        StringBuilder hashString = new StringBuilder();
//
//        String salt = PaymentConfigStore.getStringValue("PG_PU_SALT", null);
//        String key = PaymentConfigStore.getStringValue("PG_PU_KEY", null);
//
//        String txnid = unit.getTransactionId();
//        String amount = StringUtility.formatAmount(unit.getAmount());
//        String firstName = unit.getFirstName();
//
//        // String email = unit.getEmail();
//
//        String emailFromResponse = request.getParameter("Email id", "email", CustomSecurityRequestType.EMAIL);
//
//        String productInfo = "Donation for the library";
//
//        String additionalComments = StringUtility.trimAndEmptyIsNull(unit.getExtraNotes());
//        String subscriptionTypeCode = unit.getDonor().getSubscriptionType() != null ? unit.getDonor().getSubscriptionType().getCode() : null;
//        String pan = unit.getDonor().getPanNumber();
//
//        String additionalAmount = StringUtility.trimAndEmptyIsNull(request.getParameter("additionalCharges"));
//
//        hashString.append(additionalAmount != null ? additionalAmount + "|" : "").append(salt).append("|")
//                .append(statusFromResponse).append("||||||||")
//                .append(additionalComments != null ? additionalComments : "").append("|")
//                .append(subscriptionTypeCode != null ? subscriptionTypeCode : "").append("|")
//                .append(pan != null ? pan : "").append("|").append(emailFromResponse).append("|").append(firstName)
//                .append("|").append(productInfo).append("|").append(amount).append("|").append(txnid).append("|")
//                .append(key);
//
//        String generatedHash = hashCal("SHA-512", hashString.toString());
//        String hashFromResponse = request.getParameter("hash", "hash", CustomSecurityRequestType.DEFAULT_SAFE_STRING);
//
//        if (!generatedHash.equals(hashFromResponse)) {
//            s_logger.error("ERROR in matching the hash obtained from the response. Exiting");
//            s_logger.info("Generated hash: " + generatedHash);
//            s_logger.info("hash from response: " + hashFromResponse);
//            s_logger.info(requestParameters);
//
//            EmailMessage message = new EmailMessage();
//            String emailTo = ConfigStore.getStringValue(EMAIL_WEBSITE_DEV, "reachnishant26@gmail.com");
//            message.addTo(emailTo);
//            message.setFrom(fromEmail);
//            message.setSubject("Payment Failure notification - hash mismatch");
//            message.setContent(getPaymentFailedDevEmailContent(requestParameters));
//            EmailSender.sendEmail(message);
//
//            response.addProperty("isSuccess", false);
//            response.addProperty("error-msg", "Could not match hash for the transaction");
//
//            return response;
//        }
//
//        if (!isSuccess) {
//            s_logger.info("The payment failed for transaction id: " + transactionId);
//            s_logger.info("Response obtained from Gateway:");
//            s_logger.info(requestParameters);
//
//            errorMessage = StringUtility.trimAndEmptyIsNull(request.getParameter("error_Message"));
//            errorCode = StringUtility.trimAndEmptyIsNull(request.getParameter("error"));
//            JsonObject error = new JsonObject();
//            error.addProperty("error-code", errorCode == null ? "" : errorCode);
//            error.addProperty("error-msg", errorMessage == null ? "" : errorMessage);
//
//            try {
//                if (errorCode != null && !"E000".equals(errorCode)) {
//                    DBUtil.addPaymentError(transactionId, errorCode, errorMessage);
//                    response.addProperty("isSuccess", false);
//                    response.add("error", error);
//                }
//            } catch (Exception e) {
//                s_logger.error("Error in adding payment errors into db", e);
//            }
//
//        } else {
//            unit.setGatewayTransactionId(payumoneyId);
//        }
//        unit.setIsPaymentSuccessful(isSuccess);
//
//        if (isSuccess) {
//            EmailMessage message = new EmailMessage();
//            message.addTo(unit.getDonor().getEmail());
//            message.setFrom(fromEmail);
//            String paymentSuccesfullEmailContent = getEmailContentForPaymentSuccess(unit);
//            message.setSubject("Thank you! Your donation was successful");
//            message.setContent(paymentSuccesfullEmailContent);
//            EmailSender.sendEmail(message);
//        } else {
//            EmailMessage message = new EmailMessage();
//            message.addTo(unit.getDonor().getEmail());
//            message.setFrom(fromEmail);
//            String paymentFailedContent = getEmailContentForPaymentFailed(unit);
//            message.setContent(paymentFailedContent);
//            message.setSubject("Uh Oh! There has been an error.");
//            EmailSender.sendEmail(message);
//        }
//
//        if (unit.getGatewayTransactionId() != null) {
//            details.addProperty("gatewayId", unit.getGatewayTransactionId());
//        }
//
//        response.add("transactionDetails", details);
//
//        try {
//            DBUtil.updatePaymentStatus(unit);
//            response.addProperty("isSuccess", isSuccess);
//        } catch (Exception e) {
//            MessageBean.setMessage(request, MessageType.MSG_ERROR, "Error in updating payment status from gateway");
//            s_logger.error("Error in updating payment gateway status", e);
//            response.addProperty("isSuccess", false);
//            response.addProperty("error-msg", "Error in saving response in system");
//        }
//
//        return response;
//    }
//
}
