package org.communitylibrary.ui.payment.util;

import org.apache.log4j.Logger;
import org.communitylibrary.admin.entity.forms.EmailMessage;
import org.communitylibrary.data.payments.PaymentUnit;
import org.communitylibrary.ui.beans.MessageBean;
import org.communitylibrary.ui.beans.MessageBean.MessageType;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.navigation.WebsiteActions;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.ConfigStore;
import org.communitylibrary.utils.EmailSender;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.PaymentConfigStore;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonObject;
import com.instamojo.wrapper.api.Instamojo;
import com.instamojo.wrapper.api.InstamojoImpl;
import com.instamojo.wrapper.exception.ConnectionException;
import com.instamojo.wrapper.exception.InvalidPaymentOrderException;
import com.instamojo.wrapper.model.Payment;
import com.instamojo.wrapper.model.PaymentOrder;
import com.instamojo.wrapper.response.CreatePaymentOrderResponse;
import com.instamojo.wrapper.response.PaymentOrderDetailsResponse;

/**
 * @author nishant
 *
 */
public class InstamojoHandler extends PaymentHelper {
//
//    private static Logger s_logger = Logger.getLogger(InstamojoHandler.class);
//
//    /*
//     * (non-Javadoc)
//     * 
//     * @see org.communitylibrary.ui.payment.util.PaymentHelper#
//     * generatePayuMoneyPaymentForm(org.communitylibrary.data.payments.
//     * PaymentUnit,
//     * org.communitylibrary.ui.security.CustomSecurityWrapperRequest,
//     * org.communitylibrary.ui.security.CustomSecurityWrapperResponse)
//     */
//    @Override
//    public ResponseObject generatePaymentHandlingObject(PaymentUnit unit, CustomSecurityWrapperRequest request,
//            CustomSecurityWrapperResponse response) {
//
//        // success url
//        String surl = WebsiteActions.PAYMENT.getActionURL(request, response, new String[] { "status" });
//        String webhookURL = WebsiteActions.PAYMENT.getActionURL(request, response, new String[] { "status", "hook" });
//        // String surl =
//        // "http://www.thecommunitylibraryproject.org/nav/payment/status";
//
//        // String key = PaymentConfigStore.getStringValue("PG_IM_KEY", null);
//        // String authToken =
//        // PaymentConfigStore.getStringValue("PG_IM_AUTHTOKEN", null);
//        String endPoint = PaymentConfigStore.getStringValue("PG_IM_URL", null);
//        String authEndPoint = PaymentConfigStore.getStringValue("PG_IM_AUTHENDPOINT", null);
//        String clientId = PaymentConfigStore.getStringValue("PG_IM_CLIENTID", null);
//        String clientSecretKey = PaymentConfigStore.getStringValue("PG_IM_SECRETKEY", null);
//
//        PaymentOrder order = new PaymentOrder();
//
//        order.setName(StringUtility.joinIntoName(unit.getDonor().getFirstName(), unit.getDonor().getLastName()));
//        order.setEmail(unit.getDonor().getEmail());
//        order.setPhone(unit.getDonor().getPhone());
//        order.setCurrency("INR");
//        order.setAmount(NumberUtility.parsetDoubleWithDefaultOnErr(unit.getAmount(), 0.0));
//        order.setDescription("Donation for the library");
//        order.setRedirectUrl(surl);
//        order.setWebhookUrl(webhookURL);
//        order.setTransactionId(unit.getTransactionId());
//
//        Instamojo api = null;
//        ResponseObject responseObject = null;
//
//        try {
//            // gets the reference to the instamojo api
//            api = InstamojoImpl.getApi(clientId, clientSecretKey, endPoint, authEndPoint);
//            boolean isOrderValid = order.validate();
//            if (isOrderValid) {
//                try {
//                    CreatePaymentOrderResponse createPaymentOrderResponse = api.createNewPaymentOrder(order);
//                    // print the status of the payment order.
//                    s_logger.debug("JSON Payment response order: " + createPaymentOrderResponse.getJsonResponse());
//                    s_logger.debug(createPaymentOrderResponse.getPaymentOrder().getStatus());
//                    responseObject = new ResponseObject();
//                    responseObject.setUrl(createPaymentOrderResponse.getPaymentOptions().getPaymentUrl());
//                } catch (InvalidPaymentOrderException e) {
//                    if (order.isTransactionIdInvalid()) {
//                        s_logger.debug("Transaction id is invalid. This is mostly due to duplicate  transaction id.");
//                    }
//                    if (order.isCurrencyInvalid()) {
//                        s_logger.debug("Currency is invalid.");
//                    }
//                } catch (Exception e) {
//                    s_logger.error("Error while getting order response from instamojo", e);
//                }
//            } else {
//                // inform validation errors to the user.
//                if (order.isTransactionIdInvalid()) {
//                    s_logger.debug("Transaction id is invalid.");
//                }
//                if (order.isAmountInvalid()) {
//                    s_logger.debug("Amount can not be less than 9.00.");
//                }
//                if (order.isCurrencyInvalid()) {
//                    s_logger.debug("Please provide the currency.");
//                }
//                if (order.isDescriptionInvalid()) {
//                    s_logger.debug("Description can not be greater than 255 characters.");
//                }
//                if (order.isEmailInvalid()) {
//                    s_logger.debug("Please provide valid Email Address.");
//                }
//                if (order.isNameInvalid()) {
//                    s_logger.debug("Name can not be greater than 100 characters.");
//                }
//                if (order.isPhoneInvalid()) {
//                    s_logger.debug("Phone is invalid.");
//                }
//                if (order.isRedirectUrlInvalid()) {
//                    s_logger.debug("Please provide valid Redirect url.");
//                }
//
//                if (order.isWebhookInvalid()) {
//                    s_logger.debug("Provide a valid webhook url");
//                }
//            }
//        } catch (ConnectionException e) {
//            s_logger.error("Error while connecting with instamojo server", e);
//        } catch (Exception e) {
//            s_logger.error("Error while getting payment url from instamojo server", e);
//        }
//
//        return responseObject;
//
//    }
//
//    /*
//     * (non-Javadoc)
//     * 
//     * @see
//     * org.communitylibrary.ui.payment.util.PaymentHelper#handleGatewayResponse(
//     * org.communitylibrary.ui.security.CustomSecurityWrapperRequest)
//     */
//    @Override
//    public JsonObject handleGatewayResponse(CustomSecurityWrapperRequest request, String[] queryParams)
//            throws Exception {
//        // Redirect url from instamojo
//        // http://www.thecommunitylibraryproject.org/nav/payment/status?id=cf5f8faf4e5c4d75ad7c8e7bafab235f&transaction_id=TCLPB0AD0EA583&payment_id=MOJO8122005A37336430
//
//        String transactionId = StringUtility.trimAndEmptyIsNull(request.getParameter("transaction_id"));
//        String gatewayId = StringUtility.trimAndEmptyIsNull(request.getParameter("payment_id"));
//        // String statusFromResponse =
//        // StringUtility.trimAndEmptyIsNull(request.getParameter("status"));
//
//        String endPoint = PaymentConfigStore.getStringValue("PG_IM_URL", null);
//        String authEndPoint = PaymentConfigStore.getStringValue("PG_IM_AUTHENDPOINT", null);
//        String clientId = PaymentConfigStore.getStringValue("PG_IM_CLIENTID", null);
//        String clientSecretKey = PaymentConfigStore.getStringValue("PG_IM_SECRETKEY", null);
//
//        boolean isSuccess = false;
//        try {
//            Instamojo api = InstamojoImpl.getApi(clientId, clientSecretKey, endPoint, authEndPoint);
//
//            PaymentOrderDetailsResponse paymentOrderDetailsResponse = api
//                    .getPaymentOrderDetailsByTransactionId(transactionId);
//            s_logger.info("response obtained from instamojo after payment");
//            s_logger.info(paymentOrderDetailsResponse);
//
//            // print the status of the payment order.
//
//            Payment[] payments = paymentOrderDetailsResponse.getPayments();
//            if (payments == null) {
//                isSuccess = false;
//            } else {
//                for (Payment payment : payments) {
//                    if ("successful".equalsIgnoreCase(payment.getStatus())) {
//                        isSuccess = true;
//                        break;
//                    }
//                }
//            }
//
//        } catch (Exception e) {
//            s_logger.error("Error in getting payment response from the instamojo payment gateway for transaction id: "
//                    + transactionId, e);
//            isSuccess = false;
//        }
//
//        PaymentUnit unit = DBUtil.getPaymentUnitByTransactionId(transactionId);
//
//        String requestParameters = convertRequestParametersToString(request);
//
//        String fromEmail = ConfigStore.getStringValue(EMAIL_ALL_RECIPIENT, "booksforthepeopledelhi@gmail.com");
//
//        JsonObject response = new JsonObject();
//        if (unit == null) {
//
//            EmailMessage email = new EmailMessage();
//            String emailTo = ConfigStore.getStringValue(EMAIL_WEBSITE_DEV, "reachnishant26@gmail.com");
//            email.addTo(emailTo);
//            email.setFrom(fromEmail);
//            email.setSubject("Payment Failure notification");
//            email.setContent(getPaymentFailedDevEmailContent(requestParameters));
//            EmailSender.sendEmail(email);
//
//            response.addProperty("isSuccess", false);
//            response.addProperty("error-msg", "Payment Gateway error");
//            if (transactionId != null || gatewayId != null) {
//                JsonObject details = new JsonObject();
//                if (transactionId != null) {
//                    details.addProperty("transactionId", transactionId);
//                }
//
//                if (gatewayId != null) {
//                    details.addProperty("gatewayId", gatewayId);
//                }
//
//                response.add("transactionDetails", details);
//            }
//
//            return response;
//        }
//
//        JsonObject details = new JsonObject();
//        details.addProperty("transactionId", unit.getTransactionId());
//        details.addProperty("gatewayId", gatewayId);
//        response.addProperty("isSuccess", isSuccess);
//
//        if (!isSuccess) {
//            s_logger.info("The payment failed for transaction id: " + transactionId);
//            s_logger.info("Response obtained from Gateway:");
//            s_logger.info(requestParameters);
//        }
//
//        boolean isFromWebhook = queryParams.length > 1 && "hook".equalsIgnoreCase(queryParams[1]);
//
//        unit.setGatewayTransactionId(gatewayId);
//        unit.setIsPaymentSuccessful(isSuccess);
//
//        if (isFromWebhook) {
//            if (isSuccess) {
//                EmailMessage message = new EmailMessage();
//                message.addTo(unit.getDonor().getEmail());
//                message.setFrom(fromEmail);
//                String paymentSuccesfullEmailContent = getEmailContentForPaymentSuccess(unit);
//                message.setSubject("Thank you! Your donation was successful");
//                message.setContent(paymentSuccesfullEmailContent);
//                EmailSender.sendEmail(message);
//            } else {
//                EmailMessage message = new EmailMessage();
//                message.addTo(unit.getDonor().getEmail());
//                message.setFrom(fromEmail);
//                String paymentFailedContent = getEmailContentForPaymentFailed(unit);
//                message.setContent(paymentFailedContent);
//                message.setSubject("Uh Oh! There has been an error.");
//                EmailSender.sendEmail(message);
//            }
//
//            try {
//                DBUtil.updatePaymentStatus(unit);
//            } catch (Exception e) {
//                MessageBean.setMessage(request, MessageType.MSG_ERROR, "Error in updating payment status from gateway");
//                s_logger.error("Error in updating payment gateway status", e);
//                response.addProperty("isSuccess", false);
//                response.addProperty("errorMsg", "Error in saving response in system");
//            }
//        }
//
//        response.add("transactionDetails", details);
//        return response;
//    }
//
//    public static void main(String[] args) {
//
//        try {
//            // success url
//            // String surl = WebsiteActions.PAYMENT.getActionURL(request,
//            // response, new String[] { "status" });
//            // String webhookURL = WebsiteActions.PAYMENT.getActionURL(request,
//            // response, new String[] { "status", "hook" });
//            // String surl =
//            // "http://www.thecommunitylibraryproject.org/nav/payment/status";
//
//            // String key = PaymentConfigStore.getStringValue("PG_IM_KEY",
//            // null);
//            // String authToken =
//            // PaymentConfigStore.getStringValue("PG_IM_AUTHTOKEN", null);
//            PaymentConfigStore.initialise();
//            String endPoint = PaymentConfigStore.getStringValue("PG_IM_URL", null);
//            String authEndPoint = PaymentConfigStore.getStringValue("PG_IM_AUTHENDPOINT", null);
//            String clientId = PaymentConfigStore.getStringValue("PG_IM_CLIENTID", null);
//            String clientSecretKey = PaymentConfigStore.getStringValue("PG_IM_SECRETKEY", null);
//            Instamojo api = InstamojoImpl.getApi(clientId, clientSecretKey, endPoint, authEndPoint);
//            PaymentOrderDetailsResponse paymentOrder = api
//                    .getPaymentOrderDetailsByTransactionId("MOJO8c21Z05A17751046");
//            System.out.println(paymentOrder.getId());
//            System.out.println(paymentOrder.getStatus());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
}
