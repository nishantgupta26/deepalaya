package org.communitylibrary.ui.payment.util;

/**
 * @author nishant
 *
 */
public class ResponseObject {
    private String m_url;
    private String m_responseContent;

    public String getUrl() {
        return m_url;
    }

    public void setUrl(String url) {
        m_url = url;
    }

    public String getResponseContent() {
        return m_responseContent;
    }

    public void setResponseContent(String responseContent) {
        m_responseContent = responseContent;
    }

}
