package org.communitylibrary.ui.payment.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;

import org.communitylibrary.data.payments.GatewayName;
import org.communitylibrary.data.payments.PaymentUnit;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.DateUtility;

import com.google.gson.JsonObject;

/**
 * @author nishant.gupta
 *
 */
public abstract class PaymentHelper {
//    
//    protected static final String EMAIL_WEBSITE_DEV   = "EMAIL_WEBSITE_DEV";
//    protected static final String EMAIL_ALL_RECIPIENT = "EMAIL_ALL_RECIPIENT";
//
//    public abstract ResponseObject generatePaymentHandlingObject(PaymentUnit unit, CustomSecurityWrapperRequest request,
//            CustomSecurityWrapperResponse response);
//
//    public abstract JsonObject handleGatewayResponse(CustomSecurityWrapperRequest request, String[] queryParams) throws Exception;
//
//    public static PaymentHelper getHelper(GatewayName gateway) {
//
//        switch (gateway) {
//        case PayUMoney:
//            return new PayuMoneyHandler();
//        case Instamojo:
//            return new InstamojoHandler();
//        default:
//            break;
//        }
//
//        return new PayuMoneyHandler();
//    }
//
//    protected static String hashCal(String type, String str) {
//        byte[] hashseq = str.getBytes();
//        StringBuffer hexString = new StringBuffer();
//        try {
//            MessageDigest algorithm = MessageDigest.getInstance(type);
//            algorithm.reset();
//            algorithm.update(hashseq);
//            byte messageDigest[] = algorithm.digest();
//            for (int i = 0; i < messageDigest.length; i++) {
//                String hex = Integer.toHexString(0xFF & messageDigest[i]);
//                if (hex.length() == 1) {
//                    hexString.append("0");
//                }
//                hexString.append(hex);
//            }
//
//        } catch (NoSuchAlgorithmException nsae) {
//        }
//        return hexString.toString();
//    }
//
//    protected String getPaymentFailedDevEmailContent(String requestParameters) {
//        StringBuilder str = new StringBuilder("Hi,\n\r");
//        str.append(
//                "There was an error in identifying the payment unit from the response received from the gateway.\n\r\n\r");
//        str.append("We did not even receive any transaction id from the gateway.\n\r");
//        str.append("Below are some details which might help you debug the problem here\n\r");
//        str.append("Date of error: ").append(DateUtility.getDateDDMYYYYHHMM(DateUtility.getCurrentDate()))
//                .append("\n\r\n\r");
//
//        if (requestParameters != null && !requestParameters.isEmpty()) {
//            str.append("Request Parameters received: \n\r").append(requestParameters);
//        }
//
//        str.append("\n\r").append("Regards,\n\rTCLP Dev Team");
//
//        return str.toString();
//
//    }
//
//    protected String convertRequestParametersToString(CustomSecurityWrapperRequest request) {
//        Enumeration< ? > names = request.getParameterNames();
//        StringBuilder str = new StringBuilder("Request parameters\n\r");
//        while (names.hasMoreElements()) {
//            Object name = names.nextElement();
//            String value = null;
//            if ("amount_split".equals(name)) {
//                str.append("paramName: ").append(name).append(", value: ").append("<dev-comment:ignoring>")
//                        .append("\n\r");
//                continue;
//            } else {
//                value = request.getParameter(name.toString());
//            }
//            str.append("paramName: ").append(name).append(", value: ").append(value).append("\n\r");
//        }
//
//        return str.toString();
//    }
//
//    protected String getEmailContentForPaymentFailed(PaymentUnit unit) {
//        StringBuilder str = new StringBuilder();
//
//        str.append("Dear ").append(unit.getFirstName()).append(",\n\r\n\r");
//
//        str.append(
//                "We noticed that you have an incomplete/cancelled donation towards The Community Library Project. If this is due to a technical problem, please alert us and someone will be in touch with you soon to resolve the issue.\n\r\n\r");
//
//        str.append(
//                "By donating to The Community Library Project-TCLP, you will be bolstering a library movement to bring books & reading to children, young adults and adults everywhere. You will help create unfettered access to powerful tools of learning & thinking.\n\r\n\r");
//
//        str.append(
//                "Our mandate is to build, maintain and support libraries that are free and welcome all. We believe in carefully curated collections that celebrate diversity, curriculum that caters to all sections of society and safe & welcoming public spaces that nurture creativity, engagement & growth.\n\r\n\r");
//
//        str.append("In doing so, we are building an empowered citizenry of tomorrow.\n\r\n\r");
//
//        str.append("For this, we need your support & appreciate your donation.\n\r\n\r");
//
//        str.append("Warmly,\n\r");
//        str.append("The Community Library Project - TCLP");
//
//        return str.toString();
//    }
//
//    public String getEmailContentForPaymentSuccess(PaymentUnit unit) {
//        StringBuilder str = new StringBuilder();
//
//        str.append("Dear ").append(unit.getFirstName()).append(",\n\r\n\r");
//
//        str.append("Thank you.\n\r\n\r");
//        str.append(
//                "By donating to The Community Library Project-TCLP, you have joined a library movement to bring books & reading to children, young adults and adults everywhere. You are helping to create unfettered access to powerful tools of learning & thinking.\n\r\n\r");
//
//        str.append(
//                "Our mandate is to build, maintain and support libraries that are free and welcome all. We believe in carefully curated collections that celebrate diversity, curriculum that caters to all sections of society and safe & welcoming public spaces are nurture creativity, engagement & growth.\n\r\n\r");
//
//        str.append("In doing so, we are building an empowered citizenry of tomorrow.\n\r");
//
//        str.append("We thank you for supporting our mission & joining our community.\n\r\n\r");
//
//        str.append("Warmly,\n\r");
//        str.append("The Community Library Project - TCLP");
//        return str.toString();
//    }
//
}
