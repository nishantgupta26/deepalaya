package org.communitylibrary.ui.beans;

import org.apache.log4j.Logger;
import org.communitylibrary.app.config.CacheConfiguration;
import org.communitylibrary.ui.navigation.ContentActions;
import org.communitylibrary.ui.navigation.WebsiteActions;
import org.communitylibrary.ui.navigation.WebsiteNavigation;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.ConfigStore;

public class UIBean {
    private static final String CURRENT_UIACTION      = "UIBEAN_CURRENCT_UIACTION";
    private static final String CURRENT_CONTENTACTION = "UIBEAN_CURRENCT_CONTENTACTION";

    public static final String  FWD_URL_PARAM         = "fwd";

    private static final Logger s_logger              = Logger.getLogger(UIBean.class);

    private UIBean() {
    }

    // public static String getHomePageLink(CustomSecurityWrapperRequest
    // request, CustomSecurityWrapperResponse response) {
    // return WebsiteActions.HOME.getActionURL(request, response);
    // }

    public static void setCurrentUIAction(CustomSecurityWrapperRequest request, WebsiteActions action) {
        request.setAttribute(CURRENT_UIACTION, action);
    }

    public static WebsiteActions getCurrentUIAction(CustomSecurityWrapperRequest request) {
        return (WebsiteActions) request.getAttribute(CURRENT_UIACTION);
    }

    /**
     * Get url for a static file.
     * 
     * @param request
     * @param response
     * @param fileRelativePath
     * @return
     */
    public static String getStaticURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String fileRelativePath) {
        String url = request.getContextPath() + "/static/" + fileRelativePath;
        return url;
    }

    public static void setCurrentContentAction(CustomSecurityWrapperRequest request, ContentActions contentAction) {
        request.setAttribute(CURRENT_CONTENTACTION, contentAction);
    }

    public static ContentActions getCurrentContentAction(CustomSecurityWrapperRequest request) {
        Object action = request.getAttribute(CURRENT_CONTENTACTION);
        if (action == null) {
            return null;
        }

        return (ContentActions) action;
    }

    public static String getCompleteURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String restOfPath) {
        StringBuilder b = new StringBuilder(128);

        String requestURL = request.getRequestURL().toString();
        requestURL = WebsiteNavigation.extractInitialURL(requestURL, WebsiteNavigation.getServletPath());
        b.append(requestURL);

        b.append("/").append(getStaticURL(request, response, restOfPath));
        return b.toString();
    }

    public static boolean isUseOnlineDonationPage() {
        try {
            return ConfigStore.getBooleanValue(CacheConfiguration.BOL_USE_ONLINE_DONATIONS, false);
        } catch (Exception e) {
            s_logger.error("Error while checking if online payment gateway is to be used", e);
        }

        return false;
    }

    // public static String getDonateToUsURL(boolean isOnlineGateway,
    // CustomSecurityWrapperRequest secureRequest,
    // CustomSecurityWrapperResponse secureResponse) {
    // return !isOnlineGateway ?
    // WebsiteActions.CONTENT.getActionURL(secureRequest, secureResponse, null,
    // ContentActions.DONATE, false) :
    // WebsiteActions.DONATE.getActionURL(secureRequest, secureResponse, null,
    // false);
    // }
}
