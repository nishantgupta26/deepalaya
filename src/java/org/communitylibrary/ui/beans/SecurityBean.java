package org.communitylibrary.ui.beans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.owasp.esapi.filters.SecurityWrapperRequest;
import org.owasp.esapi.filters.SecurityWrapperResponse;

public class SecurityBean {
    public static CustomSecurityWrapperRequest getRequest(HttpServletRequest request) {
        if (request instanceof SecurityWrapperRequest) {
            return (CustomSecurityWrapperRequest) request;
        } else {
            return new CustomSecurityWrapperRequest(request);
        }
    }

    public static CustomSecurityWrapperResponse getResponse(HttpServletResponse response) {
        if (response instanceof SecurityWrapperResponse) {
            return (CustomSecurityWrapperResponse) response;
        } else {
            return new CustomSecurityWrapperResponse(response);
        }
    }
}
