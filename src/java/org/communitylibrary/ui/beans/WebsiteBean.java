package org.communitylibrary.ui.beans;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.communitylibrary.admin.ui.navigation.AdminActions;
import org.communitylibrary.admin.ui.navigation.AdminPages;
import org.communitylibrary.app.resources.ResourceObject;
import org.communitylibrary.app.resources.ResourceType;
import org.communitylibrary.commons.db.UserDB;
import org.communitylibrary.data.members.LibraryGroupForDigitalAccess;
import org.communitylibrary.data.resources.HomePageLink;
import org.communitylibrary.data.user.User;
import org.communitylibrary.data.user.UserRoles;
import org.communitylibrary.data.user.dto.UserLoginDTO;
import org.communitylibrary.logger.Logger;
import org.communitylibrary.ui.beans.MessageBean.MessageType;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.navigation.WebsiteActions;
import org.communitylibrary.ui.navigation.WebsiteException;
import org.communitylibrary.ui.navigation.WebsiteExceptionType;
import org.communitylibrary.ui.navigation.WebsitePages;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.ui.session.SessionStore;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class WebsiteBean {

    private static final String USER_LIST_ATTR          = "USER_LIST";
    static final int            MAX_SIZE_PER_FILE_IN_KB = 200;
    private static final String USERLOGIN_LIST_ATTR     = "USER_BEAN_LOGINS";
    private static final String CAMPAIGN_CONTENT        = "CAMPAIGN_CONTENT";
    private static final String BANNER_CONTENT          = "BANNER_CONTENT";
    private static final String HOMEPAGE_LINKS          = "HOMEPAGELINKS";
    private static Logger       s_logger                = Logger.getInstance(WebsiteBean.class);

    // public static WebsitePages logout(CustomSecurityWrapperRequest request,
    // CustomSecurityWrapperResponse response) {
    // SessionStore.setLoggedInUser(request, response, null);
    // SessionStore.invalidateSession(request);
    // MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "You have been
    // logged out successfully!");
    // return WebsitePages.setRedirectUrlInRequest(request, response,
    // WebsiteActions.LOGIN);
    // }

    public static WebsitePages login(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            boolean isPost) {

        User loggedUser = SessionStore.getLoggedInUser(request);
        if (loggedUser != null) {
            return WebsitePages.HOME_PAGE;
        }

        String email = StringUtility.trimAndEmptyIsNull(request.getParameter("email"));
        int userId = NumberUtility.parsetIntWithDefaultOnErr(email, -1);
        String password = StringUtility.trimAndEmptyIsNull(request.getParameter("password"));
        if (email == null || password == null || !isPost) {
            return WebsitePages.LOGIN_PAGE;
        }

        boolean isUserLoggedIn = loginUser(request, response, email, userId, password, false);
        if (isUserLoggedIn) {
            return WebsitePages.HOME_PAGE;
        }

        SessionStore.invalidateSession(request);
        return WebsitePages.LOGIN_PAGE;
    }

    public static boolean loginUser(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String email, int userId, String password, boolean isAddAdminCheck) {
        SessionStore.invalidateSession(request);
        try {
            User user = (userId > 0) ? UserDB.authenticateUser(null, userId, password)
                    : UserDB.authenticateUser(null, email, password);

            if (isAddAdminCheck) {
                if (user != null && !user.isAdmin()) {
                    throw new WebsiteException(WebsiteExceptionType.INVALID_ACCESS);
                }
            }
            SessionStore.setLoggedInUser(request, response, user);
            MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "You have been logged in successfully!");
            UserDB.addUserLoginInfo(user);
            return true;
        } catch (WebsiteException fe) {
            switch (fe.getType()) {
            case USER_AUTH_FAILED:
            case USER_DISABLED:
            case USER_LOGIN_INFO:
                MessageBean.setMessage(request, MessageType.MSG_ERROR, fe.getType().getDesc());
                SessionStore.invalidateSession(request);
                break;
            default:
                s_logger.error("Error in logging in user", fe);
                MessageBean.setMessage(request, MessageType.MSG_ERROR, "Invalid email or password!");
            }
        } catch (Exception e) {
            s_logger.error("Error in logging in user", e);
            MessageBean.setMessage(request, MessageType.MSG_ERROR, "Invalid email or password!");
        }

        return false;
    }

    public static boolean isUIActionAllowed(CustomSecurityWrapperRequest request, WebsiteActions action) {
        User loggedUser = SessionStore.getLoggedInUser(request);
        return isUIActionAllowed(loggedUser, action);
    }

    public static boolean isUIActionAllowed(User user, WebsiteActions action) {
        UserRoles role = (user == null) ? UserRoles.NOT_LOGGED_IN : user.getUserRole();
        return action.isRoleDefaultAllowed(role);
    }

    public static AdminPages getManageUsersPage(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams) throws Exception {

        String action = (queryParams == null || queryParams.length < 1) ? null : queryParams[0];

        if ("modify".equals(action)) {

            if (queryParams.length > 1 && "bulkadd".equals(queryParams[1])) {
                try {
                    if (!ServletFileUpload.isMultipartContent(request)) {
                        throw new RuntimeException("Request is not MultipartContent");
                    }

                    DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
                    diskFileItemFactory.setSizeThreshold(51200);
                    ServletFileUpload upload = new ServletFileUpload(diskFileItemFactory);
                    upload.setFileSizeMax(MAX_SIZE_PER_FILE_IN_KB * 1024);
                    List<FileItem> items = (List<FileItem>) upload.parseRequest(request);

                    FileItem dataItem = null;
                    String extension = null;

                    for (FileItem itm : items) {
                        if (itm.isFormField()) {
                            // Nothing to doF

                        } else {
                            dataItem = itm;
                            if (dataItem.getSize() > (MAX_SIZE_PER_FILE_IN_KB * 1024)) {
                                throw new Exception("File size too big!");
                            }

                            String name = StringUtility.trimAndEmptyIsNull(dataItem.getName());
                            if (name != null) {
                                extension = StringUtility
                                        .trimAndEmptyIsNull(name.substring(name.lastIndexOf(".") + 1).toLowerCase());
                            }
                        }
                    }
                    if (!"csv".equalsIgnoreCase(extension) && !"txt".equalsIgnoreCase(extension)) {
                        throw new Exception("Only csv and txt extension files allowed!");
                    }

                    // Parse file
                    InputStream ins = null;
                    InputStreamReader isr = null;
                    BufferedReader reader = null;

                    try {
                        int lineNo = 0;
                        String line = null;

                        ins = dataItem.getInputStream();
                        isr = new InputStreamReader(ins);
                        reader = new BufferedReader(isr);

                        List<User> users = new ArrayList<User>();
                        while ((line = reader.readLine()) != null) {
                            lineNo++;
                            if (line.trim().isEmpty() || line.trim().startsWith("userid")) {
                                continue;
                            }
                            String[] keys = line.split("[,]", -1);
                            if (keys.length < 4) {
                                s_logger.info("Ignoring line " + lineNo + " - invalid column count - " + line);
                            } else {
                                int id = NumberUtility
                                        .parsetIntWithDefaultOnErr(StringUtility.trimAndEmptyIsNull(keys[0]), -1);
                                String role = StringUtility.trimAndEmptyIsNull(keys[1].toUpperCase());
                                String name = StringUtility.trimAndEmptyIsNull(keys[2]);
                                String email = StringUtility.trimAndEmptyIsNull(keys[3]);

                                boolean isEnabled = true;
                                if (keys.length > 4) {
                                    isEnabled = "yes".equalsIgnoreCase(StringUtility.trimAndEmptyIsNull(keys[4]));
                                }

                                String password = createPassword(name, id);
                                users.add(new User(id, role, name, email, password, isEnabled));
                            }
                        }

                        s_logger.info("Report: Lines-" + lineNo + ";number of users-" + users.size());
                        int numUsersAdded = UserDB.addUsers(users);
                        MessageBean.setMessage(request, MessageType.MSG_SUCCESS,
                                "Lines-" + lineNo + ";number of users added-" + numUsersAdded);
                    } catch (Exception e) {
                        s_logger.error("Error in parsing of the file", e);
                        throw e;
                    } finally {
                        if (isr != null) {
                            try {
                                isr.close();
                            } catch (Throwable t) {
                            }
                        }
                        if (reader != null) {
                            try {
                                reader.close();
                            } catch (Throwable t) {
                            }
                        }
                        if (ins != null) {
                            try {
                                ins.close();
                            } catch (Throwable t) {
                            }
                        }
                    }
                } catch (Exception e) {
                    MessageBean.setMessage(request, MessageType.MSG_ERROR,
                            "Error in parsing of file. Could not upload users");
                }
            } else {
                int userId = UserDB.extractUserId(request.getParameter("user_id"));
                boolean isAdd = (queryParams.length > 1 && "add".equals(queryParams[1]));
                boolean updatePassword = (isAdd || "on".equalsIgnoreCase(request.getParameter("update_password")));

                String newEmailID = StringUtility.trimAndEmptyIsNull(request.getParameter("email"));
                try {

                    User editUser = new User(userId);
                    editUser.setUserRole(UserRoles.deserialize(request.getParameter("role")));
                    editUser.setName(StringUtility.trimAndEmptyIsNull(request.getParameter("name")));
                    editUser.setEmail(newEmailID);
                    editUser.setEnabled("on".equalsIgnoreCase(request.getParameter("enabled")));
                    if (updatePassword) {
                        editUser.setPassword(StringUtility.trimAndEmptyIsNull(request.getParameter("password")));
                        editUser.hashPassword();
                    }

                    if (isAdd) {
                        try {
                            UserDB.addUser(null, editUser);
                            MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "Added user successfully!");
                        } catch (Exception e) {
                            s_logger.error("Error in adding user", e);
                            MessageBean.setMessage(request, MessageType.MSG_ERROR, "Error: Could not add user!");
                        }
                    } else {
                        try {
                            UserDB.editUser(null, editUser, updatePassword);
                            MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "Updated user successfully!");
                        } catch (Exception e) {
                            s_logger.error("Error in updating user", e);
                            MessageBean.setMessage(request, MessageType.MSG_ERROR, "Error: Could not update user!");
                        }
                    }
                } catch (Exception we) {
                    if (we instanceof WebsiteException) {
                        if (((WebsiteException) we).getType() == WebsiteExceptionType.INVALID_ACCESS) {
                            MessageBean.setMessage(request, MessageType.MSG_ERROR,
                                    "Only zillious.com email ids supported");
                        }

                    }
                }
            }
        }

        request.setAttribute(USER_LIST_ATTR, UserDB.getAllUsers(null));
        return AdminPages.MANAGE_USERS_PAGE;
    }

    private static String createPassword(String name, int id) {
        name = name.toLowerCase();
        String paddedId = StringUtility.pad(String.valueOf(id), "0", 2, true);
        String[] nameTokens = name.split(" ", -1);

        String password = nameTokens[nameTokens.length - 1] + paddedId.substring(0, 1) + nameTokens[0]
                + paddedId.substring(1, 2);
        s_logger.info("userid: " + id + ", name:" + name + "-> password:" + password);
        return password;
    }

    public static void main(String[] args) {
        System.out.println(createPassword("Apoorv Upadhye", 90));
    }

    public static List<User> getUsersFromReq(HttpServletRequest request) {
        return (List<User>) request.getAttribute(USER_LIST_ATTR);
    }

    public static boolean isUserLoggedIn(CustomSecurityWrapperRequest request) {
        User loggedUser = SessionStore.getLoggedInUser(request);
        return (loggedUser != null && loggedUser.getUserRole().isLoggedInRole());
    }

    public static boolean isAdminActionAllowed(CustomSecurityWrapperRequest request, AdminActions action) {
        User loggedUser = SessionStore.getLoggedInUser(request);
        return isAdminActionAllowed(loggedUser, action);
    }

    public static boolean isAdminActionAllowed(User user, AdminActions action) {
        UserRoles role = (user == null) ? UserRoles.NOT_LOGGED_IN : user.getUserRole();
        return action.isRoleDefaultAllowed(role);
    }

    public static AdminPages getManageUserLoginsPage(HttpServletRequest request, HttpServletResponse response,
            String[] queryParams) throws Exception {
        if (queryParams != null && queryParams.length > 0 && "getSpecificLogin".equals(queryParams[0])) {
            StringBuilder jsonString = new StringBuilder("{");
            int userId = NumberUtility.parsetIntWithDefaultOnErr(request.getParameter("user_id"), -1);
            String dateString = request.getParameter("date");
            Date date = DateUtility.parseDateInDDMMYYYYNullIfError(dateString);
            int numLogins = UserDB.getnumberOfLogins(userId, date);
            if (numLogins == -1) {
                jsonString.append("\"status\":\"MSG_ERROR\"").append(",\"date\":\"")
                        .append(date != null ? DateUtility.getDateInDDMMMYYYY(date) : "").append("\"");
            } else {
                jsonString.append("\"status\":\"MSG_SUCCESS\",").append("\"date\":\"")
                        .append(DateUtility.getDateInDDMMMYYYY(date)).append("\",").append("\"numLogins\":\"")
                        .append(numLogins).append("\"");
            }
            jsonString.append("}");
            response.setContentType("application/json");
            response.getWriter().write(jsonString.toString());
            return AdminPages.NO_PAGE;
        } else {
            Map<User, UserLoginDTO> allUserLogins = UserDB.getAllUserLogins();
            request.setAttribute(USERLOGIN_LIST_ATTR, allUserLogins);
            return AdminPages.MANAGE_LOGINS_PAGE;
        }
    }

    public static Map<User, UserLoginDTO> getUserLoginsFromReq(HttpServletRequest request) {
        return (Map<User, UserLoginDTO>) request.getAttribute(USERLOGIN_LIST_ATTR);
    }

    public static WebsitePages homePage(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams, boolean isPost) {
        Map<ResourceType, List<ResourceObject>> allResourceObjects = DBUtil.getAllBannerObjects();

        List<ResourceObject> allCampaignObjects = null;
        if (allResourceObjects != null) {
            allCampaignObjects = allResourceObjects.get(ResourceType.Campaign);
        }

        setCampaignResourcesInRequest(request, allCampaignObjects);

        List<ResourceObject> allBannerObjects = null;
        if (allResourceObjects != null) {
            allBannerObjects = allResourceObjects.get(ResourceType.Banner);
        }

        setBannerResourcesInRequest(request, allBannerObjects);
        return WebsitePages.HOME_PAGE;
    }

    private static void setCampaignResourcesInRequest(CustomSecurityWrapperRequest request,
            List<ResourceObject> allBannerObjects) {
        request.setAttribute(CAMPAIGN_CONTENT, allBannerObjects);
    }

    public static List<ResourceObject> getCampaignResourcesFromRequest(CustomSecurityWrapperRequest request) {
        Object attribute = request.getAttribute(CAMPAIGN_CONTENT);
        if (attribute != null) {
            return (List<ResourceObject>) attribute;
        }

        return null;
    }

    private static void setBannerResourcesInRequest(CustomSecurityWrapperRequest request,
            List<ResourceObject> allBannerObjects) {
        request.setAttribute(BANNER_CONTENT, allBannerObjects);
    }

    public static List<ResourceObject> getBannerResourcesFromRequest(CustomSecurityWrapperRequest request) {
        Object attribute = request.getAttribute(BANNER_CONTENT);
        if (attribute != null) {
            return (List<ResourceObject>) attribute;
        }

        return null;
    }

    public static WebsitePages getHomePageWithLinks(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams) {

        if (queryParams != null && queryParams.length > 0) {
            if ("updateCount".equals(queryParams[0])) {
                if (queryParams.length > 2) {
                    try {
                        String id = queryParams[1];
                        boolean isEnglish = Boolean.valueOf(queryParams[2]);
                        boolean isUpdated = DBUtil.updateLinkCount(id, isEnglish);
                        if (isUpdated) {
                            s_logger.debug("updated count for homepage link with id: " + id + ", language: "
                                    + (isEnglish ? "English" : "Hindi"));
                        } else {
                            s_logger.debug("Could not update count for homepage link with id: " + id + ", language: "
                                    + (isEnglish ? "English" : "Hindi"));
                        }

                        return WebsitePages.NO_PAGE;
                    } catch (Exception e) {
                        s_logger.error("Error in updating count for homepage link");
                    }

                }
            }
        }

        LibraryGroupForDigitalAccess defaultgroup = LibraryGroupForDigitalAccess.UPPER;
        if (queryParams != null && queryParams.length >= 1) {
            try {
                LibraryGroupForDigitalAccess group = LibraryGroupForDigitalAccess.deserialize(queryParams[0]);
                if (group != null) {
                    defaultgroup = group;
                }
            } catch (Exception e) {

            }
        }

        List<HomePageLink> allHomePageLinks = DBUtil.getAllHomePageLinks(defaultgroup);

        // Add code to sort by number of times the link has been clicked. more
        // the times
        // a link has been clicked should appear on top.
        setHomePageLinksToRequest(request, allHomePageLinks);
        return WebsitePages.HOMEPAGELINKS;
    }

    private static void setHomePageLinksToRequest(CustomSecurityWrapperRequest request,
            List<HomePageLink> allHomePageLinks) {
        request.setAttribute(HOMEPAGE_LINKS, allHomePageLinks);
    }

    public static List<HomePageLink> getHomePageLinksFromRequest(CustomSecurityWrapperRequest request) {
        Object attribute = request.getAttribute(HOMEPAGE_LINKS);
        if (attribute != null) {
            return (List<HomePageLink>) attribute;
        }

        return null;
    }

    public static void sortLinks(List<HomePageLink> links, boolean isEnglish) {
        Collections.sort(links, new Comparator<HomePageLink>() {

            @Override
            public int compare(HomePageLink o1, HomePageLink o2) {
                int val1 = 0;
                int val2 = 0;
                if (isEnglish) {
                    val1 = o1.getEnglishLinkClicks();
                    val2 = o2.getEnglishLinkClicks();
                } else {
                    val1 = o1.getHindiLinkClicks();
                    val2 = o2.getHindiLinkClicks();
                }
                return val2 - val1;
            }

        });
    }

}
