package org.communitylibrary.ui.beans;

import org.communitylibrary.data.members.Relationship;

/**
 * @author nishant.gupta
 *
 */
public class RelationDTO {

    private String m_relativeID;
    private Relationship m_relationShip;

    public RelationDTO(String relativeId, Relationship relationShip) {
        m_relativeID = relativeId;
        m_relationShip = relationShip;
    }

    /**
     * @return the relativeID
     */
    public String getRelativeID() {
        return m_relativeID;
    }

    /**
     * @return the relationShip
     */
    public Relationship getRelationShip() {
        return m_relationShip;
    }

}
