package org.communitylibrary.ui.beans;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.communitylibrary.data.LibraryBranch;
import org.communitylibrary.data.members.Gender;
import org.communitylibrary.data.members.LibraryGroups;
import org.communitylibrary.data.members.LibraryMember;
import org.communitylibrary.data.user.User;
import org.communitylibrary.logger.Logger;
import org.communitylibrary.ui.beans.MessageBean.MessageType;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.navigation.WebsiteActions;
import org.communitylibrary.ui.navigation.WebsitePages;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.ui.session.SessionStore;
import org.communitylibrary.ui.util.IntegerUtility;
import org.communitylibrary.ui.view.dto.ViewMemberDTO;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.StringUtility;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

/**
 * @author nishant.gupta
 *
 */
public class MembersBean {
    public static final int            MAX_SIZE_PER_FILE_IN_KB = 1024;
    // private static final String ALL_MEMBERS = "ALL_MEMBERS";
    private static final String MEMBER                  = "MEMBER";
    private static Logger       s_logger                = Logger.getInstance(MembersBean.class);

    public static WebsitePages getManageMembersPage(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, WebsiteActions websiteAction)
            throws Exception {
        User loggedInUser = SessionStore.getLoggedInUser(request);
        if (!WebsiteBean.isUIActionAllowed(loggedInUser, websiteAction)) {
            s_logger.info("Add members action not available for "
                    + ((loggedInUser == null) ? "web user" : loggedInUser.getName() + ":" + loggedInUser.getUserId()));
            return WebsitePages.LOGIN_PAGE;
        }

        if (queryParams != null && queryParams.length > 0) {
            if ("add".equalsIgnoreCase(queryParams[0])) {
                if (queryParams.length > 1 && "bulkadd".equals(queryParams[1])) {

                    try {
                        if (!ServletFileUpload.isMultipartContent(request)) {
                            throw new RuntimeException("Request is not MultipartContent");
                        }

                        DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
                        diskFileItemFactory.setSizeThreshold(51200);
                        ServletFileUpload upload = new ServletFileUpload(diskFileItemFactory);
                        upload.setFileSizeMax(MAX_SIZE_PER_FILE_IN_KB * 1024);
                        List<FileItem> items = (List<FileItem>) upload.parseRequest(request);

                        FileItem dataItem = null;
                        String extension = null;

                        for (FileItem itm : items) {
                            if (itm.isFormField()) {
                                // Nothing to doF

                            } else {
                                dataItem = itm;
                                if (dataItem.getSize() > (MAX_SIZE_PER_FILE_IN_KB * 1024)) {
                                    throw new Exception("File size too big!");
                                }

                                String name = StringUtility.trimAndEmptyIsNull(dataItem.getName());
                                if (name != null) {
                                    extension = StringUtility.trimAndEmptyIsNull(
                                            name.substring(name.lastIndexOf(".") + 1).toLowerCase());
                                }
                            }
                        }
                        if (!"csv".equalsIgnoreCase(extension) && !"txt".equalsIgnoreCase(extension)) {
                            throw new Exception("Only csv and txt extension files allowed!");
                        }

                        // Parse file
                        InputStream ins = null;
                        InputStreamReader isr = null;
                        CSVReader reader = null;

                        String[] keys = null;
                        String line = null;
                        try {
                            int lineNo = 0;

                            ins = dataItem.getInputStream();
                            isr = new InputStreamReader(ins);
                            reader = new CSVReaderBuilder(isr)
                                    .withCSVParser(new CSVParserBuilder().withSeparator(';').build()).build();

                            List<LibraryMember> members = new ArrayList<LibraryMember>();
                            while ((keys = reader.readNext()) != null) {
                                try {

                                    lineNo++;
                                    // ignoring empty lines and first line
                                    if (keys.length == 0 || "id".equalsIgnoreCase(keys[0])) {
                                        continue;
                                    }
                                    line = StringUtility.join(keys, " ");

                                    if (keys.length < 15) {
                                        s_logger.info("Ignoring line " + lineNo + " - invalid column count - " + line);
                                        continue;
                                    }

                                    int i = 0;

                                    String new_id = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    // new_id =
                                    // LibraryBranch.removePrefixIfPresent(new_id);
                                    if (new_id == null) {
                                        throw new RuntimeException("ID is mandatory, line: " + line);
                                    }

                                    String firstName = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String lastName = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String name = StringUtility.joinIntoName(firstName, lastName);

                                    String dobString = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String dob = DateUtility.convertYYYYMMDDToDDMMYYYY(dobString, "-");

                                    // String age =
                                    // StringUtility.trimAndEmptyIsNull(keys[5]);
                                    String age = null;

                                    String phone1 = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String phone2 = StringUtility.trimAndEmptyIsNull(keys[i++]);

                                    String phone = StringUtility.join(phone1, phone2, ";");

                                    String motherFirstName = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String motherLastName = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String motherName = StringUtility.joinIntoName(motherFirstName, motherLastName);

                                    String fatherFirstName = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String fatherLastName = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String fatherName = StringUtility.joinIntoName(fatherFirstName, fatherLastName);

                                    String address = StringUtility.trimAndEmptyIsNull(keys[i++]);

                                    String libraryGroup = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    LibraryGroups lGroup = LibraryGroups.deserialize(libraryGroup.substring(0, 1));

                                    // String school =
                                    // StringUtility.trimAndEmptyIsNull(keys[14]);
                                    // String studyClass =
                                    // StringUtility.trimAndEmptyIsNull(keys[15]);
                                    String school = null;
                                    String studyClass = null;

                                    String genderStr = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    Gender gender = Gender.deserialize(genderStr.substring(0, 1));

                                    String joinDateString = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    String joinDate = DateUtility.convertYYYYMMDDToDDMMYYYY(joinDateString, "-");

                                    String branchCode = StringUtility.trimAndEmptyIsNull(keys[i++]);
                                    LibraryBranch libraryBranch = LibraryBranch.deserialize(branchCode);

                                    LibraryMember member = new LibraryMember(new_id, name, dob, age, phone, motherName,
                                            fatherName, address, lGroup, school, studyClass, gender, libraryBranch,
                                            joinDate);
                                    members.add(member);
                                } catch (Exception e) {
                                    s_logger.error("Error in parsing of line: " + line + "; Skipping this", e);
                                }
                            }

                            s_logger.info("Report: Lines-" + lineNo + ";number of members-" + members.size());

                            int numMembersAdded = DBUtil.addMembers(members);

                            MessageBean.setMessage(request, MessageType.MSG_SUCCESS,
                                    "Lines-" + lineNo + ";number of users added-" + numMembersAdded);
                        } catch (Exception e) {
                            s_logger.error("Error in parsing of the file", e);
                            s_logger.debug("line: " + line, e);
                            throw e;
                        } finally {
                            if (isr != null) {
                                try {
                                    isr.close();
                                } catch (Throwable t) {
                                }
                            }
                            if (reader != null) {
                                try {
                                    reader.close();
                                } catch (Throwable t) {
                                }
                            }
                            if (ins != null) {
                                try {
                                    ins.close();
                                } catch (Throwable t) {
                                }
                            }
                        }
                    } catch (Exception e) {
                        MessageBean.setMessage(request, MessageType.MSG_ERROR,
                                "Error in parsing of file. Could not upload users");
                    }

                } else {
                    // removing individual add for now
                }
            }
        }
        return WebsitePages.ADD_MEMBERS_PAGE;
    }

    public static WebsitePages getDisplayMembersPage(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, WebsiteActions websiteAction) {
        User loggedInUser = SessionStore.getLoggedInUser(request);
        if (!WebsiteBean.isUIActionAllowed(loggedInUser, websiteAction)) {
            s_logger.info("MANAGE_USERS action not available for "
                    + ((loggedInUser == null) ? "web user" : loggedInUser.getName() + ":" + loggedInUser.getUserId()));
            return WebsitePages.LOGIN_PAGE;
        }

        if (queryParams != null && queryParams.length > 0) {
            if ("view".equalsIgnoreCase(queryParams[0])) {
                if (queryParams.length > 1) {
                    int id = IntegerUtility.parseIntWithDefaultOnError(queryParams[1], -1);
                    if (id != -1) {
                        List<LibraryMember> member = DBUtil.getMembers(id);
                        if (member != null && !member.isEmpty()) {
                            setLibraryMemberInReq(request, member.get(0));
                            return WebsitePages.MEMBER_DETAILS;
                        } else {
                            MessageBean.setMessage(request, MessageType.MSG_ERROR,
                                    "Could not find any member with this id");
                            return WebsitePages.VIEW_MEMBERS_PAGE;
                        }
                    }
                }
            } else if ("delete".equalsIgnoreCase(queryParams[0])) {
                int id = IntegerUtility.parseIntWithDefaultOnError(request.getParameter("member_id"), -1);
                if (id != -1) {
                    boolean isDeleted = DBUtil.deleteMember(id);
                    if (isDeleted) {
                        MessageBean.setMessage(request, MessageType.MSG_SUCCESS, "Member with id: " + id + ", deleted");
                    } else {
                        MessageBean.setMessage(request, MessageType.MSG_ERROR,
                                "Could not delete member with id: " + id);
                    }
                }
            } else if ("edit".equalsIgnoreCase(queryParams[0])) {
                int id = IntegerUtility.parseIntWithDefaultOnError(request.getParameter("member_id"), -1);
                if (id != -1) {
                    List<LibraryMember> member = DBUtil.getMembers(id);
                    if (member != null && !member.isEmpty()) {
                        setLibraryMemberInReq(request, member.get(0));
                        return WebsitePages.ADD_MEMBERS_PAGE;
                    } else {
                        MessageBean.setMessage(request, MessageType.MSG_ERROR,
                                "Could not find any member with this id");
                    }
                }
            }
        }

        // List<LibraryMember> members = DBUtil.getMembers();
        // setLibraryMembersInReq(request, members);

        return WebsitePages.VIEW_MEMBERS_PAGE;
    }

    private static void setLibraryMemberInReq(CustomSecurityWrapperRequest request, LibraryMember libraryMember) {
        request.setAttribute(MEMBER, libraryMember);
    }

    public static LibraryMember getLibraryMemberFromReq(CustomSecurityWrapperRequest req) {
        Object members = req.getAttribute(MEMBER);
        if (members != null) {
            return (LibraryMember) members;
        }
        return null;
    }

    // private static void setLibraryMembersInReq(CustomSecurityWrapperRequest
    // request, List<LibraryMember> members) {
    // request.setAttribute(ALL_MEMBERS, members);
    // }

    public static List<LibraryMember> getLibraryMembersFromReq(CustomSecurityWrapperRequest req) {
        // Object members = req.getAttribute(ALL_MEMBERS);
        // if (members != null) {
        // return (List<LibraryMember>) members;
        // }
        return null;
    }

    public static String calculateAge(String dob) {
        if (dob == null) {
            return StringUtility.EMPTY_STRING;
        }

        int years = 0;
        int months = 0;

        try {
            Date birthDate = DateUtility.parseDateInDDMMYYYY(dob);

            if (birthDate == null) {
                return StringUtility.EMPTY_STRING;
            }

            // create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());

            // create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);

            // Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            // Get difference between months
            months = currMonth - birthMonth;
            // if month difference is in negative then reduce years by one and
            // calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }
            // Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE)) {

            } else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                now.add(Calendar.MONTH, -1);
            } else {
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }

            return years + " Years, " + months + " Months";
        } catch (Exception e) {
            s_logger.error("Error in converting dob to age: " + dob, e);
        }

        return StringUtility.EMPTY_STRING;
    }

    public static WebsitePages getAjaxMembers(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, WebsiteActions websiteActions) {
        User loggerUser = SessionStore.getLoggedInUser(request);
        if (!WebsiteBean.isUIActionAllowed(loggerUser, websiteActions)) {
            s_logger.info("Get members action not available for "
                    + ((loggerUser == null) ? "web user" : loggerUser.getName() + ":" + loggerUser.getUserId()));
            return WebsitePages.LOGIN_PAGE;
        }

        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = StringUtility.trimAndEmptyIsNull(request.getParameter("length"));
        String searchQuery = StringUtility.trimAndEmptyIsNull(request.getParameter("search[value]"));

        ViewMemberDTO dto = new ViewMemberDTO(draw, start, length, searchQuery);
        StringBuilder returnJSON = new StringBuilder("{");
        try {
            DBUtil.populateViewMembersDTO(dto);
            returnJSON.append("\"draw\":").append(draw);
            returnJSON.append(",\"recordsTotal\":").append(dto.getTotalRecords());
            returnJSON.append(",\"recordsFiltered\":").append(dto.getRecordsFiltered());
            returnJSON.append(",\"data\":").append(dto.getMembersJSON());
        } catch (Exception e) {
            returnJSON = new StringBuilder("{").append("\"error\":\"There was an error in processing\"");
        }
        returnJSON.append("}");
        s_logger.debug("json:" + returnJSON.toString());
        try {
            response.setContentType("application/json");
            response.getWriter().write(returnJSON.toString());
        } catch (Exception e) {
            s_logger.error("Error while writing to output stream", e);
        }
        // setLibraryMembersInReq(request, members);

        return WebsitePages.NO_PAGE;
    }

}
