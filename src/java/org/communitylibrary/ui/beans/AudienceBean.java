package org.communitylibrary.ui.beans;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.net.util.Base64;
import org.apache.log4j.Logger;
import org.communitylibrary.admin.entity.blog.Blogpost;
import org.communitylibrary.admin.entity.forms.EmailMessage;
import org.communitylibrary.admin.entity.forms.Form;
import org.communitylibrary.admin.entity.page.PageResource;
import org.communitylibrary.admin.ui.beans.SettingsBean;
import org.communitylibrary.app.config.CacheConfiguration;
import org.communitylibrary.app.engine.WebserviceAPI;
import org.communitylibrary.data.ContactDTO;
import org.communitylibrary.data.entity.EventData;
import org.communitylibrary.ui.navigation.ContentActions;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.navigation.WebsiteException;
import org.communitylibrary.ui.navigation.WebsiteExceptionType;
import org.communitylibrary.ui.navigation.WebsitePages;
import org.communitylibrary.ui.security.CustomSecurityRequestType;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.ConfigStore;
import org.communitylibrary.utils.EmailSender;
import org.communitylibrary.utils.JsonUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author Nishant
 * 
 */
public class AudienceBean {
    private static final String CONTENT       = "CONTENT";
    private static final String BLOGPOST_ATTR = "BLOGPOST";
    private static final String PAGE_ATTR     = "CUSTOMPAGE";
    private static final String FORM_ATTR     = "USERFORM";
    private static Logger       s_logger      = Logger.getLogger(AudienceBean.class);
    private static EmailSender  s_emailSender = EmailSender.getInstance();

    public static WebsitePages subscribeToNewsLetter(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams) {
        String ipAddress = request.getRemoteAddr();
        if (queryParams != null && queryParams.length > 0 && "addSubscription".equals(queryParams[0].trim())) {
            String message = null;
            String email = null;
            String cs = null;
            try {
                email = request.getParameter("Email id to subscribe to the mailing list", "email",
                        CustomSecurityRequestType.EMAIL);
                cs = request.getParameter("Hidden field to check if it is a valid hit", "cs",
                        CustomSecurityRequestType.DEFAULT_SAFE_STRING);
                if (email == null && (cs == null || !"sub".equals(cs))) {
                    throw new WebsiteException(WebsiteExceptionType.INVALID_ACCESS);
                }
                boolean status = DBUtil.addEmailToSubscriptionList(email, ipAddress);
                if (status) {
                    message = "{'status': 'success'}";
                }
            } catch (Exception e) {
                if (e instanceof WebsiteException) {
                    WebsiteException we = (WebsiteException) e;
                    if (we.getType() == WebsiteExceptionType.NEWSLETTER_SUBSCRIBED) {
                        message = "{'status': 'error', 'message':'" + we.getType().getDesc() + "'}";
                    } else if (we.getType() == WebsiteExceptionType.INVALID_ACCESS) {
                        message = "{'status': 'error', 'message':'" + we.getType().getDesc() + "'}";
                    }

                }
                if (message == null) {
                    message = "{'status': 'error', 'message':'There has been an error in subscribing to the newsletter. Please contact the administrator'}";
                    s_emailSender.sendNewsletterSubscriptionAlert(email, ipAddress, false);
                }
                s_logger.error("Error while saving the email to the newsletter subscription list", e);
            }
            try {
                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(message);
                response.flushBuffer();
            } catch (Exception e) {
                s_logger.error("Error while writing the json respone", e);

            }
            return WebsitePages.NO_PAGE;
        }
        return WebsitePages.TERMS_PAGE;
    }

//    public static WebsitePages addContactRequest(CustomSecurityWrapperRequest request,
//            CustomSecurityWrapperResponse response, String[] queryParams) {
//        String remoteAddr = request.getRemoteAddr();
//        if (queryParams != null && queryParams.length > 0) {
//            if ("addContactRequest".equals(queryParams[0].trim())) {
//                String email = null;
//                String name = null;
//                String phone = null;
//                String subject = null;
//                String contactMessage = null;
//                try {
//                    String ipAddress = remoteAddr;
//                    email = request.getParameter("Email id of the person requesting contact", "email",
//                            CustomSecurityRequestType.EMAIL);
//                    name = request.getParameter("Name of the person requesting contact", "name",
//                            CustomSecurityRequestType.DEFAULT_SAFE_STRING);
//                    phone = request.getParameter("Phone number of the person requesting contact", "phone",
//                            CustomSecurityRequestType.DEFAULT_SAFE_STRING);
//                    subject = request.getParameter("Subject of the message", "subject",
//                            CustomSecurityRequestType.DEFAULT_SAFE_STRING);
//                    contactMessage = request.getParameter("Contact message", "message",
//                            CustomSecurityRequestType.DEFAULT_SAFE_STRING);
//
//                    DBUtil.addContactRequest(email, name, phone, subject, contactMessage, ipAddress);
//                    MessageBean.setMessage(request, MessageType.MSG_SUCCESS,
//                            "Your Request has been received. We will get in touch with you shortly!");
//
//                } catch (Exception e) {
//                    String message2 = null;
//                    StringWriter sw = new StringWriter();
//                    PrintWriter pw = new PrintWriter(sw);
//                    e.printStackTrace(pw);
//                    if (e instanceof WebsiteException) {
//                        WebsiteException websiteException = (WebsiteException) e;
//                        WebsiteExceptionType type = websiteException.getType();
//                        if (type == WebsiteExceptionType.DUPLICATE_CONTACT_REQUEST) {
//                            message2 = websiteException.getMessage() + " : " + (websiteException.getSupport());
//                        } else {
//                            message2 = websiteException.getMessage();
//                            s_emailSender.prepareContactInquiryMailAndSend(email, name, phone, contactMessage,
//                                    remoteAddr, sw.toString(), null, null);
//                        }
//                    } else {
//                        message2 = "There has been a problem, please try again after sometime. If the problem persists, please contact the system administrator";
//                        s_emailSender.prepareContactInquiryMailAndSend(null, null, null, null, remoteAddr,
//                                sw.toString(), null, null);
//                    }
//                    if (message2 != null) {
//                        MessageBean.setMessage(request, MessageType.MSG_ERROR, message2);
//                    }
//                    s_logger.error("Error while adding a new contact request", e);
//                }
//            } else if ("ack".equals(queryParams[0].trim())) {
//                if (queryParams.length > 1) {
//                    String encodedToken = queryParams[1];
//                    try {
//                        String decodedString = new String(Base64.decodeBase64(encodedToken));
//                        s_logger.debug("Decoded Token: " + decodedString);
//                        String emailId = decodedString;
//                        DBUtil.ackContactReq(emailId);
//                    } catch (Exception e) {
//                        StringWriter sw = new StringWriter();
//                        PrintWriter pw = new PrintWriter(sw);
//                        e.printStackTrace(pw);
//                        s_emailSender.sendErrorMail("Error while marking a contact request as acknowledged",
//                                sw.toString(), ("ip: " + remoteAddr + ", content: " + encodedToken));
//                    }
//                }
//            }
//        }
//        return WebsitePages.CONTACT_PAGE;
//    }

    public static void main(String[] args) {
        String decodedString = new String(Base64.decodeBase64("cmVhY2huaXNoYW50MjZAZ21haWwuY29t"));
        System.out.println("Decoded Token: " + decodedString);
    }

//    public static void sendContactInquiryReport() {
//        try {
//            List<ContactDTO> inquiryDTOs = DBUtil.fetchUnacknowledgedInquries();
//            if (inquiryDTOs == null) {
//                return;
//            }
//
//            String serverName = ConfigStore.getStringValue(CacheConfiguration.STR_SERVER_NAME, null);
//            s_emailSender.prepareDigestContactInquiryMailAndSend(inquiryDTOs, serverName);
//        } catch (Exception e) {
//            StringWriter sw = new StringWriter();
//            PrintWriter pw = new PrintWriter(sw);
//            e.printStackTrace(pw);
//            s_emailSender.sendErrorMail("Error while sending Digest contact enquiry status email", sw.toString(), null);
//        }
//    }

    public static void sendNewsletterSubscriptionReport(boolean isFirstRun) {
        try {
            List<ContactDTO> subscriptionDTOS = DBUtil.fetchNewsletterSubscritionAlerts(isFirstRun);
            if (subscriptionDTOS == null) {
                return;
            }
            s_emailSender.prepareDigestNewsletterMailAndSend(subscriptionDTOS);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            s_emailSender.sendErrorMail("Error while sending Digest contact enquiry status email", sw.toString(), null);
        }
    }

    public static String getPopupContent() {
        return DBUtil.getPopupContent();
    }

    public static void deactivatePopupRequests() {
        DBUtil.deactivatePopupRequests();
    }

    public static WebsitePages getContentPage(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, ContentActions contentAction) {
        String content = ConfigStore.getStringValue(contentAction.getConfigType(), null);
        storeContentInRequest(request, content);
        return WebsitePages.ContentPage;
    }

    private static void storeContentInRequest(CustomSecurityWrapperRequest request, String content) {
        if (content != null) {
            request.setAttribute(CONTENT, content);
        }

    }

    public static String getContentFromRequest(CustomSecurityWrapperRequest request) {
        Object content = request.getAttribute(CONTENT);
        if (content == null) {
            return null;
        }
        if (content instanceof String) {
            return (String) content;
        }

        return null;
    }

    public static WebsitePages getEvents(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams, boolean isPost) {

        int numEvents = ConfigStore.getIntValue(CacheConfiguration.INT_NUM_EVENTS, 4);

        List<EventData> events = DBUtil.getEvents(numEvents);
        String json = SettingsBean.convertRecordsToJSON(events);

        response.setContentType("application/json");
        try {
            response.getWriter().write(json);
        } catch (Exception e) {
            s_logger.error("Error while getting response json: ", e);
        }

        return WebsitePages.NO_PAGE;
    }

    public static WebsitePages getGalleryPage(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, boolean isPost) throws Exception {

        String url = ConfigStore.getStringValue(CacheConfiguration.STR_GALLERY_SOURCE_URL, null);

        JsonObject responseObject = new JsonObject();
        if (url != null && !url.isEmpty()) {
            String nextMaxIdFromReq = request.getParameter("nextMaxId");
            String urlResponse = WebserviceAPI
                    .getResponse(url + (nextMaxIdFromReq != null ? "&max_id=" + nextMaxIdFromReq : ""), true);
            StringBuilder jsonString = new StringBuilder();
            if (urlResponse != null) {
                try {
                    JsonParser parser = new JsonParser();
                    JsonElement json = parser.parse(urlResponse);
                    if (json.isJsonObject()) {
                        JsonObject root = json.getAsJsonObject();
                        JsonObject pagination = root.get("pagination").getAsJsonObject();
                        if (pagination != null && pagination.get("next_max_id") != null) {
                            String nextMaxId = pagination.get("next_max_id").getAsString();
                            s_logger.debug("Next max id: " + nextMaxId);
                            responseObject.addProperty("next_max_id", nextMaxId);

                        }
                        JsonArray responseDataArray = new JsonArray();
                        responseObject.add("data", responseDataArray);
                        // jsonString.append("\"data\":[");
                        JsonArray dataArray = root.get("data").getAsJsonArray();
                        for (JsonElement data : dataArray) {
                            JsonObject dataObj = data.getAsJsonObject();
                            String objType = dataObj.get("type").getAsString();
                            JsonElement objCaption = dataObj.get("caption").getAsJsonObject().get("text");
                            JsonElement mediaLink = dataObj.get("link");
                            if ("image".equalsIgnoreCase(objType)) {
                                JsonObject responseDataObj = new JsonObject();
                                JsonElement lowResURL = null;
                                try {
                                    lowResURL = dataObj.get("images").getAsJsonObject().get("low_resolution")
                                            .getAsJsonObject().get("url");
                                } catch (Exception e) {
                                    s_logger.error("Error in getting low resolution error", e);
                                }

                                if (lowResURL != null) {
                                    responseDataObj.add("imageURL", lowResURL);
                                }

                                JsonElement bigURL = null;
                                try {
                                    bigURL = dataObj.get("images").getAsJsonObject().get("standard_resolution")
                                            .getAsJsonObject().get("url");
                                } catch (Exception e) {
                                    s_logger.error("Error in getting low resolution error", e);
                                }

                                if (bigURL != null) {
                                    responseDataObj.add("bigURL", bigURL);
                                }

                                responseDataObj.add("caption", objCaption);
                                responseDataObj.add("link", mediaLink);
                                responseDataArray.add(responseDataObj);
                            } else if ("carousel".equalsIgnoreCase(objType)) {
                                JsonArray carouselMedia = dataObj.get("carousel_media").getAsJsonArray();
                                for (JsonElement cMedia : carouselMedia) {
                                    JsonObject responseDataObj = new JsonObject();
                                    JsonElement lowResURL = null;

                                    try {
                                        lowResURL = cMedia.getAsJsonObject().get("images").getAsJsonObject()
                                                .get("low_resolution").getAsJsonObject().get("url");
                                    } catch (Exception e) {
                                        s_logger.error("Error in getting low resolution error", e);
                                    }

                                    if (lowResURL != null) {
                                        responseDataObj.add("imageURL", lowResURL);
                                    }

                                    JsonElement bigURL = null;
                                    try {
                                        bigURL = cMedia.getAsJsonObject().get("images").getAsJsonObject()
                                                .get("standard_resolution").getAsJsonObject().get("url");
                                    } catch (Exception e) {
                                        s_logger.error("Error in getting low resolution error", e);
                                    }

                                    if (bigURL != null) {
                                        responseDataObj.add("bigURL", bigURL);
                                    }

                                    responseDataObj.add("caption", objCaption);
                                    responseDataObj.add("link", mediaLink);
                                    responseDataArray.add(responseDataObj);
                                }
                            }
                        }

                        jsonString.append(responseObject.toString());
                    }
                } catch (Exception e) {
                    s_logger.error("Error in getting Gallery JSON", e);
                    jsonString = new StringBuilder("{");
                    jsonString = new StringBuilder("{\"data\":[]");
                    jsonString.append("}");
                }
            }

            s_logger.debug("JSON String obtained is: " + jsonString.toString());

            response.setContentType("application/json");
            response.getWriter().write(jsonString.toString());
            return WebsitePages.NO_PAGE;
        }

        return WebsitePages.GALLERY_PAGE;
    }

//    public static WebsitePages getBlogs(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//            String[] queryParams, boolean isPost) throws Exception {
//
//        boolean isFeed = (queryParams != null && queryParams.length > 0 && "feed".equalsIgnoreCase(queryParams[0]));
//
//        String pageNumber = null;
//        int numBlogs = 1;
//
//        if (!isFeed) {
//            JsonElement requestJsonElement = JsonUtility.getJsonDataFromRequest(request);
//            if (requestJsonElement != null) {
//                JsonObject requestObject = (JsonObject) requestJsonElement;
//                JsonElement numPostElement = requestObject.get("numPosts");
//                if (numPostElement != null) {
//                    numBlogs = requestObject.get("numPosts").getAsInt();
//                } else {
//                    numBlogs = ConfigStore.getIntValue(CacheConfiguration.INT_NUM_POSTS, 9);
//                }
//
//                JsonElement pageElement = requestObject.get("page");
//                if (pageElement == null) {
//                    pageNumber = "1";
//                } else {
//                    pageNumber = pageElement.getAsString();
//                }
//
//            }
//        }
//
//        List<Blogpost> blogs = DBUtil.getPublishedBlogs(pageNumber, numBlogs);
//
//        JsonObject responseObject = new JsonObject();
//        if (blogs != null) {
//
//            try {
//                String baseURL = WebsiteActions.BLOGPOST.getActionURL(request, response);
//                JsonArray responseDataArray = new JsonArray();
//                responseObject.add("data", responseDataArray);
//
//                responseObject.addProperty("hasMore", blogs.size() == numBlogs);
//
//                for (int i = 0; i < numBlogs; i++) {
//
//                    // number of blogs is less than maxnumblogs per hit
//                    if (i == blogs.size()) {
//                        break;
//                    }
//
//                    Blogpost post = blogs.get(i);
//
//                    JsonObject responseDataObj = new JsonObject();
//                    if (isFeed) {
//                        responseDataObj.addProperty("blogslink",
//                                HtmlUtility.encodeForHTML(WebsiteActions.BLOGS.getActionURL(request, response)));
//                    }
//                    responseDataObj.addProperty("title", post.getTitle());
//                    responseDataObj.addProperty("author", HtmlUtility.encodeForHTML(post.getAuthor()));
//                    responseDataObj.addProperty("publishdate",
//                            HtmlUtility.encodeForHTML(DateUtility.getDateInDDMMMYY(post.getPublishDate())));
//
//                    String imageURL = HtmlUtility.extractFirstImageSrcURLFromContent(post.getPostText());
//
//                    if (imageURL != null) {
//                        responseDataObj.addProperty("bannerImg", imageURL);
//                    }
//
//                    responseDataObj.addProperty("content",
//                            StringUtility.trimToSizeAndAppendWithSpecialUnicodeCharacters(
//                                    StringUtility.removeHtmltags(StringUtility.removeImgtags(post.getPostText())), 500,
//                                    "..."));
//                    responseDataObj.addProperty("url", baseURL + "/" + post.getShareableURL());
//                    responseDataObj.addProperty("id", HtmlUtility.encodeForHTML(post.getId()));
//                    responseDataArray.add(responseDataObj);
//                }
//
//                responseObject.addProperty("status", "success");
//                responseObject.addProperty("isSuccess", true);
//            } catch (Exception e) {
//                s_logger.error("Error in getting Gallery JSON", e);
//                responseObject = new JsonObject();
//                responseObject.addProperty("status", "error");
//            }
//
//        } else {
//            responseObject.addProperty("hasMore", false);
//        }
//
//        String jsonResponse = responseObject.toString();
//        s_logger.debug("JSON String obtained is: " + jsonResponse);
//
//        response.setContentType("application/json");
//        response.getWriter().write(jsonResponse);
//        return WebsitePages.NO_PAGE;
//    }

    public static Blogpost getPostFromRequest(CustomSecurityWrapperRequest request) {
        Object o = request.getAttribute(BLOGPOST_ATTR);
        if (o != null && o instanceof Blogpost) {
            return (Blogpost) o;
        }
        return null;
    }

//    public static WebsitePages getBlogpost(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//            String[] queryParams, boolean isPost) throws WebsiteException {
//        if (queryParams == null || queryParams.length == 0) {
//            throw new WebsiteException(WebsiteExceptionType.INVALID_ACCESS);
//        }
//
//        String postId = queryParams[0];
//        boolean isID = true;
//
//        try {
//            Integer.parseInt(postId);
//        } catch (NumberFormatException nfe) {
//            isID = false;
//        }
//
//        Blogpost blogpost = null;
//
//        if (isID) {
//            blogpost = DBUtil.getBlogpostById(postId, true);
//        } else {
//            blogpost = DBUtil.getBlogpostByURL(postId, true);
//        }
//
//        if (blogpost != null) {
//            if (isID && blogpost.getUrl() != null) {
//                String newURL = WebsiteActions.BLOGPOST.getActionURL(request, response,
//                        new String[] { blogpost.getShareableURL() });
//                WebsitePages.setRedirectUrlInRequest(request, newURL);
//                return WebsitePages.REDIRECT_URL;
//            } else {
//                addPostInRequest(request, blogpost);
//                return WebsitePages.BLOGPOST_PAGE;
//            }
//        }
//
//        return WebsitePages.HOME_PAGE;
//
//    }

    private static void addPostInRequest(CustomSecurityWrapperRequest request, Blogpost blogpost) {
        request.setAttribute(BLOGPOST_ATTR, blogpost);

    }

//    public static WebsitePages getCustomPage(CustomSecurityWrapperRequest request,
//            CustomSecurityWrapperResponse response, String[] queryParams, boolean isPost) throws WebsiteException {
//        if (queryParams == null || queryParams.length == 0) {
//            throw new WebsiteException(WebsiteExceptionType.INVALID_ACCESS);
//        }
//
//        String pageId = queryParams[0];
//        boolean isID = true;
//
//        try {
//            Integer.parseInt(pageId);
//        } catch (NumberFormatException nfe) {
//            isID = false;
//        }
//
//        PageResource page = null;
//
//        if (isID) {
//            page = DBUtil.getPageById(pageId);
//        } else {
//            page = DBUtil.getPageByURL(pageId);
//        }
//
//        if (page != null) {
//            if (isID && page.getUrl() != null) {
//                String newURL = WebsiteActions.PAGE.getActionURL(request, response,
//                        new String[] { page.getShareableURL() });
//                WebsitePages.setRedirectUrlInRequest(request, newURL);
//                return WebsitePages.REDIRECT_URL;
//            } else {
//                addPageInRequest(request, page);
//                return WebsitePages.CUSTOM_PAGE;
//            }
//        }
//
//        return WebsitePages.HOME_PAGE;
//
//    }

    private static void addPageInRequest(CustomSecurityWrapperRequest request, PageResource page) {
        request.setAttribute(PAGE_ATTR, page);

    }

    public static PageResource getPageFromRequest(CustomSecurityWrapperRequest request) {
        Object o = request.getAttribute(PAGE_ATTR);
        if (o != null && o instanceof PageResource) {
            return (PageResource) o;
        }
        return null;
    }

//    public static WebsitePages getFormDisplayPage(CustomSecurityWrapperRequest request,
//            CustomSecurityWrapperResponse response, String[] queryParams, boolean isPost) throws WebsiteException {
//        if (queryParams == null || queryParams.length == 0) {
//            throw new WebsiteException(WebsiteExceptionType.INVALID_ACCESS);
//        }
//
//        String formId = queryParams[0];
//        boolean isID = true;
//
//        try {
//            Integer.parseInt(formId);
//        } catch (NumberFormatException nfe) {
//            isID = false;
//        }
//
//        Form form = null;
//
//        form = DBUtil.getFormById(formId, isID);
//
//        if (form != null) {
//            if (isID && form.getUrl() != null) {
//
//                // forwarding to text-url based link
//                String newURL = WebsiteActions.FORM.getActionURL(request, response, new String[] { form.getUrl() });
//                WebsitePages.setRedirectUrlInRequest(request, newURL);
//                return WebsitePages.REDIRECT_URL;
//            } else {
//
//                com.google.gson.JsonObject formResources = new com.google.gson.JsonObject();
//                formResources.add("fieldTypes", ElementType.getFormFieldTypesAsJsonArray());
//                formResources.add("form", form.convertToJson());
//                addFormInRequest(request, formResources.toString());
//                return WebsitePages.FORM_PAGE;
//            }
//        }
//
//        return WebsitePages.HOME_PAGE;
//
//    }

    private static void addFormInRequest(CustomSecurityWrapperRequest request, String formPageResource) {
        request.setAttribute(FORM_ATTR, formPageResource);

    }

    public static String getFormFromRequest(CustomSecurityWrapperRequest request) {
        Object o = request.getAttribute(FORM_ATTR);
        if (o != null) {
            return (String) o;
        }

        return null;
    }

    public static WebsitePages saveFormResponse(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, boolean isPost) {
        JsonObject responseObject = new JsonObject();

        if (queryParams != null && queryParams.length > 0 && "saveResponse".equals(queryParams[0])) {
            JsonElement requestJsonElement = JsonUtility.getJsonDataFromRequest(request);

            if (requestJsonElement == null || !(requestJsonElement instanceof JsonObject)) {
                responseObject.addProperty("isSuccess", false);
            } else {
                try {
                    JsonObject requestJson = (JsonObject) requestJsonElement;

                    JsonElement idElement = requestJson.get("id");

                    if (idElement == null) {
                        throw new RuntimeException("invalid form data");
                    }

                    Form form = new Form(requestJson, true);
                    boolean isResponseRecorded = DBUtil.saveFormResponse(form);
                    if (isResponseRecorded) {
                        responseObject.addProperty("isSuccess", true);
                        EmailMessage message = form.createResponseEmail();
                        EmailSender.sendEmail(message);
                    } else {
                        responseObject.addProperty("isSuccess", false);
                    }
                } catch (Exception e) {
                    s_logger.error("Error while saving form", e);
                    responseObject.addProperty("isSuccess", false);
                }
            }
        }

        try {
            response.setContentType("application/json");
            response.getWriter().write(responseObject.toString());
        } catch (Exception e) {
            s_logger.error("Could not send ajax response", e);
        }
        return WebsitePages.NO_PAGE;
    }
}
