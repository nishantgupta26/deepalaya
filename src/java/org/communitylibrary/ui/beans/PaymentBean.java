package org.communitylibrary.ui.beans;

import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

import org.apache.log4j.Logger;
import org.communitylibrary.data.entity.Address;
import org.communitylibrary.data.payments.GatewayName;
import org.communitylibrary.data.payments.PaymentUnit;
import org.communitylibrary.data.payments.PledgeType;
import org.communitylibrary.data.payments.entity.Donor;
import org.communitylibrary.ui.beans.MessageBean.MessageType;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.navigation.WebsitePages;
import org.communitylibrary.ui.payment.util.PaymentHelper;
import org.communitylibrary.ui.payment.util.ResponseObject;
import org.communitylibrary.ui.security.CustomSecurityRequestType;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.PaymentConfigStore;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonObject;

/**
 * @author nishant
 *
 */
public class PaymentBean {

	private static final String FORM_RESOURCES = "FORM_RESOURCES";
	private static final String PAYMENT_RESPONSE = "PAYMENT_RESPONSE";

	private static Logger s_logger = Logger.getLogger(PaymentBean.class);

//	public String populatePaymentInfo(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//			PaymentUnit paymentUnit) throws Exception {
//
//		String pgType = PaymentConfigStore.getStringValue("PG_TYPE", String.valueOf(GatewayName.PayUMoney.getCode()));
//		GatewayName gateway = GatewayName.getGateway(pgType);
//
//		PaymentHelper paymentHelper = PaymentHelper.getHelper(gateway);
//
//		ResponseObject paymentRequestResponse = paymentHelper.generatePaymentHandlingObject(paymentUnit, request,
//				response);
//
//		if (paymentRequestResponse != null) {
//			String htmlResponse = paymentRequestResponse.getResponseContent();
//			if (htmlResponse != null) {
//				PrintWriter writer = response.getWriter();
//				writer.println(htmlResponse);
//				return null;
//			} else {
//				if (paymentRequestResponse.getUrl() != null) {
//					return paymentRequestResponse.getUrl();
//				}
//			}
//
//		} else {
//			throw new RuntimeException("Not a valid payment handling");
//		}
//		return null;
//
//	}

//	public WebsitePages getPaymentsPage(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//			String[] queryParams) throws Exception {
//		if (queryParams == null || queryParams.length == 0) {
//			JsonObject formResources = new JsonObject();
//			formResources.add("pledgeTypes", PledgeType.getPledgeTypesAsJsonArray());
//			addFormResourcesToRequest(request, formResources);
//			return WebsitePages.DONATION_PAGE;
//		}
//
//		if (queryParams.length > 0) {
//			if ("pay".equals(queryParams[0])) {
//
//				PaymentUnit paymentUnit = new PaymentUnit();
//
//				Donor donor = new Donor();
//				paymentUnit.setDonor(donor);
//
//				String pledgeTypeCode = StringUtility.trimAndEmptyIsNull(request.getParameter("Pledge type",
//						"pledgetypecode", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//				PledgeType pledgeType = PledgeType.getPledgeTypeFromCode(pledgeTypeCode);
//				donor.setSubscriptionType(pledgeType);
//
//				String amount = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("Amount to be donated", "amount", CustomSecurityRequestType.NUMBER));
//				paymentUnit.setAmount(amount);
//
//				paymentUnit.setTransactionId(NumberUtility.generateIdWithTime("TCLP"));
//
//				String firstName = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("first name", "fname", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//				donor.setFirstName(firstName);
//
//				String lastName = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("last name", "lname", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//				donor.setLastName(lastName);
//
//				String email = StringUtility
//						.trimAndEmptyIsNull(request.getParameter("Email id", "email", CustomSecurityRequestType.EMAIL));
//				donor.setEmail(email);
//
//				String phoneNumber = StringUtility.trimAndEmptyIsNull(request.getParameter(
//						"Phone number of the donator", "phone", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//				donor.setPhone(phoneNumber);
//
//				boolean isRequire80GReceipt = "on".equalsIgnoreCase(request.getParameter("Need 80G receipt", "receipt",
//						CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//				paymentUnit.setRequire80GReceipt(isRequire80GReceipt);
//
//				String panNumber = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("Pan number", "pannum", CustomSecurityRequestType.ALPHANUMERIC));
//				donor.setPanNumber(panNumber);
//
//				String extraNotes = StringUtility.trimAndEmptyIsNull(request.getParameter("extra comments",
//						"extracomments", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//				paymentUnit.setExtraNotes(extraNotes);
//
//				String addressLine1 = StringUtility.trimAndEmptyIsNull(request.getParameter("address line 1",
//						"addressline1", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//
//				String addressLine2 = StringUtility.trimAndEmptyIsNull(request.getParameter("address line 2",
//						"addressline2", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//
//				String city = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("city", "city", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//
//				String state = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("state", "state", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//
//				String country = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("country", "country", CustomSecurityRequestType.DEFAULT_SAFE_STRING));
//
//				String zipCode = StringUtility.trimAndEmptyIsNull(
//						request.getParameter("zipcode", "zipcode", CustomSecurityRequestType.INTEGER));
//
//				if (addressLine1 != null || addressLine2 != null || city != null || state != null || country != null
//						|| zipCode != null) {
//					Address address = new Address();
//					address.setAddressLine1(addressLine1);
//					address.setAddressLine2(addressLine2);
//					address.setCity(city);
//					address.setState(state);
//					address.setCountry(country);
//					address.setZipCode(zipCode);
//					donor.setAddress(address);
//				}
//
//				if (!paymentUnit.validate()) {
//					paymentUnit = null;
//					MessageBean.setMessage(request, MessageType.MSG_ERROR, "Problem with the information provided");
//					JsonObject formResources = new JsonObject();
//					formResources.add("pledgeTypes", PledgeType.getPledgeTypesAsJsonArray());
//					addFormResourcesToRequest(request, formResources);
//					return WebsitePages.DONATION_PAGE;
//				}
//
//				String pgType = PaymentConfigStore.getStringValue("PG_TYPE",
//						String.valueOf(GatewayName.PayUMoney.getCode()));
//				GatewayName gateway = GatewayName.getGateway(pgType);
//				paymentUnit.setPgType(gateway);
//				boolean isSaved = DBUtil.savePaymentUnit(paymentUnit, request.getRemoteAddr());
//
//				if (!isSaved) {
//					MessageBean.setMessage(request, MessageType.MSG_ERROR,
//							"Could not service your request. Please contact the Administrator");
//					JsonObject formResources = new JsonObject();
//					formResources.add("pledgeTypes", PledgeType.getPledgeTypesAsJsonArray());
//					addFormResourcesToRequest(request, formResources);
//					return WebsitePages.DONATION_PAGE;
//				}
//				try {
//					String url = populatePaymentInfo(request, response, paymentUnit);
//					if (url == null) {
//						return WebsitePages.NO_PAGE;
//					} else {
//						WebsitePages.setRedirectUrlInRequest(request, url);
//						return WebsitePages.REDIRECT_URL;
//					}
//				} catch (Exception e) {
//					s_logger.error("Error while making payment", e);
//					MessageBean.setMessage(request, MessageType.MSG_ERROR,
//							"Could not service your request. Please contact the Administrator");
//					JsonObject formResources = new JsonObject();
//					formResources.add("pledgeTypes", PledgeType.getPledgeTypesAsJsonArray());
//					addFormResourcesToRequest(request, formResources);
//					return WebsitePages.DONATION_PAGE;
//				}
//
//			}
//		}
//
//		return null;
//	}

	private void addPaymentResponseInRequest(CustomSecurityWrapperRequest request, JsonObject paymentResponse) {
		request.setAttribute(PAYMENT_RESPONSE, paymentResponse.toString());
	}

	private void addFormResourcesToRequest(CustomSecurityWrapperRequest request, JsonObject formResources) {
		if (formResources == null) {
			return;
		}

		request.setAttribute(FORM_RESOURCES, formResources.toString());
	}

	public static String getFormResourcesFromRequest(CustomSecurityWrapperRequest request) {
		Object formResources = request.getAttribute(FORM_RESOURCES);
		if (formResources != null) {
			return (String) formResources;
		}

		return null;
	}

	public static String getPaymentResponseFromRequest(CustomSecurityWrapperRequest request) {
		Object paymentStatus = request.getAttribute(PAYMENT_RESPONSE);
		if (paymentStatus != null) {
			return (String) paymentStatus;
		}

		return null;
	}

//	public WebsitePages getPaymentStatusPage(CustomSecurityWrapperRequest request,
//			CustomSecurityWrapperResponse response, String[] queryParams) {
//
//		if (queryParams.length > 0) {
//			if ("status".equals(queryParams[0])) {
//				try {
//
//					String pgType = PaymentConfigStore.getStringValue("PG_TYPE",
//							String.valueOf(GatewayName.PayUMoney.getCode()));
//					GatewayName gateway = GatewayName.getGateway(pgType);
//					PaymentHelper helper = PaymentHelper.getHelper(gateway);
//
//					// String transactionId =
//					// StringUtility.trimAndEmptyIsNull(request.getParameter("txnid"));
//
//					JsonObject paymentResponse = helper.handleGatewayResponse(request, queryParams);
//					if (paymentResponse == null) {
//						paymentResponse = new JsonObject();
//						paymentResponse.addProperty("isSuccess", false);
//					} else {
//						// if (transactionId != null) {
//						// boolean isAdd = false;
//						// if (paymentResponse.get("transactionDetails") ==
//						// null) {
//						// paymentResponse.add("transactionDetails", new
//						// JsonObject());
//						// isAdd = true;
//						// } else {
//						// JsonObject transactionElem =
//						// paymentResponse.get("transactionDetails")
//						// .getAsJsonObject();
//						// if (transactionElem.get(transactionId) == null) {
//						// isAdd = true;
//						// }
//						// }
//						//
//						// if (isAdd) {
//						// JsonObject transactionElement =
//						// paymentResponse.get("transactionDetails")
//						// .getAsJsonObject();
//						// transactionElement.addProperty("transactionId",
//						// transactionId);
//						// }
//						// }
//
//					}
//
//					addPaymentResponseInRequest(request, paymentResponse);
//
//				} catch (Exception e) {
//					s_logger.error("Error in obtaining status from the payment gateway", e);
//					s_logger.info("Response obtained from Gateway:");
//					Enumeration<?> names = request.getParameterNames();
//					while (names.hasMoreElements()) {
//						Object name = names.nextElement();
//						String value = request.getParameter(name.toString());
//						s_logger.info("paramName: " + name + ", value: " + value);
//					}
//				}
//				return WebsitePages.PaymentsStatusPage;
//			}
//		}
//
//		JsonObject formResources = new JsonObject();
//		formResources.add("pledgeTypes", PledgeType.getPledgeTypesAsJsonArray());
//		addFormResourcesToRequest(request, formResources);
//		return WebsitePages.DONATION_PAGE;
//
//	}

	public static String convertToCSV(List<PaymentUnit> units) {
		String header = "S.No.,Donation Date,Donor Name,Address, Email id, Collecting Trustee, Paid For Project / Purpose, "
				+ "Mode, Transaction / Cheque  No., Net Receipt, Receipt No, Receipt Date, PAN No";
		String newLineString = StringUtility.getNewLineString();
		String comma = ",";

		StringBuilder content = new StringBuilder(header);
		content.append(newLineString);
		int i = 1;
		for (PaymentUnit unit : units) {
			content.append(i).append(comma);
			content.append(DateUtility.getDateInDDMMMYYDashSeparator(unit.getCreationDate())).append(comma);
			content.append(unit.getName()).append(comma);
			Address add = unit.getDonor().getAddress();
			content.append(add == null ? StringUtility.EMPTY_STRING : add.serialize()).append(comma);
			content.append(StringUtility.trimAndNullIsEmpty(unit.getDonor().getEmail())).append(comma);
			content.append(StringUtility.EMPTY_STRING).append(comma);
			content.append(StringUtility.EMPTY_STRING).append(comma);
			content.append(unit.getPaymentType().name()).append(comma);
			content.append(unit.getTransactionId()).append(comma);
			content.append("INR ").append(unit.getAmount()).append(comma);
			content.append(StringUtility.trimAndNullIsEmpty(unit.getReceiptNumber())).append(comma);

			// content.append(StringUtility.EMPTY_STRING).append(comma);
			content.append(
					unit.getReceiptNumber() != null ? DateUtility.getDateInDDMMMYYDashSeparator(unit.getReceiptDate())
							: StringUtility.EMPTY_STRING)
					.append(comma);

			content.append(StringUtility.trimAndNullIsEmpty(unit.getDonor().getPanNumber())).append(comma);

			content.append(newLineString);
			i++;
		}
		return content.toString();
	}
}
