package org.communitylibrary.ui.beans;

import java.util.List;

import org.apache.log4j.Logger;
import org.communitylibrary.data.books.Book;
import org.communitylibrary.data.books.BookAccount;
import org.communitylibrary.data.books.util.BookLoader;
import org.communitylibrary.data.books.util.BookLoaderAgent;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.ui.navigation.WebsitePages;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.JsonUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.StringUtility;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author nishant
 *
 */
public class BooksBean {

    // private static final String BOOK = "BOOK";
    private static final String NUM_BOOKS = "NUM_BOOKS";

    private static Logger       s_logger  = Logger.getLogger(BooksBean.class);

    public static WebsitePages getBooksPage(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams) {

        if (queryParams != null && queryParams.length > 0) {
            JsonObject responseObject = new JsonObject();
            boolean isSuccess = false;
            if ("fetch".equalsIgnoreCase(queryParams[0])) {
                JsonElement jsonDataFromRequest = JsonUtility.getJsonDataFromRequest(request);
                if (jsonDataFromRequest != null) {
                    JsonObject requestData = (JsonObject) jsonDataFromRequest;
                    JsonElement pageNumberElement = requestData.get("page");
                    JsonElement numBooksElement = requestData.get("num");
                    if (pageNumberElement != null && numBooksElement != null) {
                        String pageNumber = pageNumberElement.getAsString();
                        String numberBooks = numBooksElement.getAsString();

                        JsonElement bookFilterElement = requestData.get("filter");
                        Book filterObject = null;
                        if (bookFilterElement != null) {
                            // Adding filters for book
                            JsonObject bookFilter = bookFilterElement.getAsJsonObject();
                            BookAccount account = null;
                            filterObject = new Book();
                            JsonElement accountFilter = bookFilter.get("bookAccount");
                            if (accountFilter != null) {
                                account = BookAccount.getBookAccountByCode(accountFilter.getAsString());
                                filterObject.setBookType(account);
                            }

                            JsonElement titleElement = bookFilter.get("title");
                            if (titleElement != null) {
                                filterObject.setTitle(StringUtility.trimAndEmptyIsNull(titleElement.getAsString()));
                            }

                            JsonElement authorElement = bookFilter.get("author");
                            if (authorElement != null) {
                                filterObject.addAuthor(StringUtility.trimAndEmptyIsNull(authorElement.getAsString()));
                            }

                            JsonElement isbnElement = bookFilter.get("isbn");
                            if (isbnElement != null) {
                                filterObject.setIsbn(StringUtility.trimAndEmptyIsNull(isbnElement.getAsString()));
                            }
                        }

                        List<Book> books = DBUtil.getBooks(pageNumber, numberBooks, filterObject);
                        if (books != null) {
                            JsonArray array = Book.convertToListJson(books);
                            if (array != null) {
                                isSuccess = true;
                                responseObject.add("data", array);
                            }

                            if (filterObject != null) {
                                int totalBooks = DBUtil.getBooksCount(filterObject);
                                responseObject.addProperty("totalBooks", totalBooks);
                            }
                        }

                    }
                }

            } else if ("book".equals(queryParams[0])) {
                if (queryParams.length > 1 && "fetch".equals(queryParams[1])) {
                    JsonElement requestElem = JsonUtility.getJsonDataFromRequest(request);
                    String id = null;
                    String isbn10 = null;
                    String isbn13 = null;
                    if (requestElem != null) {
                        JsonObject requestObject = requestElem.getAsJsonObject();
                        JsonElement idElement = requestObject.get("id");
                        JsonElement isbn10Element = requestObject.get("isbn10");
                        JsonElement isbn13Element = requestObject.get("isbn13");
                        if (idElement != null) {
                            id = idElement.getAsString();
                        }

                        if (isbn10Element != null) {
                            isbn10 = isbn10Element.getAsString();
                        }

                        if (isbn13Element != null) {
                            isbn13 = isbn13Element.getAsString();
                        }

                    }
                    Book book = new Book();
                    if (id != null) {
                        book.setId(NumberUtility.parsetIntWithDefaultOnErr(id, 0));
                    }

                    book.setIsbn10(isbn10);
                    book.setIsbn13(isbn13);

                    book = DBUtil.getBook(book, true);
                    if (book != null) {
                        // Check if loaded through external api
                        if (!book.isLoadedThroughAPI()) {
                            try {
                                BookLoader bookLoader = BookLoaderAgent.Google.loaderInstance(book);
                                if (bookLoader != null) {
                                    boolean isLoaded = bookLoader.populateBookDetailsFromAPI();
                                    if (isLoaded) {
                                        boolean isBookUpdated = DBUtil.updateBook(book);
                                        if (isBookUpdated) {
                                            s_logger.info("Book info loaded through api and updated in DB");
                                        } else {
                                            s_logger.info("Book info loaded could not be updated in DB");
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                s_logger.error("Error in getting books info through external loader api", e);
                            }
                        }

                        JsonObject jsonConvertedBook = book.convertToJsonObject(true);
                        responseObject.add("book", jsonConvertedBook);
                        isSuccess = true;
                    }
                } else {
                    return WebsitePages.BookDetailsPage;
                }
            }
            responseObject.addProperty("isSuccess", isSuccess);
            response.setContentType("application/json");
            try {
                response.getWriter().write(responseObject.toString());
            } catch (Exception e) {
                s_logger.error("Error while writing the response", e);
            }
            return WebsitePages.NO_PAGE;
        }

        String numBooks = DBUtil.getTotalBooks();
        setTotalBooks(request, numBooks);

        return WebsitePages.BooksListingPage;
    }

    // private static void setBookInRequest(CustomSecurityWrapperRequest
    // request, Book book) {
    // request.setAttribute(BOOK, book);
    // }
    //
    // public static Book getBook(CustomSecurityWrapperRequest request) {
    // Object book = request.getAttribute(BOOK);
    // if (book != null) {
    // return (Book) book;
    // }
    //
    // return null;
    // }

    private static void setTotalBooks(CustomSecurityWrapperRequest request, String numBooks) {
        request.setAttribute(NUM_BOOKS, numBooks);
    }

    public static String getTotalBooks(CustomSecurityWrapperRequest request) {
        Object numBooks = request.getAttribute(NUM_BOOKS);
        if (numBooks != null) {
            return (String) numBooks;
        }

        return null;
    }

}
