package org.communitylibrary.ui.resources;

/**
 * @author nishant.gupta
 *
 */
public class StaticFileVersions {
    public static final String CSS_VERSION = "20191201";
    public static final String JS_VERSION  = "20191201";
}
