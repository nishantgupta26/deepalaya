package org.communitylibrary.ui.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import org.communitylibrary.data.user.User;
import org.communitylibrary.data.user.UserRoles;
import org.communitylibrary.logger.Logger;
import org.communitylibrary.ui.exception.AccessException;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.StringUtility;

public class SessionStore {
    private static final Logger s_logger        = Logger.getInstance(SessionStore.class);

    // DO NOT CHANGE WITHOUT ISO APPROVAL
    private static final int    SESSION_TIMEOUT = 60 * 60 * 10;

    public static void sessionListenerDestroy(HttpSession session) {
        try {
            User rootLoggedInUser = null;

            // Check logged in user
            if (rootLoggedInUser == null) {
                rootLoggedInUser = (User) session.getAttribute(SessionObject.LOGGED_IN_USER.name());
            }

            // Logout user
            // if (rootLoggedInUser != null && rootLoggedInUser.getUserId() > 0)
            // {
            // UserLogin.storeLogout(null, rootLoggedInUser);
            // }

        } catch (Exception e) {
            s_logger.error("Error in logging out user due to session listener", e);
        }
    }

    public static void setSessionVariblesToDefault(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response) {
        HttpSession session = getSession(request);
        // Get Attribs to Persist
        for (SessionObject sO : SessionObject.values()) {
            if (sO.isPersistAcrossInvalidation()) {
                Object val = session.getAttribute(sO.name());
                if (val != null) {
                    session.setAttribute(sO.name(), null);
                }
            }
        }
    }

    public static Map<SessionObject, Object> getAttributesToPersist(HttpSession session) {
        // Get Attribs to Persist
        Map<SessionObject, Object> persistedAttribs = new HashMap<SessionStore.SessionObject, Object>(1);
        for (SessionObject sO : SessionObject.values()) {
            if (sO.isPersistAcrossInvalidation()) {
                Object val = session.getAttribute(sO.name());
                if (val != null) {
                    persistedAttribs.put(sO, val);
                }
            }
        }
        return persistedAttribs;
    }

    public static void setAttributesToPersist(CustomSecurityWrapperRequest request,
            Map<SessionObject, Object> persistedAttribs) {
        // Set Attributes to Persist
        for (SessionObject sO : persistedAttribs.keySet()) {
            Serializable val = (Serializable) persistedAttribs.get(sO);
            setSessionVariable(request, sO, val);
        }
    }

    public static void invalidateSession(CustomSecurityWrapperRequest request) {
        HttpSession session = getSession(request);
        Map<SessionObject, Object> persistedAttribs = getAttributesToPersist(session);
        session.invalidate();

        // Set Attribs to Persist
        session = getSession(request);
        setAttributesToPersist(request, persistedAttribs);
    }

    public static String getSessionId(CustomSecurityWrapperRequest request) {
        HttpSession session = request.getSession(false);
        return (session == null) ? null : session.getId();
    }

    public static User getLoggedInUser(CustomSecurityWrapperRequest request) {
        return (User) getSessionVariable(request, SessionObject.LOGGED_IN_USER);
    }

    /**
     * It is very important to make sure that the existing session is
     * invalidated using SessionStore.invalidateSession function before new
     * login user is set. This is required to prevent session-id implanting by
     * hackers.
     * 
     * @param request
     * @param response
     * @param user
     */
    public static void setLoggedInUser(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            User user) {
        setSessionVariable(request, SessionObject.LOGGED_IN_USER, user);
        setNewRandomCsrfTokens(request, response);
    }

    public static void setLastUserroleCookie(CustomSecurityWrapperResponse response, UserRoles ur) {
        Cookie cookie = new Cookie("UR", ur.serialize());
        cookie.setPath("/");
        cookie.setSecure(true);
        cookie.setMaxAge(DateUtility.APPROX_SECONDS_IN_YEAR * 3);
        response.addCookie(cookie);
    }

    public static UserRoles getLastUserroleValue(CustomSecurityWrapperRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if ("UR".equals(c.getName())) {
                    return UserRoles.deserialize(c.getValue());
                }
            }
        }
        return null;
    }

    private static Serializable getSessionVariable(CustomSecurityWrapperRequest request, SessionObject key) {
        HttpSession session = getSession(request);
        return (Serializable) (session == null ? null : session.getAttribute(key.name()));
    }

    private static void setSessionVariable(CustomSecurityWrapperRequest request, SessionObject key,
            Serializable value) {
        HttpSession session = getSession(request);
        session.setAttribute(key.name(), value);
    }

    public static void setCurrentUITab(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String uiTab) {
        setSessionVariable(request, SessionObject.CURRENT_UI_TAB, uiTab);
    }

    public static String getCurrentUITab(CustomSecurityWrapperRequest request) {
        return (String) getSessionVariable(request, SessionObject.CURRENT_UI_TAB);
    }

    public static void setNewRandomCsrfTokens(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response) {
        setNewRandomCsrfUrlToken(request, response);
        setNewRandomCsrfPostToken(request, response);
    }

    private static void setNewRandomCsrfUrlToken(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response) {
        setSessionVariable(request, SessionObject.CSRF_URL_TOKEN, User.generateRandomCSRFToken(30));
    }

    public static String getCsrfUrlToken(CustomSecurityWrapperRequest request) {
        return (String) getSessionVariable(request, SessionObject.CSRF_URL_TOKEN);
    }

    public static boolean compareCsrfUrlToken(CustomSecurityWrapperRequest request, String compareWith)
            throws AccessException {
        String token = getCsrfUrlToken(request);
        if (token != null && !StringUtility.equalsIgnoreCaseWithTrimAndBothNullCheck(token, compareWith)) {
            return false;
        }
        return true;
    }

    private static void setNewRandomCsrfPostToken(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response) {
        setSessionVariable(request, SessionObject.CSRF_POST_TOKEN, User.generateRandomCSRFToken(100));
    }

    public static String getCsrfPostToken(CustomSecurityWrapperRequest request) {
        return (String) getSessionVariable(request, SessionObject.CSRF_POST_TOKEN);
    }

    public static boolean compareCsrfPostToken(CustomSecurityWrapperRequest request, String compareWith)
            throws AccessException {
        String token = getCsrfPostToken(request);
        if (token != null && !StringUtility.equalsIgnoreCaseWithTrimAndBothNullCheck(token, compareWith)) {
            return false;
        }
        return true;
    }

    private static HttpSession getSession(CustomSecurityWrapperRequest request) {
        int sessionTimeout = SESSION_TIMEOUT;
        HttpSession session = request.getSession(true);
        if (session == null) {
            return null;
        }
        session.setMaxInactiveInterval(sessionTimeout);
        return session;
    }

    private SessionStore() {
    }

    private static enum SessionObject {
        LOGGED_IN_USER(false),

        CURRENT_UI_TAB(false),

        CSRF_URL_TOKEN(false),

        CSRF_POST_TOKEN(false),

        LANGUAGE(false),

        COUNTRY(false),

        ;

        private boolean m_presistAcrossInvalidation = false;

        SessionObject(boolean persist) {
            m_presistAcrossInvalidation = persist;
        }

        public boolean isPersistAcrossInvalidation() {
            return m_presistAcrossInvalidation;
        }
    }
}
