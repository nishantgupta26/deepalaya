package org.communitylibrary.ui.exception;

public class AccessException extends Exception {
    private static final long serialVersionUID = -7547497394676652691L;

    public AccessException(AccessExceptionType typ) {
        this(typ, null);
    }

    public AccessException(AccessExceptionType typ, String msg) {

    }
}
