package org.communitylibrary.ui.exception;

public enum AccessExceptionType {
    HTTPS_REQUIRED,

    CSRF_POST_VERIFICATION_FAILED,

    CSRF_URL_VERIFICATION_FAILED,

    PAGE_MISSING,

    INPUT_VALIDATION_FAILED,

    USER_UNAUTHORIZED,

    ;

}
