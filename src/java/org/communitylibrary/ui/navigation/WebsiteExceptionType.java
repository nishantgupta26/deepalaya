package org.communitylibrary.ui.navigation;

/**
 * @author Nishant
 * 
 */
public enum WebsiteExceptionType {
    DUPLICATE_CONTACT_REQUEST("The request for this query has already been received by the system."),

    INVALID_QUERY("Invalid query"),

    CAPTCHA_FAIL("Could not verify the captcha information. Please try after sometime."),

    OTHER("All other unaccounted errors"),

    NEWSLETTER_SUBSCRIBED("We have detected an existing subscription against the email id."),

    ACTIVE_REQUEST_PRESENT("We have detected an existing active request. Please discontinue that first."),

    INVALID_ACCESS("The link is not directly accessible"),

    USER_AUTH_FAILED("Invalid User account or password!"),

    USER_DISABLED("User account is disabled!"),

    USER_LOGIN_INFO("User Login info could not be added!"),

    CANNOTRETURNPAGE("This action should not be returning an html page"),

    INVALID_DATA("The request data is invalid"),

    PG_ERROR("Payment Gateway error"), ;

    private String m_desc;

    WebsiteExceptionType(String desc) {
        m_desc = desc;
    }

    public String getDesc() {
        return m_desc;
    }

    public void setDesc(String desc) {
        m_desc = desc;
    }

}
