package org.communitylibrary.ui.navigation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.communitylibrary.admin.entity.blog.Blogpost;
import org.communitylibrary.admin.entity.forms.EmailMessage;
import org.communitylibrary.admin.entity.forms.Form;
import org.communitylibrary.admin.entity.forms.FormOption;
import org.communitylibrary.admin.entity.forms.FormResponse;
import org.communitylibrary.admin.entity.forms.Formfield;
import org.communitylibrary.admin.entity.page.PageResource;
import org.communitylibrary.app.config.ElementType;
import org.communitylibrary.app.resources.ResourceObject;
import org.communitylibrary.app.resources.ResourceType;
import org.communitylibrary.data.ContactDTO;
import org.communitylibrary.data.LibraryBranch;
import org.communitylibrary.data.books.Book;
import org.communitylibrary.data.common.Phone;
import org.communitylibrary.data.digitalaccess.Slot;
import org.communitylibrary.data.digitalaccess.SlotStatus;
import org.communitylibrary.data.entity.EventData;
import org.communitylibrary.data.members.LibraryGroupForDigitalAccess;
import org.communitylibrary.data.members.LibraryGroups;
import org.communitylibrary.data.members.LibraryMember;
import org.communitylibrary.data.payments.PaymentUnit;
import org.communitylibrary.data.payments.entity.Donor;
import org.communitylibrary.data.resources.HomePageLink;
import org.communitylibrary.data.user.User;
import org.communitylibrary.db.WebsiteDBConnector;
import org.communitylibrary.ui.beans.RelationDTO;
import org.communitylibrary.ui.util.IntegerUtility;
import org.communitylibrary.ui.view.dto.ViewMemberDTO;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.StringUtility;

/**
 * @author Nishant
 */
public class DBUtil {
    private static final String SQL_INSERT_NEWSLETTERSUBSCRIPTION = "insert into newsletter_subscription(email, client_ip, gen_ts) values (?,?, NOW())";
    private static final String SQL_INSERT_CONTACT_REQUEST = "insert into contact_request(email, name, phone, subject, message, client_ip, gen_ts) values (?,?,?,?,?,?, NOW())";
    private static final String SQL_SELECT_ALL_LIBRARY_BRANCHES = "select * from librarybranch";
    private static final String SQL_INSERT_SLOT_TYPE = "insert into slottype(slot_type, gen_ts) values (?,NOW())";
    private static final String SQL_DELETE_SLOT_TYPE = "delete from slottype where slot_type=?";
    private static final String SQL_SELECT_ALL_SLOT_TYPES = "select * from slottype";
    private static Logger s_logger = Logger.getLogger(DBUtil.class);
    private static WebsiteDBConnector s_staticInstance = WebsiteDBConnector.getStaticInstance();

    public static boolean addEmailToSubscriptionList(String email, String ipAddress) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        boolean isAlreadyExists = checkIfAlreadyExists(email);
        if (!isAlreadyExists) {
            try {
                conn = s_staticInstance.getConnection(false);
                stmt = conn.prepareStatement(SQL_INSERT_NEWSLETTERSUBSCRIPTION);
                s_staticInstance.setString(stmt, 1, email, 50);
                s_staticInstance.setString(stmt, 2, ipAddress, 15);
                s_logger.debug("Query to add the user to the newsletter email list:" + stmt);
                stmt.execute();
                conn.commit();
                return true;

            } catch (Exception e) {
                s_logger.error("Error while adding people to subscription list of the newsletter", e);
                throw new WebsiteException(WebsiteExceptionType.OTHER);
            } finally {
                s_staticInstance.closeAll(conn, stmt, null, true);
            }

        } else {
            throw new WebsiteException(WebsiteExceptionType.NEWSLETTER_SUBSCRIBED);
        }
    }

    private static boolean checkIfAlreadyExists(String email) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement("select * from newsletter_subscription where email=? limit 1");
            s_staticInstance.setString(stmt, 1, email);
            rs = stmt.executeQuery();
            if (rs.isBeforeFirst()) {
                return true;
            }
            return false;

        } catch (Exception e) {
            s_logger.error("Error while checking if email already exists", e);

        } finally {
            s_staticInstance.closeAll(conn, stmt, rs, true);
        }
        return true;

    }

    public static void addContactRequest(String email, String name, String phone, String subject, String contactMessage,
                                         String ipAddress) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        Date generatedDate = checkIfRequestAlreadyExists(email);

        if (generatedDate == null) {
            try {
                conn = s_staticInstance.getConnection(false);
                stmt = conn.prepareStatement(SQL_INSERT_CONTACT_REQUEST);
                int i = 1;
                s_staticInstance.setString(stmt, i++, email, 50);
                s_staticInstance.setString(stmt, i++, name, 50);
                s_staticInstance.setString(stmt, i++, phone, 20);
                s_staticInstance.setString(stmt, i++, subject, 20);
                s_staticInstance.setString(stmt, i++, contactMessage, 1024 * 1024);
                s_staticInstance.setString(stmt, i++, ipAddress, 15);
                s_logger.debug("Query to add the contact request:" + stmt);
                stmt.execute();
                conn.commit();
            } catch (Exception e) {
                s_logger.error("Error while adding contact request", e);
                throw e;
            } finally {
                s_staticInstance.closeAll(conn, stmt, null, true);
            }
        } else {
            throw new WebsiteException(WebsiteExceptionType.DUPLICATE_CONTACT_REQUEST, generatedDate);
        }
    }

    /**
     * This api checks whether the same query contact requests have been
     * requested for more than 5 times in the last 48 hours
     *
     */
    private static Date checkIfRequestAlreadyExists(String email) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement("select gen_ts from contact_request where email=? and ack=0");
            s_staticInstance.setString(stmt, 1, email);
            rs = stmt.executeQuery();
            if (rs.isBeforeFirst()) {
                if (rs.next()) {
                    return s_staticInstance.getTimeStampAsDate(rs, "gen_ts");
                }
            }
            return null;
        } catch (Exception e) {
            s_logger.error(
                    "Error while checking if this query has been requested more than 5 times in the last 48 hours", e);
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, rs, true);
        }
    }

    public static void ackContactReq(String email) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement("update contact_request set ack=1 where email=?");
            s_staticInstance.setString(stmt, 1, email);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error(
                    "Error while checking if this query has been requested more than 5 times in the last 48 hours", e);
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static Calendar getLastTimeForDay(Date ticketingDate, boolean isToday) {
        Calendar cal = Calendar.getInstance();
        if (!isToday) {
            cal.setTime(ticketingDate);
        }
        // Subtracting 1 second from the start time of the next day
        cal.add(Calendar.DATE, 1);
        cal = removeTime(false, cal);
        cal.add(Calendar.MILLISECOND, -1);
        return cal;
    }

    public static Date removeTime(Date date, boolean isToday) {
        Calendar cal = Calendar.getInstance();
        if (!isToday) {
            cal.setTime(date);
        }
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Calendar removeTime(boolean isToday, Calendar date) {
        Calendar cal = Calendar.getInstance();
        if (!isToday) {
            cal.setTime(date.getTime());
        }
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public static List<ContactDTO> fetchUnacknowledgedInquries() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<ContactDTO> dtos = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement("select * from contact_request where gen_ts <= NOW() and ack=0");
            rs = stmt.executeQuery();
            if (rs.isBeforeFirst()) {
                dtos = new ArrayList<>();
            }
            while (rs.next()) {
                String emailID = rs.getString("email");
                String name = rs.getString("name");
                String phone = rs.getString("phone");
                String message = rs.getString("message");
                String clientIP = rs.getString("client_ip");
                ContactDTO dto = new ContactDTO(emailID, name, phone, message, clientIP);
                dtos.add(dto);
            }
            return dtos;
        } catch (Exception e) {
            s_logger.error("Error while fetching the contact enquiry list from the database");
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, rs, true);
        }
    }

    public static List<ContactDTO> fetchNewsletterSubscritionAlerts(boolean isFirstRun) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<ContactDTO> dtos = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement("select * from newsletter_subscription where gen_ts <= NOW()"
                    + (!isFirstRun ? " and gen_ts >= ?" : ""));
            if (!isFirstRun) {
                s_staticInstance.setTimestamp(stmt, 1, DateUtility.getPreviousDaysDate(10, 0, 0));
            }
            rs = stmt.executeQuery();
            if (rs.isBeforeFirst()) {
                dtos = new ArrayList<ContactDTO>();
            }
            while (rs.next()) {
                String emailID = rs.getString("email");
                String clientIP = rs.getString("client_ip");
                ContactDTO dto = new ContactDTO(emailID, null, null, null, clientIP);
                dtos.add(dto);
            }
            return dtos;
        } catch (Exception e) {
            s_logger.error("Error while fetching the Newsletter subscription requests list from the database");
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, rs, true);
        }
    }

    public static boolean addPopupContentRequest(String content, String ipAddress, Calendar startDate, Calendar endDate)
            throws WebsiteException {
        Connection conn = null;
        PreparedStatement stmt = null;
        boolean isAlreadyExists = checkIfAnyRequestExists();
        if (!isAlreadyExists) {
            try {
                conn = s_staticInstance.getConnection(false);
                stmt = conn.prepareStatement(
                        "insert into popup_requests(content, client_ip, start_time, end_time, is_active, gen_ts) values (?,?,?,?,?, NOW())");
                s_staticInstance.setString(stmt, 1, content, 102400);
                s_staticInstance.setString(stmt, 2, ipAddress, 15);
                s_staticInstance.setTimestamp(stmt, 3, startDate);
                s_staticInstance.setTimestamp(stmt, 4, endDate);
                s_staticInstance.setString(stmt, 5, "Y");
                s_logger.debug("Query to add the popup content request:" + stmt);
                stmt.execute();
                conn.commit();
                return true;

            } catch (Exception e) {
                s_logger.error("Error while adding people to subscription list of the newsletter", e);
                throw new WebsiteException(WebsiteExceptionType.OTHER);
            } finally {
                s_staticInstance.closeAll(conn, stmt, null, true);
            }

        } else {
            throw new WebsiteException(WebsiteExceptionType.ACTIVE_REQUEST_PRESENT);
        }
    }

    private static boolean checkIfAnyRequestExists() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement("select * from popup_requests where is_active=? limit 1");
            s_staticInstance.setString(stmt, 1, "Y");
            rs = stmt.executeQuery();
            if (rs.isBeforeFirst()) {
                return true;
            }
            return false;

        } catch (Exception e) {
            s_logger.error("Error while checking if any active content already exists", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, rs, true);
        }
        return true;
    }

    public static void approveCurrentlyActivePopupRequest(boolean isApprove) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            if (!isApprove) {
                stmt = conn.prepareStatement("update popup_requests set is_approved = 'N', is_active='N'");
            } else {
                stmt = conn.prepareStatement("update popup_requests set is_approved = 'Y' where is_active='Y'");
            }
            stmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error while checking if any active content already exists", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

    }

    public static String getPopupContent() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement(
                    "select content from popup_requests where is_active='Y' and is_approved='Y' and start_time <= NOW() and end_time >= NOW()");
            rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getString("content");
            }
        } catch (Exception e) {
            s_logger.error("Error while checking if any active content already exists", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, rs, true);
        }
        return null;
    }

    /**
     * This API runs every day, and sets is_active value of the currently active
     * popup_requests to false, so that they do not appear on the home page
     */
    public static void deactivatePopupRequests() {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(
                    "update popup_requests set is_approved = 'N', is_active='N' where start_time > NOW() or end_time < NOW()");
            stmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error while deactivating the active requests", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    private static final String SQL_ADD_MEMBER = "insert ignore into library_members (id,name,dob,age,phone,mother_name,father_name,address,library_group,school,class,gender, branch_code, join_date) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static int addMembers(List<LibraryMember> newMembers) throws Exception {
        PreparedStatement stmt = null;
        Connection conn = null;
        int totalCount = 0;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(SQL_ADD_MEMBER);
            int batchCount = 0;
            for (LibraryMember member : newMembers) {
                addValuesToStatement(member, stmt);
                stmt.addBatch();
                batchCount++;

                if (batchCount == 100) {
                    totalCount += batchCount;
                    batchCount = 0;
                    stmt.executeBatch();
                }
            }

            if (batchCount > 0) {
                totalCount += batchCount;
                batchCount = 0;
                stmt.executeBatch();
            }
            s_logger.debug("query: " + stmt);
            conn.commit();
            // addMapping(conn, oldToNew, relationMap);

        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
        return totalCount;
    }

    private static boolean addMapping(Connection conn, Map<String, String> oldToNew,
                                      Map<String, RelationDTO> relationMap) {
        PreparedStatement stmt = null;
        PreparedStatement stmtFetch = null;
        boolean isCloseConnection = false;
        try {
            if (conn == null) {
                conn = s_staticInstance.getConnectionForBatchInsert();
                isCloseConnection = true;
            }
            StringBuilder sqlString = new StringBuilder("select id from library_members where id in (");
            for (String memberId : relationMap.keySet()) {
                sqlString.append(memberId).append(",");
            }
            sqlString.deleteCharAt(sqlString.length() - 1);
            sqlString.append(")");
            stmtFetch = conn.prepareStatement(sqlString.toString());
            s_logger.debug("Query to fetch existing users: " + sqlString.toString());
            ResultSet rs = stmtFetch.executeQuery();
            List<String> members = null;
            if (rs.isBeforeFirst()) {
                members = new ArrayList<String>();
            }

            while (rs.next()) {
                members.add(rs.getString(1));
            }

            s_staticInstance.closeAll(conn, stmtFetch, rs, false);

            stmt = conn.prepareStatement(
                    "insert ignore into member_relations(member_id, relative_id, relation) values (?,?,?)");
            for (String memberId : members) {
                RelationDTO relationDTO = relationMap.get(memberId);
                if (relationDTO != null) {
                    String val = oldToNew.get(relationDTO.getRelativeID());
                    if (val == null) {
                        s_logger.debug("problem id: " + memberId);
                        continue;
                    }

                    s_staticInstance.setString(stmt, 1, memberId);
                    s_staticInstance.setString(stmt, 2, val);
                    s_staticInstance.setString(stmt, 3, relationDTO.getRelationShip().serialize());
                    stmt.addBatch();
                }
            }
            s_logger.debug("query: " + stmt);
            stmt.executeBatch();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error while adding mappings", e);
            return false;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, isCloseConnection);
        }

        return true;

    }

    private static void addValuesToStatement(LibraryMember member, PreparedStatement stmt) throws Exception {
        int i = 1;
        s_staticInstance.setString(stmt, i++, member.getId());
        s_staticInstance.setString(stmt, i++, member.getName());
        s_staticInstance.setString(stmt, i++, member.getDob());
        s_staticInstance.setString(stmt, i++, member.getAge());
        s_staticInstance.setString(stmt, i++, Phone.serialize(member.getPhone()));
        s_staticInstance.setString(stmt, i++, member.getMotherName());
        s_staticInstance.setString(stmt, i++, member.getFatherName());
        s_staticInstance.setString(stmt, i++, member.getAddress());
        s_staticInstance.setString(stmt, i++, LibraryGroups.serialize(member.getLibraryGroup()));
        s_staticInstance.setString(stmt, i++, member.getSchool());
        s_staticInstance.setString(stmt, i++, member.getStudyClass());
        s_staticInstance.setString(stmt, i++, member.getGender().serialize());
        s_staticInstance.setString(stmt, i++, member.getBranch().serialize());
        s_staticInstance.setString(stmt, i++, member.getJoinDate());
    }

    private static final String SQL_SELECT_ALL = "select * from library_members";

    public static List<LibraryMember> getMembers(Integer id) {
        List<LibraryMember> users = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement(SQL_SELECT_ALL + (id != null ? " where id=?" : ""));
            if (id != null) {
                stmt.setString(1, id.toString());
            }
            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                users = new ArrayList<LibraryMember>();
            }
            while (result.next()) {
                users.add(new LibraryMember(result));
            }

            return users;
        } catch (Exception e) {
            s_logger.error("Error in fetching the Member list", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, result, true);
        }

        return new ArrayList<LibraryMember>();

    }

    public static boolean deleteMember(int id) {
        PreparedStatement stmt = null;
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt2 = conn.prepareStatement("delete from member_relations where member_id=? or relative_id=?");
            stmt2.setInt(1, id);
            stmt2.setInt(2, id);
            s_logger.debug("query to delete relations: " + stmt2);
            stmt2.execute();

            s_staticInstance.closeAll(null, stmt2, null, false);

            stmt = conn.prepareStatement("delete from library_members where id=?");
            s_staticInstance.setIntDefaultNull(stmt, 1, id, 0);
            s_logger.debug("query: " + stmt);
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while deleting the member from db", e);
            return false;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static boolean editMemberDetails(LibraryMember member) {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);

            stmt = conn.prepareStatement(SQL_UPDATE);
            addValuesToStatementForEdit(member, stmt);
            s_logger.debug("query: " + stmt);
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while updating values", e);
            return false;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    private static void addValuesToStatementForEdit(LibraryMember member, PreparedStatement stmt) throws Exception {
        int i = 1;
        s_staticInstance.setString(stmt, i++, member.getName());
        s_staticInstance.setString(stmt, i++, member.getDob());
        s_staticInstance.setString(stmt, i++, member.getAge());
        s_staticInstance.setString(stmt, i++, Phone.serialize(member.getPhone()));
        s_staticInstance.setString(stmt, i++, member.getMotherName());
        s_staticInstance.setString(stmt, i++, member.getFatherName());
        s_staticInstance.setString(stmt, i++, member.getAddress());
        s_staticInstance.setString(stmt, i++, LibraryGroups.serialize(member.getLibraryGroup()));
        s_staticInstance.setString(stmt, i++, member.getSchool());
        s_staticInstance.setString(stmt, i++, member.getStudyClass());
        s_staticInstance.setString(stmt, i++, member.getPhotoId());
        s_staticInstance.setString(stmt, i++, member.getGender().serialize());
        s_staticInstance.setString(stmt, i++, member.getId());
    }

    private static final String SQL_UPDATE = "update library_members set name=?, dob=?, age=?, phone=?, mother_name=?, father_name=?, address=?, library_group=?, school=?, class=?, photo_id=IF(photo_id IS NULL, ?, photo_id), gender=? where id = ?";

    public static void populateViewMembersDTO(ViewMemberDTO dto) throws Exception {
        PreparedStatement stmt = null;
        ResultSet result = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();

            String sqlQuery = dto.createSearchQuery();
            stmt = conn.prepareStatement(sqlQuery);
            s_logger.debug("Query: " + stmt);

            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                dto.setMembers(new ArrayList<LibraryMember>());
            }
            while (result.next()) {
                dto.add(new LibraryMember(result));
            }

            s_staticInstance.closeAll(conn, stmt, result, false);
            stmt = conn.prepareStatement("select count(*) from library_members");
            result = stmt.executeQuery();
            while (result.next()) {
                dto.setTotalRecords(result.getInt(1));
            }

        } catch (Exception e) {
            s_logger.error("Error in fetching the Member list", e);
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, result, true);
        }
    }

    private static final String SQL_ADD_EVENT = "insert into events(id, title, start_date, end_date, address, description, link, time, duration) values(?,?,?,?,?,?,?,?,?)";
    private static final String SQL_UPDATE_EVENT = "update events set title=?, start_date=?, end_date=?, address=?, description=?, link=?, time=?, duration=? where id=?";
    private static final String SQL_DELETE_EVENT = "delete from events where id=?";
    private static final String SQL_SELECT_ALL_EVENTS = "select * from events where start_date >= ?";
    private static final String SQL_SELECT_ALL_EVENTS_DISP = "select * from events where end_date >= ?";

    public static void addEventInDB(EventData event) throws Exception {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);

            stmt = conn.prepareStatement(SQL_ADD_EVENT);
            addValuesToStatement(event, stmt, true);
            s_logger.debug("query: " + stmt);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    private static void addValuesToStatement(EventData event, PreparedStatement stmt, boolean isAdd) throws Exception {
        int i = 1;
        if (isAdd) {
            s_staticInstance.setString(stmt, i++, event.getId());
        }
        s_staticInstance.setString(stmt, i++, event.getTitle());
        s_staticInstance.setTimestamp(stmt, i++, event.getStart());
        s_staticInstance.setTimestamp(stmt, i++, event.getEnd());
        s_staticInstance.setString(stmt, i++, event.getAddress());
        s_staticInstance.setString(stmt, i++, event.getDescription());
        s_staticInstance.setString(stmt, i++, event.getEventLink());
        s_staticInstance.setString(stmt, i++, event.getTime());
        s_staticInstance.setString(stmt, i++, event.getDuration());
        if (!isAdd) {
            s_staticInstance.setString(stmt, i++, event.getId());
        }
    }

    public static void editEventInDB(EventData event) throws Exception {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);

            stmt = conn.prepareStatement(SQL_UPDATE_EVENT);
            addValuesToStatement(event, stmt, false);
            s_logger.debug("query: " + stmt);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static void deleteEventInDB(EventData event) throws Exception {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);

            stmt = conn.prepareStatement(SQL_DELETE_EVENT);
            s_staticInstance.setString(stmt, 1, event.getId());
            s_logger.debug("query: " + stmt);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static List<EventData> getEvents(Calendar cal) {
        List<EventData> events = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement(SQL_SELECT_ALL_EVENTS);
            s_staticInstance.setTimestamp(stmt, 1, cal);

            s_logger.debug("Events Fetch Query: " + stmt);

            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                events = new ArrayList<EventData>();
            }
            while (result.next()) {
                events.add(new EventData(result, s_staticInstance));
            }

            return events;
        } catch (Exception e) {
            s_logger.error("Error in fetching the Member list", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, result, true);
        }

        return new ArrayList<EventData>();
    }

    public static List<EventData> getEvents(int numEvents) {
        List<EventData> events = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement(SQL_SELECT_ALL_EVENTS_DISP + " order by start_date limit " + numEvents);
            Calendar cal = DateUtility.removeTime(true, null);
            s_staticInstance.setTimestamp(stmt, 1, cal);

            s_logger.debug("Events Fetch Query: " + stmt);

            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                events = new ArrayList<EventData>();
            }
            while (result.next()) {
                events.add(new EventData(result, s_staticInstance));
            }

            return events;
        } catch (Exception e) {
            s_logger.error("Error in fetching the Member list", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, result, true);
        }

        return new ArrayList<EventData>();
    }

    private static final String SQL_ADD_BANNER = "insert into library_banner (urlLink, imageURL, textContent, user_id, type,gen_ts) values (?,?,?,?,?,NOW())";

    public static int addBannerObject(ResourceObject bannerObj, User user) throws Exception {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(SQL_ADD_BANNER, PreparedStatement.RETURN_GENERATED_KEYS);

            int i = 1;
            s_staticInstance.setString(stmt, i++, bannerObj.getUrlLink());
            s_staticInstance.setString(stmt, i++, bannerObj.getImageUrl());
            s_staticInstance.setString(stmt, i++, bannerObj.getTextContent());
            s_staticInstance.setString(stmt, i++, user.getUserId());
            s_staticInstance.setString(stmt, i++, String.valueOf(bannerObj.getResourceType().getCode()));

            s_logger.debug("query: " + stmt);
            stmt.executeUpdate();
            conn.commit();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            return rs.getInt(1);
        } catch (Exception e) {
            s_logger.error("Error while adding banner info to DB", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return -1;
    }

    private static final String SQL_GET_ALL_BANNER = "select * from library_banner";

    public static Map<ResourceType, List<ResourceObject>> getAllBannerObjects() {

        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_GET_ALL_BANNER);
            rs = pStmt.executeQuery();

            Map<ResourceType, List<ResourceObject>> bannerItems = null;

            if (rs.isBeforeFirst()) {
                bannerItems = new HashMap<ResourceType, List<ResourceObject>>();
            }
            while (rs.next()) {
                ResourceObject object = new ResourceObject(rs);
                ResourceType type = object.getResourceType();
                if (!bannerItems.containsKey(type)) {
                    bannerItems.put(type, new ArrayList<ResourceObject>());
                }

                bannerItems.get(type).add(object);
            }

            return bannerItems;
        } catch (Exception e) {
            s_logger.error("Error while getting banner items", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, null, true);
        }

        return null;
    }

    public static boolean deleteBanner(String id) {
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt2 = conn.prepareStatement("delete from library_banner where id=?");
            stmt2.setInt(1, Integer.parseInt(id));
            s_logger.debug("query to delete bannery content: " + stmt2);
            stmt2.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while deleting the member from db", e);
            return false;
        } finally {
            s_staticInstance.closeAll(conn, stmt2, null, true);
        }
    }

    private static final String SQL_GETALL_BLOGS = "select id, title, author, postedby_id, post_date, publish_date, url from blogposts order by post_date desc";

    public static List<Blogpost> getAllBlogposts() {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<Blogpost> list = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_GETALL_BLOGS);
            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<Blogpost>();
            }

            while (rs.next()) {
                Blogpost post = new Blogpost(rs, false);
                list.add(post);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching blogposts", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    private static final String SQL_BLOG_BY_ID = "select * from blogposts where id=?";

    public static Blogpost getBlogpostById(String id, boolean isPublished) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_BLOG_BY_ID + (isPublished ? " and publish_date < ?" : ""));
            s_staticInstance.setLongDefaultNull(pStmt, 1, Long.parseLong(id), 0);
            if (isPublished) {
                Date cal = DateUtility.getLastTimeForDay(DateUtility.getCurrentDate());
                s_staticInstance.setTimestamp(pStmt, 2, cal);
            }
            rs = pStmt.executeQuery();
            while (rs.next()) {
                Blogpost post = new Blogpost(rs, true);
                return post;
            }

        } catch (Exception e) {
            s_logger.error("Error in fetching blogposts", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static void addBlogpost(Blogpost newPost) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(Blogpost.getInsertQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, newPost.getTitle());
            s_staticInstance.setString(stmt, i++, newPost.getAuthor());
            s_staticInstance.setIntDefaultNull(stmt, i++,
                    IntegerUtility.parseIntWithDefaultOnError(newPost.getPostedBy().getUserId(), 0), -1);
            s_staticInstance.setTimestamp(stmt, i++, newPost.getPostDate());
            s_staticInstance.setTimestamp(stmt, i++, newPost.getPublishDate());
            s_staticInstance.setString(stmt, i++, newPost.getPostText());
            s_staticInstance.setString(stmt, i++, newPost.getUrl());

            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in adding blogpost", e);
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

    }

    public static void editPost(Blogpost newPost) {
        try {
            newPost.updateTable();
        } catch (Exception e) {
            s_logger.error("Error in editing the post: " + newPost.getId());
        }

    }

    public static void editBlogpost(Blogpost blogpost, Connection conn) throws Exception {
        PreparedStatement stmt = null;
        boolean isCloseConnection = false;
        try {

            if (conn == null) {
                conn = s_staticInstance.getConnection(false);
                isCloseConnection = true;
            }
            stmt = conn.prepareStatement(blogpost.getEditQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, blogpost.getTitle());
            s_staticInstance.setString(stmt, i++, blogpost.getAuthor());
            s_staticInstance.setIntDefaultNull(stmt, i++,
                    IntegerUtility.parseIntWithDefaultOnError(blogpost.getPostedBy().getUserId(), 0), -1);
            s_staticInstance.setTimestamp(stmt, i++, blogpost.getPostDate());
            // s_staticInstance.setTimestamp(stmt, i++,
            // blogpost.getPublishDate());
            s_staticInstance.setString(stmt, i++, blogpost.getPostText());
            s_staticInstance.setString(stmt, i++, blogpost.getUrl());
            s_staticInstance.setLongDefaultNull(stmt, i++, blogpost.getId(), 0);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, isCloseConnection);
        }

    }

    public static void deletePost(String id) {
        Blogpost blog = new Blogpost(Long.parseLong(id));
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            blog.addInHistory(conn);
            stmt = conn.prepareStatement(blog.getDeleteQuery());
            s_staticInstance.setLongDefaultNull(stmt, 1, blog.getId(), 0);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in deleting the blogpost with id: " + id, e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

    }

    public static List<Blogpost> getPublishedBlogs(String pageNumber, int num) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<Blogpost> list = null;
        try {
            String limitString = null;
            if (pageNumber == null) {
                limitString = String.valueOf(num);
            } else {
                int start = (NumberUtility.parsetIntWithDefaultOnErr(pageNumber, 1) - 1) * num;
                int end = num;
                limitString = start + ", " + end;
            }
            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(
                    "select * from blogposts where publish_date < ? order by publish_date desc limit " + limitString);
            Date cal = DateUtility.getLastTimeForDay(DateUtility.getCurrentDate());
            s_staticInstance.setTimestamp(pStmt, 1, cal);
            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<Blogpost>();
            }

            while (rs.next()) {
                Blogpost post = new Blogpost(rs, true);
                list.add(post);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching blogposts", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    private static final String SQL_BLOG_BY_URL = "select * from blogposts where url=?";

    public static Blogpost getBlogpostByURL(String url, boolean isPublished) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_BLOG_BY_URL + (isPublished ? " and publish_date < ?" : ""));
            s_staticInstance.setString(pStmt, 1, url);
            if (isPublished) {
                Date cal = DateUtility.getLastTimeForDay(DateUtility.getCurrentDate());
                s_staticInstance.setTimestamp(pStmt, 2, cal);
            }
            rs = pStmt.executeQuery();
            while (rs.next()) {
                Blogpost post = new Blogpost(rs, true);
                return post;
            }

        } catch (Exception e) {
            s_logger.error("Error in fetching blogposts", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }

        return null;
    }

    /**
     * PageResources section
     */

    private static final String SQL_GETALL_PAGES = "select id, title, creation_date, mod_date, url from page_resources";

    public static List<PageResource> getAllPages() {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<PageResource> list = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_GETALL_PAGES);
            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<PageResource>();
            }

            while (rs.next()) {
                PageResource post = new PageResource(rs, false);
                list.add(post);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching pages", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    private static final String SQL_PAGE_BY_ID = "select * from page_resources where id=?";

    public static PageResource getPageById(String id) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_PAGE_BY_ID);
            s_staticInstance.setLongDefaultNull(pStmt, 1, Long.parseLong(id), 0);
            rs = pStmt.executeQuery();
            while (rs.next()) {
                PageResource post = new PageResource(rs, true);
                return post;
            }

        } catch (Exception e) {
            s_logger.error("Error in fetching page resource", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static void addPage(PageResource newPost) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(PageResource.getInsertQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, newPost.getTitle());
            s_staticInstance.setTimestamp(stmt, i++, newPost.getCreationDate());
            s_staticInstance.setString(stmt, i++, newPost.getPageText());
            s_staticInstance.setString(stmt, i++, newPost.getUrl());

            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in adding new Page", e);
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

    }

    public static void editPage(PageResource newPost) {
        try {
            newPost.updateTable();
        } catch (Exception e) {
            s_logger.error("Error in editing the post: " + newPost.getId());
        }

    }

    public static void editPage(PageResource page, Connection conn) throws Exception {
        PreparedStatement stmt = null;
        boolean isCloseConnection = false;
        try {

            if (conn == null) {
                conn = s_staticInstance.getConnection(false);
                isCloseConnection = true;
            }
            stmt = conn.prepareStatement(page.getEditQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, page.getTitle());
            s_staticInstance.setString(stmt, i++, page.getPageText());
            s_staticInstance.setString(stmt, i++, page.getUrl());
            s_staticInstance.setLongDefaultNull(stmt, i++, page.getId(), 0);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, isCloseConnection);
        }

    }

    public static void deletePage(String id) {
        PageResource blog = new PageResource(Long.parseLong(id));
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            blog.addInHistory(conn);
            stmt = conn.prepareStatement(blog.getDeleteQuery());
            s_staticInstance.setLongDefaultNull(stmt, 1, blog.getId(), 0);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in deleting the page with id: " + id, e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

    }

    private static final String SQL_PAGE_BY_URL = "select * from page_resources where url=?";

    public static PageResource getPageByURL(String url) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_PAGE_BY_URL);
            s_staticInstance.setString(pStmt, 1, url);
            rs = pStmt.executeQuery();
            while (rs.next()) {
                PageResource post = new PageResource(rs, true);
                return post;
            }

        } catch (Exception e) {
            s_logger.error("Error in fetching custom page", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }

        return null;
    }

    public static boolean saveForm(Form form) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(Form.getInsertQuery(), PreparedStatement.RETURN_GENERATED_KEYS);
            int i = 1;
            s_staticInstance.setString(stmt, i++, form.getTitle());
            s_staticInstance.setString(stmt, i++, form.getUrl());
            s_staticInstance.setString(stmt, i++, form.getEmails());

            stmt.execute();
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys != null && generatedKeys.next()) {
                form.setId(generatedKeys.getInt(1));
            }

            s_staticInstance.closeAll(conn, stmt, generatedKeys, false);

            stmt = conn.prepareStatement(Formfield.getInsertQuery());
            List<Formfield> formFields = form.getFormFields();

            int count = 0;
            for (Formfield formField : formFields) {
                i = 1;
                s_staticInstance.setIntDefaultNull(stmt, i++, formField.getId(), -1);
                s_staticInstance.setIntDefaultNull(stmt, i++, form.getId(), -1);
                s_staticInstance.setString(stmt, i++, formField.isMandatory() ? "Y" : "N");
                s_staticInstance.setString(stmt, i++, String.valueOf(formField.getType().getCode()));
                s_staticInstance.setString(stmt, i++, formField.getQuestionText());
                s_staticInstance.setString(stmt, i++, formField.getValidationRegex());
                stmt.addBatch();
                count++;

                if (count == 50) {
                    stmt.executeBatch();
                    count = 0;
                }
            }

            if (count > 0) {
                stmt.executeBatch();
                count = 0;
            }

            s_staticInstance.closeAll(conn, stmt, generatedKeys, false);

            stmt = conn.prepareStatement(FormOption.getInsertQuery(), PreparedStatement.RETURN_GENERATED_KEYS);

            for (Formfield parentField : formFields) {
                List<FormOption> formOptions = parentField.getFormOptions();
                if (formOptions != null) {
                    for (int optionNum = 0; optionNum < formOptions.size(); optionNum++) {
                        FormOption option = formOptions.get(optionNum);
                        i = 1;

                        option.setOptionId(form.getId() + "_" + parentField.getId() + "_" + "_" + optionNum);

                        s_staticInstance.setString(stmt, i++, option.getOptionId());
                        s_staticInstance.setIntDefaultNull(stmt, i++, parentField.getId(), -1);
                        s_staticInstance.setString(stmt, i++, option.getOptionText());
                        s_staticInstance.setString(stmt, i++, option.isOtherOption() ? "Y" : "N");
                        stmt.addBatch();
                        count++;

                        if (count == 50) {
                            stmt.executeBatch();
                            count = 0;
                        }
                    }
                }

            }

            if (count > 0) {
                stmt.executeBatch();
                count = 0;
            }

            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error in saving form", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error while rolling back the saving of the form", e1);
            }
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return false;
    }

    public static Form getFormById(String formId, boolean isID) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        Form form = null;
        Map<Formfield, List<FormOption>> fieldToOptions = new LinkedHashMap<Formfield, List<FormOption>>();
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(Form.getFormAndFieldsFetchSQL(isID));
            if (isID) {
                s_staticInstance.setIntDefaultNull(pStmt, 1, Integer.parseInt(formId), 0);
            } else {
                s_staticInstance.setString(pStmt, 1, formId);
            }
            rs = pStmt.executeQuery();
            while (rs.next()) {
                if (form == null) {
                    form = new Form(rs, 0);
                }

                Formfield field = new Formfield(rs, 5);
                if (!fieldToOptions.containsKey(field)) {
                    fieldToOptions.put(field, new ArrayList<FormOption>());
                }

                if (field.getType() == ElementType.RADIOBOX || field.getType() == ElementType.CHECKBOX) {
                    FormOption option = new FormOption(rs, 12);
                    fieldToOptions.get(field).add(option);
                }
            }

            List<Formfield> fields = new ArrayList<Formfield>();
            for (Formfield field : fieldToOptions.keySet()) {
                List<FormOption> list = fieldToOptions.get(field);
                if (!list.isEmpty()) {
                    field.setFormOptions(list);
                }

                fields.add(field);
            }

            form.setFormFields(fields);
            return form;

        } catch (Exception e) {
            s_logger.error("Error in fetching page resource", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    private static final String SQL_GETALL_FORMS = "select id, title, url from forms";
    private static final String SQL_GETALL_FORMS_RESPONSES = "select forms.id as form_id, forms.title as form_title, count(form_responses.id) as count from forms left join form_responses on forms.id = form_responses.form_id group by forms.id;";

    public static List<Form> getAllForms() {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<Form> list = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_GETALL_FORMS);
            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<Form>();
            }

            while (rs.next()) {
                Form post = new Form();
                post.setId(rs.getInt(1));
                post.setTitle(rs.getString(2));
                post.setUrl(rs.getString(3));
                list.add(post);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching pages", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static void deleteForm(int formId) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(Form.getDeleteQuery());
            s_staticInstance.setLongDefaultNull(stmt, 1, formId, 0);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in deleting the form with id: " + formId, e);
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static boolean saveFormResponse(Form form) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(Form.getFormResponseSaveQuery(), PreparedStatement.RETURN_GENERATED_KEYS);
            int i = 1;
            s_staticInstance.setIntDefaultNull(stmt, i++, form.getId(), -1);
            stmt.execute();
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            int responseId = -1;
            if (generatedKeys != null && generatedKeys.next()) {
                responseId = generatedKeys.getInt(1);
            }

            s_staticInstance.closeAll(conn, stmt, generatedKeys, false);
            stmt = conn.prepareStatement(Formfield.getSaveResponseQuery());
            List<Formfield> formFields = form.getFormFields();

            int count = 0;
            for (Formfield formField : formFields) {
                i = 1;
                s_staticInstance.setIntDefaultNull(stmt, i++, formField.getId(), -1);
                s_staticInstance.setIntDefaultNull(stmt, i++, responseId, 0);
                s_staticInstance.setString(stmt, i++, formField.getAnswer());
                stmt.addBatch();
                count++;

                if (count == 50) {
                    stmt.executeBatch();
                    count = 0;
                }
            }

            if (count > 0) {
                stmt.executeBatch();
                count = 0;
            }

            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error in saving form response", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error while rolling back the saving of the form", e1);
            }
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return false;
    }

    public static List<FormResponse> getAllFormsWithResponseCount() {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<FormResponse> list = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_GETALL_FORMS_RESPONSES);
            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<FormResponse>();
            }

            while (rs.next()) {
                FormResponse response = new FormResponse();
                Form post = new Form();
                post.setId(rs.getInt(1));
                post.setTitle(rs.getString(2));
                response.setForm(post);
                response.setResponseCount(rs.getInt(3));
                list.add(response);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching forms and responses", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    private static final String SQL_GET_FORM_RESPONSES = "select forms.id as form_id, forms.title as form_title, form_responses.id as response_id, "
            + "form_fields.question_text as question, response_answers.response "
            + "from forms left join form_responses "
            + "on forms.id = form_responses.form_id left join response_answers on form_responses.id = response_answers.response_id "
            + "left join form_fields on form_fields.id = response_answers.field_id " + "where forms.id = ?";

    private static final String SQL_GET_SUCCESFUL_DONATIONS = "select * from payment_unit where payment_status = 'Y' order by gen_ts desc limit ?,?";
    private static final String SQL_GET_DONATION_AMOUNT = "select sum(convert(int, amount)) from payment_unit where payment_status = 'Y'";

    /**
     * This api returns back form's field and responses for the form id
     *
     * @param formId
     * @return
     */
    public static Map<Form, Map<String, List<Formfield>>> getFormResponsesByFormId(int formId) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        Map<Form, Map<String, List<Formfield>>> formResponses = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_GET_FORM_RESPONSES);
            pStmt.setInt(1, formId);

            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                formResponses = new HashMap<Form, Map<String, List<Formfield>>>();
            }

            while (rs.next()) {
                Form post = new Form();
                post.setId(rs.getInt("form_id"));
                post.setTitle(rs.getString("form_title"));
                if (!formResponses.containsKey(post)) {
                    formResponses.put(post, new HashMap<String, List<Formfield>>());
                }

                Map<String, List<Formfield>> responseIdToResponsesMap = formResponses.get(post);
                String responseId = rs.getString("response_id");
                if (!responseIdToResponsesMap.containsKey(responseId)) {
                    responseIdToResponsesMap.put(responseId, new ArrayList<Formfield>());
                }

                List<Formfield> fieldList = responseIdToResponsesMap.get(responseId);
                String question = rs.getString("question");
                String response = StringUtility.trimAndEmptyOrNullIsNull(rs.getString("response"));
                Formfield field = new Formfield();
                field.setQuestionText(question);
                field.setAnswer(response);
                fieldList.add(field);
            }

            return formResponses;

        } catch (Exception e) {
            s_logger.error("Error in fetching forms and responses", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static boolean savePaymentUnit(PaymentUnit paymentUnit, String ipaddress) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(PaymentUnit.getCreatePaymentUnitQuery());
            int i = 1;
            Donor donor = paymentUnit.getDonor();
            s_staticInstance.setString(stmt, i++, paymentUnit.getTransactionId());
            s_staticInstance.setString(stmt, i++, paymentUnit.getFirstName());
            s_staticInstance.setString(stmt, i++, donor.getLastName());
            s_staticInstance.setString(stmt, i++, donor.getPhone());
            s_staticInstance.setString(stmt, i++, donor.getEmail());
            s_staticInstance.setString(stmt, i++, paymentUnit.getAmount());
            s_staticInstance.setString(stmt, i++,
                    donor.getSubscriptionType() != null ? donor.getSubscriptionType().getCode() : null);
            s_staticInstance.setString(stmt, i++,
                    paymentUnit.getExtraNotes() != null ? paymentUnit.getExtraNotes() : null);
            s_staticInstance.setString(stmt, i++, paymentUnit.isRequire80GReceipt() ? "Y" : "N");
            s_staticInstance.setString(stmt, i++, donor.getAddress() == null ? null : donor.getAddress().serialize());
            s_staticInstance.setString(stmt, i++, donor.getPanNumber());
            s_staticInstance.setString(stmt, i++,
                    paymentUnit.getPgType() == null ? null : String.valueOf(paymentUnit.getPgType().getCode()));
            s_staticInstance.setString(stmt, i++, String.valueOf(paymentUnit.getPaymentType().getCode()));
            s_staticInstance.setString(stmt, i++,
                    paymentUnit.getIsPaymentSuccessful() == null || !paymentUnit.getIsPaymentSuccessful() ? "N" : "Y");
            s_staticInstance.setString(stmt, i++, paymentUnit.getGatewayTransactionId());
            s_staticInstance.setString(stmt, i++, paymentUnit.getReceiptNumber());
            s_staticInstance.setTimestamp(stmt, i++, paymentUnit.getReceiptDate());
            s_staticInstance.setTimestamp(stmt, i++,
                    paymentUnit.getCreationDate() == null ? DateUtility.getCurrentDate()
                            : paymentUnit.getCreationDate());
            s_staticInstance.setString(stmt, i++, ipaddress);

            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error in saving payment unit", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error while rolling back the saving of the payment unit", e1);
            }
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return false;
    }

    public static PaymentUnit getPaymentUnitByTransactionId(String transactionId) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(PaymentUnit.getSelectPaymentUnitQuery());
            s_staticInstance.setString(pStmt, 1, transactionId);
            rs = pStmt.executeQuery();

            PaymentUnit unit = null;
            while (rs.next()) {
                unit = new PaymentUnit(rs);
                break;
            }

            return unit;

        } catch (Exception e) {
            s_logger.error("Error in fetching payment unit for transaction id: " + transactionId, e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static void addPaymentError(String transactionId, String errorCode, String errorMessage) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(
                    "insert into payment_error (txn_id, error_code, error_message, gen_ts) values (?,?,?, NOW())");
            int i = 1;
            s_staticInstance.setString(stmt, i++, transactionId);
            s_staticInstance.setString(stmt, i++, errorCode);
            s_staticInstance.setString(stmt, i++, errorMessage);

            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in saving payment error codes and messages", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error while rolling back the saving of the payment error", e1);
            }

            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static void updatePaymentStatus(PaymentUnit unit) throws Exception {

        PreparedStatement stmt = null;
        boolean isCloseConnection = false;
        Connection conn = null;
        try {

            conn = s_staticInstance.getConnection(false);
            isCloseConnection = true;
            stmt = conn.prepareStatement(unit.getEditQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++,
                    unit.getIsPaymentSuccessful() == null || !unit.getIsPaymentSuccessful() ? "N" : "Y");
            s_staticInstance.setString(stmt, i++, unit.getGatewayTransactionId());
            s_staticInstance.setString(stmt, i++, unit.getTransactionId());
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, isCloseConnection);
        }

    }

    public static List<PaymentUnit> getAllDonations(int page, int size) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<PaymentUnit> list = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(SQL_GET_SUCCESFUL_DONATIONS);
            pStmt.setInt(1, page * size);
            pStmt.setInt(2, size);
            s_logger.info("query for getting successful donations: " + pStmt);
            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<PaymentUnit>();
            }

            while (rs.next()) {
                PaymentUnit unit = new PaymentUnit(rs);
                list.add(unit);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching payment units", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static void saveEmailInDB(EmailMessage emailMessage, String errorLogs) {

        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(EmailMessage.getSaveEmailQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, StringUtility.join(emailMessage.getTo(), ";"));
            s_staticInstance.setString(stmt, i++, StringUtility.join(emailMessage.getCc(), ";"));
            s_staticInstance.setString(stmt, i++, emailMessage.getFrom());
            s_staticInstance.setString(stmt, i++, emailMessage.getSubject());
            s_staticInstance.setString(stmt, i++, emailMessage.getContent());
            s_staticInstance.setString(stmt, i++, errorLogs == null ? "Y" : "N");
            s_staticInstance.setString(stmt, i++, errorLogs);

            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            s_logger.error("Error in saving Email message in DB", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error while rolling back the saving of email message", e1);
            }
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

    }

    public static boolean saveBooks(List<Book> booksFromCSVFiles) {

        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(Book.getInsertQuery());
            int count = 0;
            for (Book book : booksFromCSVFiles) {
                if (book.getTitle() == null) {
                    s_logger.debug("Title null for book: " + book);
                    continue;
                }

                int i = 1;
                s_staticInstance.setString(stmt, i++, book.getBookType().getCode());
                s_staticInstance.setIntDefaultNull(stmt, i++, book.getBookCode(), 0);
                s_staticInstance.setString(stmt, i++, book.getIsbn10());
                s_staticInstance.setString(stmt, i++, book.getIsbn13());
                s_staticInstance.setString(stmt, i++, book.getTitle());
                s_staticInstance.setString(stmt, i++, StringUtility.join(book.getAuthors(), ";"));
                s_staticInstance.setIntWithDefault(stmt, i++, book.getNumCopies(), 1, 1);
                stmt.addBatch();
                count++;

                if (count == 50) {
                    stmt.executeBatch();
                    count = 0;
                }
            }

            if (count > 0) {
                stmt.executeBatch();
                count = 0;
            }

            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error in saving books", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error while rolling back the saving of the books", e1);
            }
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return false;

    }

    public static String getTotalBooks() {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement("select count(*) from books");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
            s_logger.error("Error in getting number of books", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error in getting number of books", e1);
            }
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return null;
    }

    public static List<Book> getBooks() {
        return getBooks(null, null, null);
    }

    public static List<Book> getBooks(String pageNumber, String numberBooks, Book filterObject) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<Book> list = null;
        try {
            String limitString = null;
            if (pageNumber != null && numberBooks != null) {
                int num = NumberUtility.parsetIntWithDefaultOnErr(numberBooks, 0);
                int start = (NumberUtility.parsetIntWithDefaultOnErr(pageNumber, 1) - 1) * num;
                int end = num;
                limitString = start + ", " + end;
            }

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(Book.getBookListQuery(filterObject) + " order by book_type, book_code"
                    + (limitString != null ? (" limit " + limitString) : ""));
            if (filterObject != null) {
                int i = 1;
                if (filterObject.getTitle() != null) {
                    String title = filterObject.getTitle().toLowerCase();
                    title = StringUtility.convertToSQLStringMatch(title);
                    s_staticInstance.setString(pStmt, i++, title);
                }

                if (filterObject.getIsbn10() != null || filterObject.getIsbn13() != null) {
                    String isbn = filterObject.getIsbn10() != null ? filterObject.getIsbn10()
                            : filterObject.getIsbn13();
                    s_staticInstance.setString(pStmt, i++, isbn);
                    s_staticInstance.setString(pStmt, i++, isbn);
                }

                if (filterObject.getBookType() != null) {
                    s_staticInstance.setString(pStmt, i++, filterObject.getBookType().getCode());
                }

                if (filterObject.getAuthors() != null) {
                    String authors = StringUtility.join(filterObject.getAuthors(), " ").toLowerCase();
                    authors = StringUtility.convertToSQLStringMatch(authors);
                    s_staticInstance.setString(pStmt, i++, authors);
                }

            }

            s_logger.debug("query: " + pStmt);
            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<Book>();
            }

            while (rs.next()) {
                Book post = new Book(rs, false);
                list.add(post);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching blogposts", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static Book getBook(Book book, boolean loadMetadata) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(Book.getSearchBookQuery(book));
            int i = 1;
            s_staticInstance.setIntDefaultNull(pStmt, i++, book.getId(), 0);
            if (book.getIsbn10() != null) {
                s_staticInstance.setString(pStmt, i++, book.getIsbn10());
            }

            if (book.getIsbn13() != null) {
                s_staticInstance.setString(pStmt, i++, book.getIsbn13());
            }

            rs = pStmt.executeQuery();

            while (rs.next()) {
                book = new Book(rs, loadMetadata);
                return book;
            }
        } catch (Exception e) {
            s_logger.error("Error in fetching book", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static int getBooksCount(Book filterObject) {

        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement(Book.getBookCountQuery(filterObject));
            if (filterObject != null) {
                int i = 1;
                if (filterObject.getTitle() != null) {
                    String title = filterObject.getTitle().toLowerCase();
                    title = StringUtility.convertToSQLStringMatch(title);
                    s_staticInstance.setString(pStmt, i++, title);
                }

                if (filterObject.getIsbn10() != null || filterObject.getIsbn13() != null) {
                    String isbn = filterObject.getIsbn10() != null ? filterObject.getIsbn10()
                            : filterObject.getIsbn13();
                    s_staticInstance.setString(pStmt, i++, isbn);
                    s_staticInstance.setString(pStmt, i++, isbn);
                }

                if (filterObject.getBookType() != null) {
                    s_staticInstance.setString(pStmt, i++, filterObject.getBookType().getCode());
                }

                if (filterObject.getAuthors() != null) {
                    String authors = StringUtility.join(filterObject.getAuthors(), " ").toLowerCase();
                    authors = StringUtility.convertToSQLStringMatch(authors);
                    s_staticInstance.setString(pStmt, i++, authors);
                }

            }

            s_logger.debug("query: " + pStmt);

            rs = pStmt.executeQuery();

            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            s_logger.error("Error in getting books count", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return 0;

    }

    public static boolean updateBook(Book book) {
        PreparedStatement stmt = null;
        boolean isCloseConnection = false;
        Connection conn = null;
        try {

            conn = s_staticInstance.getConnection(false);
            isCloseConnection = true;
            stmt = conn.prepareStatement(book.getEditQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, book.getIsbn10());
            s_staticInstance.setString(stmt, i++, book.getIsbn13());
            s_staticInstance.setString(stmt, i++, book.getTitle());
            s_staticInstance.setString(stmt, i++, StringUtility.join(book.getAuthors(), ";"));
            s_staticInstance.setString(stmt, i++, book.getPublisher());
            s_staticInstance.setString(stmt, i++, book.getDescription());
            s_staticInstance.setString(stmt, i++, book.isLoadedThroughAPI() ? "Y" : "N");
            s_staticInstance.setString(stmt, i++, book.getLoaderAgent().getCode());
            s_staticInstance.setString(stmt, i++, StringUtility.join(book.getImageLinks(), ";"));
            stmt.setInt(i++, book.getId());
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while updating the books information", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, isCloseConnection);
        }

        return false;
    }

    public static boolean updatePaymentUnit(PaymentUnit unit) {

        PreparedStatement stmt = null;
        boolean isCloseConnection = false;
        Connection conn = null;
        try {

            conn = s_staticInstance.getConnection(false);
            isCloseConnection = true;
            stmt = conn.prepareStatement(unit.getUpdatePaymentUnitQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, unit.getReceiptNumber());
            s_staticInstance.setTimestamp(stmt, i++, unit.getReceiptDate());
            s_staticInstance.setString(stmt, i++, unit.getTransactionId());
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while updating the receipt number for the payment", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, isCloseConnection);
        }

        return false;

    }

    private static final String SQL_GETALL_HOMEPAGELINKS = "select * from homepagelinks";

    public static List<HomePageLink> getAllHomePageLinks(LibraryGroupForDigitalAccess group) {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<HomePageLink> list = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            String query = SQL_GETALL_HOMEPAGELINKS;
            if (group != null) {
                query += " where library_group=?";
            }
            pStmt = conn.prepareStatement(query);

            if (group != null) {
                pStmt.setString(1, String.valueOf(group.getCode()));
            }

            rs = pStmt.executeQuery();
            if (rs.isBeforeFirst()) {
                list = new ArrayList<HomePageLink>();
            }

            while (rs.next()) {
                HomePageLink link = new HomePageLink(rs);
                list.add(link);
            }

            return list;

        } catch (Exception e) {
            s_logger.error("Error in fetching home page links", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return null;
    }

    public static boolean saveHomePageLink(HomePageLink link) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert(true);
            stmt = conn.prepareStatement(link.getInsertQuery(), PreparedStatement.RETURN_GENERATED_KEYS);
            int i = 1;
            s_staticInstance.setString(stmt, i++, link.getName());
            s_staticInstance.setString(stmt, i++, link.getDesc());
            s_staticInstance.setString(stmt, i++, link.getHindiName());
            s_staticInstance.setString(stmt, i++, link.getHindiDesc());
            s_staticInstance.setString(stmt, i++, link.getUrl());
            s_staticInstance.setString(stmt, i++, String.valueOf(link.getGroup().getCode()));

            stmt.execute();
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys != null && generatedKeys.next()) {
                link.setId(generatedKeys.getInt(1));
            }

            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while saving the link", e);
            return false;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static boolean deleteLink(int linkId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(HomePageLink.getDeleteQuery());
            s_staticInstance.setLongDefaultNull(stmt, 1, linkId, 0);
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error in deleting homepagelink with id : " + linkId, e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return false;

    }

    private static final String SQL_ADD_SLOT = "insert into slots(id, member_id, start_date, end_date, slot_date, start_time, end_time, duration, notes, status, slot_type) values(?,?,?,?,?,?,?,?,?,?,?)";
    private static final String SQL_DELETE_SLOT = "delete from slots where id=? and status <> '"
            + SlotStatus.Attended.getCode() + "'";

    private static final String SQL_SELECT_ALL_SLOTS = "select s.*, lm.id, lm.name from slots as s left join library_members as lm on s.member_id = lm.id where start_date >= ? and end_date <= ? and lm.branch_code=? order by start_date";
    private static final String SQL_SELECT_SLOTS_DATE = "select s.*, lm.id, lm.name from slots as s left join library_members as lm on s.member_id = lm.id where slot_date = ? and lm.branch_code=? order by start_date";

    // private static final String SQL_SELECT_ALL_EVENTS_DISP =
    // "select * from events where end_date >= ?";
    public static void addSlotInDB(Slot slot) throws Exception {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);

            stmt = conn.prepareStatement(SQL_ADD_SLOT);
            addValuesToStatement(slot, stmt);
            s_logger.info("query: " + stmt);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    private static void addValuesToStatement(Slot slot, PreparedStatement stmt) throws Exception {
        int i = 1;
        s_staticInstance.setString(stmt, i++, slot.getId());
        s_staticInstance.setString(stmt, i++, slot.getMember().getId());
        s_staticInstance.setTimestamp(stmt, i++, slot.getStart());
        s_staticInstance.setTimestamp(stmt, i++, slot.getEnd());
        s_staticInstance.setString(stmt, i++, slot.getDate());
        s_staticInstance.setString(stmt, i++, slot.getStartTime());
        s_staticInstance.setString(stmt, i++, slot.getEndTime());
        s_staticInstance.setString(stmt, i++, slot.getDuration());
        s_staticInstance.setString(stmt, i++, slot.getNotes());
        s_staticInstance.setString(stmt, i++, String.valueOf(slot.getStatus().getCode()));
        s_staticInstance.setString(stmt, i++, slot.getSlotType());
    }

    public static List<Slot> getSlots(LocalDate firstDate, LocalDate lastDate, LibraryBranch libraryBranch) {
        List<Slot> slots = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement(SQL_SELECT_ALL_SLOTS);
            s_staticInstance.setTimestamp(stmt, 1, firstDate);
            s_staticInstance.setTimestamp(stmt, 2, lastDate, false);
            s_staticInstance.setString(stmt, 3, libraryBranch.getCode());

            s_logger.debug("Slots Fetch Query: " + stmt);

            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                slots = new ArrayList<Slot>();
            }
            while (result.next()) {
                slots.add(new Slot(result));
            }

            return slots;
        } catch (Exception e) {
            s_logger.error("Error in fetching the Member list", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, result, true);
        }

        return new ArrayList<Slot>();
    }

    public static List<Slot> getSlotsForDate(String date, LibraryBranch branch) {
        List<Slot> slots = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement(SQL_SELECT_SLOTS_DATE);
            s_staticInstance.setString(stmt, 1, date);
            s_staticInstance.setString(stmt, 2, branch.getCode());

            s_logger.debug("Slots Fetch Query: " + stmt);

            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                slots = new ArrayList<Slot>();
            }
            while (result.next()) {
                slots.add(new Slot(result));
            }

            return slots;
        } catch (Exception e) {
            s_logger.error("Error in fetching the Member list", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, result, true);
        }

        return new ArrayList<Slot>();
    }

    public static boolean updateSlotStatus(int id, SlotStatus status) {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement("update slots set status = ? where id = ?");
            stmt.setString(1, String.valueOf(status.getCode()));
            stmt.setString(2, String.valueOf(id));
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while updating status for slot id: " + id, e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, false);
        }

        return false;

    }

    /**
     * API updates the hindi version of the link if it already exists without it
     *
     * @param link
     * @return
     */
    public static boolean updateLinkInfo(HomePageLink link) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(link.getUpdateQuery());
            int i = 1;
            if (link.getHindiName() != null) {
                s_staticInstance.setString(stmt, i++, link.getHindiName(), 100);
            }

            if (link.getHindiDesc() != null) {
                s_staticInstance.setString(stmt, i++, link.getHindiDesc(), 500);
            }

            s_staticInstance.setLongDefaultNull(stmt, i++, link.getId(), 0);
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error in updating homepagelink with id : " + link.getId(), e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return false;

    }

    public static boolean updateLinkCount(String id, boolean isEnglish) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(HomePageLink.getLinkCountUpdateQuery(isEnglish));
            int i = 1;
            s_staticInstance.setLongDefaultNull(stmt, i++, Long.valueOf(id), 0);
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error in updating homepagelink click count with id : " + id + " for language: "
                    + (isEnglish ? "English" : "Hindi"), e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }

        return false;
    }

    /**
     * Deletes slots if not already marked
     *
     * @param slot
     * @return
     */
    public static boolean deleteSlotFromDB(Slot slot) {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt = conn.prepareStatement(SQL_DELETE_SLOT);
            stmt.setString(1, slot.getId());
            stmt.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while deleting slot id: " + slot.getId(), e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, false);
        }

        return false;
    }

    public static int addPaymentUnits(List<PaymentUnit> paymentUnits, String ipaddress) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(PaymentUnit.getCreatePaymentUnitQuery());
            int totalCount = 0;
            int batchCount = 0;
            for (PaymentUnit paymentUnit : paymentUnits) {
                int i = 1;
                Donor donor = paymentUnit.getDonor();
                s_staticInstance.setString(stmt, i++, paymentUnit.getTransactionId());
                s_staticInstance.setString(stmt, i++, paymentUnit.getFirstName());
                s_staticInstance.setString(stmt, i++, donor.getLastName());
                s_staticInstance.setString(stmt, i++, donor.getPhone());
                s_staticInstance.setString(stmt, i++, donor.getEmail());
                s_staticInstance.setString(stmt, i++, paymentUnit.getAmount());
                s_staticInstance.setString(stmt, i++,
                        donor.getSubscriptionType() != null ? donor.getSubscriptionType().getCode() : null);
                s_staticInstance.setString(stmt, i++,
                        paymentUnit.getExtraNotes() != null ? paymentUnit.getExtraNotes() : null);
                s_staticInstance.setString(stmt, i++, paymentUnit.isRequire80GReceipt() ? "Y" : "N");
                s_staticInstance.setString(stmt, i++,
                        donor.getAddress() == null ? null : donor.getAddress().serialize());
                s_staticInstance.setString(stmt, i++, donor.getPanNumber());
                s_staticInstance.setString(stmt, i++,
                        paymentUnit.getPgType() == null ? null : String.valueOf(paymentUnit.getPgType().getCode()));
                s_staticInstance.setString(stmt, i++, String.valueOf(paymentUnit.getPaymentType().getCode()));
                s_staticInstance.setString(stmt, i++,
                        paymentUnit.getIsPaymentSuccessful() == null || !paymentUnit.getIsPaymentSuccessful() ? "N"
                                : "Y");
                s_staticInstance.setString(stmt, i++, paymentUnit.getGatewayTransactionId());
                s_staticInstance.setString(stmt, i++, paymentUnit.getReceiptNumber());
                s_staticInstance.setTimestamp(stmt, i++, paymentUnit.getReceiptDate());
                s_staticInstance.setTimestamp(stmt, i++,
                        paymentUnit.getCreationDate() == null ? DateUtility.getCurrentDate()
                                : paymentUnit.getCreationDate());
                s_staticInstance.setString(stmt, i++, ipaddress);

                stmt.addBatch();
                batchCount++;

                if (batchCount == 100) {
                    totalCount += batchCount;
                    batchCount = 0;
                    stmt.executeBatch();
                }
            }

            if (batchCount > 0) {
                totalCount += batchCount;
                batchCount = 0;
                stmt.executeBatch();
            }
            s_logger.debug("query: " + stmt);
            conn.commit();
            return totalCount;
        } catch (Exception e) {
            s_logger.error("Error in saving payment units", e);
            try {
                conn.rollback();
            } catch (Exception e1) {
                s_logger.error("Error while rolling back the saving of the payment unit", e1);
            }
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
        return 0;
    }

    public static boolean deletePaymentUnit(String transactionId) {

        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            conn = s_staticInstance.getConnection(false);
            stmt2 = conn.prepareStatement("delete from payment_unit where txn_id=?");
            stmt2.setString(1, transactionId);
            s_logger.debug("query to delete payment_unit: " + stmt2);
            stmt2.execute();
            conn.commit();
            return true;
        } catch (Exception e) {
            s_logger.error("Error while deleting the donation from db", e);
            return false;
        } finally {
            s_staticInstance.closeAll(conn, stmt2, null, true);
        }

    }

    public static int getTotalDonatedAmount() {
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {

            conn = s_staticInstance.getReadOnlyConnection();
            pStmt = conn.prepareStatement("select sum(amount) from payment_unit where payment_status = 'Y'");
            s_logger.info("query for getting donation Amount: " + pStmt);
            rs = pStmt.executeQuery();
            int amount = 0;

            while (rs.next()) {
                amount = (int) rs.getLong(1);
            }

            return amount;

        } catch (Exception e) {
            s_logger.error("Error in fetching total donation amount", e);
        } finally {
            s_staticInstance.closeAll(conn, pStmt, rs, true);
        }
        return 0;

    }

    public static void addLibraryBranch(LibraryBranch newBranch) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(LibraryBranch.getInsertQuery());
            int i = 1;
            s_staticInstance.setString(stmt, i++, newBranch.getCode());
            s_staticInstance.setString(stmt, i++, newBranch.getName());
            s_staticInstance.setString(stmt, i++, newBranch.getIdPrefix());
            s_logger.debug("query: " + stmt);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static List<LibraryBranch> getAllLibraryBranches() {
        List<LibraryBranch> libraryBranches = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        Connection conn = null;
        try {
            conn = s_staticInstance.getReadOnlyConnection();
            stmt = conn.prepareStatement(SQL_SELECT_ALL_LIBRARY_BRANCHES);

            s_logger.debug("Library branch Fetch Query: " + stmt);

            result = stmt.executeQuery();
            if (result.isBeforeFirst()) {
                libraryBranches = new ArrayList<>();
            }
            while (result.next()) {
                libraryBranches.add(new LibraryBranch(result));
            }

            return libraryBranches;
        } catch (Exception e) {
            s_logger.error("Error in fetching the library branches", e);
        } finally {
            s_staticInstance.closeAll(conn, stmt, result, true);
        }

        return new ArrayList<>();
    }

    public static void addSlotTypeInDB(String slotType)  throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(SQL_INSERT_SLOT_TYPE);
            int i = 1;
            s_staticInstance.setString(stmt, i++, slotType);
            s_logger.debug("query: " + stmt);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static void deleteSlotType(String slotType) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = s_staticInstance.getConnectionForBatchInsert();
            stmt = conn.prepareStatement(SQL_DELETE_SLOT_TYPE);
            int i = 1;
            s_staticInstance.setString(stmt, i++, slotType);
            s_logger.info("query: " + stmt);
            stmt.execute();
            conn.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            s_staticInstance.closeAll(conn, stmt, null, true);
        }
    }

    public static List<String> getSlotTypes() {
            List<String> slotTypes = null;
            PreparedStatement stmt = null;
            ResultSet result = null;
            Connection conn = null;
            try {
                conn = s_staticInstance.getReadOnlyConnection();
                stmt = conn.prepareStatement(SQL_SELECT_ALL_SLOT_TYPES);

                s_logger.debug("slot types Fetch Query: " + stmt);

                result = stmt.executeQuery();
                if (result.isBeforeFirst()) {
                    slotTypes = new ArrayList<>();
                }
                while (result.next()) {
                    slotTypes.add(result.getString("slot_type"));
                }

                return slotTypes;
            } catch (Exception e) {
                s_logger.error("Error in fetching the slot types", e);
            } finally {
                s_staticInstance.closeAll(conn, stmt, result, true);
            }

            return new ArrayList<>();
    }

// public static List<LibraryBranch> getLibraryBranches() {
//
// PreparedStatement stmt = null;
// ResultSet result = null;
// Connection conn = null;
// try {
// conn = s_staticInstance.getReadOnlyConnection();
// stmt = conn.prepareStatement(LibraryBranch.getAllBranchesSQL());
// result = stmt.executeQuery();
// List<LibraryBranch> branches = null;
// if (result.isBeforeFirst()) {
// branches = new ArrayList<LibraryBranch>();
// }
//
// while (result.next()) {
// branches.add(new LibraryBranch(result));
// }
//
// return branches;
// } catch (Exception e) {
// s_logger.error("Error in fetching the Member list", e);
// } finally {
// s_staticInstance.closeAll(conn, stmt, result, true);
// }
//
// return new ArrayList<LibraryBranch>();
//
// }
}
