package org.communitylibrary.ui.navigation;

import org.communitylibrary.app.config.CacheConfiguration;

/**
 * @author nishant.gupta
 *
 */
public enum ContentActions {
    ABOUT("about", CacheConfiguration.STR_ABOUT_CONTENT),

    PRESS("press", CacheConfiguration.STR_PRESS_CONTENT),

    VOLUNTEER("volunteer_with_us", CacheConfiguration.STR_VOLUNTEER_CONTENT),

    DONATE("donate_to_us", CacheConfiguration.STR_DONATE_CONTENT),

    OMODEL("organising_model", CacheConfiguration.STR_OMODEL_CONTENT),

    D2DOPERATIONS("daytodayoperations", CacheConfiguration.STR_D2DOPERATIONS_CONTENT),

    ONOURSHELVES("on_our_shelves", CacheConfiguration.STR_ONOURSHELVES_CONTENT),
    
    POLICYDOCUMENTS("policy_documents", CacheConfiguration.STR_POLICYDOCUMENT_CONTENT),

    CURRICULLUM("curriculum", CacheConfiguration.STR_CURRICULLUM_CONTENT),

    LIBRARY_MOVEMENT("library-movement", CacheConfiguration.STR_LIBRARY_MOVEMENT),
    
    SOCIALMEDIAGUIDELINES("media-guidelines", CacheConfiguration.STR_SOCIAL_MEDIA_GUIDELINES),
    
    VOLUNTEEROPERATIONS("volunteer-operations", CacheConfiguration.STR_VOLUNTEER_OPERATIONS),
    

    ;

    private CacheConfiguration m_configType;
    private String             m_identifier;

    ContentActions(String param, CacheConfiguration configType) {
        m_identifier = param;
        m_configType = configType;
    }

    /**
     * @return the configType
     */
    public CacheConfiguration getConfigType() {
        return m_configType;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return m_identifier;
    }

    public static ContentActions getContentAction(String string) {
        try {
            for (ContentActions action : ContentActions.values()) {
                if (action.getIdentifier().equals(string)) {
                    return action;
                }
            }
        } catch (Exception e) {

        }
        return null;
    }
}
