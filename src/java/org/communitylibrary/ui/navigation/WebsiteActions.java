package org.communitylibrary.ui.navigation;

import org.communitylibrary.app.config.CacheConfiguration;
import org.communitylibrary.data.user.UserRoles;
import org.communitylibrary.logger.Logger;
import org.communitylibrary.ui.beans.AudienceBean;
import org.communitylibrary.ui.beans.BooksBean;
import org.communitylibrary.ui.beans.MembersBean;
import org.communitylibrary.ui.beans.PaymentBean;
import org.communitylibrary.ui.beans.WebsiteBean;
import org.communitylibrary.ui.security.CustomSecurityWrapperRequest;
import org.communitylibrary.ui.security.CustomSecurityWrapperResponse;
import org.communitylibrary.ui.session.SessionStore;
import org.communitylibrary.utils.ConfigStore;

public enum WebsiteActions {
    /*
     * Static Pages
     */
//    HOME("home", WebsitePages.HOME_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return WebsiteBean.homePage(request, response, queryParams, isPost);
//        }
//    },
//
//    ABOUT("about", WebsitePages.ABOUT_PAGE, UserRoles.getAllRoles(), false),
//
//    BLOGS("blogs", WebsitePages.ALL_BLOGS, UserRoles.getAllRoles(), false),
//
//    PRODUCTS("products", WebsitePages.PRODUCTS_PAGE, UserRoles.getAllRoles(), false),
//
//    SERVICES("services", WebsitePages.SERVICES_PAGE, UserRoles.getAllRoles(), false),
//
//    CAREERS("careers", WebsitePages.CAREERS_PAGE, UserRoles.getAllRoles(), false),
//
//    PRIVACY("privacy", WebsitePages.PRIVACY_PAGE, UserRoles.getAllRoles(), false),
//
//    TERMS("terms", WebsitePages.TERMS_PAGE, UserRoles.getAllRoles(), false),
//
    BROWSER_UNSUPPORTED("unsupported_browser", WebsitePages.UNSUPPORTED_BROWSER, UserRoles.getAllRoles(), false),
//
//    SWITCHTOADMIN("adminconsole", WebsitePages.NO_PAGE, UserRoles.getAdminPageLoggedInRoles(), false),
//
//    GALLERY("gallery", WebsitePages.GALLERY_PAGE, UserRoles.getAllRoles(), false),
//
//    EVENTS("events", WebsitePages.EVENTS_PAGE, UserRoles.getAllRoles(), false),
//
//    AJAX_GET_EVENTS("get_events", WebsitePages.NO_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.getEvents(request, response, queryParams, isPost);
//        }
//    },
//
//    AJAX_GET_GALLERYIMAGES("get_images", WebsitePages.NO_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.getGalleryPage(request, response, queryParams, isPost);
//        }
//    },
//
//    AJAX_GET_PUBLISHED_BLOGS("get_blogs", WebsitePages.NO_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.getBlogs(request, response, queryParams, isPost);
//        }
//    },
//
//    BLOGPOST("post", WebsitePages.BLOGPOST_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.getBlogpost(request, response, queryParams, isPost);
//        }
//    },
//
//    PAGE("page", WebsitePages.CUSTOM_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.getCustomPage(request, response, queryParams, isPost);
//        }
//    },
//
//    FORM("form", WebsitePages.FORM_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.getFormDisplayPage(request, response, queryParams, isPost);
//        }
//    },
//
//    FORM_RESPONSE("form_response", WebsitePages.FORM_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.saveFormResponse(request, response, queryParams, isPost);
//        }
//    },
//
//    /*
//     * Actions
//     */
//    CONTACT("contact", WebsitePages.CONTACT_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.addContactRequest(request, response, queryParams);
//        }
//    },
//
//    NEWSLETTER("newsletter", WebsitePages.TERMS_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return AudienceBean.subscribeToNewsLetter(request, response, queryParams);
//        }
//    },

    ADD_MEMBERS("add_members", WebsitePages.ADD_MEMBERS_PAGE, UserRoles.getLoggedInRoles(), false) {
        @Override
        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                String[] queryParams, boolean isPost) throws Exception {
            return MembersBean.getManageMembersPage(request, response, queryParams, this);
        }
    },

    DISPLAY_MEMBER("display_members", WebsitePages.VIEW_MEMBERS_PAGE, UserRoles.getLoggedInRoles(), false) {
        @Override
        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                String[] queryParams, boolean isPost) throws Exception {
            return MembersBean.getDisplayMembersPage(request, response, queryParams, this);
        }
    },

//    CONTENT("content", WebsitePages.ContentPage, UserRoles.getAllRoles(), false) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, ContentActions contentAction, boolean isPost) throws Exception {
//            return AudienceBean.getContentPage(request, response, queryParams, contentAction);
//        }
//    },
//
//    DONATE("donate_to_us", WebsitePages.DONATION_PAGE, UserRoles.getAllRoles(), false) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//
//            boolean isUseOnlinePayments = ConfigStore.getBooleanValue(CacheConfiguration.BOL_USE_ONLINE_DONATIONS,
//                    false);
//            
//            if (!isUseOnlinePayments) {
//                throw new WebsiteException(WebsiteExceptionType.INVALID_ACCESS);
//            }
//            
//            PaymentBean paymentBean = new PaymentBean();
//            return paymentBean.getPaymentsPage(request, response, queryParams);
//        }
//    },
//
//    PAYMENT("payment", WebsitePages.PaymentsStatusPage, UserRoles.getAllRoles(), false) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            PaymentBean paymentBean = new PaymentBean();
//            return paymentBean.getPaymentStatusPage(request, response, queryParams);
//        }
//    },
//
//    BOOKS("books", WebsitePages.BooksListingPage, UserRoles.getAllRoles(), false) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return BooksBean.getBooksPage(request, response, queryParams);
//        }
//    },

//    LOGIN("login", WebsitePages.LOGIN_PAGE, UserRoles.getNoLoggedInRoles(), true) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return WebsiteBean.login(request, response, isPost);
//        }
//    },
//
//    LOGOUT("logout", WebsitePages.LOGIN_PAGE, UserRoles.getLoggedInRoles(), true) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return WebsiteBean.logout(request, response);
//        }
//    },

    AJAX_GET_MEMBER("get_members", WebsitePages.NO_PAGE, UserRoles.getLoggedInRoles(), false) {
        @Override
        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
                String[] queryParams, boolean isPost) throws Exception {
            return MembersBean.getAjaxMembers(request, response, queryParams, this);
        }
    },
    
//    HOMEPAGE_LINKS("tclp", WebsitePages.HOMEPAGELINKS, UserRoles.getLoggedInRoles(), false) {
//        @Override
//        public WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
//                String[] queryParams, boolean isPost) throws Exception {
//            return WebsiteBean.getHomePageWithLinks(request, response, queryParams);
//        }
//    },

    ;

    /*
     * Static Variables and Functions
     */
    private static final Logger s_logger = Logger.getInstance(WebsiteActions.class);

    public static WebsiteActions getUIAction(String action) {
        s_logger.info("WebsiteAction: " + action);

        if (action == null || action.length() < 1) {
            throw new RuntimeException("Unknown action");
        }
        
        WebsiteActions[] actions = WebsiteActions.values();
        for (int i = 0; i < actions.length; i++) {
            if (actions[i].getDisplayName().equalsIgnoreCase(action)) {
                return actions[i];
            }
        }
        throw new RuntimeException("Unknown action");
    }

    public final WebsitePages executeAction(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, ContentActions contentAction, boolean isPost)
            throws Exception {

        if (s_logger.isDebugEnabled()) {
            StringBuilder buf = new StringBuilder();
            buf.append("Executing action: ").append(m_displayName).append(", params{");
            for (int i = 0; (queryParams != null) && (i < queryParams.length); i++) {
                if (i != 0) {
                    buf.append(",");
                }
                buf.append(queryParams[i]);
            }
            buf.append("}").append(contentAction != null ? "content action: " + contentAction.name() : "");
            s_logger.debug(buf.toString());
        }
        return execute(request, response, queryParams, contentAction, isPost);

    }

    public static WebsiteActions deserializeUIAction(String action) {
        if (action == null || action.length() < 1) {
            return null;
        }

        WebsiteActions[] actions = WebsiteActions.values();
        for (int i = 0; i < actions.length; i++) {
            if (actions[i].getDisplayName().equalsIgnoreCase(action)) {
                return actions[i];
            }
        }
        return null;
    }

    /*
     * Object Variables and Functions
     */
    private String       m_displayName;
    private UserRoles[]  m_defaultAllowedRoles;
    private WebsitePages m_defaultUIPage        = null;
    private boolean      m_requiresCsrfCheck    = false;
    private boolean      m_storeAccessEntry     = false;
    private boolean      m_isAllowedOnDashBoard = false;

    WebsiteActions(String displayName, UserRoles[] allowedRoles, boolean requiresCsrfCheck, boolean storeAccessEntry,
            boolean isAllowedOnDashBoard) {
        m_displayName = displayName;
        m_defaultAllowedRoles = allowedRoles;
        m_requiresCsrfCheck = requiresCsrfCheck;
        m_storeAccessEntry = storeAccessEntry;
        m_isAllowedOnDashBoard = isAllowedOnDashBoard;
    }

    WebsiteActions(String displayName, WebsitePages page, UserRoles[] allowedRoles, boolean isAllowedOnDashBoard) {
        this(displayName, allowedRoles, false, false, false);
        m_defaultUIPage = page;
        m_isAllowedOnDashBoard = isAllowedOnDashBoard;
    }

    public String getDisplayName() {
        return m_displayName;
    }

    public boolean isStoreAccessEntry() {
        return m_storeAccessEntry;
    }

    public boolean requiresCsrfCheck() {
        return m_requiresCsrfCheck;
    }

    public boolean isAllowedOnDashBoard() {
        return m_isAllowedOnDashBoard;
    }

    public final WebsitePages executeAction(CustomSecurityWrapperRequest request,
            CustomSecurityWrapperResponse response, String[] queryParams, boolean isPost) throws Exception {
        if (s_logger.isDebugEnabled()) {
            StringBuilder buf = new StringBuilder();
            buf.append("Executing action: ").append(m_displayName).append(", params{");
            for (int i = 0; (queryParams != null) && (i < queryParams.length); i++) {
                if (i != 0) {
                    buf.append(",");
                }
                buf.append(queryParams[i]);
            }
            buf.append("}");
            s_logger.debug(buf.toString());
        }
        return execute(request, response, queryParams, isPost);
    }

    public String getActionURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response) {
        return getActionURL(request, response, null,false);
    }

    public String getActionURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams) {
        return getActionURL(request, response, queryParams, false);
    }

    public String getActionURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams, boolean forceSSL) {
        StringBuilder b = new StringBuilder(128);
        // int serverPort = request.getServerPort();
        // b.append(forceSSL ? "https://" :
        // "http://").append(request.getServerName());
        // if (serverPort != 80 && serverPort != 443) {
        // b.append(":").append(serverPort);
        // }
        // b.append(WebsiteNavigation.getServletPath());
        //
        // if (requiresCsrfCheck()) {
        // b.append(getCsrfUrlToken(request));
        // }

        //Remove please if ssl not present.
        forceSSL = false;
        
        String requestURL = request.getRequestURL().toString();
        requestURL = WebsiteNavigation.extractInitialURL(requestURL, WebsiteNavigation.getServletPath());

        if (forceSSL && !requestURL.startsWith("https")) {
            requestURL = "https" + requestURL.substring(4);
        }

        b.append(requestURL);
        b.append("/").append(getDisplayName());
        if (queryParams != null) {
            for (int i = 0; i < queryParams.length; i++) {
                b.append("/").append(queryParams[i]);
            }
        }

        // Removed Encode URL for security reasons
        // if (response != null) {
        // return response.encodeURL(url);
        // } else {
        return b.toString();
    }

    public String getActionURL(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams, ContentActions contentAction, boolean forceSSL) {
        String actionURL = getActionURL(request, response, queryParams, forceSSL);

        if (contentAction != null) {
            actionURL += "/" + contentAction.getIdentifier();
        }

        return actionURL;
    }

    public static String getCsrfUrlToken(CustomSecurityWrapperRequest request) {
        String urlToken = SessionStore.getCsrfUrlToken(request);
        if (urlToken == null) {
            return "";
        } else {
            return "/" + WebsiteNavigation.CSRF_URL_PREPEND + SessionStore.getCsrfUrlToken(request);
        }
    }

    public String getCsrfPostInputEntry(CustomSecurityWrapperRequest request) {
        if (requiresCsrfCheck()) {
            String csrfPostToken = SessionStore.getCsrfPostToken(request);
            if (csrfPostToken != null) {
                return "<input type=\"hidden\" name=\"" + WebsiteNavigation.CSRF_POST_PARAM + "\" value=\""
                        + csrfPostToken + "\" \\>";
            }
        }
        return "";
    }

    public String getCsrfPostParameter(CustomSecurityWrapperRequest request) {
        if (requiresCsrfCheck()) {
            String csrfPostToken = SessionStore.getCsrfPostToken(request);
            if (csrfPostToken != null) {
                return "&" + WebsiteNavigation.CSRF_POST_PARAM + "=" + csrfPostToken;
            }
        }
        return "";
    }

    public boolean isRoleDefaultAllowed(UserRoles role) {
        for (int i = 0; i < m_defaultAllowedRoles.length; i++) {
            if (m_defaultAllowedRoles[i] == role) {
                return true;
            }
        }
        return false;
    }

    protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams, ContentActions contentAction, boolean isPost) throws Exception {
        if (contentAction != null) {
            throw new RuntimeException("need custom handler to get content.");
        }

        return execute(request, response, queryParams, isPost);
    }

    protected WebsitePages execute(CustomSecurityWrapperRequest request, CustomSecurityWrapperResponse response,
            String[] queryParams, boolean isPost) throws Exception {
        if (m_defaultUIPage != null) {
            return m_defaultUIPage;
        } else {
            throw new RuntimeException("Cannot rely on default execute when default page is not specified.");
        }
    }

    public String getServletPath() {
        return WebsiteNavigation.getServletPath();
    }
}
