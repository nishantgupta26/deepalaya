package org.communitylibrary.ui.view;

/**
 * @author nishant.gupta
 *
 */
public enum SearchParam {
    ALL, NAME, ID, MOTHERNAME, FATHERNAME, ADDRESS, GROUP, SCHOOL, CLASS,;

    public boolean isSearchParamSupported(SearchParam name) {
        return this == SearchParam.ALL || this == name;
    }
}
