package org.communitylibrary.ui.view.dto;

import java.util.Iterator;
import java.util.List;

import org.communitylibrary.data.LibraryBranch;
import org.communitylibrary.data.common.Phone;
import org.communitylibrary.data.members.LibraryMember;
import org.communitylibrary.ui.view.SearchParam;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class ViewMemberDTO {

    private String              m_draw;
    private String              m_start;
    private String              m_length;
    private String              m_search;

    private final String        tableName = "library_members";

    private List<LibraryMember> members   = null;
    private int                 m_totalRecords;
    // private boolean m_nameOnly;
    private SearchParam         m_searchParam;
    private LibraryBranch       m_libraryBranch;

    public ViewMemberDTO(String draw, String start, String length, String searchQuery) {
        m_draw = draw;
        m_start = start;
        m_length = length;
        m_search = searchQuery;
        m_searchParam = SearchParam.ALL;
        if (draw == null) {
            m_searchParam = SearchParam.NAME;
        } else {
            if (m_search != null) {

                if (m_search.toLowerCase().startsWith("name-")) {
                    m_searchParam = SearchParam.NAME;
                    m_search = m_search.substring(5);
                } else if (m_search.toLowerCase().startsWith("id-")) {
                    m_searchParam = SearchParam.ID;
                    m_search = m_search.substring(3);
                } else if (m_search.toLowerCase().startsWith("mother-")) {
                    m_searchParam = SearchParam.MOTHERNAME;
                    m_search = m_search.substring(7);
                } else if (m_search.toLowerCase().startsWith("father-")) {
                    m_searchParam = SearchParam.FATHERNAME;
                    m_search = m_search.substring(7);
                } else if (m_search.toLowerCase().startsWith("group-")) {
                    m_searchParam = SearchParam.GROUP;
                    m_search = m_search.substring(6);
                } else

                if (m_search.toLowerCase().startsWith("school-")) {
                    m_searchParam = SearchParam.SCHOOL;
                    m_search = m_search.substring(7);
                } else

                if (m_search.toLowerCase().startsWith("class-")) {
                    m_searchParam = SearchParam.CLASS;
                    m_search = m_search.substring(6);
                } else

                if (m_search.toLowerCase().startsWith("address-")) {
                    m_searchParam = SearchParam.ADDRESS;
                    m_search = m_search.substring(8);
                }
            }
        }

    }

    /**
     * @return the draw
     */
    public String getDraw() {
        return m_draw;
    }

    /**
     * @return the start
     */
    public String getStart() {
        return m_start;
    }

    /**
     * @return the length
     */
    public String getLength() {
        return m_length;
    }

    /**
     * @return the search
     */
    public String getSearch() {
        return m_search;
    }

    public String createSearchQuery() {
        StringBuilder sql = new StringBuilder("select * from ");
        sql.append(tableName);

        if (m_search != null) {
            String search = m_search.toLowerCase();
            sql.append(" where ");

            if (m_libraryBranch != null) {
                sql.append("branch_code=\"").append(m_libraryBranch.getCode()).append("\" AND (");
            }

            boolean isParamAdded = false;
            if (m_searchParam.isSearchParamSupported(SearchParam.NAME)) {
                sql.append("LCASE(name) like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_searchParam.isSearchParamSupported(SearchParam.ID)) {
                if (isParamAdded) {
                    sql.append(" OR ");
                }
                sql.append("id like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_searchParam.isSearchParamSupported(SearchParam.MOTHERNAME)) {
                if (isParamAdded) {
                    sql.append(" OR ");
                }
                sql.append("LCASE(mother_name) like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_searchParam.isSearchParamSupported(SearchParam.FATHERNAME)) {
                if (isParamAdded) {
                    sql.append(" OR ");
                }
                sql.append("LCASE(father_name) like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_searchParam.isSearchParamSupported(SearchParam.ADDRESS)) {
                if (isParamAdded) {
                    sql.append(" OR ");
                }
                sql.append("LCASE(address) like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_searchParam.isSearchParamSupported(SearchParam.GROUP)) {
                if (isParamAdded) {
                    sql.append(" OR ");
                }
                sql.append("LCASE(library_group) like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_searchParam.isSearchParamSupported(SearchParam.SCHOOL)) {
                if (isParamAdded) {
                    sql.append(" OR ");
                }
                sql.append("LCASE(school) like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_searchParam.isSearchParamSupported(SearchParam.CLASS)) {
                if (isParamAdded) {
                    sql.append(" OR ");
                }
                sql.append("LCASE(class) like \"%").append(search).append("%\"");
                isParamAdded = true;
            }

            if (m_libraryBranch != null) {
                sql.append(")");
            }

            sql.append(" order by name, id");

        } else {

            if (m_libraryBranch != null) {
                sql.append("branch_code=").append(m_libraryBranch.getCode());
            }

            sql.append(" order by LCASE(name), id");
            sql.append(" limit ").append(m_start).append(",").append(m_length != null ? m_length : "25");
        }
        return sql.toString();
    }

    /**
     * @return the members
     */
    public List<LibraryMember> getMembers() {
        return members;
    }

    /**
     * @param members the members to set
     */
    public void setMembers(List<LibraryMember> members) {
        this.members = members;
    }

    public void add(LibraryMember libraryMember) {
        members.add(libraryMember);
    }

    /**
     * @return the totalRecords
     */
    public int getTotalRecords() {
        return m_totalRecords;
    }

    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(int totalRecords) {
        m_totalRecords = totalRecords;
    }

    public int getRecordsFiltered() {
        if (m_search != null) {
            return members == null ? 0 : members.size();
        }

        return members == null ? 0 : m_totalRecords;
    }

    public String getMembersJSON() {
        if (members == null) {
            return "[]";
        }
        StringBuilder data = new StringBuilder("[");
        Iterator<LibraryMember> iter = members.iterator();
        while (iter.hasNext()) {
            LibraryMember member = iter.next();
            data.append("[");

            data.append("\"").append(member.getId()).append("\"");
            data.append(",\"").append(member.getName()).append("\"");
            data.append(",\"").append(member.getGender().name()).append("\"");
            data.append(",\"").append(StringUtility.trimWithNullAndEmptyAsUnset(member.getDob())).append("\"");
            data.append(",\"").append(Phone.getPhoneNumberString(member.getPhone())).append("\"");
            data.append(",\"").append(StringUtility.trimWithNullAndEmptyAsUnset(member.getMotherName())).append("\"");
            data.append(",\"").append(StringUtility.trimWithNullAndEmptyAsUnset(member.getFatherName())).append("\"");
            data.append(",\"").append(StringUtility.trimWithNullAndEmptyAsUnset(member.getAddress())).append("\"");
            data.append(",\"").append(member.getLibraryGroup() != null ? member.getLibraryGroup().name() : "Unset")
                    .append("\"");
            data.append(",\"").append(StringUtility.trimWithNullAndEmptyAsUnset(member.getSchool())).append("\"");
            data.append(",\"").append(StringUtility.trimWithNullAndEmptyAsUnset(member.getStudyClass())).append("\"");
            data.append(",\"").append(StringUtility.trimAndNullIsEmpty(member.getPhotoId())).append("\"");
            data.append(",\"").append("<form id='action_").append(member.getId())
                    .append("'><a onclick='view_members.getPrintablePage(\\\"").append(member.getId())
                    .append("\\\")'>Print</a><a onclick='view_members.editMember(\\\"").append(member.getId())
                    .append("\\\")'>Edit</a><a onclick='view_members.deleteMember(\\\"").append(member.getId())
                    .append("\\\")'>Delete</a></form>").append("\"");

            data.append("]");

            if (iter.hasNext()) {
                data.append(",");
            }
        }
        data.append("]");
        return data.toString();
    }

    public void setLibraryBranch(LibraryBranch libraryBranch) {
        m_libraryBranch = libraryBranch;

    }

}
