package org.communitylibrary.timer;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.communitylibrary.ui.beans.AudienceBean;
import org.communitylibrary.utils.ConfigStore;
import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.PaymentConfigStore;

public class TimerUtility {
    private static Logger    s_logger           = Logger.getLogger(TimerUtility.class);
    private static Timer     m_timer            = null;
    public static final long ONE_HOUR_IN_MILLIS = 1000 * 60 * 60;
    public static final long ONE_DAY_IN_MILLIS  = 24 * 60 * 60 * 1000;

    public static void initialize() {
        if (m_timer == null) {
            m_timer = new Timer("Website Timer", true);
        }

        TimerTask everyDayRunTask = new TimerTask() {
            boolean isFirstRun = true;

            @Override
            public void run() {
//                TimerUtility.runEveryDayTasks(isFirstRun);
                isFirstRun = false;
            }

        };

        reload();

        TimerTask reloadTask = new TimerTask() {

            @Override
            public void run() {
                TimerUtility.reload();
            }

        };

        m_timer.scheduleAtFixedRate(everyDayRunTask, DateUtility.getDateAfterNowWithHourMinuteSecond(10, 0, 0),
                ONE_DAY_IN_MILLIS);

        m_timer.scheduleAtFixedRate(reloadTask, DateUtility.getDateAfterNowWithHourMinuteSecond(10, 0, 0),
                15 * 60 * 1000);
    }

    protected static void reload() {
        ConfigStore.initialise();
        PaymentConfigStore.initialise();
    }

//    private static void runEveryDayTasks(boolean isFirstRun) {
//        AudienceBean.sendContactInquiryReport();
//        AudienceBean.sendNewsletterSubscriptionReport(isFirstRun);
//        // AudienceBean.deactivatePopupRequests();
//    }

    public static void shutdown() {
        if (m_timer != null) {
            try {
                m_timer.cancel();
                m_timer.purge();
                s_logger.info("Timer to download files from FTP and reload cache stopped");
            } catch (Throwable t) {
                s_logger.error("Error in canceling timer task.", t);
            }
            m_timer = null;
        }
    }
}
