package org.thecommunitylibraryproject.standalone.dataentry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.communitylibrary.data.books.Book;
import org.communitylibrary.data.books.BookAccount;
import org.communitylibrary.data.books.util.BookLoader;
import org.communitylibrary.data.books.util.BookLoaderAgent;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.util.CollectionsUtil;
import org.communitylibrary.utils.BookUtility;
import org.communitylibrary.utils.NumberUtility;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class BooksMetadataPopulator {

    private static Logger s_logger = Logger.getLogger(BooksMetadataPopulator.class);

    // private static ExecutorService threadService =
    // Executors.newFixedThreadPool(50);

    public static void main(String[] args) {
        // location to store response from the 3d party api
        // folder to get CSV file from
        if (args.length < 2) {
            System.out.println("Could not run the loader");
            return;
        }

        String locationToFetchCSVFile = args[0];
        String locationToStore = args[1];

        File from = new File(locationToFetchCSVFile);
        File to = new File(locationToStore);

        if (!from.exists() || !from.isDirectory()) {
            System.out.println("need directory to fetch csv data to load");
            return;
        }

        if (!to.exists() || !to.isDirectory()) {
            System.out.println("need directory to fetch csv data to load");
            return;
        }

        FilenameFilter csvFileFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith("csv");
            }
        };

        String[] filesInFolder = from.list(csvFileFilter);
        if (filesInFolder == null || filesInFolder.length == 0) {
            System.out.println("No CSV file to load");
        }

        List<Book> booksFromCSVFiles = new ArrayList<Book>();
        for (String file : filesInFolder) {
            File csvFileToLoad = new File(from, file);
            List<Book> books = getBooksFromCSVFile(csvFileToLoad);
            if (!CollectionsUtil.isEmpty(books)) {
                booksFromCSVFiles.addAll(books);
            } else {
                System.out.println("no books loaded from csv file: " + file);
            }
        }

        boolean booksSaved = DBUtil.saveBooks(booksFromCSVFiles);
        String booksSaveResponse = "Books saved? : " + booksSaved;
        s_logger.info(booksSaveResponse);
        System.out.println(booksSaveResponse);
    }

    private static List<Book> getBooksFromCSVFile(File csvFileToLoad) {
        List<Book> books = null;
        BufferedReader br = null;
        String line = null;
        Map<String, Book> addedBooksFromSheet = new HashMap<String, Book>();
        try {
            br = new BufferedReader(new FileReader(csvFileToLoad));
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i == 0) {
                    books = new ArrayList<Book>();
                    i++;
                    continue;
                }
                i++;
                if (line.toLowerCase().contains("intentionally left blank")) {
                    s_logger.info("not adding: " + line);
                    continue;
                }
                String[] tokens = StringUtility.splitNullForEmpty(line, "[|]");
                if (tokens == null || tokens.length < 4) {
                    s_logger.info(i + " - problem tokenizing line: " + line + " in file: " + csvFileToLoad.getName());
                    continue;
                }

                String accountNumber = StringUtility.trimAndEmptyIsNull(tokens[0]);
                String isbn = StringUtility.trimAndEmptyIsNull(tokens[1]);
                if (isbn != null) {
                    isbn = BookUtility.convertToFlatISBN(isbn);
                }
                String author = StringUtility.trimAndEmptyIsNull(tokens[2]);
                String title = StringUtility.trimAndEmptyIsNull(tokens[3]);

                Book book;
                if (accountNumber == null && author == null && title == null && isbn != null) {
                    book = books.get(books.size() - 1);
                    boolean isIsbnSet = book.setIsbn(isbn);
                    if (!isIsbnSet) {
                        s_logger.info("Could not set isbn for book: " + book + ", isbn: " + isbn);
                    }
                } else {
                    book = new Book();
                    String bookCode = null;
                    if (accountNumber != null) {
                        bookCode = StringUtility.extractDigits(accountNumber);
                        if (bookCode != null) {
                            book.setBookCode(NumberUtility.parsetIntWithDefaultOnErr(bookCode, 0));
                        }

                        BookAccount bookType = BookAccount.getBookAccount(accountNumber);
                        if (bookType == null) {
                            s_logger.info("booktype null: " + accountNumber);
                        }
                        book.setBookType(bookType);
                    }
                    if (author != null) {
                        String[] authorTokens = StringUtility.splitNullForEmpty(author, ";");
                        if (authorTokens != null) {
                            List<String> authors = new ArrayList<String>();
                            for (String authorToken : authorTokens) {
                                authorToken = StringUtility.trimAndEmptyIsNull(authorToken);
                                if (authorToken != null) {
                                    authors.add(authorToken);
                                }
                            }
                            if (!authors.isEmpty()) {
                                book.setAuthors(authors);
                            }
                        }
                    }

                    book.setTitle(title);
                    book.setIsbn(isbn);
                    if (!addedBooksFromSheet.containsKey(bookCode) && book.isAdd()) {
                        book.setNumCopies(1);
                        books.add(book);
                        addedBooksFromSheet.put(bookCode, book);
                    } else {
                        Book addedBook = addedBooksFromSheet.get(bookCode);
                        if (addedBook != null) {
                            addedBook.setNumCopies(addedBook.getNumCopies() + 1);
                        } else {
                            s_logger.info("not adding: " + line + " because problem with book");
                        }
                    }
                }

            }
        } catch (Exception e) {
            System.out.println("Error in loading csv file: " + csvFileToLoad.getName() + ", line: " + line);
            s_logger.error("Error in loading csv file: " + csvFileToLoad.getName() + ", line: " + line, e);
            return null;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    s_logger.error("Error while closing the bufferedReader");
                }
            }
        }

        return books;
    }
}

class BookPopulator implements Runnable {
    private Book          m_book;
    private static Logger s_logger = Logger.getLogger(BookPopulator.class);

    public BookPopulator(Book book) {
        m_book = book;
    }

    @Override
    public void run() {
        BookLoader bookLoader = BookLoaderAgent.Google.loaderInstance(m_book);
        if (bookLoader != null) {
            bookLoader.populateBookDetailsFromAPI();
        } else {
            s_logger.info("loader instance could not be fetched");
        }
    }

}
