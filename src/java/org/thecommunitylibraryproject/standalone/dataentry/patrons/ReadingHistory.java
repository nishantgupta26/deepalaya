package org.thecommunitylibraryproject.standalone.dataentry.patrons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Calendar;

import org.communitylibrary.utils.DateUtility;
import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class ReadingHistory {

    /**
     * args[0] will be the csv file containing id and number of read books,
     * output will be an excel sheet with format "cardnumber, Accessionnumber,
     * IssueDate, DueDate, Return Date"
     * 
     * Cardnumber format: TCLPD<number> Cardnumber format: TCLPA<number>
     * 
     * 
     * @param args
     */
    public static void main(String[] args) {
        String inputCSVFile = args[0];
        System.out.println(inputCSVFile);
        File inputFile = new File(inputCSVFile);
        if (!inputFile.exists()) {
            System.out.println("Input file does not exists");
            System.exit(0);
        }

        BufferedReader br = null;
        String line = null;
        PrintWriter out = null;
        try {
            br = new BufferedReader(new FileReader(inputFile));
            out = new PrintWriter(new FileWriter(new File(inputFile.getParent(), "converted-" + inputFile.getName())));
            String header = "cardnumber,Accessionnumber,IssueDate,DueDate,Return Date";

            String accessionNumber = "E/FG 11";

            String newline = StringUtility.getNewLineString();
            int num = 0;
            // HashSet<String> nonMembers = new HashSet<String>();
            while ((line = br.readLine()) != null) {
                if (num == 0) {
                    out.write(header);
                    out.write(newline);
                    num++;
                    continue;
                }

                num++;
                String[] tokens = line.split(",", -1);
                // String cardNumber = "TCLPD-" + tokens[0];
                String cardNumber = tokens[0];
                tokens[1] = tokens[1].trim();
                if (tokens[1].isEmpty() || "0".equals(tokens[1])) {
                    // nonMembers.add(cardNumber);
                    String row = cardNumber + ",,,,";
                    out.write(row);
                    out.write(newline);
                    continue;
                }
                int numberOfBooks = Integer.parseInt(tokens[1]);
                // if (numberOfBooks == 0) {
                // continue;
                // }

                Calendar currentCal = Calendar.getInstance();
                while (numberOfBooks != 0) {
                    // currentIssueDate is 1 week before currentduedate
                    // currentreturndate is currentduedate
                    String currentDueDate = DateUtility.getDateInYYYYMMDDDash(currentCal);
                    String currentReturnDate = currentDueDate;
                    currentCal.add(Calendar.DATE, -7);
                    String currentIssueDate = DateUtility.getDateInYYYYMMDDDash(currentCal);
                    String row = cardNumber + "," + accessionNumber + "," + currentIssueDate + "," + currentDueDate
                            + "," + currentReturnDate;

                    out.write(row);
                    out.write(newline);
                    numberOfBooks--;
                }

            }
        } catch (Exception e) {
            System.out.println("Error in working with line: " + line);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }

                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (Exception e) {
                System.out.println("could not close stream");
            }
        }
    }

}
