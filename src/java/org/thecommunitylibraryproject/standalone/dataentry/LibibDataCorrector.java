/**
 * 
 */
package org.thecommunitylibraryproject.standalone.dataentry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.communitylibrary.utils.StringUtility;

/**
 * @author nishant.gupta
 *
 */
public class LibibDataCorrector {

    /**
     * @param args
     */
    public static void main(String[] args) {
        BufferedReader br = null;
        try {

            String headerLine = "Account number,ISBN,Author/Publication,Title,Copies,Status";
            String newLine = "\r\n";

            br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Hi. Welcome to TCLP CSV data mapper");
            System.out.println("Please enter the full path of the CSV file");
            String filePath = br.readLine();
            File file = new File(filePath);
            if (!file.exists()) {
                System.out.println("The file does not exists. Please make sure to copy correct path");
                throw new RuntimeException();
            }
            File parent = file.getParentFile();
            File newFile = new File(parent, "corrected-" + file.getName());

            InputStream ins = null;
            InputStreamReader isr = null;
            BufferedReader reader = null;

            int lineNo = 0;
            String line = null;
            PrintWriter writer = null;
            try {
                ins = new FileInputStream(file);
                isr = new InputStreamReader(ins);
                reader = new BufferedReader(isr);
                writer = new PrintWriter(new FileWriter(newFile));
                writer.write(headerLine);
                writer.write(newLine);
                while ((line = reader.readLine()) != null) {
                    if (lineNo == 0) {
                        // skipping the header row
                        lineNo++;
                        continue;
                    }

                    if (line.trim().isEmpty()) {
                        continue;
                    }

                    String[] tokens = StringUtility.splitNullForEmpty(line, "[,]");
                    if (tokens == null || tokens.length == 0) {
                        continue;
                    }

                    lineNo++;
                    String author = StringUtility.trimAndEmptyIsNull(tokens[0]);
                    String publisher = StringUtility.trimAndEmptyIsNull(tokens[16]);
                    String title = StringUtility.trimAndEmptyIsNull(tokens[3]);
                    String isbn13 = StringUtility.trimAndEmptyIsNull(tokens[21]);
                    String isbn10 = StringUtility.trimAndEmptyIsNull(tokens[20]);
                    String copies = StringUtility.trimAndEmptyIsNull(tokens[14]);

                    if (author != null) {
                        StringBuilder authorBuilder = new StringBuilder(author);
                        int indexOfBy = authorBuilder.indexOf("by ");
                        if (indexOfBy != -1) {
                            authorBuilder.delete(indexOfBy, indexOfBy + 3);
                        }

                        int indexOfAuthor = authorBuilder.indexOf("(Author)");
                        if (indexOfAuthor != -1) {
                            authorBuilder.delete(indexOfAuthor, indexOfAuthor + 8);
                        }

                        author = authorBuilder.toString().trim();

                        boolean b = author.matches("^[\u0000-\u0080]+$");
                        if (!b) {
                            author = null;
                        }
                    }

                    StringBuilder lineToWrite = new StringBuilder(",");
                    lineToWrite.append(isbn13 != null ? isbn13 : (isbn10 != null ? isbn10 : "")).append(",");

                    if (!(author == null && publisher == null)) {
                        lineToWrite.append(convertToValidName(author != null ? author : publisher));
                    }
                    lineToWrite.append(",");

                    lineToWrite.append(title).append(",");
                    lineToWrite.append(copies == null ? 1 : copies).append(",");
                    writer.write(lineToWrite.toString());
                    writer.write(newLine);
                }
            } finally {
                reader.close();
                isr.close();
                ins.close();
                writer.close();
            }

        } catch (Exception e) {
            System.out
                    .println("There had been some problem with mapping of the file. Please try again or contact nishant at reachnishant26@gmail.com if problem persists");
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    System.out.println("Potential memory leak, could not close buffered reader");
                }
            }
        }

        System.out.println("Completed!");

    }

    private static String convertToValidName(String string) {
        StringBuilder newString = new StringBuilder(string);
        int indexOfQuotes = newString.indexOf("\"");
        while (indexOfQuotes != -1) {
            newString.deleteCharAt(indexOfQuotes);
            indexOfQuotes = newString.indexOf("\"");
        }
        string = newString.toString().trim();
        String[] tokens = string.split("[ ]");
        if (tokens.length > 1) {
            String convertedString = "\"" + tokens[tokens.length - 1] + ","
                    + StringUtility.join(tokens, " ", 0, tokens.length > 2 ? tokens.length - 2 : 0) + "\"";
            return convertedString;
        }

        return string;
    }
}
