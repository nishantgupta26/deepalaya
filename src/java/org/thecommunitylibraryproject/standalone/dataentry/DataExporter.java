package org.thecommunitylibraryproject.standalone.dataentry;

import java.io.FileWriter;
import java.util.List;

import org.communitylibrary.data.books.Book;
import org.communitylibrary.ui.navigation.DBUtil;
import org.communitylibrary.utils.StringUtility;

public class DataExporter {
    public static void main(String[] args) {
        List<Book> books = DBUtil.getBooks();

        if (books != null) {
            // forGoogleSheets(books);
            forKoha(books);
        } else {
            System.out.println("no books found");
        }
    }

    private static void forKoha(List<Book> books) {
        try {
            FileWriter writer = new FileWriter("E:/personal/workspace/communitylibrary/resources1/export_koha.csv");
            writer.write("Account| isbn|author|title|copies");
            writer.write(StringUtility.getNewLineString());
            for (Book book : books) {
                int numCopies = book.getNumCopies();
                String firstColumn = book.getBookType().getDisp() + " " + book.getBookCode();
                String str = firstColumn
                        + "|"
                        + (book.getIsbn10() == null && book.getIsbn13() == null ? "-"
                                : (book.getIsbn10() != null ? book.getIsbn10() : book.getIsbn13()));
                str += "|" + StringUtility.join(book.getAuthors(), ";") + "|" + book.getTitle() + "|" + numCopies;
                writer.write(str);
                writer.write(StringUtility.getNewLineString());
            }

            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void forGoogleSheets(List<Book> books) {
        try {
            FileWriter writer = new FileWriter("E:/personal/workspace/communitylibrary/resources1/export.csv");
            writer.write("Account| isbn10| isbn13|author|title");
            writer.write(StringUtility.getNewLineString());
            for (Book book : books) {
                int numCopies = book.getNumCopies();
                if (numCopies < 2) {
                    String firstColumn = book.getBookType().getDisp() + " " + book.getBookCode();
                    String str = firstColumn + "|" + (book.getIsbn10() == null ? "-" : book.getIsbn10()) + "|"
                            + (book.getIsbn13() == null ? "-" : book.getIsbn13());
                    str += "|" + StringUtility.join(book.getAuthors(), ";") + "|" + book.getTitle();
                    writer.write(str);
                    writer.write(StringUtility.getNewLineString());
                } else {

                    for (int i = 0; i < numCopies; i++) {
                        String firstColumn = book.getBookType().getDisp() + " " + book.getBookCode() + " ("
                                + (char) (65 + i) + ")";
                        String str = firstColumn + "|" + (book.getIsbn10() == null ? "-" : book.getIsbn10()) + "|"
                                + (book.getIsbn13() == null ? "-" : book.getIsbn13());
                        str += "|" + StringUtility.join(book.getAuthors(), ";") + "|" + book.getTitle();
                        writer.write(str);
                        writer.write(StringUtility.getNewLineString());
                    }

                }
            }

            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
