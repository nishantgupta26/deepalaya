var App = angular.module("App", []);

angular.module('App').controller("PaymentStatusController", function($scope) {
	if (!responseJson) {
		return;
	}

	$scope.status = responseJson;
	if(responseJson.transactionDetails) {
		$scope.transactionDetails = angular.copy(responseJson.transactionDetails);		
	}

});
