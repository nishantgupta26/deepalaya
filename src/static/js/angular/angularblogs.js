var App = angular.module("BlogsAPP", []);

angular.module('BlogsAPP').controller("BlogsController", function($scope, $http, $window) {

	
	$scope.getPreviousPage = function() {
		if($scope.dispPage  == 1) {
			return;
		}
		
		$scope.dispPage = $scope.dispPage - 1;
		$scope.dispPosts = [];
		
		var startIndex = ($scope.dispPage - 1) * $scope.numposts;
		for(var i = startIndex; i < startIndex + $scope.numposts; i++) {
			$scope.dispPosts.push(angular.copy($scope.allposts[i]));
		}
	}
	
	$scope.getNextPage = function() {
		if($scope.dispPage  < $scope.pageToLoad) {
			$scope.dispPage = $scope.dispPage + 1;
			$scope.dispPosts = [];
			
			var startIndex = ($scope.dispPage - 1) * $scope.numposts;
			for(var i = startIndex; i < startIndex + $scope.numposts; i++) {
				$scope.dispPosts.push(angular.copy($scope.allposts[i]));
			}
		} else {
			if($scope.hasMore) {
				$scope.loadBlogs();
			}
		}
	}
	
	$scope.isMorePostsAvailable = function() {
		
		if ($scope.dispPage < $scope.pageToLoad) {
			return true;
		}
		
		return $scope.hasMore;
	}
	
	$scope.haveMorePosts = function() {
		return !$scope.isMorePostsAvailable();
	}
	
	$scope.init = function() {
		$scope.pageToLoad = 0;
		$scope.numposts = numPosts;
		
		$scope.dispPage = 1;
		
		$scope.allposts = [];
		$scope.dispPosts = [];
		
		$scope.loadBlogs();
	}
	
	$scope.loadBlogs = function() {
		$scope.pageToLoad = $scope.pageToLoad + 1;
		var content = {
			page: $scope.pageToLoad,
			numPosts: $scope.numposts
		};
		
		var contentType = "application/json";
		var uRL = formURL;

		$http({
			url : uRL,
			method : 'POST',
			data : content,
			headers : {
				'Content-Type' : contentType
			}
		}).then(function mysucess(response) {
			if (response && response.data) {
				if (response.data.isSuccess && response.data.isSuccess === true && response.data.data) {
					$scope.dispPosts = [];
					for (var i = 0; i < response.data.data.length; i++) {
						$scope.allposts.push(angular.copy(response.data.data[i]));
						$scope.dispPosts.push(angular.copy(response.data.data[i]));
					}
					$scope.hasMore = response.data.hasMore;
					$scope.dispPage = $scope.pageToLoad;
				} else {
					$scope.hasMore = false;
					$scope.pageToLoad = $scope.pageToLoad - 1;
				}
			} else {
				$scope.hasMore = false;
				$scope.pageToLoad = $scope.pageToLoad - 1;
			}
		}, function error(response) {
			alert("Could not load blogposts");
			$scope.hasMore = false;
			$scope.pageToLoad = $scope.pageToLoad - 1;
		});
	}
	
	$scope.init();

});

App.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});
