var App = angular.module("App", [
  'ui.bootstrap'
]);

angular.module('App').controller("BooksListingController", function($scope, $http, $window) {

  var contentType = "application/json";
  $scope.baseURL = baseURL;
  $scope.totalBooks = totalBooks;
  $scope.currentPage = 1;
  $scope.perPage = 10;
  $scope.pagesLoaded = [];
  $scope.bookAccounts = bookAccounts;
  $scope.isRefresh = false;
  
  $scope.refresh = function() {
    $scope.books = [];
    for (var i = 0; i < $scope.totalBooks; i++) {
      $scope.books[i] = {};
    }
    
    $scope.pagesLoaded = [];

    $scope.dispbooks = [];
  }

  $scope.init = function() {
    $scope.refresh();

    $scope.getBooks();
  }

  $scope.getBooks = function() {
    var content = {
      page : $scope.currentPage,
      num : $scope.perPage
    };
    
    if($scope.filterObject) {
      content.filter = angular.copy($scope.filterObject);
    }

    var uRL = $scope.baseURL + "/fetch";

    $http({
      url : uRL,
      method : 'POST',
      data : content,
      headers : {
        'Content-Type' : contentType
      }
    }).then(function mysucess(response) {
      if (response && response.data && response.data.isSuccess === true) {
        if($scope.isRefresh === true) {
          if($scope.filterObject) {
            $scope.totalBooks = response.data.totalBooks;
          } else {
            $scope.totalBooks = totalBooks;  
          }
          
          $scope.refresh();
        }
        
        $scope.dispbooks = [];
        var startIndex = ($scope.currentPage - 1) * $scope.perPage;
        for (var i = 0; i < response.data.data.length; i++) {
          $scope.books[startIndex + i] = angular.copy(response.data.data[i]);
          $scope.dispbooks.push(angular.copy(response.data.data[i]));
        }

        $scope.pagesLoaded.push($scope.currentPage);
      } else {
        if($scope.isRefresh === true) {
          if($scope.filterObject) {
            $scope.totalBooks = 0;
            $scope.dispbooks = [];
            $scope.books = [];
          } else {
            $scope.totalBooks = totalBooks;  
          }
          
          $scope.refresh();
        }
        //APP.addError("Could not get books data");
      }
    }, function error(response) {
      //APP.addError("Could not get books data");
    });
  }

  $scope.pageChanged = function() {
    if ($scope.isPageLoaded() == false) {
      $scope.isRefresh = false;
      $scope.getBooks();
    } else {
      $scope.dispbooks = [];

      var startIndex = ($scope.currentPage - 1) * $scope.perPage;
      for (var i = startIndex; i < startIndex + $scope.perPage; i++) {
        if (i >= $scope.books.length) {
          break;
        }
        $scope.dispbooks.push(angular.copy($scope.books[i]));
      }
    }

    $scope.scrollToTop();

  };

  $scope.scrollToTop = function($var) {
    // 'html, body' denotes the html element, to go to any other custom element,
    // use '#elementID'
    $('html, body').animate({
      scrollTop : 0
    }, 'fast'); // 'fast' is for fast animation
  };

  $scope.isPageLoaded = function() {
    for (var i = 0; i < $scope.pagesLoaded.length; i++) {
      if ($scope.currentPage === $scope.pagesLoaded[i]) {
        return true;
      }
    }

    return false;
  };

  $scope.fetchBook = function(book) {
    var fullPath = $scope.baseURL + "/book?id=" + book.id;
    if (book.isbn10 != null) {
      fullPath += "&isbn10=" + book.isbn10;
    }

    if (book.isbn13 != null) {
      fullPath += "&isbn13=" + book.isbn13;
    }

    $window.open(fullPath, '_blank');
  }
  
  $scope.clearFilter = function() {
    if($scope.filterObject) {
      delete $scope.filterObject;
      $scope.currentPage = 1;
      $scope.isRefresh = true;
      $scope.getBooks();
    }
  }
  
  $scope.applyFilters = function() {
    $scope.currentPage = 1;
    $scope.isRefresh = true;
    $scope.getBooks();    
  }

  $scope.init();

});
