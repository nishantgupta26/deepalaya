var App = angular.module("App", []);

angular.module('App').controller("BookDisplayController", function($scope, $http, $window) {

  var contentType = "application/json";
  $scope.baseURL = baseURL;
  $scope.isbn10 = isbn10;
  $scope.isbn13 = isbn13;
  $scope.id = id;

  $scope.getBook = function() {
    var content = {};

    if ($scope.id) {
      content.id = $scope.id;
    }

    if ($scope.isbn10) {
      content.isbn10 = $scope.isbn10;
    }

    if ($scope.isbn13) {
      content.isbn13 = $scope.isbn13;
    }

    if ($scope.filterObject) {
      content.filter = angular.copy($scope.filterObject);
    }

    var uRL = $scope.baseURL + "/book/fetch";

    $http({
      url : uRL,
      method : 'POST',
      data : content,
      headers : {
        'Content-Type' : contentType
      }
    }).then(function mysucess(response) {
      if (response && response.data && response.data.isSuccess === true) {
        $scope.book = angular.copy(response.data.book);
      }
    }, function error(response) {
      // APP.addError("Could not get books data");
    });
  }

  $scope.getBook();

});
