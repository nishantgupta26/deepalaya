var App = angular.module("App", []);

angular.module('App').controller("DispFormController", function($scope, $http, $window) {
  if (!formResources) {
    return;
  }

  $scope.isFormPristine = true;

  $scope.fieldTypes = formResources.fieldTypes;

  $scope.userForm = angular.copy(formResources.form);

  $scope.isFormOkay = function() {

    for (var i = 0; i < $scope.userForm.fields.length; i++) {
      var field = $scope.userForm.fields[i];
      if (field.isMandatory === true) {
        if (!field.answer) {
          return false;
        }

        if (angular.isObject(field.answer)) {
          var isAnswerOkay = false;
          var isOtherAnswerOkay = true;

          for ( var key in field.answer) {
            if (key === 'Other' && field.answer[key] === true) {
              if (!field.otheranswer) {
                isOtherAnswerOkay = false;
                break;
              }
            } else {
              if (field.answer[key] === true) {
                isAnswerOkay = true;
              }
            }
          }

          if (isOtherAnswerOkay === false && isAnswerOkay === false) {
            return false;
          }
        } else {

          if (field.answer === 'Other') {
            if (!field.otheranswer) {
              return false;
            }
          }
        }
      }
    }

    return true;

  }

  $scope.submitResponse = function() {

    var content = angular.copy($scope.userForm);
    var contentType = "application/json";
    var uRL = formURL + "/saveResponse";

    $http({
      url : uRL,
      method : 'POST',
      data : content,
      headers : {
        'Content-Type' : contentType
      }
    }).then(function mysucess(response) {
      if (response && response.data && response.data.isSuccess && response.data.isSuccess === true) {
        $scope.isFormPristine = false;
      } else {
        alert("Could not save the response");
      }
    }, function error(response) {
      alert("Could not save the response");
    });
  }

});
