function LoginJS(form, action) {
  this.form = form;
  this.formAction = action;
  var _self = this;

  this.init = function() {
    $(this.form).on('submit', function(e) {
      e.preventDefault();
      var validStatus = _self.validateLoginForm(this);
      if (validStatus == true) {
        $(_self.form).attr('action', _self.formAction);
        this.submit();
      }
    });
  }
  this.validateLoginForm = function(formElem) {
    var emailId = $(formElem).find('input[name="email"]').val();
    var password = $(formElem).find('input[name="password"]').val();

    if (emailId == '') {
      this.addError("Invalid Email id");
      return false;
    }

    if (password == '' || !WEB_UTILS.isValidPassword(password)) {
      this.addError("Invalid Password");
      return false;
    }

    return true;
  }

  this.addError = function(errorMsg) {
    alert(errorMsg);
  }

  this.init();
}

function MemberObject() {
  this.name;
  this.dob;
  this.age;
  this.phone;
  this.mn;
  this.fn;
  this.address;
  this.lg;
  this.school;
  this.studyClass;
  this.relation;
}

function MemberJS(formElem, url, dataJson, relationshipJSON) {
  this.form = formElem;
  this.url = url;
  this.dataJson = dataJson; // dataJson contains infromation about library
                            // groups and genders for now.
  var _self = this;
  this.isBulk = false;
  this.relJSON = relationshipJSON;
  this.members = new Array();

  this.init = function() {
    var dobElem = $(this.form).find("input[name='dob']");
    WEB_UTILS.initPreviousDate(dobElem[0], "dd/mm/yy");

    $(this.form).find("input[type='button'][name='submit_button']").click(function() {
      if (_self.members.length > 0) {
        $(_self.form).html("");
        for (var i = 0; i < _self.members.length; i++) {
          var member = _self.members[i];

          $.each(member, function(key, value) {
            if (key == "relation" && i == 0) {
              return true;
            }

            $('<input>').attr({
              type : 'hidden',
              name : key + (i == 0 ? "" : i),
              value : value
            }).appendTo(_self.form);
          });
        }
      } else {
        if (_self.isBulk == false) {
          if (!_self.validateForm(true)) {
            return false;
          }
        }
      }
      $(_self.form).attr("action", _self.url);
      $(_self.form).submit();

    });
  }

  this.addRelative = function(anchorTag) {

    if (this.isBulk == true) {
      WEB_UTILS.addError("You have already selected bulk upload option. You cannot choose to add a single member right now");
      return false;
    }

    if (this.validateForm(true)) {
      this.populateMember();
      $(this.form).trigger("reset");
      var relationElem = $(this.form).find("select[name='relation']");
      if (relationElem.length == 0) {
        var relTR = '<tr><td>Relationship<\/td><td><select name=\'relation\'>';
        for (var i = 0; i < this.relJSON.length; i++) {
          var relObj = this.relJSON[i];
          relTR += "<option value='" + relObj.id + "'>" + relObj.name + "</option>";
        }
        relTR += "</select>";
        $('#singleMemberTable tr:last').after(relTR);
        $(anchorTag).html("Add more relatives");
        $("#anchor_container").append("<a style=\"margin-left:2%\" onclick=\"memberObj.doneAdding();\" href=\"javascript:void(0);\">Done Adding</a>");
      }

      $("#add_members").html("Members added so far: " + this.members.length);

    }
  }

  this.doneAdding = function() {

    if (this.isBulk == true) {
      WEB_UTILS.addError("You have already selected bulk upload option. You cannot choose to add a single member right now");
      return false;
    }

    var isConfirm = false;

    if (this.validateForm(false)) {
      this.populateMember();
      isConfirm = true;
    } else {
      isConfirm = window.confirm("You have an error in the form. Do you want to skip this one?");
    }

    if (isConfirm == true) {
      $("#rel_anchor").remove();
      $("#singleMemberTable").empty();
      $("#anchor_container").remove();
      $("#add_members").html("Members added so far: " + this.members.length);
    } else {
      return false;
    }

  }

  this.populateMember = function() {
    var member = new MemberObject();
    member.name = $(this.form).find("input[name='name']").val();
    member.dob = $(this.form).find("input[name='dob']").val();
    member.age = $(this.form).find("input[name='age']").val();
    member.phone = $(this.form).find("input[name='phone']").val();
    member.mn = $(this.form).find("input[name='mn']").val();
    member.fn = $(this.form).find("input[name='fn']").val();
    member.address = $(this.form).find("input[name='address']").val();
    member.lg = $(this.form).find("select[name='lg']").val();
    member.gender = $(this.form).find("select[name='gender']").val();
    member.school = $(this.form).find("input[name='school']").val();
    member.studyClass = $(this.form).find("input[name='class']").val();
    var relationElem = $(this.form).find("select[name='relation']");
    if (relationElem.length > 0) {
      member.relation = $(relationElem).val();
    }
    this.members.push(member);
    // console.log(this.members);
  }

  this.validateForm = function(isShowPopup) {

    /*
     * var id = $(this.form).find("input[name='id']").val(); if
     * (!WEB_UTILS.isValidPositiveInteger(id)) { WEB_UTILS.addError("Please
     * enter a valid numeric id"); return false; }
     */

    var name = $(this.form).find("input[name='name']").val();
    if (!WEB_UTILS.isValidNonEmptyString(name)) {
      if (isShowPopup == true) {
        WEB_UTILS.addError("Please enter a valid name.");
      }
      return false;
    }

    var dob = $(this.form).find("input[name='dob']").val();
    var age = $(this.form).find("input[name='age']").val();

    /*
     * if (!WEB_UTILS.isValidNonEmptyString(age) &&
     * !WEB_UTILS.isValidNonEmptyString(dob)) { (isShowPopup == true) {
     * WEB_UTILS.addError("Please enter either valid date of birth or age"); }
     * return false; }
     */

    var phone = $(this.form).find("input[name='phone']").val();
    if (!WEB_UTILS.isValidPhone(phone)) {
      if (isShowPopup == true) {
        WEB_UTILS.addError("Please enter valid phone number(s).only 10 digit numbers, separated by ,");
      }
      return false;
    }

    var address = $(this.form).find("input[name='address']").val();
    if (address != "" && !WEB_UTILS.isValidNonEmptyString(address)) {
      if (isShowPopup == true) {
        WEB_UTILS.addError("Please enter a valid Address.");
      }
      return false;
    }

    return true;

  }

  this.uploadFile = function() {
    var url = this.url;
    var fileName = $(this.form).find("input[name='upload_file']").val().trim();
    var extension = fileName.replace(/^.*\./, '').toLowerCase();
    if ((fileName == '') || ((extension != "csv") && (extension != "txt"))) {
      WEB_UTILS.addError(fileName == "" ? "Please select a file" : "Only TXT and CSV files supported");
      return false;
    }
    url += "/bulkadd";
    $(this.form).attr("enctype", "multipart/form-data");
    $(this.form).attr("action", url);
    this.isBulk = true;
    $(this.form).submit();
  }

  this.downloadFormat = function() {
    var csv = 'ID;firstname;surname;DOB(YYYY-MM-DD);Contact Number;Alternate Contact Number;Mothers First Name;Mothers Last Name;Fathers First Name;Fathers Last Name;Address;Group;Gender;Date Of Joining(YYYY-MM-DD);Branch Code\n';
    // var hiddenElement = document.createElement('a');
    // hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    // hiddenElement.target = '_blank';
    // hiddenElement.download = 'patron_sample.csv';
    // hiddenElement.click();

    try {
      var file = new File([
        csv
      ], 'Donation Summary TCLP.csv', {
        type : 'text/csv',
      });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    } catch (err) {
      alert("Could not export to CSV");
    }
  }

  this.init();
}

function ViewMemberJS(form, url, ajax_get_url) {
  this.form = form;
  this.url = url;
  this.ajaxURL = ajax_get_url;
  var _self = this;

  this.init = function() {
    $("#existing_members").DataTable({
      "scrollX" : true,
      ordering : false,
      serverSide : true,
      ajax : {
        url : _self.ajaxURL,
        type : 'POST'
      }
    });
  }

  this.getPrintablePage = function(id) {
    if (!WEB_UTILS.isValidPositiveInteger(id)) {
      WEB_UTILS.addError("Please select a valid id to display picture");
      return false;
    }
    window.open(this.url + "/view/" + id);
    return false;
  }

  this.editMember = function(id) {
    if (!WEB_UTILS.isValidPositiveInteger(id)) {
      WEB_UTILS.addError("Please select a valid id to edit information");
      return false;
    }

    var form = $("#action_" + id);
    $(form).attr("action", this.url + "/edit");
    $(form).attr("method", "post");
    $(form).attr("target", "_blank");
    console.log($(form));
    $('<input>').attr({
      type : 'hidden',
      name : 'member_id',
      value : id
    }).appendTo($(form));
    $(form).submit();
  }

  this.deleteMember = function(id) {
    if (!WEB_UTILS.isValidPositiveInteger(id)) {
      WEB_UTILS.addError("Please select a valid id to delete member");
      return false;
    }

    var isConfirm = window.confirm("Are you sure you want to delete this member details?");
    if (isConfirm == true) {
      var form = $("#action_" + id);
      $(form).attr("action", this.url + "/delete");
      $(form).attr("method", "post");
      $('<input>').attr({
        type : 'hidden',
        name : 'member_id',
        value : id
      }).appendTo($(form));
      $(form).submit();
    }
  }

  this.init();
}

function NewsletterJS(form, url) {
  this.form = form;
  this.url = url;
  var _newsletter = this;
  this.init = function() {
    $(this.form).find($('[name="addSubscription"]')).click(function() {
      var emailBox = $(_newsletter.form).find('[name="email"]');
      var csVal = $(_newsletter.form).find('[name="cs"]').val();
      var emailID = $(emailBox).val();
      if (WEB_UTILS.isValidEmail(emailID)) {
        $.post(_newsletter.url, {
          email : emailID,
          cs : csVal
        }, function(response, status) {
          var responseJSON = eval('(' + response + ')')
          if (responseJSON.status == "success") {
            _newsletter.addMessage("Successfully Subscribed", 'message-green');
          } else if (responseJSON.status == "error") {
            _newsletter.addMessage(responseJSON.message, 'message-red');
          }
          $(_newsletter.form).trigger('reset');
        }, "text");
      } else {
        _newsletter.addMessage("Invalid Email Id", 'message-red');
      }
    });
  }

  this.addMessage = function(message, style) {
    if ($('#ftr-msg').length > 0) {
      $("#ftr-msg").html(message).addClass(style).show();
      $("#ftr-msg")[0].scrollIntoView(0);
    }
  }

  this.init();
}

function contactJS(form, action) {
  this.form = form;
  this.formAction = action;
  var _self = this;

  this.init = function() {
    $(this.form).on('submit', function(e) {
      e.preventDefault();
      var validStatus = _self.validateContactForm(this);
      if (validStatus == true) {
        $(_self.form).attr('action', _self.formAction);
        this.submit();
      }
    });
  }
  this.validateContactForm = function(formElem) {
    var phoneNumber = $(formElem).find('input[name="phone"]').val();
    if (phoneNumber.match(/[a-z]/i) || !this.isEnglish(phoneNumber)) {
      this.addError("Invalid phone number", true);
      return false;
    }

    var email = $(formElem).find("input[name='email']").val();
    if (!WEB_UTILS.isValidEmail(email)) {
      this.addError("Please enter a valid email address. For example abc@domain.com");
      return false;
    }

    var name = $(formElem).find('input[name="name"]').val();
    var nameRegex = /^[a-zA-Z ]{2,30}$/;
    if (!nameRegex.test(name)) {
      this.addError("Invalid Name", true);
      return false;
    }

    var message = $(formElem).find('textarea[name="message"]').val();
    if (!this.isEnglish(message)) {
      this.addError("Invalid Message", true);
      return false;
    }

    return true;
  }

  this.hideErrorDiv = function() {
    if ($("#errorDiv").length > 0) {
      $("#errorDiv").hide();
    }
  }

  this.addError = function(errorMsg, isTransform) {
    if ($("#errorDiv").length > 0) {
      $("#errorDiv").html(errorMsg).show();
      $("#errorDiv").css('color', 'red');
      $("#errorDiv")[0].scrollIntoView(0);
    }
  }

  this.isEnglish = function(msg) {
    var english = /^[A-Za-z0-9\+\-\\. ]*$/;
    return english.test(msg);
  }

  this.init();
}

function EventJS(ajaxURL) {
  this.url = ajaxURL;
  this.init = function() {
    WEB_UTILS.getJSONData(this.url, this.populateDOMElements);
  }

  this.populateDOMElements = function(responseJSON) {

    if (responseJSON.length == 0) {
      $("#no-events").show();
    } else {

      var line = 0;
      var html = new Array();
      for (var i = 0; i < responseJSON.length; i++) {
        var event = responseJSON[i];
        var startDate = event.start;
        // var date = "yyyy-mm-dd";
        var m = parseInt(startDate.substring(5, 7), 10);
        var d = startDate.substring(8);
        var mm = WEB_UTILS.getMonthName(m - 1);

        var title = event.title;
        var location = event.location;
        var url = event.url;
        var description = event.description;
        var time = event.time;
        var duration = event.duration;

        html[line++] = "<div style=\"margin: 0.5em 0.5em 0.5em 0; border-bottom: 1px solid; padding: 1em 0;\">";
        html[line++] = "<div class=\"col-sm-12\">";
        html[line++] = "<div class=\"row\">";
        html[line++] = "<div class=\"col-md-2\"><h3>" + d + "</h3><h4>" + mm + "</h4></div>";
        html[line++] = "<div class=\"col-md-10\"><h2>" + title + "</h2></div>";
        html[line++] = "</div>";
        html[line++] = "<div class=\"row\"><div class=\"col-sm-12\"><strong>Location</strong>: " + location + "</div></div>";

        if ((url && url != "null") || (time && time != "null") || (duration && duration != "null")) {

          if ((time && time != "null") || (duration && duration != "null")) {
            html[line++] = "<div class=\"row\">";
            if (time && time != "null") {
              html[line++] = "<div class=\"col-sm-12\"><strong>Time</strong>: " + time + "</div>";
            }

            if (duration && duration != "null") {
              html[line++] = "<div class=\"col-sm-12\"><strong>For</strong>: " + duration + "</div>";
            }

            html[line++] = "</div>";
          }

          if (url && url != "null") {
            html[line++] = "<div class=\"row event-text\" style=\"text-align:center\"><a href=\"" + url
                + "\" target=\"_blank\">Go to Event Page</a></div>";
          }

        }

        html[line++] = "</div>";
        html[line++] = "<div class=\"clearfix\"></div>";
        html[line++] = "</div>";
      }

      $("#events-container").html(html.join(""));
    }
  }
  this.init();
}

function GalleryJS(baseURL) {
  this.url = baseURL;
  _self = this;
  this.init = function() {
    // initial loading
    WEB_UTILS.makeAJAXHit(this.url, 'POST', null, this.populateDOMElements);

    $("#gallery-more").click(function() {
      var nextId = $("#next_max_id").val();
      WEB_UTILS.makeAJAXHit(_self.url, 'POST', {
        "nextMaxId" : nextId
      }, _self.populateDOMElements);
    });
  }

  this.populateDOMElements = function(responseJSON) {
    if (responseJSON.data.length == 0) {
      $("#no-content").show();
      $("#gallery-main").hide();
    } else {
      var line = 0;
      var html = new Array();
      var responseData = responseJSON.data;
      for (var i = 0; i < responseData.length; i++) {
        var data = responseData[i];

        var caption = WEB_UTILS.trim(data.caption, 200);

        var postLink = data.link;
        var imageURL = data.imageURL;
        var bigURL = data.bigURL ? data.bigURL : imageURL;

        // html[line++] = "<div class=\"col-md-4 gallery-grid\">";
        // html[line++] = "<div class=\"gallery-grid-effects\">";
        // html[line++] = "<div class=\"ih-item square effect4
        // bottom_to_top\"
        // style=\"height:320px;width:320px\"><a href=\"" + postLink
        // + "\" target=\"_blank\">";
        // html[line++] = "<div class=\"img\"><img src=\"" + imageURL +
        // "\"
        // alt=\"img\"></div>";
        // html[line++] = "<div class=\"mask1\"></div>";
        // html[line++] = "<div class=\"mask2\"></div>";
        // html[line++] = "<div class=\"info\">";
        // if (caption.length > 122) {
        // caption = caption.substring(0, 118) + "...";
        // }
        // html[line++] = "<p>" + caption + "</p>";
        // html[line++] = "</div></a>";
        // html[line++] = "</div>";
        // html[line++] = "</div>";
        // html[line++] = "</div>";

        html[line++] = "<div class=\"col-sm-6 col-md-4\">";
        html[line++] = "<div class=\"thumbnail\">";
        html[line++] = "<a class=\"lightbox\" href=\"" + bigURL + "\">";
        html[line++] = "<img src=\"" + imageURL + "\" alt=\"" + postLink + "\">";
        html[line++] = "</a>";
        html[line++] = "<div class=\"caption\">";
        html[line++] = "<p>" + caption + "</p>";
        html[line++] = "</div>";
        html[line++] = "</div>";
        html[line++] = "</div>";
      }

      var initialContent = $("#gallery-content").html();
      $("#gallery-content").html(initialContent + html.join(""));

      baguetteBox.run('#gallery-main', {
        captions : function(element) {
          var postLink = $(element).find('img').attr('alt');
          var a = "<a style='text-decoration:none; color: black; background-color:white;' href='" + postLink + "' target='_blank'>"
              + $(element).parent().find('.caption p').html() + "</a>";
          return a;
        }
      });

      if (!responseJSON.next_max_id) {
        $("#gallery-more").hide();
      } else {
        $("#next_max_id").val(responseJSON.next_max_id);
        $("#gallery-more").show();
      }

      $("#no-content").hide();
      $("#gallery-main").show();
    }
  }

  this.init();
}

function ALLBlogsJS(ajax_url) {
  this.ajax_url = ajax_url;

  this.init = function() {
    var _self = this;

    // initial loading
    WEB_UTILS.makeAJAXHit(this.ajax_url, 'POST', null, this.populateDOMElements);

    $("#blogs-more").click(function() {
      var nextId = $("#next_max_id").val();
      WEB_UTILS.makeAJAXHit(_self.ajax_url, 'POST', {
        "nextMaxId" : nextId
      }, _self.populateDOMElements);
    });
  }

  this.populateDOMElements = function(responseJSON) {

    if (responseJSON.status == "success") {
      $("#no-content").remove();

      var line = 0;
      var html = new Array();
      var responseData = responseJSON.data;
      html[line++] = "<div class=\"row\">";
      for (var i = 0; i < responseData.length; i++) {
        var data = responseData[i];

        if (i != 0 && i % 3 == 0) {
          html[line++] = "</div>";// end of row
          html[line++] = "<div class=\"row\">";
        }

        html[line++] = "<div class=\"col-lg-4 sb-preview text-center\">";
        html[line++] = "<div class=\"card h-100\">";
        html[line++] = "<div class=\"card-block\">";
        html[line++] = "<h4 class=\"card-title\">" + data.title + "</h4>";
        html[line++] = "<div class=\"card-text\">";
        html[line++] = "<div>";
        html[line++] = "Published by <span>" + data.author + "</span>";
        html[line++] = "</div>";
        html[line++] = "<div>";
        html[line++] = "on <span>" + data.publishdate + "</span>";
        html[line++] = "</div>";
        html[line++] = "</div>";
        html[line++] = "</div>";
        html[line++] = "<div class=\"card-content\">" + data.content + "</div>";
        html[line++] = "<div class=\"card-footer\">";
        html[line++] = "<a href=\"" + data.url + "\" class=\"btn btn-primary\">View Post</a>";
        html[line++] = "<a href=\"" + data.url + "#disqus_thread" + "\" class=\"btn btn-sm\" data-disqus-identifier=\"" + data.id
            + "\">0 Comments</a>";
        html[line++] = "</div>";
        html[line++] = "</div>";
        html[line++] = "</div>";

      }

      html[line++] = "</div>";

      var initialContent = $("#content").html();
      $("#content").html(initialContent + html.join(""));
      $("#content").show();

      if (!responseJSON.next_max_id) {
        $("#blogs-more").hide();
      } else {
        $("#next_max_id").val(responseJSON.next_max_id);
        $("#blogs-more").show();
      }
    }
  }

  this.init();
}

function FeedBlogsJS(ajax_url) {
  this.ajax_url = ajax_url;

  this.init = function() {
    var _self = this;

    // initial loading
    WEB_UTILS.makeAJAXHit(this.ajax_url, 'POST', null, this.populateDOMElements);
  }

  this.populateDOMElements = function(responseJSON) {

    if (responseJSON.status == "success") {
      var line = 0;
      var html = new Array();
      var responseData = responseJSON.data;
      html[line++] = "<div class=\"rss-box\">";
      var data = responseData[0];

      html[line++] = "<p class=\"rss-title\">";
      html[line++] = "<a class=\"rss-title\" href=\"" + data.blogslink + "\" target=\"_blank\">Community Library Blog</a><br>";
      html[line++] = "<span class=\"rss-item\"></span>";
      html[line++] = "</p>";

      html[line++] = "<ul class=\"rss-items\">";
      html[line++] = "<li class=\"rss-item\"><a class=\"rss-item\"";
      html[line++] = "href=\"" + data.url + "\" target=\"_blank\">" + data.title + "</a> <span class=\"rss-item-auth\">(" + data.author + ")</span>";
      html[line++] = "<div class=\"content\">" + data.content + "</div>";
      html[line++] = "</li>";
      html[line++] = "</ul>";
      html[line++] = "</div>";

      var initialContent = $(".blogposts-main");
      $(initialContent).html(html.join(""));
    }
  }
  this.init();
}

function PaymentJs(formResources, formURL, formElem) {
  this.formResources = formResources;
  this.formURL = formURL;
  this.formElem = formElem;

  this.init = function() {

    $("#receiptHolder").hide();

    if ($("#receipt").length > 0) {
      $("#receipt").change(function() {
        if ($(this).is(":checked")) {
          $("#receiptHolder").show();
        } else {
          $("#receiptHolder").hide();
        }
      });
    }

    $("#amount").change(function() {
      var value = 0;

      try {
        value = parseInt($(this).val());
      } catch (err) {

      }

      if (value >= 100000) {
        $("#receiptHolder").show();
      } else {
        $("#receiptHolder").hide();
      }
    });

    // if (this.formResources) {
    // var html = new Array();
    // var count = 0;
    // for (var i = 0; i < this.formResources.pledgeTypes.length; i++) {
    // var pledgeType = this.formResources.pledgeTypes[i];
    // html[count++] = "<label class=\"radio radio-inline\">";
    // html[count++] = "<input style=\"width:10%;\" type=\"radio\"
    // name=\"pledgetypecode\" value=\"" + pledgeType.code + "\">";
    // html[count++] = pledgeType.name + "</label>";
    // }
    //
    // $("#pledge-container").html(html.join(""));
    // }

    var _self = this;

    $(this.formElem).on('submit', function(e) {
      var validStatus = _self.validateForm(this);
      if (validStatus == true) {
        $(_self.formElem).attr('action', _self.formURL);
        return true;
      } else {
        return false;
      }
    });
  }

  this.validateForm = function(formElem) {
    var firstName = $(formElem).find("input[name='fname']").val();
    if (!WEB_UTILS.isLettersOnly(firstName)) {
      WEB_UTILS.addError("Please enter a valid first name. Do not use anything other than English letters. Not even spaces");
      return false;
    }

    var lastName = $(formElem).find("input[name='lname']").val();
    if (!WEB_UTILS.isLettersOnly(lastName)) {
      WEB_UTILS.addError("Please enter a valid Last name. Do not use anything other than English letters. Not even spaces");
      return false;
    }

    var email = $(formElem).find("input[name='email']").val();
    if (!WEB_UTILS.isValidEmail(email)) {
      WEB_UTILS.addError("Please enter a valid email-id");
      return false;
    }

    var phone = $(formElem).find("input[name='phone']").val();
    if (!WEB_UTILS.isStrictValidPhone(phone)) {
      WEB_UTILS.addError("Please enter a valid phone number");
      return false;
    }

    var amount = $(formElem).find("input[name='amount']").val();

    if (!WEB_UTILS.isValidNonZeroAmount(amount)) {
      WEB_UTILS.addError("Please enter an amount greater than INR 10");
      return false;
    }

    if (amount >= 100000) {
      var panNumber = $(formElem).find("input[name='pannum']").val();
      if (!WEB_UTILS.isValidPanNumber(panNumber)) {
        WEB_UTILS.addError("Pan number is mandatory for donations more than INR 1Lakh");
        return false;
      }
    }

    return true;
  }

  this.init();
}
