var App = angular.module("App", []);

angular.module('App').controller("SlotSettingsController", function ($scope, $http) {
  var contentType = "application/json";

  var addLibraryBranchURL = slotSettingsURL + "/library/branches";

  var libraryBranchBase = {
    code: "",
    name: "",
    memberIDPrefix: ""
  };

  $scope.libraryBranches = libraryBranchesJson;
  $scope.libraryBranch = angular.copy(libraryBranchBase);

  $scope.isFormDescriptionValid = function () {
    if (!APP.isValidNonEmptyString($scope.libraryBranch.code)) {
      return false;
    }

    if (!APP.isValidNonEmptyString($scope.libraryBranch.name)) {
      return false;
    }

    if (!APP.isValidMemberIdPrefix($scope.libraryBranch.memberIDPrefix)) {
      return false;
    }

    return true;

  }

  $scope.addLibraryBranch = function () {
    var content = angular.copy($scope.libraryBranch);
    $scope.insertLibraryBranch(content);
  }

  $scope.editLibraryBranch = function (branch) {
    $scope.libraryBranch = angular.copy(branch);
  }

  $scope.insertLibraryBranch = function (content) {
    $http({
      url: addLibraryBranchURL,
      method: 'POST',
      data: content,
      headers: {
        'Content-Type': contentType
      }
    }).then(function mysuccess(response) {
      var isSuccess = false;
      if (response && response.data) {
        if (response.data.isSuccess) {
          isSuccess = response.data.isSuccess;
        }
      } else {
        isSuccess = false;
      }

      if (isSuccess) {
        $scope.libraryBranches.push(angular.copy(content));
        $scope.libraryBranch = angular.copy(libraryBranchBase);
      }
    }, function error(response) {
      APP.addError("Error in inserting a new library branch: " + content + ". Please contact the administrator");
    });
  }


  //Code to manage slot types begin here
  var slotTypeURL = slotSettingsURL + "/slotType"
  $scope.slotTypes = slotTypesJson;

  $scope.isSlotTypeValid = function () {
    return APP.isValidNonEmptyString;
  }

  $scope.addSlotType = function () {
    $http({
      url: slotTypeURL,
      method: 'POST',
      data: {slotType: angular.copy($scope.slotType)},
      headers: {
        'Content-Type': contentType
      }
    }).then(function mysuccess(response) {
      var isSuccess = false;
      if (response && response.data) {
        if (response.data.isSuccess) {
          isSuccess = response.data.isSuccess;
        }
      } else {
        isSuccess = false;
      }

      if (isSuccess) {
        $scope.slotTypes.push(angular.copy($scope.slotType));
        $scope.slotType = "";
      }
    }, function error(response) {
      APP.addError("Error in inserting a new library branch: " + content + ". Please contact the administrator");
    });
  }

  $scope.deleteSlotType = function (slotType) {

    $http({
      url: slotTypeURL + "/" + slotType,
      method: 'GET',
      headers: {
        'Content-Type': contentType
      }
    }).then(function mysuccess(response) {
      var isSuccess = false;
      if (response && response.data) {
        if (response.data.isSuccess) {
          isSuccess = response.data.isSuccess;
        }
      } else {
        isSuccess = false;
      }

      if (isSuccess) {
        $scope.slotTypes = $scope.slotTypes.filter(type => type.toLowerCase() !== slotType.toLowerCase());
      }
    }, function error(response) {
      APP.addError("Error in deleting the slot type:" + slotType + ". Please contact the administrator");
    });
  }
});
