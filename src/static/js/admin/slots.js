function ManageSlotJS(baseURI, branchElem, slotsJson, ajaxSearchURL) {

  this.calendar;
  this.slotsJson = slotsJson;
  this.ajaxSearchURL = ajaxSearchURL;
  var _self = this;
  this.calendarCreated = false;
  this.branchElem = branchElem;

  this.validatePopupForm = function () {
    var memberId = $(_self.formElem).find("input[name='memberid']").val();
    var notes = $(_self.formElem).find("textarea[name='notes']").val();

    if (!APP.isValidMemberId(memberId)) {
      APP.addError("Need member details");
      return false;
    }

    if (APP.isValidNonEmptyString(notes) && notes.length > 500) {
      APP.addError("Only 500 characters allowed for notes");
      return false;
    }

    return true;
  }

  $(this.branchElem).change(function () {
    var branch = $(this).val();
    if (_self.calendar) {
      $.post(baseURI + "/getEvents", "branch=" + branch, function (data) {
        if (data) {
          if (data.status && data.status == true) {
            $(_self.calendar).fullCalendar('removeEvents');
            $(_self.calendar).fullCalendar('addEventSource', data.slots);
          } else {
            if (data.reason) {
              APP.addError(data.reason);
            }
          }
        } else {
          APP.addError("Did not recieve any communication from Server");
        }
      }, "json");
    }

  });

  this.createCalendar = function (elem, isAdmin, eventsJSON, validateForm, eventDialogElem, baseURI) {
    var calendar = $(elem).fullCalendar(
      {
        header: {
          left: 'prev,next',
          center: 'title',
        },
        minTime: "09:00:00",
        maxTime: "19:00:00",
        defaultView: 'agendaWeek',
        events: eventsJSON,
        selectable: true,
        selectHelper: true,
        viewRender: function (view, element) {
          // Do something
          if (_self.calendarCreated) {
            console.log(APP.getDateInYYYYMMDDMoment(view.start));
            console.log(APP.getDateInYYYYMMDDMoment(view.end));
            var branch = $(_self.branchElem).val();
            $.post(baseURI + "/getEvents", "startdate=" + APP.getDateInYYYYMMDDMoment(view.start) + "&enddate="
              + APP.getDateInYYYYMMDDMoment(view.end) + "&branch=" + branch, function (data) {
                if (data) {
                  if (data.status && data.status == true) {
                    $(_self.calendar).fullCalendar('removeEvents');
                    $(_self.calendar).fullCalendar('addEventSource', data.slots);
                  } else {
                    if (data.reason) {
                      APP.addError(data.reason);
                    }
                  }
                } else {
                  APP.addError("Did not recieve any communication from Server");
                }
              }, "json");

          }
        },
        select: function (start, end, allDay) {
          if (eventDialogElem) {
            $(_self.formElem)[0].reset();
            $(eventDialogElem).dialog(
              {
                modal: true,
                width: 550,
                closeOnEscape: false,
                open: function (event, ui) {
                  var date = APP.getDateInYYYYMMDD(start);
                  $(_self.formElem).find("input[name='date']").val(date);
                  $("#date").html(date);

                  var time = APP.getTimeFromMoment(start);
                  $(_self.formElem).find("input[name='starttime']").val(time);
                  $("#starttime").html(time);

                  var duration = 30;
                  if (end != null) {
                    var endTime = APP.getTimeFromMoment(end);
                    $(_self.formElem).find("input[name='endtime']").val(endTime);
                    $("#endtime").html(endTime);

                    duration = APP.calculateMinuteDifference(start, end);
                  }

                  var durationString = duration + " minute(s)";
                  $(_self.formElem).find("input[name='duration']").val(durationString);
                  $("#duration").html(durationString);
                },

                buttons: {
                  "Add Slot": function () {
                    if (validateForm.call()) {
                      var currTSID = Math.floor(Date.now() / 1000);

                      var formattedStartDate = APP.getDateInDateTime(start);
                      var formattedEndDate = APP.getDateInDateTime(end);

                      $.post(baseURI + "/add", $(_self.formElem).serialize() + "&id=" + currTSID + "&start=" + formattedStartDate + "&end="
                        + formattedEndDate, function (data) {
                          if (data) {
                            if (data.status && data.status == true) {
                              var eventObj = {
                                id: currTSID,
                                title: $(_self.formElem).find("input[name='member']").val(),
                                start: start,
                                end: end,
                                starttime: $(_self.formElem).find("input[name='starttime']").val(),
                                endtime: $(_self.formElem).find("input[name='endtime']").val(),
                                duration: $(_self.formElem).find("input[name='duration']").val(),
                                allDay: allDay,
                                slotdate: $(_self.formElem).find("input[name='date']").val(),
                                slotType: $(_self.formElem).find("select[name='slotType']").val(),
                                notes: $(_self.formElem).find("textarea[name='notes']").val(),
                              };
                              calendar.fullCalendar('renderEvent', eventObj, true);

                              alert("New slot Added");

                              $(eventDialogElem).dialog("close");
                              calendar.fullCalendar('unselect');

                            } else {
                              if (data.reason) {
                                APP.addError(data.reason);
                                $(eventDialogElem).dialog("close");
                                calendar.fullCalendar('unselect');
                              }
                            }
                          } else {
                            APP.addError("Did not recieve any communication from Server");
                            $(eventDialogElem).dialog("close");
                            calendar.fullCalendar('unselect');
                          }
                        }, "json");
                    }
                  },
                  Cancel: function () {
                    $(this).dialog("close");
                  }
                }

              });

          }
        },

        eventClick: function (calEvent, calElement) {
          if (eventDialogElem) {
            $(_self.formElem)[0].reset();
            var currTSID = calEvent._id;
            $(eventDialogElem).dialog({
              modal: true,
              width: 550,
              closeOnEscape: false,
              open: function (event, ui) {
                console.log(calEvent);

                $(_self.formElem).find("input[name='member']").val(calEvent.title);
                $("#date").html(calEvent.slotdate);
                $("#starttime").html(calEvent.starttime);
                $("#endtime").html(calEvent.endtime);
                $("#duration").html(calEvent.duration);
                $("#slotType").val(calEvent.slotType).change();
                $(_self.formElem).find("textarea[name='notes']").val(calEvent.notes == null ? "" : calEvent.notes);
              },

              buttons: {
                // "Delete Slot" : function() {
                // if (confirm("Delete this slot?")) {
                // $.post(baseURI + "/delete", "id=" + currTSID,
                // function(data) {
                // if (data) {
                // if (data.status && data.status == "true") {
                // calendar.fullCalendar('removeEvents', currTSID);
                // alert("Slot Deleted");
                // $(eventDialogElem).dialog("close");
                // } else {
                // if (data.reason) {
                // APP.addError(data.reason);
                // }
                // }
                // } else {
                // APP.addError("Did not recieve any communication from
                // Server");
                // }
                // }, "json");
                // }
                // },
                Cancel: function () {
                  $(this).dialog("close");
                }
              }

            });

          }

          // if there is an event link, do not go there on clicking it from
          // here
          // in the admin panel
          return false;
        }
      });
    this.calendarCreated = true;
    return calendar;
  }

  this.init = function () {
    this.calElem = $("#slottimingcalendar");
    this.formElem = $("#add-slot-form");
    if (this.calElem) {
      this.calendar = this.createCalendar(this.calElem, true, this.slotsJson, this.validatePopupForm, $("#slotContent"), baseURI);
    }

    // var memberElem = $(this.formElem).find("input[name='member']");
    var memberElem = $("#member");
    // $(memberElem).select2({
    // placeholder: 'Select an option',
    // ajax: {
    // url: _self.ajaxSearchURL,
    // dataType: 'json',
    // data: function (params) {
    // return {
    // search: params.term, // search term
    // };
    // },
    // processResults: function (data, params) {
    // return {
    // results: data.map(item => item.name)
    // }
    // }
    // },
    // // Additional AJAX parameters go here; see the end of this chapter for
    // the full code of this example
    // });

    $(memberElem).autocomplete({
      source: function (req, add) {
        // pass request to server
        var branch = $(_self.branchElem).val();
        $.getJSON(_self.ajaxSearchURL + "?search=" + req.term + "&branch=" + branch, req, function (data) {

          var suggestions = data.map(function (item, i, data) {
            var obj = {
              id: item[0],
              value: item[1],
              label: "<span>" + item[0] + ": " + item[1] + ": " + item[2] + "</span>"
            };
            return obj;
          });
          // pass array to callback
          add(suggestions);
        });
      },
      minLength: 3,
      select: function (event, ui) {
        var memberId = ui.item.id;
        $("#memberid").val(memberId);
      },

      html: true, // optional (jquery.ui.autocomplete.html.js required)

      // optional (if other layers overlap autocomplete list)
      open: function (event, ui) {
        $(".ui-autocomplete").css("z-index", 1000);
      }
    });

    // $(memberElem).typeahead({
    // onSelect: function(item) {
    // console.log(item);
    // },
    // ajax: {
    // url: _self.ajaxSearchURL,
    // timeout: 500,
    // displayField: "name",
    // triggerLength: 3,
    // method: "get",
    // loadingClass: "loading-circle",
    // preDispatch: function (query) {
    // return {
    // search: query
    // }
    // },
    // preProcess: function (data) {
    // if (!data || data.length == 0) {
    // // Hide the list, there was some error
    // return false;
    // }
    // // We good!
    // return data;
    // }
    // }
    // });
  }

  this.init();
}

var App = angular.module("App", [
  'ui.bootstrap'
]);

angular.module('App').controller("SlotsController", function ($scope, $http) {
  var contentType = "application/json";

  var getSlotsURL = slotURL + "/get";
  var changeSlotStatusURL = slotURL + "/changeStatus";
  var deleteSlotURL = slotURL + "/deleteSlot";

  $scope.libraryBranch = $("#library-branch").val();

  $scope.isopened = false;
  $scope.open = function () {
    $scope.isopened = true;
  };

  $scope.today = function () {
    $scope.dt = new Date();
    $scope.selectedDate = APP.getDateInYYYYMMDD($scope.dt);
  };

  $scope.options = {
    showWeeks: true
  };

  $scope.today();

  $scope.getSlots = function () {
    $scope.selectedDate = APP.getDateInYYYYMMDD($scope.dt);
    var content = {
      date: $scope.selectedDate,
      branch: $scope.libraryBranch
    };

    $scope.makeHit(content);
  }

  $scope.makeHit = function (content) {
    $scope.slots = [];
    $http({
      url: getSlotsURL,
      method: 'POST',
      data: content,
      headers: {
        'Content-Type': contentType
      }
    }).then(function mysucess(response) {
      var isSuccess = false;
      if (response && response.data) {
        if (response.data.isSuccess) {
          isSuccess = response.data.isSuccess;
        }
      } else {
        isSuccess = false;
      }

      if (isSuccess) {
        $scope.slots = angular.copy(response.data.slots);
      } else {
        // APP.addError("Could not fetch slots for date: " + content.date);
      }
    }, function error(response) {
      APP.addError("Error in fetching slots for date: " + content.date + ". Please contact the administrator");
    });
  }
  $scope.slots = [];
  $scope.getSlots();

  $scope.deleteSlot = function (slot) {
    if (!slot) {
      return;
    }

    if (!slot.id) {
      alert("no id present");
    }

    var content = {};
    content.id = slot.id;

    $http({
      url: deleteSlotURL,
      method: 'POST',
      data: content,
      headers: {
        'Content-Type': contentType
      }
    }).then(function mysucess(response) {
      var isSuccess = false;
      if (response && response.data) {
        if (response.data.isSuccess) {
          isSuccess = response.data.isSuccess;
        }
      } else {
        isSuccess = false;
      }

      if (isSuccess) {
        for (var i = 0; i < $scope.slots.length; i++) {
          var existingSlot = $scope.slots[i];
          if (slot.id === existingSlot.id) {
            $scope.slots.splice(i, 1);
            APP.addError("Slot deleted");
            break;
          }
        }
      } else {
        APP.addError("Could not delete the slot");
      }
    }, function error(response) {
      APP.addError("Could not delete the slot");
    });

  }

  $scope.markAsAttended = function (slot) {
    if (!slot) {
      return;
    }

    if (!slot.id) {
      alert("no id present");
    }

    var content = {};
    content.id = slot.id;

    $http({
      url: changeSlotStatusURL,
      method: 'POST',
      data: content,
      headers: {
        'Content-Type': contentType
      }
    }).then(function mysucess(response) {
      var isSuccess = false;
      if (response && response.data) {
        if (response.data.isSuccess) {
          isSuccess = response.data.isSuccess;
        }
      } else {
        isSuccess = false;
      }

      if (isSuccess) {
        for (var i = 0; i < $scope.slots.length; i++) {
          var existingSlot = $scope.slots[i];
          if (slot.id === existingSlot.id) {
            existingSlot.status = 'A';
            break;
          }
        }
      } else {
        APP.addError("Could not change the status of the slot");
      }
    }, function error(response) {
      APP.addError("Could not change the status of the slot");
    });

  }

  $scope.set_color = function (slot) {

    return {
      "background-color": slot.color
    }

  }

  $("#library-branch").change(function () {
    var value = $(this).val();
    $scope.$apply(function () {
      $scope.libraryBranch = value;
      $scope.getSlots();
    });
  });
});
