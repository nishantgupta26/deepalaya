function ManageUsersJS(form, baseURI, userRoles) {
	this.form = form;
	this.baseUrl = baseURI;
	this.userRoles = userRoles;
	this.isEdit = false;

	this.init = function() {
		APP.dataTable($("#existing_users"));

	}

	this.modifyUser = function() {
		var url = this.baseUrl;
		var name = $(this.form).find("input[name='name']").val();
		if (!APP.isValidNonEmptyString(name) || !APP.isEnglish(name)) {
			APP.addError("Please enter a valid name for link in English.");
			return false;
		}

		var email = $(this.form).find("input[name='email']").val();
		if (!APP.isValidEmail(email)) {
			APP
					.addError("Please enter a valid email address. For example abc@domain.com");
			return false;
		}

		var userID = $(this.form).find("input[name='user_id']").val();
		var updatePasswordElem = $(this.form).find(
				"input[name='update_password']");
		if (userID == "-1" || $(updatePasswordElem[0]).is(':checked')) {
			var password = $(this.form).find("input[name='password']").val();
			if (!APP.isValidPassword(password)) {
				APP
						.addError("Please enter a valid password, having atleast 8 characters.");
				return false;
			}
		}
		url += ((this.isEdit == false) ? "/add" : "");

		$(this.form).attr("action", url);
		$(this.form).submit();
	}

	this.editUser = function(trElem) {
		$(this.form)[0].scrollIntoView();
		var td = $(trElem).find('td');

		var id = $(td[0]).html().trim();
		var role = $(td[1]).html().trim();
		var name = $(td[2]).html().trim();
		var email = $(td[4]).html().trim();
		var isEnabled = ("Enabled" == $(td[3]).html().trim());
		var roleID;
		for (var i = 0; i < this.userRoles.userroles.length; i++) {
			var userRole = this.userRoles.userroles[i];
			if (userRole.name == role) {
				roleID = userRole.id;
				break;
			}
		}

		$("#user_id").parent().html(
				id + "<input type='hidden' id='user_id' name='user_id' value='"
						+ id + "' />");

		var userRoleElem = $(this.form).find("select[name='role']");
		APP.setSelectBoxByValue($(userRoleElem), roleID);

		$(this.form).find("input[name='name']").val(name);
		$(this.form).find("input[name='email']").val(email);
		$(this.form).find("input[name='enabled']").prop('checked', (isEnabled));
		$(this.form).find("input[name='update_password']").prop('checked',
				false);
		$(this.form).find("button[name='modButton']").html(
				"Modify Existing User");
		$("#password_div").show();
		$(userRoleElem).focus();

		this.isEdit = true;
		// $("#singleUploadSpan").html("Edit Single User");
	}

	this.gotoAdd = function() {
		$(this.form)[0].scrollIntoView();
	}

	this.init();
}

function ManageSettingsJS(uploadURL) {
	this.init = function() {
		var elems = $("textarea");
		for (var i = 0; i < elems.length; i++) {
			APP.createEditor(elems[i], uploadURL);
		}
	}

	this.init();
}

function ManageCalendarJS(baseURI, eventsJSON) {

	this.calendar;
	this.eventsJSON = eventsJSON;
	var _self = this;

	this.validatePopupForm = function() {
		var formElem = $("#event-details-form");

		var formTitle = $(formElem).find("input[name='title']").val();
		var start = $(formElem).find("input[name='start']").val();
		var end = $(formElem).find("input[name='end']").val();
		var where = $(formElem).find("textarea[name='where']").val();
		var eventLink = $(formElem).find("input[name='event-link']").val();
		var duration = $(formElem).find("input[name='duration']").val();

		if (!APP.isValidNonEmptyString(formTitle)) {
		  APP.addError("Error in title of the event");
			return false;
		}
		
    if (formTitle.length > 50) {
      APP.addError("Title should be max 50 characters");
      return false;
    }

		if (!APP.isValidNonEmptyString(start)) {
		  APP.addError("Error in start date of the event");
			return false;
		}

		if (!APP.isValidNonEmptyString(end)) {
		  APP.addError("Error in end date of the event");
			return false;
		}

		if (!APP.isValidNonEmptyString(where)) {
		  APP.addError("Please provide a location for the event");
			return false;
		}
		
    if (APP.isValidNonEmptyString(duration) && duration.length > 50) {
      APP.addError("Only 50 characters allowed for Duration");
      return false;
    }

		if (!APP.isValidNonEmptyString(eventLink)) {
			return true;
		} else if (!(eventLink.startsWith("http") || eventLink
				.startsWith("www"))) {
		  APP.addError("Event link can only start with http, https, or www");
			return false;
		}

		return true;
	}

	this.init = function() {
		this.calElem = $("#calendar");
		if (this.calElem) {
			this.calendar = APP.createCalendar(this.calElem, true,
					this.eventsJSON, this.validatePopupForm,
					$("#eventContent"), baseURI);
		}
		
		var elem = $("#time");
		if(elem) {
		  $(elem).timepicker({
		    minuteStep: 5,
        showInputs: false,
        disableFocus: true
		  });
		}
	}

	this.init();
}

function UserLoginJS(form, url) {
	this.form = form;
	this.url = url;

	this.init = function() {
		var self = this;
		$(this.form).find("input[name='date']").datetimepicker({
			format : 'DD/MM/YYYY',
			autoclose : true,
			icons : {
				date : "fa fa-calendar",
			}
		});

		$(this.form).find("button[name='hideButton']").click(function() {
			self.hideDetailsDiv();
		});

		$(this.form).find("button[name='getDetailsButton']").click(function() {
			self.getLoginInformation();
		});
	}

	this.showDetailsDiv = function(userId) {
		$("#user_id_div").html(userId);
		$(this.form).find("input[name='user_id']").val(userId);
		$("#getLoginInfoDTO").show();
	}

	this.hideDetailsDiv = function() {
		$("#getLoginInfoDTO").hide();
		($(this.form)[0]).reset();
		$("#date_div").html("");
		$("#num_div").html("");
	}

	this.getLoginInformation = function() {
		APP.makeAJAXHit(this.url, 'POST', $(this.form).serialize(), 'json',
				function(data) {
					$("#date_div").html(data.date);
					if (data.status == "MSG_SUCCESS") {
						$("#num_div").html(data.numLogins);
					} else {
						$("#num_div").html("No login information obtained");
					}
					// $("#resultDiv").show();
				});
	}

	this.init();
}

function ManageBannerJS(formElem, tableElem, url, uploadURL) {
	this.formElem = formElem;
	this.dispTableElem = tableElem;
	var _self = this;
	this.url = url;
	var number = 0;
	
	var content = new Array();

	this.init = function() {

		APP.makeAJAXHit(_self.url + "/getAll", 'GET', null, 'json',
				_self.addTableRows, _self.error);
		
		APP.createEditor($(_self.formElem).find("textarea[name='textContent']"), uploadURL);

		$(this.formElem)
				.find("button")
				.click(
						function() {
							var imageUrl = $(_self.formElem).find(
									"input[name='imageUrl']").val();
							if ((imageUrl = $.trim(imageUrl)) != '') {
								if(!imageUrl.startsWith("http")) {// image
									alert("Only valid url applicable for image type");
									return false;
								}
							}
							
           var urlLink = $(_self.formElem).find(
               "input[name='urlLink']").val();
           if ((urlLink = $.trim(urlLink)) != '') {
             if(!urlLink.startsWith("http")) {
               alert("Only valid url applicable for campaign");
               return false;
             }
           }

							APP.makeAJAXHit(_self.url + "/add", 'POST', $(
									_self.formElem).serialize(), 'json',
									_self.successfullyAdded, _self.error);
						});
	}

	this.successfullyAdded = function(data) {
		_self.addTableRows(data);
		$(_self.formElem)[0].reset();
		
	}

	this.addTableRows = function(data) {
		if(!data || !data.status || data.status===false) {
			return;
		}
		
		for (var i = 0; i < data.rows.length; i++) {
			var row = data.rows[i];
			content.push(row);
			$(_self.dispTableElem)
					.find('tbody')
					.append(
							$('<tr>')
									.append($('<td>').append(++number))
									.append($('<td>').append(row.type))
									.append($('<td>').append(row.urlLink))
									.append($('<td>').append(row.imageUrl))
									.append($('<td>').append(row.textContent))
									.append(
											$('<td>')
													.append(
															"<span onclick=\"manage_banner.delete("
																	+ row.id
																	+ ", this)\" class=\"glyphicon glyphicon-remove-sign\" title=\"Delete\"></span>")));
		}

	}
	
	this.delete = function(id, elem) {
		APP.makeAJAXHit(_self.url + "/delete", 'POST', "id="+id, 'json',
				_self.deleteRow, null, elem);
	}
	
	this.deleteRow = function(data, context) {
		if(data.status == true) {
			
			$(_self.dispTableElem)
			.find('tbody tr' ).each( function(){
					  this.parentNode.removeChild( this ); 
		});
			var count = 0;
			for(var i = 0; i < content.length; i++) {
				var row = content[i];
				if(row.id == data.id) {
					content.splice(i, 1);
					i--;
				} else {
					$(_self.dispTableElem)
					.find('tbody')
					.append(
							$('<tr>')
									.append($('<td>').append(++count))
									.append($('<td>').append(row.type))
									.append($('<td>').append(row.value))
									.append(
											$('<td>')
													.append(
															"<span onclick=\"manage_banner.delete("
																	+ row.id
																	+ ", this)\" class=\"glyphicon glyphicon-remove-sign\" title=\"Delete\"></span>")));
				}
			}
			
			number = count;
		}
	}

	this.error = function() {
		alert("Some error occurred. Please contact the system administrator");
	}

	this.init();
}


function AddBlog(formElem, baseURL, uploadURL) {
	this.formElem = formElem;
	this.baseURL = baseURL + "/addoredit";
	
	this.init = function() {
		var _self = this;
		if($(this.formElem).find("input[name='date']").length > 0) {
			$(this.formElem).find("input[name='date']").datetimepicker({
				format : 'DD/MM/YYYY',
				autoclose : true,
				icons : {
					date : "fa fa-calendar",
				}
			});			
		}
		
		if($(this.formElem).find("input[name='url']").length > 0) {
		  $(this.formElem).find("input[name='title']").change(function() {
		    if($("#enter-manual").is(':checked') === false) {
		      var val = $(this).val();
		      if(APP.isEnglish(val) === false) {
		        $(_self.formElem).find("input[name='url']").val('');
		      } else {
	          // val = val.replace(/\s+/g, '-').toLowerCase();
		        val = val.trim().replace(/[^A-Za-z0-9]/g, '-').replace(/[-]+/g, '-').toLowerCase();
		        $(_self.formElem).find("input[name='url']").val(val);
		      }
		    }
		  });
    }
		
		var elems = $(this.formElem).find("textarea[name='textdata']");
		for (var i = 0; i < elems.length; i++) {
			APP.createEditor(elems[i], uploadURL);
		}
		
		$(this.formElem).find("button[name='publish-now']").click(function() {
			if(_self.validateForm()) {
				$(_self.formElem).attr("action", _self.baseURL);
				$(_self.formElem).submit();
			}
		});
		
		$(this.formElem).find("button[name='publish-later']").click(function() {
			if(_self.validateForm()) {
				var date = $(_self.formElem).find("input[name='date']").val();
				var isValidDate = false;
				try{
					var date = APP.getDate(date);
					isValidDate = APP.isDateLaterThanToday(date);
				} catch(err) {
					isValidDate  = false;
				}
				
				if(isValidDate == true) {
					$(_self.formElem).attr("action", _self.baseURL);
					$(_self.formElem).submit();					
				} else {
					APP.addError("Please select a publishing date later than today");
				}
			}
		});
	}
	
	this.validateForm = function() {
		var title = $(formElem).find("input[name='title']").val().trim();
		
		if(!APP.isValidNonEmptyString(title)) {
			APP.addError("Please enter the Title");
			return false;
		}
		
    if(title.length > 100) {
      APP.addError("Title exceeds maxlength of 100 characters");
      return false;
    }
    
    $(formElem).find("input[name='title']").val(title);
		
    if($(this.formElem).find("input[name='url']").length > 0) {
      var repURL = $(this.formElem).find("input[name='url']").val();
      if(repURL.trim() === '') {
        APP.addError("URL cannot be empty");
        return false;
      } 
      
      if(APP.isEnglish(repURL) === false) {
          APP.addError("Only alpha-numeric and - characters allowed");
          return false;
      }
      
      if(APP.isValidRepresentationalURL(repURL) === false) {
        APP.addError("Only alpha-numeric and - characters allowed");
        return false;
      }
      
      if(repURL.length > 100) {
        APP.addError("URL exceeds maxlength of 100 characters");
        return false;
      }
    }
		
		var text = $(this.formElem).find("textarea[name='textdata']").val();
		if(!APP.isValidNonEmptyString(text)) {
			APP.addError("Blog content too short");
			return false;
		}
		
		return true;
		
	}

	this.init();
}

function ManageBlogs(formElem, url) {
	this.formElem = formElem;
	this.url = url;
	
	this.edit = function(id) {
		$(this.formElem).find("input[name='id']").val(id);
		$(this.formElem).attr('action', this.url + "/edit");
		$(this.formElem).submit();
	}
	
	this.delete = function(id) {
        if (confirm("Delete this post?")) {
    		$(this.formElem).find("input[name='id']").val(id);
    		$(this.formElem).attr('action', this.url + "/delete");
    		$(this.formElem).submit();
        }
	}
}



function AddPage(formElem, baseURL, dispURL, uploadURL) {
	this.formElem = formElem;
	this.baseURL = baseURL + "/addoredit";
	this.dispURL = dispURL.endsWith("/") ? dispURL : dispURL + "/";
	
	this.init = function() {
		var _self = this;
		
		if($(this.formElem).find("input[name='url']").length > 0) {
		  $(this.formElem).find("input[name='title']").change(function() {
		    if($("#enter-manual").is(':checked') === false) {
		      var val = $(this).val();
		      if(APP.isEnglish(val) === false) {
		        $(_self.formElem).find("input[name='url']").val('');
		        $("#generated-url").html('');
		      } else {
	          // val = val.replace(/\s+/g, '-').toLowerCase();
		        val = val.trim().replace(/[^A-Za-z0-9]/g, '-').replace(/[-]+/g, '-').toLowerCase();
		        $(_self.formElem).find("input[name='url']").val(val);
		        $("#generated-url").html(_self.dispURL + val);
		      }
		    }
		  });
		  
		  $(this.formElem).find("input[name='url']").change(function() {
			      var val = $(this).val().trim();
			      if(val === '') {
			        $("#generated-url").html('');
			      } else {
			        $("#generated-url").html(_self.dispURL + val);
			      }
			  });
    }
		
		var elems = $(this.formElem).find("textarea[name='textdata']");
		for (var i = 0; i < elems.length; i++) {
			APP.createEditor(elems[i], uploadURL);
		}
		
		$(this.formElem).find("button[name='publish-now']").click(function() {
			if(_self.validateForm()) {
				$(_self.formElem).attr("action", _self.baseURL);
				$(_self.formElem).submit();
			}
		});
		
	}
	
	this.validateForm = function() {
		var title = $(formElem).find("input[name='title']").val().trim();
		
		if(!APP.isValidNonEmptyString(title)) {
			APP.addError("Please enter the Title");
			return false;
		}
		
    if(title.length > 100) {
      APP.addError("Title exceeds maxlength of 100 characters");
      return false;
    }
    
    $(formElem).find("input[name='title']").val(title);
		
    if($(this.formElem).find("input[name='url']").length > 0) {
      var repURL = $(this.formElem).find("input[name='url']").val();
      if(repURL.trim() === '') {
        APP.addError("URL cannot be empty");
        return false;
      } 
      
      if(APP.isEnglish(repURL) === false) {
          APP.addError("Only alpha-numeric and - characters allowed");
          return false;
      }
      
      if(APP.isValidRepresentationalURL(repURL) === false) {
        APP.addError("Only alpha-numeric and - characters allowed");
        return false;
      }
      
      if(repURL.length > 100) {
        APP.addError("URL exceeds maxlength of 100 characters");
        return false;
      }
    }
		
		var text = $(this.formElem).find("textarea[name='textdata']").val();
		if(!APP.isValidNonEmptyString(text)) {
			APP.addError("Page content too short");
			return false;
		}
		
		return true;
		
	}

	this.init();
}