var App = angular.module("App", []);

angular.module('App').controller("FormController", function($scope, $http, $window) {
  $scope.formResources = formResources;

  if ($scope.formResources) {
    $scope.fieldTypes = $scope.formResources.fieldTypes;
  }

  $scope.formData = {}

  $scope.status = {
    isURLValid : false,
    isTitleValid : false,
    isEmailsValid : false
  }

  if ($scope.formResources.form) {
    $scope.formData = $scope.formResources.form;
    $scope.status = {
      isURLValid : true,
      isTitleValid : true,
      isEmailsValid : true
    }
  }

  $scope.formData.manualURL = false;

  $scope.generateURL = function() {

    $scope.isValidTitle();

    if ($scope.formData.manualURL === true) {
      return;
    }

    var title = $scope.formData.title;

    if ($scope.status.isTitleValid === true) {
      var url = title.trim().replace(/[^A-Za-z0-9]/g, '-').replace(/[-]+/g, '-').toLowerCase();
      $scope.formData.url = url;
      $scope.validateURL();
    }
  }

  $scope.validateURL = function() {
    if (!APP.isValidURL($scope.formData.url)) {
      $scope.status.isURLValid = false;
    } else {
      $scope.status.isURLValid = true;
    }
  }

  $scope.validateEmailIds = function() {
    if (!APP.isValidNonEmptyString($scope.formData.emails)) {
      $scope.status.isEmailsValid = false;
    } else {
      $scope.status.isEmailsValid = true;
    }
  }

  $scope.isValidTitle = function() {
    if (!APP.isEnglish($scope.formData.title)) {
      $scope.status.isTitleValid = false;
    } else {
      $scope.status.isTitleValid = true;
    }
  }

  $scope.getAllTypes = function() {
    if (!$scope.fieldTypes) {
      return;
    }

    return $scope.fieldTypes.map(function(item) {
      return item;
    });
  }

  $scope.isFormDescValid = function() {
    $scope.isFormDescriptionValid = $scope.status.isURLValid === true && $scope.status.isTitleValid === true && $scope.status.isEmailsValid === true;
    return $scope.isFormDescriptionValid;
  }

  // Field specific code
  $scope.resetField = function() {
    $scope.newField = {
      isMandatory : false
    };

    $scope.newFieldStatus = {
      isQuestionValid : false
    };

    $scope.editIndex = -1;
  }

  $scope.resetField();

  $scope.isValidQuestion = function() {
    var text = $scope.newField.question;
    if (!APP.isValidLength(text, 3, 200)) {
      $scope.newFieldStatus.isQuestionValid = false;
    } else {
      $scope.newFieldStatus.isQuestionValid = true;
    }
  }

  $scope.canAddField = function() {
    if ($scope.newFieldStatus.isQuestionValid === false) {
      return false;
    }

    if (!$scope.newField.type) {
      return false;
    }

    var type = $scope.newField.type;
    if ((type.code === 'C' || type.code === 'R') && !$scope.newField.options) {
      return false;
    }

    return true;
  }

  $scope.addFieldToForm = function() {

    if (!$scope.formData.fields) {
      $scope.formData.fields = new Array();
    }

    if ($scope.newField.type.code === 'T' && $scope.newField.options) {
      delete $scope.newField.options;
    }

    if ($scope.editIndex === -1) {
      $scope.formData.fields.push(angular.copy($scope.newField));
    } else {
      $scope.formData.fields.splice($scope.editIndex, 0, angular.copy($scope.newField));
    }

    $scope.resetField();
  }

  $scope.deleteField = function(index) {
    if (!$scope.formData.fields || index >= $scope.formData.fields.length || index < 0) {
      return;
    }

    $scope.formData.fields.splice(index, 1);
  }

  $scope.editField = function(index) {
    if (!$scope.formData.fields || index >= $scope.formData.fields.length || index < 0) {
      return;
    }

    $scope.resetField();
    $scope.newFieldStatus = {
      isQuestionValid : true
    };

    $scope.editIndex = index;

    $scope.newField = angular.copy($scope.formData.fields[index]);
    $scope.formData.fields.splice(index, 1);
  }

  // Option specific code
  $scope.resetOption = function() {
    $scope.newOption = {};
    $scope.isValidNewOptionText = false;
  }

  $scope.resetOption();

  $scope.isNewOptionTextValid = function() {
    var text = $scope.newOption.text;
    if (!text || !APP.isValidNonEmptyString(text) || !APP.isEnglish(text)) {
      $scope.isValidNewOptionText = false;
    } else {
      $scope.isValidNewOptionText = true;
    }
  }

  $scope.addOption = function() {
    if (!$scope.newField) {
      $scope.newField = {};
    }

    if (!$scope.newField.options) {
      $scope.newField.options = new Array();
    }

    $scope.newField.options.push(angular.copy($scope.newOption));
    $scope.resetOption();
  }

  $scope.deleteOption = function(index) {
    if (!$scope.newField || !$scope.newField.options || index >= $scope.newField.options.length || index < 0) {
      return;
    }

    $scope.newField.options.splice(index, 1);

  }

  $scope.isOtherOptionPresent = function() {
    if (!$scope.newField || !$scope.newField.options) {
      return false;
    }

    var flag = false;
    for (var i = 0; i < $scope.newField.options.length; i++) {
      var option = $scope.newField.options[i];
      if (option.isOtherField && option.isOtherField === true) {
        flag = true;
        break;
      }
    }

    return flag;
  }

  $scope.addOtherOption = function() {
    $scope.newOption.text = "Other";
    $scope.isNewOptionTextValid();
  }

  // End of options

  $scope.submitForm = function() {

    var content = angular.copy($scope.formData);
    var contentType = "application/json";
    var uRL = formURL + "/saveForm";

    $http({
      url : uRL,
      method : 'POST',
      data : content,
      headers : {
        'Content-Type' : contentType
      }
    }).then(function mysucess(response) {
      if (response && response.data && response.data.url) {
        $window.location.href = response.data.url;
      } else {
        APP.addError("Could not save the form");
      }
    }, function error(response) {
      APP.addError("Could not save the form");
    });
  }
});
