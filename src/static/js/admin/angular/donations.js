var App = angular.module("App", [
  'ui.bootstrap.modal', 'ui.bootstrap'
]);

angular
  .module('App')
  .controller(
    "DonationsController",
    function ($scope, $http, $window, fileUpload) {

      var contentType = "application/json";
      var uRL = baseURL + "/get_all_donations";
      var saveEntryURL = baseURL + "/saveEntry";
      var saveChangesURL = baseURL + "/saveChanges";
      var deleteDonationURL = baseURL + "/deleteDonation";
      var downloadCSV = baseURL + "/downloadCSV";
      var downloadPDF = baseURL + "/downloadPDF";
      var uploadURL = baseURL + "/uploadDonations";

      $scope.currentPage = 0;
      pageSize = 100;
      $scope.donations = [];

      $scope.getDonations = function (totalAmount) {
        $http({
          url: uRL,
          method: 'POST',
          data: { page: $scope.currentPage, size: pageSize, totalAmount: totalAmount },
          headers: {
            'Content-Type': contentType
          }
        }).then(function mysucess(response) {
          if (response && response.data) {
            if (response.data.donations) {
              $scope.donations = $scope.donations.concat(response.data.donations);
              $scope.currentPage++;
            } else {
              APP.addError("Could not fetch existing donations data for page: " + currentPage);
            }

            // if (response.data.totalAmount) {
            //   $scope.totalAmount = response.data.totalAmount;
            // }

            if (response.data.pledgeTypes) {
              $scope.pledgeTypes = angular.copy(response.data.pledgeTypes);
            } else {
              $scope.pledgeTypes = [];
            }
          } else {
            APP.addError("Could not fetch existing donations data");
          }
        }, function error(response) {
          APP.addError("Could not fetch existing donations info");
        });
      }

      $scope.getDonations(false);

      $scope.calculateAmount = function () {
        if (!$scope.donations) {
          return 0;
        }

        var amount = 0;
        for (var i = 0; i < $scope.donations.length; i++) {
          var unit = $scope.donations[i];

          if (unit.isSuccess === true) {
            amount += parseInt(unit.amount);
          }
        }

        return amount;
        // return $scope.totalAmount;
      }

      $scope.openModal = function (transactionId) {
        if ($scope.selectedUnit) {
          delete $scope.selectedUnit;
        }

        if (!$scope.donations) {
          return;
        }

        for (var i = 0; i < $scope.donations.length; i++) {
          var unit = $scope.donations[i];

          if (unit.txn_id === transactionId) {
            $scope.selectedUnit = angular.copy(unit);
            break;
          }
        }

        if (!$scope.selectedUnit) {
          return;
        }

        $scope.extraDetailsModal = true;
      }

      $scope.cancel = function () {
        $scope.extraDetailsModal = false;
      }

      /**
       * Regarding new manual entry
       */

      $scope.newEntry = {
        receipt: false
      };

      $scope.saveEntry = function () {

        var content = angular.copy($scope.newEntry);
        $http({
          url: saveEntryURL,
          method: 'POST',
          data: content,
          headers: {
            'Content-Type': contentType
          }
        }).then(function mysucess(response) {
          if (response && response.data) {

            if (response.data.error && response.data.error === true) {
              var errorMessage = null;
              if (response.data.errorMsg) {
                errorMessage = response.data.errorMsg;
              }

              if (errorMessage == null) {
                errorMessage = "Could not save Manual Payment Entry";
              }
              APP.addError(errorMessage);
            } else {
              if (response.data.donation) {
                $scope.donations.push(angular.copy(response.data.donation));
                $scope.newEntry = {
                  receipt: false
                };
                APP.addError("Manual Payment Entry Saved");
              } else {
                APP.addError("Could not save manual entry");
              }
            }
          } else {
            APP.addError("Could not save manual entry");
          }
        }, function error(response) {
          APP.addError("Could not save manual entry");
        });
      }

      // For Donations listing tab
      $scope.selectedDonations = [];
      $scope.selectDonation = function (unit) {
        if (unit.selected === false) {
          for (var i = 0; i < $scope.selectedDonations.length; i++) {
            var donation = $scope.selectedDonations[i];
            if (unit.txn_id === donation.txn_id) {
              $scope.selectedDonations.splice(i, 1);
              break;
            }
          }
        }

        if (unit.selected === true) {
          $scope.selectedDonations.push(unit);
        }
      }

      $scope.selectAllDonations = function () {
        if ($scope.selectedAll === false) {
          for (var i = 0; i < $scope.selectedDonations.length; i++) {
            var donation = $scope.donations[i];
            donation.selected = false;
          }

          $scope.selectedDonations = [];
        }

        if ($scope.selectedAll === true) {
          $scope.selectedDonations = [];
          for (var i = 0; i < $scope.donations.length; i++) {
            var donation = $scope.donations[i];
            donation.selected = true;
            $scope.selectedDonations.push(donation);
          }
        }
      }

      // TODO: add code to make hit to the server and receive csv file
      $scope.downloadAsCSV = function (unit) {

        var content = [];
        if (angular.isUndefined(unit)) {
          if ($scope.selectedDonations.length < 1) {
            APP.addError("No donation selected");
            return;
          }

          content = angular.copy($scope.selectedDonations);
        } else {
          content.push(angular.copy(unit));
        }

        $http({
          url: downloadCSV,
          method: 'POST',
          data: content,
          headers: {
            'Content-Type': contentType
          },
          responseType: 'arraybuffer'
        }).then(function mysuccess(response) {
          if (!response || !response.data || (response.data && response.data.error)) {
            APP.addError("Could not export to CSV");
            return;
          }

          try {
            var file = new File([
              response.data
            ], 'Donation Summary TCLP.csv', {
              type: 'text/csv',
            });
            var fileURL = URL.createObjectURL(file);
            window.open(fileURL);
          } catch (err) {
            APP.addError("Could not export to CSV");
          }

        }, function error(response) {
          APP.addError("Could not save receipt number");
        });

      }

      // TODO: add code to make hit to the server and download PDF file.
      $scope.downloadAsPDF = function (unit) {

        if (angular.isUndefined(unit.receiptnumber)) {
          APP.addError("Need receipt number to generate PDF");
          return;
        }

        $window.open(downloadPDF + "/" + unit.txn_id, "_blank");
      }

      $scope.sendDonationPDF = function (unit) {
        if (angular.isUndefined(unit.receipt_number)) {
          APP.addError("Need receipt number to generate PDF");
          return;
        }
      }

      $scope.getTemplate = function (data) {

        if (data.txn_id == null || angular.isUndefined($scope.selectedDonation)) {
          return 'display';
        }

        if (data.txn_id === $scope.selectedDonation.txn_id) {
          return 'edit';
        } else {
          return 'display';
        }
      };

      $scope.deleteData = function (selectedDonation) {
        var content = angular.copy(selectedDonation);
        $http({
          url: deleteDonationURL,
          method: 'POST',
          data: content,
          headers: {
            'Content-Type': contentType
          }
        }).then(function mysuccess(response) {
          var isSuccessful = false;
          if (response && response.data) {
            if (response.data.error === false) {
              for (var i = 0; i < $scope.donations.length; i++) {
                if ($scope.donations[i].txn_id === selectedDonation.txn_id) {
                  $scope.donations.splice(i, 1);
                  APP.addError("Entry deleted");
                  isSuccessful = true;
                  break;
                }
              }
            }
          }

          if (isSuccessful === false) {
            APP.addError("could not delete entry");
          }
        }, function error(response) {
          APP.addError("could not delete entry");
        });

      }

      $scope.upload = function () {
        var file = $scope.donationsFile;
        fileUpload.uploadFileToUrl(file, uploadURL);
      }
    }).directive('filemodel', [
      '$parse', function ($parse) {
        return {
          restrict: 'A',
          link: function (scope, element, attrs) {
            var model = $parse(attrs.filemodel);
            var modelSetter = model.assign;
            element.bind('change', function () {
              scope.$apply(function () {
                modelSetter(scope, element[0].files[0]);
              });
            });
          }
        };
      }
    ]).service('fileUpload', [
      '$http', function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
          var fd = new FormData();
          fd.append('file', file);
          $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {
              'Content-Type': undefined
            }
          }).then(function (success) {
            console.log(success);
            if (success.data.error === true) {
              APP.addError(success.data.errorMsg);
            } else {
              APP.addError(success.data.message);
              APP.addError("success. Please refresh the page");
            }
          });
        }
      }
    ]);
