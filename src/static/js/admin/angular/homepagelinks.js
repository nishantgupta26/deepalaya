var App = angular.module("App", []);

angular.module('App').controller("HPLinksController", function($scope, $http) {
//  $scope.availableLinks = availableLinks;

  var contentType = "application/json; charset=utf-8";

  
  var getAllLinks = formURL + "/getAll";
  var saveLinkURL = formURL + "/saveLink";
  var deleteLinkURL = formURL + "/deleteLink";
  var editLinkURL = formURL + "/editLink";

  $scope.addLink = function() {
    if (!$scope.newlink) {
      return;
    }

    if (!APP.isValidNonEmptyString($scope.newlink.name)) {
      alert("no name");
      return;
    }

    if (!APP.isValidNonEmptyString($scope.newlink.url) || !$scope.newlink.url.startsWith("http")) {
      APP.addError("no url");
      return;
    }

    var content = angular.copy($scope.newlink);

    $http({
      url : saveLinkURL,
      method : 'POST',
      data : content,
      headers : {
        'Content-Type' : contentType
      }
    }).then(function mysucess(response) {
      if (response && response.data) {

        if (response.data.error && response.data.error === true) {
          var errorMessage = null;
          if (response.data.errorMsg) {
            errorMessage = response.data.errorMsg;
          }

          if (errorMessage == null) {
            errorMessage = "Could not save new Link";
          }
          APP.addError(errorMessage);
        } else {
          if (response.data.id) {
            content.id = response.data.id;
            $scope.availableLinks.push(angular.copy(content));
            APP.addError("new link added");
            delete $scope.newlink;
          } else {
            APP.addError("Could not save new link");
          }
        }
      } else {
        APP.addError("Could not save new link");
      }
    }, function error(response) {
      APP.addError("Could not save new link");
    });

  }

  $scope.deleteLink = function(link) {
    if (!link) {
      return;
    }

    if (!link.id) {
      alert("no id present");
    }

    var content = {};
    content.id = link.id;

    $http({
      url : deleteLinkURL,
      method : 'POST',
      data : content,
      headers : {
        'Content-Type' : contentType
      }
    }).then(function mysucess(response) {
      var isSuccess = false;
      if (response && response.data) {
        if (response.data.isSuccess) {
          isSuccess = response.data.isSuccess;
        }
      } else {
        isSuccess = false;
      }

      if (isSuccess) {
        for (var i = 0; i < $scope.availableLinks.length; i++) {
          var existingLink = $scope.availableLinks[i];
          if (link.id === existingLink.id) {
            $scope.availableLinks.splice(i, 1);
            APP.addError("Link deleted");
            break;
          }
        }
      } else {
        APP.addError("Could not delete the link");
      }
    }, function error(response) {
      APP.addError("Could not delete the link");
    });

  }
  
  $scope.updateLink = function(link) {
	    if (!link) {
	      return;
	    }

	    if (!link.id) {
	      alert("no id present");
	    }

	    var content = {};
	    content.id = link.id;
	    if(link.newhindiname == null && link.newhindidesc == null) {
	    	alert("No Non-English info to be updated");	
	    }
	    
	    if(link.newhindiname != null) {
	    	content.hindiname = link.newhindiname;
	    }
	    
	    if(link.newhindidesc != null) {
	    	content.hindidesc = link.newhindidesc;
	    }

	    $http({
	      url : editLinkURL,
	      method : 'POST',
	      data : content,
	      headers : {
	        'Content-Type' : contentType
	      }
	    }).then(function mysucess(response) {
	      var isSuccess = false;
	      if (response && response.data) {
	        if (response.data.isSuccess) {
	          isSuccess = response.data.isSuccess;
	          if(content.hindiname != null) {
	        	  link.hindiname = content.hindiname;
	          }
	          
	          if(content.hindidesc != null) {
	        	  link.hindidesc = content.hindidesc;
	          }
	        }
	      } else {
	        isSuccess = false;
	      }

	      if (isSuccess) {
		        APP.addError("Link information updated");
	      } else {
	        APP.addError("Could not update the link");
	      }
	    }, function error(response) {
	      APP.addError("Could not update the link");
	    });

	  }
  
  $scope.init = function() {
	  $http({
		  url : getAllLinks,
	      method : 'POST',
	      data : "{'id': 'temp'}",
	      headers : {
	        'Content-Type' : contentType
	      }
	    }).then(function mysucess(response) {
	      var isSuccess = false;
	      if (response && response.data) {
	    	  if (response.data.isSuccess) {
	    		  isSuccess = response.data.isSuccess;
	    	  }
	      } else {
	        isSuccess = false;
	      }

	      if (isSuccess) {
	    	  $scope.availableLinks = response.data.availableLinks;
	      } else {
	        APP.addError("Error in fetching homepage links");
	        $scope.availableLinks = [];
	      }
	    }, function error(response) {
	      APP.addError("Error in fetching homepage links");
	    });
	}
  
  $scope.init();

});

App.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});
