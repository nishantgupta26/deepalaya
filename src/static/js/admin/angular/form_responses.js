var App = angular.module("App", ['ui.bootstrap']);


angular.module('App').controller("FormResponseController", function($scope, $http, $window) {
  $scope.availableForms = availableForms;

  $scope.getResponses = function(formId) {
    var formData;
    for (var i = 0; i < $scope.availableForms.length; i++) {
      if ($scope.availableForms[i].id === formId) {
        formData = angular.copy($scope.availableForms[i]);
        break;
      }
    }

    if (formData && formData.count == 0) {
      return;
    }
    
    try {
      delete $scope.selectedForm;
    } catch (err) {
    }

    var contentType = "application/json";
    var content = formData;

    var uRL = formURL + "/getResponses";

    $http({
      url : uRL,
      method : 'POST',
      data : content,
      headers : {
        'Content-Type' : contentType
      }
    }).then(function mysucess(response) {
      if (response && response.data && response.data.isSuccess === true) {
        $scope.selectedForm = angular.copy(response.data.response.form);
        $scope.selectedForm.totalResponses = response.data.response.responses.length;
        $scope.selectedForm.formResponses = angular.copy(response.data.response.responses);
        $scope.currentPage = 1;
        $scope.selectedForm.selectedResponse = $scope.selectedForm.formResponses[0];
      } else {
        APP.addError("Could not get form Responses");
      }
    }, function error(response) {
      APP.addError("Could not get form Responses");
    });
  }

  $scope.setPage = function(pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function() {
    $scope.selectedForm.selectedResponse = $scope.selectedForm.formResponses[$scope.currentPage - 1];
  };

});
