var APP = new function() {

  this.isEnglish = function(str) {
    var regex = new RegExp("[a-zA-Z0-9\"\'\-\/][a-zA-Z0-9\"\'\-\/ ]+$");
    return regex.test(str);

    // return str.match(/^[a-zA-Z0-9\"\'\-\/][a-zA-Z0-9\"\'\-\/ ]+$/gi);
  }

  this.isValidLength = function(str, min, max) {
    if (!str) {
      return false;
    }

    return str.length >= min && str.length <= max;
  }

  this.isValidRepresentationalURL = function(str) {
    var regex = new RegExp("[a-zA-Z0-9\-]+$");
    return regex.test(str);
  }

  this.isValidURL = function(str) {
    if (!str) {
      return false;
    }

    str = str.toLowerCase();
    var flag = false;
    for (var i = 0; i < str.length; i++) {
      var c = str.charAt(i);
      if (!((c >= 'a' && c <= 'z') || c == '-')) {
        flag = true;
        break;
      }
    }

    if (flag === true) {
      return false;
    }

    return true;

  }

  this.dataTable = function(elem) {
    if (!elem) {
      return;
    }

    // TODO: add custom options support
    $(elem).DataTable();

  }

  this.createEditor = function(elem, uploadURL) {
    if (!elem) {
      this.addError("Could not find elem to convert to editor");
      return;
    }

    var extraPlugin = uploadURL ? 'uploadimage,image2,videoembed' : "image2,videoembed";
    var url = uploadURL ? uploadURL : "";

    $(elem).ckeditor({
      width : '98%',
      height : '150px',
      uploadUrl : url,
      extraPlugins : extraPlugin,
      image_prefillDimensions : true,
      image2_prefillDimensions : true,
      filebrowserImageUploadUrl : url

    });
  }

  this.isValidNonEmptyString = function(cStr) {
    return cStr && cStr != "";
  }

  this.isValidEmail = function(email) {
    return email
        .match(/^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/);
  };

  this.isValidPassword = function(cStr) {
    return cStr.match(/^[0-9a-zA-Z\!\@\#\$\%\^\&\*\(\)\-\_\+\=\{\}\[\]\|\:\;\<\,\>\.\?\~]{8,}$/);
  };

  this.isValidPositiveInteger = function(cStr) {
    return cStr.match(/^[0-9]{1,}$/);
  }
  
  this.isValidMemberId = function(cStr) {
    return cStr.match(/^M[A-Z0-9\-]{1,}$/);
  }

  this.isValidMemberIdPrefix = function(cStr) {
    return cStr.match(/^M[A-Z]{1,1}$/);
  }

  this.isValidPhone = function(cStr) {
    if (cStr == "") {
      return true;
    }

    return cStr.match(/^[0-9]{10}((\,){0,1}(\s){0,}[0-9]{10}){0,}$/);
  }

  this.addError = function(errorMsg, elem) {
    if (!elem) {
      alert(errorMsg);
    } else {
      // TODO: highlight error
    }

  };

  this.getDate = function(value) {
    var date = value.split("/");
    var d = parseInt(date[0], 10), m = parseInt(date[1], 10), y = parseInt(date[2], 10);
    return new Date(y, m - 1, d);
  }

  this.isDateLaterThanToday = function(date) {
    var currentDate = new Date();
    return date > currentDate;
  }

  this.setSelectBoxByValue = function(selectElem, value) {
    $(selectElem).val(value).prop('selected', true);
  }

  this.getDateInYYYYMMDD = function(date) {
    try {
      if (!date) {
        return "";
      }

      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDate();
      return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day);
    } catch (err) {
      // compatibility with newer fullcalendar
      return this.getDateInYYYYMMDDMoment(date);
    }
  }

  this.getDateInYYYYMMDDMoment = function(date) {
    if (!date) {
      return null;
    }

    try {
      return moment(date).format("YYYY-MM-DD");
    } catch (err) {
      if (console) {
        console.log("error occurred: " + err);
      }
    }

    return null;
  }

  this.getTimeFromMoment = function(date) {
    if (!date) {
      return null;
    }

    try {
      return moment(date).format("hh:mm A");
    } catch (err) {
      if (console) {
        console.log("error occurred: " + err);
      }
    }

    return null;
  }

  this.calculateMinuteDifference = function(start, end) {
    try {
      var duration = moment.duration(moment(end).diff(moment(start)));
      var minutes = duration.asMinutes();
      return minutes;
    } catch (err) {
      if (console) {
        console.log("error occurred: " + err);
      }
    }

    return null;

  }

  this.createCalendar = function(elem, isAdmin, eventsJSON, validateForm, eventDialogElem, baseURI) {
    var calendar = $(elem).fullCalendar(
        {
          header : {
            left : 'prev,next',
            center : 'title',
            right : 'month,basicWeek,basicDay'
          },
          events : eventsJSON,
          selectable : isAdmin,
          selectHelper : isAdmin,
          select : function(start, end, allDay) {
            if (isAdmin && eventDialogElem) {
              $("#event-details-form")[0].reset();
              $(eventDialogElem).dialog({
                modal : true,
                width : 550,
                closeOnEscape : false,
                open : function(event, ui) {
                  var formElem = $("#event-details-form");
                  $(formElem).find("input[name='start']").val(APP.getDateInYYYYMMDD(start));
                  $(formElem).find("input[name='end']").val(APP.getDateInYYYYMMDD(end));
                },

                buttons : {
                  "Add Event" : function() {

                    var formElem = $("#event-details-form");

                    if (validateForm.call()) {
                      var currTSID = Math.floor(Date.now() / 1000);

                      $.post(baseURI + "/add", $(formElem).serialize() + "&id=" + currTSID, function(data) {
                        if (data) {
                          if (data.status && data.status == "true") {
                            calendar.fullCalendar('renderEvent', {
                              id : currTSID,
                              title : $(formElem).find("input[name='title']").val(),
                              start : start,
                              end : end,
                              allDay : allDay,
                              url : $(formElem).find("input[name='event-link']").val(),
                              location : $(formElem).find("textarea[name='where']").val(),
                              description : $(formElem).find("textarea[name='event-desc']").val(),
                              time : $(formElem).find("input[name='time']").val(),
                              duration : $(formElem).find("input[name='duration']").val(),
                            }, true);

                            alert("New Event Added");

                            $(eventDialogElem).dialog("close");
                            calendar.fullCalendar('unselect');

                          } else {
                            if (data.reason) {
                              APP.addError(data.reason);
                            }
                          }
                        } else {
                          APP.addError("Did not recieve any communication from Server");
                        }
                      }, "json");
                    }
                  },
                  Cancel : function() {
                    $(this).dialog("close");
                  }
                }

              });

            }
          },

          eventClick : function(calEvent, calElement) {
            if (isAdmin && eventDialogElem) {
              $("#event-details-form")[0].reset();
              var currTSID = calEvent._id;
              $(eventDialogElem).dialog(
                  {
                    modal : true,
                    width : 550,
                    closeOnEscape : false,
                    open : function(event, ui) {
                      var formElem = $("#event-details-form");
                      $(formElem).find("input[name='title']").val(calEvent.title);
                      $(formElem).find("input[name='start']").val(APP.getDateInYYYYMMDD(calEvent.start));
                      $(formElem).find("input[name='end']").val(APP.getDateInYYYYMMDD(calEvent.end == null ? calEvent.start : calEvent.end));
                      $(formElem).find("textarea[name='where']").val(calEvent.location == null ? "" : calEvent.location);
                      $(formElem).find("textarea[name='event-desc']").val(
                          (calEvent.description == null || calEvent.description == "null") ? "" : calEvent.description);
                      $(formElem).find("input[name='event-link']").val((calEvent.url == null || calEvent.url == "null") ? "" : calEvent.url);
                      $(formElem).find("input[name='duration']").val(
                          (calEvent.duration == null || calEvent.duration == "null") ? "" : calEvent.duration);
                      $(formElem).find("input[name='time']").val((calEvent.time == null || calEvent.time == "null") ? "" : calEvent.time);
                    },

                    buttons : {
                      "Edit Event" : function() {
                        if (validateForm.call()) {
                          var formElem = $("#event-details-form");
                          calEvent.title = $(formElem).find("input[name='title']").val();
                          calEvent.start = $(formElem).find("input[name='start']").val();
                          calEvent.end = $(formElem).find("input[name='end']").val();
                          calEvent.url = $(formElem).find("input[name='event-link']").val();
                          calEvent.location = $(formElem).find("textarea[name='where']").val();
                          calEvent.description = $(formElem).find("textarea[name='event-desc']").val();
                          calEvent.duration = $(formElem).find("input[name='duration']").val();
                          calEvent.time = $(formElem).find("input[name='time']").val();
                          $.post(baseURI + "/update", $(formElem).serialize() + "&id=" + currTSID, function(data) {
                            if (data) {
                              if (data.status && data.status == "true") {
                                calendar.fullCalendar('updateEvent', calEvent);
                                alert("Event Changes Successful");
                                $(eventDialogElem).dialog("close");
                              } else {
                                if (data.reason) {
                                  APP.addError(data.reason);
                                }
                              }
                            } else {
                              APP.addError("Did not recieve any communication from Server");
                            }
                          }, "json");
                        }
                      },
                      "Delete Event" : function() {
                        if (confirm("Delete this event?")) {
                          $.post(baseURI + "/delete", "id=" + currTSID, function(data) {
                            if (data) {
                              if (data.status && data.status == "true") {
                                calendar.fullCalendar('removeEvents', currTSID);
                                alert("Event Deleted");
                                $(eventDialogElem).dialog("close");
                              } else {
                                if (data.reason) {
                                  APP.addError(data.reason);
                                }
                              }
                            } else {
                              APP.addError("Did not recieve any communication from Server");
                            }
                          }, "json");
                        }
                      },
                      Cancel : function() {
                        $(this).dialog("close");
                      }
                    }

                  });

            }

            // if there is an event link, do not go there on clicking it from
            // here
            // in the admin panel
            return false;
          }
        });
    return calendar;
  }

  // type= GET or POST; datatype = json
  this.makeAJAXHit = function(url, type, data, expectedResponseDataType, successHandler, errorHandler, context) {
    $.ajax({
      url : url,
      data : data,
      type : type ? type : 'GET',
      dataType : expectedResponseDataType ? expectedResponseDataType : 'json'
    }).done(function(data) {
      if (successHandler) {
        successHandler(data, context);
      }
    }).fail(function() {
      if (errorHandler) {
        errorHandler(context);
      }
    });
  }

  this.getDateInDateTime = function(obj) {
    return moment(obj).format("YYYY-MM-DD HH:mm:ss");
  }
}
