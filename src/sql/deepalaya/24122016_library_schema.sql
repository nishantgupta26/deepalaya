create database if not exists community_library CHARACTER SET utf8 COLLATE utf8_bin;
use community_library;

CREATE TABLE newsletter_subscription (
  email varchar(50) COLLATE utf8_bin NOT NULL PRIMARY KEY,
  client_ip char(15) COLLATE utf8_bin DEFAULT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE contact_request (
  email varchar(50) NOT NULL PRIMARY KEY,
  name  varchar(50) NOT NULL,
  phone varchar(20) NOT NULL,
  subject varchar(50),
  message text NOT NULL,
  client_ip char(15) DEFAULT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ack tinyint(1) NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Table structure for table library_users
--

DROP TABLE IF EXISTS library_users;
CREATE TABLE library_users (
  user_id int(10) unsigned NOT NULL,
  is_enabled char(1) COLLATE utf8_bin NOT NULL DEFAULT 'N',
  auth_token varchar(150) COLLATE utf8_bin DEFAULT NULL,
  role char(2) COLLATE utf8_bin NOT NULL,
  name varchar(100) COLLATE utf8_bin NOT NULL,
  email varchar(100) CHARACTER SET utf8 NOT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id),
  UNIQUE KEY email (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- INSERT INTO library_users VALUES (0,'Y','jVverfphEZ/QB5ieUSRFb2Qyrb2TAeEiW+4NO79N5gRuQo42OWlbIjykezTLBGLw8iFpUAxxbvg0DR97nO7cMw==','A','Library Administrator','admin@deepalaya.org',NOW());

-- Table structure for library members
DROP TABLE IF EXISTS library_members;
CREATE TABLE library_members (
  id varchar(10) NOT NULL,
  name varchar(100) COLLATE utf8_bin NOT NULL,
  dob varchar(10) collate utf8_bin null,
  age varchar(20) collate utf8_bin null,
  phone varchar(50) collate utf8_bin null,
  mother_name varchar(50) collate utf8_bin null,
  father_name varchar(50) collate utf8_bin null,
  address varchar(50) collate utf8_bin null,
  library_group varchar(2) collate utf8_bin,
  school varchar(50) collate utf8_bin null,
  class varchar(10) collate utf8_bin null,
  photo_id varchar(105) collate utf8_bin null,
  gender char(1) collate utf8_bin not null default 'M',
  branch_code varchar(8) COLLATE utf8_bin NOT NULL DEFAULT 'TCLP-01',
  join_date varchar(10) collate utf8_bin null,
  PRIMARY KEY (id, branch_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- alter table library_members add column photo_id varchar(105) collate utf8_bin null;
-- alter table library_members add column gender char(1) collate utf8_bin not null default 'M';
-- alter table library_members add column branch_code varchar(8) COLLATE utf8_bin NOT NULL DEFAULT 'TCLP-01';
-- alter table library_members add column join_date varchar(10) collate utf8_bin null;

--Table structure for member relationship
DROP TABLE IF EXISTS member_relations;
CREATE TABLE member_relations (
  member_id varchar(10) NOT NULL,
  relative_id varchar(10) NOT NULL,
  relation varchar(2) not null,
  primary key (member_id, relative_id),
  constraint member_fk FOREIGN KEY(member_id) REFERENCES  library_members(id),
  constraint relative_fk FOREIGN KEY(relative_id) REFERENCES  library_members(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--Table structure for configstore
CREATE TABLE config_store (
  config_id           varchar(200) NOT NULL PRIMARY KEY,
  config_value        TEXT
) ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_bin;

--Table structure for events info
CREATE TABLE events (
  id                  varchar(50) NOT NULL PRIMARY KEY,
  title               varchar(50) NOT NULL,
  start_date          timestamp NULL,
  end_date            timestamp NULL,
  address             varchar(100) NOT NULL,
  description         varchar(500) NULL,
  link                varchar(250) NULL,
  time                varchar(20) NULL,
  duration            varchar(50) NULL
) ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_bin;
-- alter table events modify column link varchar(250) NULL;
-- alter table events add column time varchar(20) NULL;
-- alter table events add column duration varchar(50) NULL;

-- User login info tables
CREATE TABLE user_login_info (
  login_ts               timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id              int(10) unsigned NOT NULL,
  KEY fk (user_id),
  CONSTRAINT user_login_info_ibfk_1 FOREIGN KEY (user_id) REFERENCES library_users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
CREATE INDEX userid_index USING BTREE ON user_login_info (user_id, login_ts);

-- Banner related info table
DROP TABLE IF EXISTS library_banner;
CREATE TABLE library_banner (
  id int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  type char(1) NULL,
  urlLink varchar(200) null,
  imageURL varchar(200) null,
  textContent TEXT COLLATE utf8_bin NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_id int(10) unsigned NOT NULL,
  CONSTRAINT banner_user_fk FOREIGN KEY (user_id) REFERENCES  library_users(user_id) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- alter table library_banner add column urlLink varchar(200) null; 

--
-- Table structure for table library_users
--

DROP TABLE IF EXISTS blogposts;
CREATE TABLE blogposts (
  id BigInt(10) unsigned NOT NULL auto_increment,
  title varchar(100) COLLATE utf8_bin NOT NULL,
  author varchar(100) COLLATE utf8_bin NOT NULL,
  postedby_id int(10) unsigned NOT NULL,
  post_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  publish_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text_data LONGTEXT COLLATE utf8_bin NULL,
  url varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT blog_poster_ibfk_1 FOREIGN KEY (postedby_id) REFERENCES library_users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE blogposts add column url varchar(100) COLLATE utf8_bin NOT NULL; 


DROP TABLE IF EXISTS blogposts_hist;
CREATE TABLE blogposts_hist (
  id BigInt(10) unsigned NOT NULL,
  title varchar(100) COLLATE utf8_bin NOT NULL,
  author varchar(100) COLLATE utf8_bin NOT NULL,
  postedby_id int(10) unsigned NOT NULL,
  post_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  publish_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text_data LONGTEXT COLLATE utf8_bin NULL,
  url varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ALTER TABLE blogposts_hist add column url varchar(100) COLLATE utf8_bin NOT NULL;



--
-- Table structure for table custom pages
--

DROP TABLE IF EXISTS page_resources;
CREATE TABLE page_resources (
  id BigInt(10) unsigned NOT NULL auto_increment,
  title varchar(100) COLLATE utf8_bin NOT NULL,
  creation_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text_data LONGTEXT COLLATE utf8_bin NULL,
  url varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS page_resources_hist;
CREATE TABLE page_resources_hist (
  id BigInt(10) unsigned NOT NULL,
  title varchar(100) COLLATE utf8_bin NOT NULL,
  creation_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text_data LONGTEXT COLLATE utf8_bin NULL,
  url varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for forms
--

DROP TABLE IF EXISTS forms;
CREATE TABLE forms (
  id int(10) unsigned NOT NULL auto_increment,
  title varchar(100) COLLATE utf8_bin NOT NULL,
  url varchar(100) COLLATE utf8_bin NOT NULL,
  emails varchar(500) COLLATE utf8_bin NOT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE(url)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS form_fields;
CREATE TABLE form_fields (
  id int(10) unsigned NOT NULL,
  form_id int(10) unsigned NOT NULL,
  question_text varchar(500) COLLATE utf8_bin NOT NULL,
  is_mandatory char(1) COLLATE utf8_bin NOT NULL,
  field_type char(1) COLLATE utf8_bin NOT NULL,
  validation_regex varchar(100) COLLATE utf8_bin NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id, form_id),
  FOREIGN KEY (form_id) REFERENCES forms(id) on delete cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS form_options;
CREATE TABLE form_options (
  option_id varchar(50) COLLATE utf8_bin NOT NULL,
  field_id int(10) unsigned NOT NULL,
  option_text varchar(500) COLLATE utf8_bin NOT NULL,
  is_other char(1) COLLATE utf8_bin NOT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (option_id),
  FOREIGN KEY (field_id) REFERENCES form_fields(id) on delete cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS form_responses;
CREATE TABLE form_responses (
  id int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  form_id int(10) unsigned NOT NULL,
  gen_ts timestamp not null default current_timestamp,
  FOREIGN KEY (form_id) REFERENCES forms(id) on delete cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--  alter table form_responses add column gen_ts timestamp not null default current_timestamp;


DROP TABLE IF EXISTS response_answers;
CREATE TABLE response_answers (
  id int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  field_id int(10) unsigned NOT NULL,
  response_id int(10) unsigned NOT NULL,
  response text COLLATE utf8_bin NULL,
  FOREIGN KEY (response_id) REFERENCES form_responses(id) on delete cascade,
  FOREIGN KEY (field_id) REFERENCES form_fields(id) on delete cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- join statement
-- select forms.id as form_id, forms.title as form_title,form_responses.id as response_id,form_fields.id as field_id, form_fields.question_text as question, response_answers.response_id as responseanswerid, response_answers.response from forms left join form_responses on forms.id = form_responses.form_id left join response_answers on form_responses.id = response_answers.response_id left join form_fields on form_fields.id=response_answers.field_id where forms.id=1;


--
-- Payments Related
--
DROP TABLE IF EXISTS payment_unit;
CREATE TABLE payment_unit (
  txn_id varchar(20) NOT NULL PRIMARY KEY,
  receipt_number varchar(18) NULL,
  receiptdate timestamp NULL,
  first_name varchar(50) COLLATE utf8_bin NOT NULL,
  last_name varchar(50) COLLATE utf8_bin NOT NULL,
  phone varchar(20) COLLATE utf8_bin NOT NULL,
  email varchar(100) COLLATE utf8_bin NOT NULL,
  amount varchar(20) COLLATE utf8_bin NOT NULL,
  address varchar(150) COLLATE utf8_bin NULL,
  subscription_type char(1) COLLATE utf8_bin NULL,
  extra_notes varchar(100) COLLATE utf8_bin NULL,
  require_receipt char(1) COLLATE utf8_bin NOT NULL,
  pan char(10) COLLATE utf8_bin NULL,
  payment_status char(1) COLLATE utf8_bin NULL,
  gateway_transaction_id varchar(50) COLLATE utf8_bin,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  gateway_name char(1) COLLATE utf8_bin NULL default 'P',
  payment_type char(1) COLLATE utf8_bin NOT NULL default 'N',
  ipaddress varchar(20) not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- alter table payment_unit add column address varchar(150) COLLATE utf8_bin NULL after amount;
-- alter table payment_unit add column pan char(10) COLLATE utf8_bin NULL after require_receipt;
-- alter table payment_unit add column gateway_name char(1) COLLATE utf8_bin NULL;
-- alter table payment_unit add column payment_type char(1) COLLATE utf8_bin NOT NULL default 'N';
-- alter table payment_unit modify column txn_id char(18) NOT NULL;
-- alter table payment_unit add column receipt_number varchar(18) NULL;
-- alter table payment_unit add column receiptdate timestamp NULL;
-- alter table payment_unit add column ipaddress varchar(20) not null;
-- alter table payment_unit modify column txn_id varchar(20) NOT NULL;


DROP TABLE IF EXISTS payment_error;
CREATE TABLE payment_error (
txn_id char(14) NOT NULL,
error_code varchar(10) COLLATE utf8_bin NOT NULL,
error_message varchar(50) COLLATE utf8_bin NOT NULL,
gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- Email Message Store
DROP TABLE IF EXISTS msg_store;
CREATE TABLE msg_store (
  id int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  email_to varchar(200) COLLATE utf8_bin NULL,
  email_cc varchar(200) COLLATE utf8_bin NULL,
  email_from varchar(70) COLLATE utf8_bin NOT NULL,
  subject varchar(200) COLLATE utf8_bin NOT NULL,
  content text NULL,
  is_success char(1) COLLATE utf8_bin NOT NULL,
  error_logs varchar(200) COLLATE utf8_bin DEFAULT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- books information
DROP TABLE IF EXISTS books;
CREATE TABLE books (
  id int(20) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  book_type varchar(5) not null,
  book_code int(10) unsigned not null,
  isbn_10 char(10) COLLATE utf8_bin NULL,
  isbn_13 char(13) COLLATE utf8_bin NULL,
  images varchar(500) COLLATE utf8_bin NULL,
  thumbnail varchar(50) COLLATE utf8_bin NULL,
  title varchar(100) COLLATE utf8_bin not NULL,
  description text NULL,
  copies int(5) unsigned not null default 1;
  author varchar(100) collate utf8_bin null,
  publisher varchar(100) collate utf8_bin null,
  is_loaded char(1) COLLATE utf8_bin NOT NULL default 'N',
  loader char(1) COLLATE utf8_bin NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  loader_ts timestamp NULL,
  unique key(book_type, book_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- alter table books add column copies int(5) unsigned not null default 1 after description;
-- alter table books add column publisher varchar(100) collate utf8_bin null after author;
-- alter table books modify column title varchar(500) COLLATE utf8_bin not NULL;

-- homepagelinks

DROP TABLE IF EXISTS homepagelinks;
CREATE TABLE homepagelinks (
  id int(11) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(50) collate utf8_bin not null,
  description varchar(200) collate utf8_bin  null,
  hindiname varchar(100) collate utf8_bin null,
  hindidescription varchar(500) collate utf8_bin  null,
  url varchar(100)  collate utf8_bin not null,
  library_group char(1) not null default 'U',
  numhindi int(11) unsigned not null default 0,
  numenglish int(11) unsigned not null default 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- alter table homepagelinks add column numhindi int(11) unsigned not null default 0; 
-- alter table homepagelinks add column numenglish int(11) unsigned not null default 0;

-- slots
drop table if exists slots;
CREATE TABLE slots (
  id                  varchar(50) NOT NULL PRIMARY KEY,
  member_id           varchar(10) NOT NULL,
  start_date          timestamp NULL,
  end_date            timestamp NULL,
  slot_date           varchar(15) NOT NULL,
  start_time          varchar(15) NOT NULL,
  end_time            varchar(15) NOT NULL,
  duration            varchar(15) NOT NULL,
  notes               varchar(500) NULL,
  status              char(1) NOT NULL,
  slot_type           varchar(40) NOT NULL,
  constraint slot_member_fk FOREIGN KEY(member_id) REFERENCES library_members(id)
) ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_bin;

--slottypes
drop table if exists slottype;
CREATE TABLE slottype (
  id int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  slot_type           varchar(40) NOT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY slotType (slot_type)
) ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_bin;

-- library branch
drop table if exists librarybranch;
CREATE TABLE librarybranch (
  code  varchar(10) NOT NULL PRIMARY KEY,
  name varchar(50) NOT NULL,
  member_id_prefix varchar(5) NOT NULL,
  gen_ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY libraryname (name)
) ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_bin;